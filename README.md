# About

libsnc is a C++ library for numeric machine control.


## Copying

libsnc is distributed under the terms in the LICENSE-libsnc.txt text file that
comes with this package.


## Installation

libsnc uses the CMake build system.

For a system wide installation call:

```
mkdir build-libsnc
cd build-libsnc
cmake <libsnc_git_dir>
make -j5
sudo make install
```
