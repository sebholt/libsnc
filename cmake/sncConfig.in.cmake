
# Directories
set ( _SNC_INCLUDE_DIR "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_INCLUDE@" )

# Components
set( _SNC_COMP_CORE Off )
set( _SNC_COMP_CORE_DEBUG Off )

# Check components
foreach( comp ${snc_FIND_COMPONENTS} )
  if( comp STREQUAL "core" )
    set( _SNC_COMP_CORE On )
  elseif( comp STREQUAL "core_debug" )
    set( _SNC_COMP_CORE_DEBUG On )
  endif()
endforeach()

# Setup core library
if( _SNC_COMP_CORE AND NOT TARGET snc::core )
  # Find required packages
  find_package( Qt5 COMPONENTS Core REQUIRED )
  find_package( sev COMPONENTS core qt REQUIRED )

  # Find core library
  find_library(
    _SNC_LIBRARY_CORE
    NAMES "@LIB_NAME_CORE_BINARY_RELEASE@" "lib@LIB_NAME_CORE_BINARY_RELEASE@"
    PATHS "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_LIB@" )

  # Setup core target
  if( _SNC_LIBRARY_CORE )
    add_library( snc::core SHARED IMPORTED )
    set_target_properties( snc::core
      PROPERTIES
        IMPORTED_LOCATION "${_SNC_LIBRARY_CORE}"
        INTERFACE_INCLUDE_DIRECTORIES "${_SNC_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "sev::core;sev::qt;Qt5::Core" )
  else()
    if( snc_FIND_REQUIRED_core )
      message( FATAL_ERROR "Could NOT find @LIB_NAME_CORE_BINARY_RELEASE@ library" )
    endif()
  endif()
  unset( _SNC_LIBRARY_CORE )
endif()

# Setup core_debug target
if( _SNC_COMP_CORE_DEBUG AND NOT TARGET snc::core_debug )
  # Find required packages
  find_package( Qt5 COMPONENTS Core REQUIRED )
  find_package( sev COMPONENTS core_debug qt_debug REQUIRED )

  # Find core library
  find_library(
    _SNC_LIBRARY_CORE_DEBUG
    NAMES "@LIB_NAME_CORE_BINARY_DEBUG@" "lib@LIB_NAME_CORE_BINARY_DEBUG@"
    PATHS "@CMAKE_INSTALL_PREFIX@/@INSTALL_DIR_LIB@" )

  # Setup core target
  if( _SNC_LIBRARY_CORE_DEBUG )
    add_library( snc::core_debug SHARED IMPORTED )
    set_target_properties( snc::core_debug
      PROPERTIES
        IMPORTED_LOCATION "${_SNC_LIBRARY_CORE_DEBUG}"
        INTERFACE_INCLUDE_DIRECTORIES "${_SNC_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "sev::core_debug;sev::qt_debug;Qt5::Core" )
  else()
    if( snc_FIND_REQUIRED_core_debug )
      message( FATAL_ERROR "Could NOT find @LIB_NAME_CORE_BINARY_DEBUG@ library" )
    endif()
  endif()
  unset( _SNC_LIBRARY_CORE_DEBUG )
endif()

unset( _SNC_COMP_CORE )
unset( _SNC_COMP_CORE_DEBUG )
unset( _SNC_INCLUDE_DIR )
