/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "battery.hpp"
#include <snc/bat/detail/detail.hpp>

namespace snc::bat
{

Battery::Battery ( sev::logt::Context log_context_n,
                   sev::thread::Tracker * thread_tracker_n,
                   sev::unicode::View thread_name_n )
: _detail ( std::make_unique< detail::Detail > (
      std::move ( log_context_n ), thread_tracker_n, thread_name_n ) )
{
}

Battery::~Battery () = default;

const sev::logt::Context &
Battery::log () const
{
  return _detail->log ();
}

bool
Battery::start ( Starter && starter_n )
{
  return _detail->start ( std::move ( starter_n ) );
}

void
Battery::abort ()
{
  return _detail->abort ();
}

void
Battery::stop ()
{
  return _detail->stop ();
}

bool
Battery::is_running () const
{
  return _detail->is_running ();
}

bool
Battery::is_aborting () const
{
  return _detail->is_aborting ();
}

} // namespace snc::bat
