/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <snc/bat/starter.hpp>
#include <memory>

// -- Forward declaration
namespace snc::bat::detail
{
class Detail;
} // namespace snc::bat::detail

namespace snc::bat
{

/// @brief Battery of cells that are processed by a thread
///
class Battery
{
  public:
  // -- Construction

  Battery ( sev::logt::Context log_context_n,
            sev::thread::Tracker * thread_tracker_n,
            sev::unicode::View thread_name_n );

  Battery ( const Battery & battery_n ) = delete;

  Battery ( Battery && battery_n ) = delete;

  virtual ~Battery ();

  // -- Assigment operators

  Battery &
  operator= ( const Battery & battery_n ) = delete;

  Battery &
  operator= ( Battery && battery_n ) = delete;

  // -- Accessors

  /// @brief Logging context
  const sev::logt::Context &
  log () const;

  // -- Session control interface

  /// @brief Start the session
  bool
  start ( Starter && starter_n );

  /// @brief Abort the session
  void
  abort ();

  /// @brief Stop the session
  void
  stop ();

  // -- Session state interface

  /// @brief Returns trus if the session is running
  bool
  is_running () const;

  /// @brief Returns trus if the session is running and aborting
  bool
  is_aborting () const;

  private:
  // -- Attributes
  std::unique_ptr< detail::Detail > _detail;
};

} // namespace snc::bat
