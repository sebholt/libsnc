/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <sev/unicode/view.hpp>
#include <snc/bat/cell_context.hpp>
#include <snc/bat/cell_init.hpp>
#include <string>

// -- Forward declaration
namespace snc::bat
{
class Signal;
}
namespace snc::bat::ifc
{
class Client_Abstract;
class Service_Abstract;
} // namespace snc::bat::ifc
namespace snc::bat::detail
{
class Detail;
}

namespace snc::bat
{

/// @brief Cell of a processing battery
///
class Cell
{
  // -- Types
  public:
  using Cell_Init = snc::bat::Cell_Init;
  using Client_Abstract = snc::bat::ifc::Client_Abstract;
  using Service_Abstract = snc::bat::ifc::Service_Abstract;
  struct Cell_Session
  {
    bool is_running = false;
    bool is_aborting = false;
    bool is_good = false;
  };

  // -- Construction

  protected:
  Cell ( const Cell_Init & cell_init_n,
         sev::unicode::View cell_type_n,
         sev::unicode::View log_name_n );

  public:
  Cell ( const Cell & cell_n ) = delete;

  Cell ( Cell && cell_n ) = delete;

  virtual ~Cell ();

  // -- Assignment operators

  Cell &
  operator= ( const Cell & cell_n ) = delete;

  Cell &
  operator= ( Cell && cell_n ) = delete;

  // -- Accessors

  /// @brief Shared cell context
  const Cell_Context &
  cell_context () const
  {
    return _cell_context;
  }

  /// @brief Cell type
  const std::string &
  cell_type () const
  {
    return _cell_type;
  }

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  const auto &
  services () const
  {
    return _services;
  }

  // -- Session control interface

  const Cell_Session &
  cell_session () const
  {
    return _cell_session;
  }

  /// @brief Begin the processing session
  ///
  /// All cells
  virtual void
  cell_session_begin ();

  virtual void
  cell_session_abort ();

  virtual bool
  cell_session_is_ready_to_end () const;

  /// @brief End the processing session
  virtual void
  cell_session_end ();

  // -- Signal interface

  void
  signal_connect ( Signal * signal_n );

  void
  signal_disconnect ( Signal * signal_n );

  void
  signal_disconnect_all ();

  // -- Central processing request

  /// @brief Processing request callback
  const std::function< void () > &
  processing_request_async () const
  {
    return _processing_request_async;
  }

  /// @brief True if processing_request() or request_processing() was triggered
  ///       already
  bool
  processing_requested () const
  {
    return _processing_requested;
  }

  /// @brief Processing request callback
  const std::function< void () > &
  processing_request () const
  {
    return _processing_request;
  }

  /// @brief Request processing
  void
  request_processing () const;

  // -- Central processing

  /// @brief Called synchronously when an processing_request_async()
  ///        was triggered.
  ///
  /// The default implementation does nothing.
  virtual void
  process_async ();

  /// @brief Central processing function.  Gets called when
  ///        processing_request() or request_processing was triggered.
  ///
  /// The default implementation does nothing.
  virtual void
  process ();

  // -- Client registration
  private:
  friend snc::bat::ifc::Client_Abstract;

  void
  clients_add ( Client_Abstract * client_n );

  void
  clients_drop ( Client_Abstract * client_n );

  // -- Service registration
  private:
  friend snc::bat::ifc::Service_Abstract;

  void
  services_add ( Service_Abstract * service_n );

  void
  services_drop ( Service_Abstract * service_n );

  private:
  // -- Private interface
  friend class snc::bat::detail::Detail;

  void
  pick_up_processing ()
  {
    _processing_requested = false;
    this->process ();
  }

  private:
  // -- Meta
  Cell_Context & _cell_context;
  std::string _cell_type;
  sev::logt::Context _log;
  // -- Session state
  Cell_Session _cell_session;
  // -- Processing requests
  mutable bool _processing_requested = false;
  std::function< void () > _processing_request;
  std::function< void () > _processing_request_async;
  // -- Signaling
  std::vector< Signal * > _signal_connections;
  /// @brief Service clients register
  std::vector< Client_Abstract * > _clients;
  /// @brief Provided services register
  std::vector< Service_Abstract * > _services;
};

} // namespace snc::bat
