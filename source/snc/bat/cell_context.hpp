/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accus_async/condition.hpp>
#include <sev/event/timer_queue/chrono/queue.hpp>
#include <sev/logt/reference.hpp>
#include <sev/mem/view.hpp>
#include <sev/thread/tracker.hpp>
#include <snc/bat/detail/cell_ring.hpp>

// -- Forward declaration
namespace snc::bat
{
class Cell;
class Battery;
} // namespace snc::bat
namespace snc::bat::detail
{
class Detail;
} // namespace snc::bat::detail

namespace snc::bat
{

/// @brief Context of a processing battery cell
///
class Cell_Context
{
  public:
  // -- Construction

  Cell_Context ( sev::logt::Reference log_parent_n,
                 sev::event::bit_accus_async::Condition * event_accu_n,
                 sev::event::timer_queue::chrono::Queue_Steady * timer_queue_n,
                 sev::thread::Tracker * thread_tracker_n )
  : _log_parent ( std::move ( log_parent_n ) )
  , _event_accu ( event_accu_n )
  , _timer_queue ( timer_queue_n )
  , _thread_tracker ( thread_tracker_n )
  {
  }

  Cell_Context ( const Cell_Context & cell_context_n );

  Cell_Context ( Cell_Context && cell_context_n );

  ~Cell_Context ();

  // -- Assignment operators

  Cell_Context &
  operator= ( const Cell_Context & cell_context_n );

  Cell_Context &
  operator= ( Cell_Context && cell_context_n );

  // -- Accessors

  /// @brief Logging context
  const sev::logt::Reference &
  log_parent () const
  {
    return _log_parent;
  }

  /// @brief Cells
  const sev::mem::View< Cell * > &
  cells () const
  {
    return _cells;
  }

  /// @brief Timer queue
  sev::event::timer_queue::chrono::Queue_Steady *
  timer_queue () const
  {
    return _timer_queue;
  }

  /// @brief Thread tracker
  sev::thread::Tracker *
  thread_tracker () const
  {
    return _thread_tracker;
  }

  private:
  // -- Private interface
  friend class detail::Detail;
  friend class Cell;

  sev::mem::View< Cell * > &
  cells_ref ()
  {
    return _cells;
  }

  detail::Cell_Ring &
  processing_ring_ref ()
  {
    return _processing_ring;
  }

  void
  register_for_processing ( const Cell * cell_n );

  private:
  // -- Attributes
  sev::logt::Reference _log_parent;
  sev::mem::View< Cell * > _cells;
  detail::Cell_Ring _processing_ring;
  sev::event::bit_accus_async::Condition * _event_accu = nullptr;
  sev::event::timer_queue::chrono::Queue_Steady * _timer_queue = nullptr;
  sev::thread::Tracker * _thread_tracker = nullptr;
};

} // namespace snc::bat
