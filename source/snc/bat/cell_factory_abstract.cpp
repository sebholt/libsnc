/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cell_factory_abstract.hpp"

namespace snc::bat
{

Cell_Factory_Abstract::Cell_Factory_Abstract () = default;

Cell_Factory_Abstract::~Cell_Factory_Abstract () = default;

} // namespace snc::bat
