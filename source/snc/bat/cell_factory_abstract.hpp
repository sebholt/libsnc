/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/align.hpp>
#include <snc/bat/cell_init.hpp>
#include <cstddef>
#include <functional>
#include <memory>
#include <utility>

namespace snc::bat
{

// -- Forward declaration
class Cell;

// -- Types
using Cell_Post_Init = std::function< void ( Cell * ) >;

/// @brief Cell factory and owner
///
class Cell_Factory_Abstract
{
  public:
  // -- Types

  using Cell_Init = snc::bat::Cell_Init;
  using CFA = Cell_Factory_Abstract;

  // -- Construction

  Cell_Factory_Abstract ();

  Cell_Factory_Abstract ( const CFA & factory_n ) = delete;

  Cell_Factory_Abstract ( CFA && factory_n ) = delete;

  virtual ~Cell_Factory_Abstract ();

  // -- Assignment operators (delete)

  CFA &
  operator= ( const CFA & factory_n ) = delete;

  CFA &
  operator= ( CFA && factory_n ) = delete;

  // -- Construction interface

  protected:
  /// @brief Return the unaligned cell size
  virtual std::size_t
  cell_size () const = 0;

  public:
  /// @brief Computes the aligned cell size
  std::size_t
  cell_size_aligned () const
  {
    return sev::mem::aligned_up ( this->cell_size () );
  }

  /// @brief Constructs the cell at the given address
  virtual Cell *
  cell_init ( void * memory_n, const Cell_Init & cell_init_n ) const = 0;

  // -- Post construction callback

  const Cell_Post_Init &
  post_init () const
  {
    return _post_init;
  }

  void
  set_post_init ( Cell_Post_Init && func_n )
  {
    _post_init = std::move ( func_n );
  }

  private:
  // -- Attributes
  Cell_Post_Init _post_init;
};

} // namespace snc::bat
