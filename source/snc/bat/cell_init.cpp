/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "cell_init.hpp"

namespace snc::bat
{

Cell_Init::~Cell_Init () = default;

} // namespace snc::bat
