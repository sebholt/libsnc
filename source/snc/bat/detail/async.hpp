/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <atomic>
#include <cstdint>

namespace snc::bat::detail
{

struct Async
{
  // -- Asynchronous event notification
  using Bits_Int = std::uintptr_t;
  using Bits_Atomic = std::atomic< Bits_Int >;
  static constexpr std::size_t bytes_per_reg = sizeof ( Bits_Int );
  static constexpr std::size_t bits_per_reg = bytes_per_reg * 8;
};

} // namespace snc::bat::detail
