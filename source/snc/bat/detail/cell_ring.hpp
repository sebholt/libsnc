/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <snc/bat/detail/async.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::bat
{
class Cell;
}

namespace snc::bat::detail
{

// -- Loop event bits

struct LEvent
{
  static constexpr std::uint_fast32_t PROCESS_ASYNC = ( 1 << 0 );
  static constexpr std::uint_fast32_t PROCESS = ( 1 << 1 );
  static constexpr std::uint_fast32_t ABORT = ( 1 << 2 );
};

/// @brief Ring buffer view into a list of Cell pointers
///
class Cell_Ring
{
  public:
  // -- Setup

  void
  setup ( Cell ** area_begin_n, std::size_t size_n )
  {
    _size = 0;
    _capacity = size_n;
    _begin = area_begin_n;
    _end = area_begin_n;
    _area_begin = area_begin_n;
    _area_end = area_begin_n + size_n;
  }

  // -- Sizes

  std::size_t
  size () const
  {
    return _size;
  }

  std::size_t
  capacity () const
  {
    return _capacity;
  }

  bool
  is_empty () const
  {
    return ( _size == 0 );
  }

  bool
  is_full () const
  {
    return ( _size == _capacity );
  }

  // -- Write

  void
  push ( Cell * cell_n )
  {
    DEBUG_ASSERT ( !is_full () );
    *_end = cell_n;
    increment ( _end );
    ++_size;
  }

  Cell *
  pop ()
  {
    DEBUG_ASSERT ( !is_empty () );
    Cell * res = *_begin;
    increment ( _begin );
    --_size;
    return res;
  }

  private:
  void
  increment ( Cell **& ptr_n )
  {
    ++ptr_n;
    if ( ptr_n == _area_end ) {
      ptr_n = _area_begin;
    }
  }

  private:
  std::size_t _size = 0;
  std::size_t _capacity = 0;
  Cell ** _begin = nullptr;
  Cell ** _end = nullptr;
  Cell ** _area_begin = nullptr;
  Cell ** _area_end = nullptr;
};

} // namespace snc::bat::detail
