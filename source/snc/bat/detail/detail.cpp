/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "detail.hpp"
#include <sev/mem/align.hpp>
#include <sev/utility.hpp>
#include <snc/bat/cell.hpp>

namespace snc::bat::detail
{

namespace
{

/** @brief Memory area */
template < typename T >
class Area
{
  public:
  // -- Aligned size

  std::size_t
  size_aligned () const
  {
    return _size_aligned;
  }

  void
  set_size ( std::size_t size_n )
  {
    _size_aligned = sev::mem::aligned_up ( size_n );
  }

  void
  set_size ( std::size_t size_n, std::size_t num_n )
  {
    _size_aligned = sev::mem::aligned_up ( size_n * num_n );
  }

  // -- Memory access

  T *
  item ( std::size_t index_n = 0 ) const
  {
    return reinterpret_cast< T * > ( _begin ) + index_n;
  }

  std::byte *
  begin () const
  {
    return _begin;
  }

  void
  set_begin ( std::byte * begin_n )
  {
    _begin = begin_n;
    DEBUG_ASSERT ( sev::mem::is_aligned ( _begin ) );
  }

  private:
  std::size_t _size_aligned = 0;
  std::byte * _begin = nullptr;
};

} // namespace

Detail::Detail ( sev::logt::Context log_n,
                 sev::thread::Tracker * thread_tracker_n,
                 sev::unicode::View thread_name_n )
: _log ( std::move ( log_n ) )
, _cell_context ( _log, &_event_waiter, &_timer_queue_steady, thread_tracker_n )
, _thread ( thread_name_n.string (), thread_tracker_n )
{
}

Detail::~Detail ()
{
  stop ();
}

bool
Detail::start ( Starter && starter_n )
{
  if ( is_running () ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Start rejected.  Already running." );
    return false;
  }
  if ( starter_n.cell_factories.empty () ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Start rejected.  No cell factories." );
    return false;
  }

  // Clear flags
  _event_waiter.fetch_and_clear ();
  _loop_events.clear ();
  // Accept starter resources
  _cell_factories = std::move ( starter_n.cell_factories );
  _end_callback = std::move ( starter_n.end_callback );
  _end_cells_not_ready.reserve ( _cell_factories.size () );
  _end_message_time = std::chrono::steady_clock::now ();

  try {
    // Initialize arena
    construct_cells ();
    // Start thread
    _thread.start ( [ this ] () { this->operator() (); } );
    _is_running = true;
  }
  catch ( ... ) {
    destruct_cells ();
  }

  return _is_running;
}

void
Detail::abort ()
{
  if ( !is_running () || is_aborting () ) {
    return;
  }
  _is_aborting = true;
  _event_waiter.set ( LEvent::ABORT );
}

void
Detail::stop ()
{
  if ( !is_running () ) {
    return;
  }
  if ( !is_aborting () ) {
    abort ();
  }

  // Join thread and clean up
  _thread.join ();
  destruct_cells ();
  // Reset state
  _is_running = false;
  _is_aborting = false;
}

void
Detail::construct_cells ()
{
  // -- Constants
  // Number of cells
  const std::size_t num_cells = _cell_factories.size ();
  // Asynchronous bit registers
  const std::size_t async_bit_registers =
      sev::mem::aligned_up ( num_cells, Async::bits_per_reg ) /
      Async::bits_per_reg;

  // -- Arena
  struct Arena
  {
    // Processing request registers - sync
    Area< Cell * > prr_sync;
    // Processing request registers - async
    Area< Async::Bits_Atomic > prr_async;
    // Cell pointers list
    Area< Cell * > cells_list;
    // Cells instances
    Area< std::byte > cells;
  } arena;

  arena.prr_sync.set_size ( sizeof ( Cell * ), num_cells );
  arena.prr_async.set_size ( Async::bytes_per_reg, async_bit_registers );
  arena.cells_list.set_size ( sizeof ( Cell * ), num_cells );
  {
    std::size_t cells_size = 0;
    for ( auto & fact : _cell_factories ) {
      cells_size += fact->cell_size_aligned ();
    }
    arena.cells.set_size ( cells_size );
  }

  // Allocate memory arena
  std::size_t area_total = 0;
  area_total += arena.prr_sync.size_aligned ();
  area_total += arena.prr_async.size_aligned ();
  area_total += arena.cells_list.size_aligned ();
  area_total += arena.cells.size_aligned ();

  _memory_arena.resize ( area_total + sev::mem::align_block_size );
  std::byte * arena_begin = sev::mem::aligned_up ( _memory_arena.data () );
  DEBUG_ASSERT ( ( &*_memory_arena.end () - arena_begin ) >=
                 static_cast< std::ptrdiff_t > ( area_total ) );

  // Compute locations
  arena.prr_sync.set_begin ( arena_begin );
  arena.prr_async.set_begin ( arena_begin + //
                              arena.prr_sync.size_aligned () );
  arena.cells_list.set_begin ( arena_begin +                    //
                               arena.prr_sync.size_aligned () + //
                               arena.prr_async.size_aligned () );
  arena.cells.set_begin ( arena_begin +                     //
                          arena.prr_sync.size_aligned () +  //
                          arena.prr_async.size_aligned () + //
                          arena.cells_list.size_aligned () );

  // Processing request registers - sync
  for ( std::size_t ii = 0; ii != num_cells; ++ii ) {
    new ( arena.prr_sync.item ( ii ) ) Cell *( nullptr );
  }
  _cell_context.processing_ring_ref ().setup ( arena.prr_sync.item (),
                                               num_cells );

  // Processing request registers - async
  _processing_registers.reset ( arena.prr_async.item (), 0 );
  for ( std::size_t ii = 0; ii != async_bit_registers; ++ii ) {
    new ( arena.prr_async.item ( ii ) ) Async::Bits_Atomic ( 0 );
    _processing_registers.add_back ( 1 );
  }

  // Cells list
  for ( std::size_t ii = 0; ii != num_cells; ++ii ) {
    new ( arena.cells_list.item ( ii ) ) Cell *( nullptr );
  }
  _cell_context.cells_ref ().reset ( arena.cells_list.item (), 0 );

  // Cells
  {
    std::byte * cell_area = arena.cells.begin ();
    for ( std::size_t ii = 0; ii != num_cells; ++ii ) {
      auto & factory = _cell_factories[ ii ];

      Async::Bits_Int reg_bits = 1 << ( ii % Async::bits_per_reg );
      const std::size_t reg_index = ( ii / Async::bits_per_reg );
      Async::Bits_Atomic * reg = &_processing_registers[ reg_index ];

      // Create cell and increment memory pointer
      Cell * cell = factory->cell_init (
          cell_area,
          Cell_Init ( _cell_context, [ reg, reg_bits, &ec = _event_waiter ] () {
            if ( reg->fetch_or ( reg_bits ) == 0 ) {
              ec.set ( LEvent::PROCESS_ASYNC );
            }
          } ) );
      // Register cell in list
      _cell_context.cells_ref ().add_back ( 1 );
      _cell_context.cells_ref ().back () = cell;
      // Call post init callback
      if ( factory->post_init () ) {
        factory->post_init () ( cell );
      }

      cell_area += factory->cell_size_aligned ();
    }
  }
}

void
Detail::destruct_cells ()
{
  // Destruct cells in reverse order
  {
    auto & cells = _cell_context.cells_ref ();
    while ( !cells.is_empty () ) {
      cells.back ()->~Cell ();
      cells.drop_back ( 1 );
    }
    cells.clear ();
  }

  // Destruct processing registers
  {
    while ( !_processing_registers.is_empty () ) {
      _processing_registers.back ().~atomic ();
      _processing_registers.drop_back ( 1 );
    }
    _processing_registers.clear ();
  }

  // Cleanup
  _memory_arena.clear ();
  _cell_factories.clear ();
  _end_callback = std::function< void () > ();
}

void
Detail::operator() ()
{
  // -- Cell sessions begin
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Battery start: Beginning cell sessions of " << num_cells ()
         << " cells.";
  }
  for ( Cell * cell : _cell_context.cells () ) {
    cell->cell_session_begin ();
    DEBUG_ASSERT ( cell->cell_session ().is_running );
    DEBUG_ASSERT ( !cell->cell_session ().is_aborting );
  }

  // -- Loop normal
  loop_normal ();

  // -- Cell sessions abort
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Battery abort: Aborting cell sessions of " << num_cells ()
         << " cells.";
  }
  for ( Cell * cell : _cell_context.cells () ) {
    cell->cell_session_abort ();
    DEBUG_ASSERT ( cell->cell_session ().is_running );
    DEBUG_ASSERT ( cell->cell_session ().is_aborting );
  }

  // -- Loop with abort checks
  loop_abort ();

  // -- Cell sessions end
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Battery abort: Ending cell sessions of " << num_cells ()
         << " cells.";
  }
  for ( Cell * cell : sev::reverse ( _cell_context.cells () ) ) {
    cell->cell_session_end ();
    DEBUG_ASSERT ( !cell->cell_session ().is_running );
    DEBUG_ASSERT ( !cell->cell_session ().is_aborting );
  }

  // -- Notify end callback
  if ( _end_callback ) {
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Battery abort: Notifying end callback and fade out." );
    _end_callback ();
  }
}

void
Detail::fetch_events ()
{
  // -- Fetch events
  if ( _timer_queue_steady.is_empty () ) {
    // Wait for events bits only
    _loop_events.set (
        _event_waiter.fetch_and_clear_wait ( _loop_events.is_empty () ) );
  } else {
    // Wait for event bits or timeout
    auto res = _event_waiter.fetch_and_clear_events_wait (
        _timer_queue_steady.front (), _loop_events.is_empty () );
    _loop_events.set ( res.bits );
    // Process timeouts on demand
    if ( res.timeout ) {
      _timer_queue_steady.process_timeouts ( res.now );
    }
  }
}

void
Detail::cells_process ()
{
  // - Process number-of-cells times at maximum
  // - Clear processing flag only when the ring is empty
  auto & cell_ring = _cell_context.processing_ring_ref ();
  const std::size_t num_cells = _cell_factories.size ();
  for ( std::size_t ii = 0; ii != num_cells; ++ii ) {
    if ( cell_ring.is_empty () ) {
      _loop_events.unset ( LEvent::PROCESS );
      break;
    }
    // Process cell
    Cell * cell = cell_ring.pop ();
    cell->pick_up_processing ();
  }
}

void
Detail::cells_process_async ()
{
  const std::size_t num_cells = _cell_factories.size ();
  std::size_t cell_index = 0;
  for ( auto & async_reg : _processing_registers ) {
    // Fetch clear register and evaluate bits
    auto flags = async_reg.exchange ( 0 );
    // Any events in this register?
    if ( flags == 0 ) {
      cell_index += Async::bits_per_reg;
      continue;
    }
    // Process cells
    for ( std::size_t ii = 0;
          ( ii != Async::bits_per_reg ) && ( cell_index != num_cells );
          ++ii, ++cell_index ) {
      if ( ( flags & ( 1 << ii ) ) == 0 ) {
        continue;
      }
      DEBUG_ASSERT ( cell_index < _cell_context.cells ().size () );
      Cell * cell = _cell_context.cells ()[ cell_index ];
      cell->process_async ();
    }
  }
}

void
Detail::loop_normal ()
{
  while ( true ) {
    fetch_events ();
    if ( _loop_events.is_empty () ) {
      continue;
    }
    if ( _loop_events.test_any_unset ( LEvent::PROCESS_ASYNC ) ) {
      cells_process_async ();
    }
    if ( _loop_events.test_any ( LEvent::PROCESS ) ) {
      cells_process ();
    }
    if ( _loop_events.test_any_unset ( LEvent::ABORT ) ) {
      break;
    }
  }
}

void
Detail::loop_abort ()
{
  while ( true ) {
    // Check if all cells are ready to end
    for ( Cell * cell : _cell_context.cells () ) {
      if ( cell->cell_session_is_ready_to_end () ) {
        continue;
      }
      _end_cells_not_ready.push_back ( cell );
    }

    // Leave main loop when all cells are ready to end
    if ( !_end_cells_not_ready.empty () ) {
      // Log cells that are not ready to end
      auto tnow = std::chrono::steady_clock::now ();
      if ( ( tnow - _end_message_time ) > std::chrono::milliseconds ( 1000 ) ) {
        _end_message_time = tnow;
        if ( auto olog = log ().ostr ( sev::logt::FL_DEBUG ) ) {
          olog << "Battery: Cells not ready to end:";
          for ( Cell * cell : _end_cells_not_ready ) {
            olog << "\n  " << cell->cell_type ();
          }
        }
      }

      // Clear list of cells
      _end_cells_not_ready.clear ();
    } else {
      // Leave loop
      break;
    }

    // Regular loop processing
    fetch_events ();
    if ( _loop_events.test_any_unset ( LEvent::PROCESS_ASYNC ) ) {
      cells_process_async ();
    }
    if ( _loop_events.test_any ( LEvent::PROCESS ) ) {
      cells_process ();
    }
  }
}

} // namespace snc::bat::detail
