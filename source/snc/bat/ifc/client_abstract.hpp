/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/session.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::bat
{
class Cell;
}

namespace snc::bat::ifc
{

// -- Forward declaration
class Service_Abstract;
class Control_Unique;

/// @brief Service client base class
///
class Client_Abstract
{
  public:
  // -- Types
  using User = snc::bat::Cell;
  using Control_Unique = snc::bat::ifc::Control_Unique;
  using Service_Abstract = snc::bat::ifc::Service_Abstract;
  using Callback = std::function< void () >;

  // -- Construction

  Client_Abstract ( User * user_n );

  Client_Abstract ( const Client_Abstract & client_n ) = delete;

  Client_Abstract ( Client_Abstract && client_n ) = delete;

  virtual ~Client_Abstract ();

  // -- Assignment operators

  Client_Abstract &
  operator= ( const Client_Abstract & client_n ) = delete;

  Client_Abstract &
  operator= ( Client_Abstract && client_n ) = delete;

  // -- Accessors

  /// @brief Service user clerk that was passed to the contructor
  User *
  user () const
  {
    return _user;
  }

  // -- Service provider connection

  bool
  is_connected () const
  {
    return ( _service_abstract != nullptr );
  }

  /// @brief Service instance
  Service_Abstract *
  service_abstract () const
  {
    return _service_abstract;
  }

  /// @brief Find and connect to a service
  bool
  connect ();

  /// @brief Calls connect() and throws an exception if it fails.
  void
  connect_required ();

  /// @brief Automatically called in Clerk::cell_session_end() of user()
  virtual void
  disconnect ();

  // -- Service signals

  void
  signal_state_connect () const;

  void
  signal_state_disconnect () const;

  // -- Session interface

  const Session &
  session () const
  {
    return _session;
  }

  Session &
  session ()
  {
    return _session;
  }

  // -- Service control interface

  bool
  controlling () const
  {
    return ( _controller != nullptr );
  }

  /// @return true if the service control can be acquired
  bool
  control_available () const;

  protected:
  // -- Utility

  /// @brief Try to connect to the given service provider
  virtual bool
  connect_to ( Service_Abstract * sb_n ) = 0;

  /// @brief Called when the client connects
  virtual void
  connecting ();

  /// @brief Called when the client disconnects
  virtual void
  disconnecting ();

  /// @brief Throws and exception if the client is not connected
  void
  require_connection () const;

  private:
  // -- Control lock interface
  friend snc::bat::ifc::Control_Unique;
  friend snc::bat::ifc::Service_Abstract;

  bool
  control_acquire ( Control_Unique * control_n );

  void
  control_release ();

  void
  control_move ( Control_Unique * from_n, Control_Unique * to_n );

  private:
  User * const _user = nullptr;
  Service_Abstract * _service_abstract = nullptr;
  Control_Unique * _controller = nullptr;
  Session _session;
};

} // namespace snc::bat::ifc
