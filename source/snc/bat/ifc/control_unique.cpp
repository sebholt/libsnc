/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "control_unique.hpp"
#include <sev/assert.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <utility>

namespace snc::bat::ifc
{

Control_Unique::Control_Unique ( Client_Abstract * client_n )
{
  // Try to acquire the control lock
  if ( ( client_n != nullptr ) && client_n->control_acquire ( this ) ) {
    _client_abstract = client_n;
  }
}

Control_Unique::Control_Unique ( Control_Unique && control_n )
{
  if ( control_n.is_valid () ) {
    std::swap ( _client_abstract, control_n._client_abstract );
    _client_abstract->control_move ( &control_n, this );
    DEBUG_ASSERT ( is_valid () );
  }
}

Control_Unique::~Control_Unique ()
{
  release ();
}

Control_Unique &
Control_Unique::operator= ( Control_Unique && control_n )
{
  release ();
  if ( control_n.is_valid () ) {
    std::swap ( _client_abstract, control_n._client_abstract );
    _client_abstract->control_move ( &control_n, this );
    DEBUG_ASSERT ( is_valid () );
  }
  return *this;
}

bool
Control_Unique::is_valid () const
{
  return ( _client_abstract != nullptr ) &&
         ( _client_abstract->_controller == this );
}

void
Control_Unique::release ()
{
  // Drop control reference
  if ( is_valid () ) {
    _client_abstract->control_release ();
  }
  _client_abstract = nullptr;
}

} // namespace snc::bat::ifc
