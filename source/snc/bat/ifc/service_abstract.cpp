/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_abstract.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/bat/cell.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <stdexcept>
#include <utility>

namespace snc::bat::ifc
{

Service_Abstract::Service_Abstract ( Provider_Cell & provider_n,
                                     sev::unicode::View log_name_n )
: _provider_cell ( provider_n )
, _log ( provider_n.log (), log_name_n )
, _signal_state ( "state" )
{
  // Register service
  provider_cell ().services_add ( this );
}

Service_Abstract::~Service_Abstract ()
{
  // All clients should be disconnected
  DEBUG_ASSERT ( _clients.list.empty () );

  // Unregister service
  provider_cell ().services_drop ( this );
}

void
Service_Abstract::client_add ( Client_Abstract * client_n )
{
  _clients.list.push_back ( client_n );
}

void
Service_Abstract::client_drop ( Client_Abstract * client_n )
{
  auto it =
      std::find ( _clients.list.begin (), _clients.list.end (), client_n );
  if ( it != _clients.list.end () ) {
    _clients.list.erase ( it );
  }
}

void
Service_Abstract::clients_disconnect ()
{
  while ( !_clients.list.empty () ) {
    _clients.list.back ()->disconnect ();
  }
}

void
Service_Abstract::clients_session_begin ()
{
  for ( auto * client : _clients.list ) {
    client->session ().begin ();
  }
}

void
Service_Abstract::clients_session_end ()
{
  for ( auto * client : _clients.list ) {
    client->session ().end ();
  }
}

bool
Service_Abstract::control_acquirable () const
{
  return !controlled () && provider_cell ().cell_session ().is_good;
}

bool
Service_Abstract::control_acquirable_change_poll ()
{
  bool changed =
      sev::change ( _control_was_acquirable, this->control_acquirable () );
  if ( changed ) {
    signal_state ().send ();
  }
  return changed;
}

bool
Service_Abstract::control_acquire ( Client_Abstract * client_n )
{
  // Check if the lock is acquirable by the client.
  if ( !this->control_acquirable () ) {
    return false;
  }

  // Accept new client
  _clients.controller = client_n;
  this->control_acquired ( client_n );
  return true;
}

void
Service_Abstract::control_release ( Client_Abstract * client_n )
{
  if ( !controlled_by ( client_n ) ) {
    return;
  }
  _clients.controller = nullptr;
  this->control_released ( client_n );
}

void
Service_Abstract::control_release ()
{
  if ( !controlled () ) {
    return;
  }
  _clients.controller->control_release ();
  DEBUG_ASSERT ( _clients.controller == nullptr );
}

void
Service_Abstract::control_acquired ( Client_Abstract * client_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Control acquired by " << client_n->user ()->cell_type ();
  }
  signal_state ().send ();
}

void
Service_Abstract::control_released ( Client_Abstract * client_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Control released by " << client_n->user ()->cell_type ();
  }
  signal_state ().send ();
}

} // namespace snc::bat::ifc
