/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <functional>

namespace snc::bat::ifc
{

/// @brief Client session interface
class Session
{
  public:
  // -- Types

  using Callback = std::function< void () >;

  // -- Construction

  Session ();

  Session ( const Session & session_n ) = delete;

  Session ( Session && session_n ) = delete;

  ~Session ();

  // -- Assignment operators

  Session &
  operator= ( const Session & session_n ) = delete;

  Session &
  operator= ( Session && session_n ) = delete;

  // -- Interface

  /// @brief Session is started
  bool
  is_good () const
  {
    return _is_good;
  }

  /// @brief Session begin
  void
  begin ();

  /// @brief Session end
  void
  end ();

  // -- Callbacks

  auto &
  begin_callback () const
  {
    return _begin_callback;
  }

  void
  set_begin_callback ( Callback callback_n );

  auto &
  end_callback () const
  {
    return _end_callback;
  }

  void
  set_end_callback ( Callback callback_n );

  private:
  bool _is_good = false;
  Callback _begin_callback;
  Callback _end_callback;
};

} // namespace snc::bat::ifc
