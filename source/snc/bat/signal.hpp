/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/unicode/view.hpp>
#include <functional>
#include <string>
#include <vector>

namespace snc::bat
{

// Forward declaration
class Cell;

/// @brief Signal, that cells can connect to, to get a processing request.
///
class Signal
{
  public:
  // -- Construction

  Signal ( sev::unicode::View name_n );

  Signal ( const Signal & signal_n ) = delete;

  Signal ( Signal && signal_n ) = delete;

  ~Signal ();

  // -- Assignment operators

  Signal &
  operator= ( const Signal & signal_n ) = delete;

  Signal &
  operator= ( Signal && signal_n ) = delete;

  // -- Accessors

  /// @brief Signal name
  const std::string &
  name () const
  {
    return _name;
  }

  // -- Interface

  /// @brief Emit the signal
  void
  send () const;

  private:
  // -- Private interface
  friend class Cell;

  /// @brief Connect a cell to this signal
  void
  connect ( Cell * cell_n );

  /// @brief Disconnect a cell from this signal
  void
  disconnect ( Cell * cell_n );

  private:
  // -- Attributes
  std::string _name;
  std::vector< Cell * > _client_cells;
};

} // namespace snc::bat
