/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_factory_abstract.hpp>
#include <functional>
#include <vector>

namespace snc::bat
{

/// @brief Cell factory handle
using Cell_Factory_Handle = std::shared_ptr< Cell_Factory_Abstract >;

/// @brief Cell factories container type
using Cell_Factories = std::vector< Cell_Factory_Handle >;

/// @brief Battery processing starter
struct Starter
{
  Cell_Factories cell_factories;
  std::function< void () > end_callback;
};

} // namespace snc::bat
