/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "info.hpp"
#include <snc/device/state/state.hpp>
#include <snc/device/statics/statics.hpp>
#include <utility>

namespace snc::device
{

Info::Info ()
{
  reset ();
}

Info::Info ( const Info & info_n )
{
  assign ( info_n );
}

Info::Info ( const statics::handle::Statics & statics_n )
{
  reset ( statics_n );
}

Info::~Info () = default;

void
Info::reset ( const statics::handle::Statics & statics_n )
{
  _statics = statics_n;
  _state.reset ( _statics );
}

void
Info::reset ()
{
  reset ( snc::device::statics::handle::make () );
}

void
Info::reset_state ()
{
  _state.reset ( _statics );
}

void
Info::assign ( const Info & info_n )
{
  if ( _statics != info_n._statics ) {
    reset ( info_n._statics );
  }
  _state.assign ( info_n.state () );
}

snc::device::Axis_Info
Info::axis ( std::uint_fast32_t index_n ) const
{
  if ( index_n < _statics->axes ().size () ) {
    return { statics ()->axes ().get ( index_n ),
             state ().axis_shared ( index_n ) };
  }
  return {};
}

} // namespace snc::device
