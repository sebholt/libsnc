/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Axes.hpp"
#include <snc/device/qt/ItemModel_T.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemModel_T< snc::device::qt::axes::Axis >;

namespace snc::device::qt
{

Axes::Axes ( QObject * qparent_n, snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

Axes::~Axes () = default;

std::size_t
Axes::statics_count () const
{
  return info ().statics ()->axes ().size ();
}

} // namespace snc::device::qt
