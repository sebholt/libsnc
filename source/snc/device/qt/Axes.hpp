/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/qt/ItemModel_T.hpp>
#include <snc/device/qt/axes/Axis.hpp>

namespace snc::device::qt
{

/// @brief Axes model
///
class Axes : public ItemModel_T< axes::Axis >
{
  Q_OBJECT

  public:
  // -- Types

  using Super = ItemModel_T< axes::Axis >;

  // -- Properties

  Q_PROPERTY ( int count READ countInt NOTIFY countChanged )

  public:
  // -- Construction

  Axes ( QObject * qparent_n, snc::device::Info & info_n );

  ~Axes ();

  // -- Get

  Q_INVOKABLE
  QObject *
  getOrCreate ( int index_n )
  {
    return Super::getOrCreate ( index_n );
  }

  Q_INVOKABLE
  QObject *
  getByKey ( const QString & key_n )
  {
    return Super::getByKey ( key_n );
  }

  Q_INVOKABLE
  QObject *
  getByName ( const QString & name_n )
  {
    return Super::getByName ( name_n );
  }

  private:
  std::size_t
  statics_count () const override;
};

} // namespace snc::device::qt
