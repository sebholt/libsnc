/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Info.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::device::qt
{

Info::Info ( QObject * qparent_n )
: QObject ( qparent_n )
, _statics ( this, _info )
, _state ( this, _info )
, _sensors ( this, _info )
, _controls ( this, _info )
, _axes ( this, _info )
{
}

Info::~Info () = default;

void
Info::update_info ( const snc::device::Info & info_n )
{
  bool stats_changed = ( _info.statics () != info_n.statics () );
  _info = info_n;
  if ( stats_changed ) {
    update_statics ();
  }
}

void
Info::update_statics ()
{
  _statics.update_statics ();
  _sensors.update_statics ();
  _controls.update_statics ();
  _axes.update_statics ();

  update_available_properties ();
}

void
Info::update_valid_properties ()
{
  _state.update_properties ();
  _sensors.update_valid_properties ();
  _controls.update_valid_properties ();
  _axes.update_valid_properties ();
}

void
Info::update_available_properties ()
{
  _state.update_properties ();
  _sensors.update_available_properties ();
  _controls.update_available_properties ();
  _axes.update_available_properties ();
}

} // namespace snc::device::qt
