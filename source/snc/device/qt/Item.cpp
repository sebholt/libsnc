/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Item.hpp"
#include <sev/utility.hpp>

namespace snc::device::qt
{

Item::Item ( QObject * qparent_n, int index_n )
: QObject ( qparent_n )
, _index ( index_n )
{
}

Item::~Item () = default;

void
Item::setKey ( const QString & key_n )
{
  if ( sev::change ( _key, key_n ) ) {
    emit keyChanged ();
  }
}

void
Item::setName ( const QString & name_n )
{
  if ( sev::change ( _name, name_n ) ) {
    emit nameChanged ();
  }
}

} // namespace snc::device::qt
