/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/device/qt/sensors/i1/State.hpp>
#include <QObject>

namespace snc::device::qt
{

/// @brief Info item
///
class Item : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( QString key READ key NOTIFY keyChanged )
  Q_PROPERTY ( QString name READ name NOTIFY nameChanged )

  public:
  // -- Construction

  Item ( QObject * qparent_n, int index_n );

  ~Item ();

  // -- Index

  int
  index () const
  {
    return _index;
  }

  // -- Key

  const QString &
  key () const
  {
    return _key;
  }

  Q_SIGNAL
  void
  keyChanged ();

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  Q_SIGNAL
  void
  nameChanged ();

  protected:
  void
  setKey ( const QString & key_n );

  void
  setName ( const QString & name_n );

  private:
  // -- Attributes
  int _index = 0;
  QString _key;
  QString _name;
};

} // namespace snc::device::qt
