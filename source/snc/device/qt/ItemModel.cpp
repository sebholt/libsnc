/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "ItemModel.hpp"

namespace snc::device::qt
{

ItemModel::ItemModel ( QObject * qparent_n, snc::device::Info & info_n )
: QAbstractListModel ( qparent_n )
, _info ( info_n )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::listItem, "listItem" );
}

ItemModel::~ItemModel () = default;

QHash< int, QByteArray >
ItemModel::roleNames () const
{
  return _roleNames;
}

} // namespace snc::device::qt
