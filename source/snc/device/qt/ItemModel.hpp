/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <QAbstractListModel>
#include <QHash>

namespace snc::device::qt
{

/// @brief Item model base class
///
class ItemModel : public QAbstractListModel
{
  Q_OBJECT

  public:
  // -- Types

  enum ExtraRole
  {
    listItem = Qt::UserRole
  };

  public:
  // -- Construction

  ItemModel ( QObject * qparent_n, snc::device::Info & info_n );

  ~ItemModel ();

  // -- Device info

  snc::device::Info const &
  info () const
  {
    return _info;
  }

  snc::device::Info &
  info ()
  {
    return _info;
  }

  // -- Number of sensors

  Q_SIGNAL
  void
  countChanged ();

  // -- QAbstractListModel interface

  QHash< int, QByteArray >
  roleNames () const override;

  protected:
  /// @brief Read the number of items from the statics
  virtual std::size_t
  statics_count () const = 0;

  private:
  // -- Attributes
  snc::device::Info & _info;
  QHash< int, QByteArray > _roleNames;
};

} // namespace snc::device::qt
