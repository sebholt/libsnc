/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/qt/ItemModel.hpp>

namespace snc::device::qt
{

/// @brief Item model template
template < typename T >
class ItemModel_T : public ItemModel
{
  public:
  // -- Types
  using Super = ItemModel;
  using Handle = std::shared_ptr< T >;
  using Item_List = std::vector< Handle >;

  // -- Construction

  ItemModel_T ( QObject * qparent_n, snc::device::Info & info_n );

  ~ItemModel_T ();

  // -- Number of items

  std::size_t
  count () const
  {
    return _valid.size ();
  }

  int
  countInt () const
  {
    return _valid.size ();
  }

  // -- Get

  std::shared_ptr< T >
  getOrCreateShared ( std::size_t index_n );

  QObject *
  getOrCreate ( int index_n );

  QObject *
  getByKey ( const QString & key_n );

  QObject *
  getByName ( const QString & name_n );

  // -- QAbstractListModel interface

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  QVariant
  data ( const QModelIndex & index_n,
         int role_n = Qt::DisplayRole ) const override;

  // -- Interface

  void
  update_statics ();

  void
  update_valid_properties ();

  void
  update_available_properties ();

  private:
  // -- Model indices

  bool
  validParentIndex ( const QModelIndex & index_n ) const
  {
    return !index_n.isValid ();
  }

  bool
  validIndex ( const QModelIndex & index_n ) const;

  void
  create_valid ( std::size_t size_n );

  void
  remove_valid ( std::size_t size_n );

  void
  ensure_size_available ( std::size_t size_n );

  private:
  // -- Attributes
  Item_List _valid;
  Item_List _available;
};

} // namespace snc::device::qt
