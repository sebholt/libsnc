/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "ItemModel_T.hpp"
#include <snc/device/statics/statics.hpp>
#include <stdexcept>

namespace snc::device::qt
{

template < typename T >
ItemModel_T< T >::ItemModel_T ( QObject * qparent_n,
                                snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

template < typename T >
ItemModel_T< T >::~ItemModel_T () = default;

template < typename T >
std::shared_ptr< T >
ItemModel_T< T >::getOrCreateShared ( std::size_t index_n )
{
  ensure_size_available ( index_n + 1 );
  return _available.at ( index_n );
}

template < typename T >
QObject *
ItemModel_T< T >::getOrCreate ( int index_n )
{
  if ( index_n < 0 ) {
    throw std::runtime_error ( "Negative index." );
  }
  ensure_size_available ( index_n + 1 );
  return _available.at ( index_n ).get ();
}

template < typename T >
QObject *
ItemModel_T< T >::getByKey ( const QString & key_n )
{
  for ( auto & item : _available ) {
    if ( item->key () == key_n ) {
      return item.get ();
    }
  }
  return nullptr;
}

template < typename T >
QObject *
ItemModel_T< T >::getByName ( const QString & name_n )
{
  for ( auto & item : _available ) {
    if ( item->name () == name_n ) {
      return item.get ();
    }
  }
  return nullptr;
}

template < typename T >
int
ItemModel_T< T >::rowCount ( const QModelIndex & parent_n ) const
{
  return validParentIndex ( parent_n ) ? _valid.size () : 0;
}

template < typename T >
QVariant
ItemModel_T< T >::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( validIndex ( index_n ) ) {
    switch ( role_n ) {
    case ExtraRole::listItem:
      return QVariant::fromValue ( _valid.at ( index_n.row () ).get () );
    default:
      break;
    }
  }
  return QVariant ();
}

template < typename T >
void
ItemModel_T< T >::update_statics ()
{
  std::size_t const num_old = _valid.size ();
  std::size_t const num_new = this->statics_count ();
  if ( num_old != num_new ) {
    if ( num_old < num_new ) {
      // Add items
      beginInsertRows ( QModelIndex (), num_old, num_new - 1 );
      create_valid ( num_new - num_old );
      endInsertRows ();
    } else {
      // Remove items
      beginRemoveRows ( QModelIndex (), num_new, num_old - 1 );
      remove_valid ( num_old - num_new );
      endRemoveRows ();
    }
    emit countChanged ();
  }

  // Update statics of all items
  for ( auto & item : _available ) {
    item->update_statics ( info () );
  }
}

template < typename T >
void
ItemModel_T< T >::update_valid_properties ()
{
  for ( auto & sensor : _valid ) {
    sensor->update_properties ();
  }
}

template < typename T >
void
ItemModel_T< T >::update_available_properties ()
{
  for ( auto & sensor : _available ) {
    sensor->update_properties ();
  }
}

template < typename T >
bool
ItemModel_T< T >::validIndex ( const QModelIndex & index_n ) const
{
  return index_n.isValid () && //
         ( std::size_t ( index_n.row () ) < _valid.size () ) &&
         validParentIndex ( index_n.parent () );
}

template < typename T >
void
ItemModel_T< T >::create_valid ( std::size_t delta_n )
{
  auto valid_size_new = _valid.size () + delta_n;
  ensure_size_available ( valid_size_new );
  for ( std::size_t ii = 0; ii != delta_n; ++ii ) {
    _valid.push_back ( _available[ _valid.size () ] );
  }
}

template < typename T >
void
ItemModel_T< T >::remove_valid ( std::size_t delta_n )
{
  _valid.resize ( _valid.size () - delta_n );
}

template < typename T >
void
ItemModel_T< T >::ensure_size_available ( std::size_t size_n )
{
  while ( _available.size () < size_n ) {
    std::size_t index = _available.size ();
    _available.push_back ( std::make_shared< T > ( this, index ) );
  }
}

} // namespace snc::device::qt
