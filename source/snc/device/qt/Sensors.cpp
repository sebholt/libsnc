/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Sensors.hpp"

namespace snc::device::qt
{

Sensors::Sensors ( QObject * qparent_n, snc::device::Info & info_n )
: QObject ( qparent_n )
, _i1 ( this, info_n )
, _i8 ( this, info_n )
, _i16 ( this, info_n )
, _i32 ( this, info_n )
, _i64 ( this, info_n )
{
}

Sensors::~Sensors () = default;

void
Sensors::update_statics ()
{
  _i1.update_statics ();
  _i8.update_statics ();
  _i16.update_statics ();
  _i32.update_statics ();
  _i64.update_statics ();
}

void
Sensors::update_valid_properties ()
{
  _i1.update_valid_properties ();
  _i8.update_valid_properties ();
  _i16.update_valid_properties ();
  _i32.update_valid_properties ();
  _i64.update_valid_properties ();
}

void
Sensors::update_available_properties ()
{
  _i1.update_available_properties ();
  _i8.update_available_properties ();
  _i16.update_available_properties ();
  _i32.update_available_properties ();
  _i64.update_available_properties ();
}

} // namespace snc::device::qt
