/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <snc/device/qt/sensors/I1.hpp>
#include <snc/device/qt/sensors/I16.hpp>
#include <snc/device/qt/sensors/I32.hpp>
#include <snc/device/qt/sensors/I64.hpp>
#include <snc/device/qt/sensors/I8.hpp>
#include <QObject>

namespace snc::device::qt
{

/// @brief Sensors container
///
class Sensors : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( snc::device::qt::sensors::I1 * i1 READ i1 CONSTANT )
  Q_PROPERTY ( snc::device::qt::sensors::I8 * i8 READ i8 CONSTANT )
  Q_PROPERTY ( snc::device::qt::sensors::I16 * i16 READ i16 CONSTANT )
  Q_PROPERTY ( snc::device::qt::sensors::I32 * i32 READ i32 CONSTANT )
  Q_PROPERTY ( snc::device::qt::sensors::I64 * i64 READ i64 CONSTANT )

  public:
  // -- Construction

  Sensors ( QObject * qparent_n, snc::device::Info & info_n );

  ~Sensors ();

  // -- Groups

  snc::device::qt::sensors::I1 *
  i1 ()
  {
    return &_i1;
  }

  snc::device::qt::sensors::I8 *
  i8 ()
  {
    return &_i8;
  }

  snc::device::qt::sensors::I16 *
  i16 ()
  {
    return &_i16;
  }

  snc::device::qt::sensors::I32 *
  i32 ()
  {
    return &_i32;
  }

  snc::device::qt::sensors::I64 *
  i64 ()
  {
    return &_i64;
  }

  // -- Interface

  void
  update_statics ();

  void
  update_valid_properties ();

  void
  update_available_properties ();

  private:
  // -- Attributes
  snc::device::qt::sensors::I1 _i1;
  snc::device::qt::sensors::I8 _i8;
  snc::device::qt::sensors::I16 _i16;
  snc::device::qt::sensors::I32 _i32;
  snc::device::qt::sensors::I64 _i64;
};

} // namespace snc::device::qt
