/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <QObject>

namespace snc::device::qt
{

/// @brief Device state
///
class State : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( double serialBitrateIn READ serialBitrateIn NOTIFY
                   serialBitrateInChanged )
  Q_PROPERTY ( double serialBitrateOut READ serialBitrateOut NOTIFY
                   serialBitrateOutChanged )
  Q_PROPERTY ( double serialBitrateTotal READ serialBitrateTotal NOTIFY
                   serialBitrateTotalChanged )
  Q_PROPERTY ( double latency READ latency NOTIFY latencyChanged )

  public:
  // -- Construction

  State ( QObject * qparent_n, snc::device::Info & info_n );

  ~State ();

  // -- Serial IO

  double
  serialBitrateIn () const
  {
    return _serialBitrateIn;
  }

  Q_SIGNAL
  void
  serialBitrateInChanged ();

  double
  serialBitrateOut () const
  {
    return _serialBitrateOut;
  }

  Q_SIGNAL
  void
  serialBitrateOutChanged ();

  double
  serialBitrateTotal () const
  {
    return _serialBitrateTotal;
  }

  Q_SIGNAL
  void
  serialBitrateTotalChanged ();

  // -- Latency

  double
  latency () const
  {
    return _latency;
  }

  Q_SIGNAL
  void
  latencyChanged ();

  // -- Interface

  void
  update_properties ();

  private:
  // -- Attributes
  snc::device::Info & _info;
  double _serialBitrateIn = 0.0;
  double _serialBitrateOut = 0.0;
  double _serialBitrateTotal = 0.0;
  double _latency = 0.0;
};

} // namespace snc::device::qt
