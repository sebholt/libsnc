/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <QObject>

namespace snc::device::qt
{

/// @brief Device statics
///
class Statics : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( double speedMax READ speedMax NOTIFY speedMaxChanged )
  Q_PROPERTY (
      double speedPlanarMax READ speedPlanarMax NOTIFY speedPlanarMaxChanged )
  Q_PROPERTY (
      double speedNormalMax READ speedNormalMax NOTIFY speedNormalMaxChanged )

  public:
  // -- Construction

  Statics ( QObject * qparent_n, snc::device::Info & info_n );

  ~Statics ();

  // -- Speeds

  double
  speedMax () const
  {
    return _speedMax;
  }

  Q_SIGNAL
  void
  speedMaxChanged ();

  double
  speedPlanarMax () const
  {
    return _speedPlanarMax;
  }

  Q_SIGNAL
  void
  speedPlanarMaxChanged ();

  double
  speedNormalMax () const
  {
    return _speedNormalMax;
  }

  Q_SIGNAL
  void
  speedNormalMaxChanged ();

  // -- Interface

  void
  update_statics ();

  private:
  // -- Attributes
  snc::device::Info & _info;
  double _speedMax = 0.0;
  double _speedPlanarMax = 0.0;
  double _speedNormalMax = 0.0;
};

} // namespace snc::device::qt
