/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Axis.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::device::qt::axes
{

Axis::Axis ( QObject * qparent_n, int index_n )
: Item ( qparent_n, index_n )
, _state ( this, index_n )
{
}

Axis::~Axis () = default;

double
Axis::speedMaxRoundsPerMinute () const
{
  return ( _speedMax * 60.0 ) / sev::lag::two_pi_d;
}

void
Axis::update_statics ( const snc::device::Info & info_n )
{
  const bool was_valid = isValid ();
  auto & sstats = info_n.statics ()->axes ();
  if ( index () < static_cast< int > ( sstats.size () ) ) {
    // -- Valid axis
    _statics = sstats.get ( index () );
    setKey ( QString::fromStdString ( _statics->key () ) );
    setName ( QString::fromStdString ( _statics->name () ) );
    if ( sev::change ( _isLinear, _statics->geo ().linear () ) ) {
      emit geometryChanged ();
    }
    if ( sev::change ( _isRotational, _statics->geo ().rotational () ) ) {
      emit geometryChanged ();
    }
    if ( sev::change ( _length, _statics->geo ().length () ) ) {
      emit lengthChanged ();
    }
    if ( sev::change ( _lengthPerStep, _statics->geo ().length_per_step () ) ) {
      emit lengthPerStepChanged ();
    }
    if ( sev::change ( _speedMin, _statics->geo ().speed_min () ) ) {
      emit speedMinChanged ();
    }
    if ( sev::change ( _speedMax, _statics->geo ().speed_max () ) ) {
      emit speedMaxChanged ();
    }
    if ( sev::change ( _accelMax, _statics->geo ().accel_max () ) ) {
      emit accelMaxChanged ();
    }
  } else {
    // -- Invalid axis
    _statics.reset ();
    setKey ( QString () );
    setName ( QString () );
    if ( sev::change ( _isLinear, false ) ) {
      emit geometryChanged ();
    }
    if ( sev::change ( _isRotational, false ) ) {
      emit geometryChanged ();
    }
    if ( sev::change ( _length, 0.0 ) ) {
      emit lengthChanged ();
    }
    if ( sev::change ( _lengthPerStep, 0.0 ) ) {
      emit lengthPerStepChanged ();
    }
    if ( sev::change ( _speedMin, 0.0 ) ) {
      emit speedMinChanged ();
    }
    if ( sev::change ( _speedMax, 0.0 ) ) {
      emit speedMaxChanged ();
    }
    if ( sev::change ( _accelMax, 0.0 ) ) {
      emit accelMaxChanged ();
    }
  }
  // -- Update validity
  if ( isValid () != was_valid ) {
    emit isValidChanged ();
  }

  // -- Update state statics
  _state.update_statics ( info_n );
}

void
Axis::update_properties ()
{
  _state.update_properties ();
}

} // namespace snc::device::qt::axes
