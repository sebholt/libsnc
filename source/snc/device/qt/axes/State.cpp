/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "State.hpp"
#include <sev/assert.hpp>
#include <sev/lag/math.hpp>
#include <sev/utility.hpp>

namespace snc::device::qt::axes
{

State::State ( QObject * qparent_n, int index_n )
: QObject ( qparent_n )
, _index ( index_n )
{
}

State::~State () = default;

double
State::radians () const
{
  return sev::lag::radians_in_two_pi ( _position );
}

double
State::degrees () const
{
  return sev::lag::radians_to_degrees (
      sev::lag::radians_in_two_pi ( _position ) );
}

double
State::speedRoundsPerMinute () const
{
  return ( _speed * 60.0 / sev::lag::two_pi_d );
}

void
State::update_statics ( const snc::device::Info & info_n )
{
  _source = info_n.state ().axis_shared ( index () );
}

void
State::update_properties ()
{
  if ( _source ) {
    // -- Valid axis
    if ( sev::change ( _is_aligned, _source->is_aligned () ) ) {
      emit alignedChanged ();
    }
    if ( sev::change ( _position,
                       _source->kinematics_current ().position () ) ) {
      emit positionChanged ();
    }
    if ( sev::change ( _speed, _source->kinematics_current ().speed () ) ) {
      emit speedChanged ();
    }
  } else {
    // -- Invalid axis
    if ( sev::change ( _is_aligned, false ) ) {
      emit alignedChanged ();
    }
    if ( sev::change ( _position, 0.0 ) ) {
      emit positionChanged ();
    }
    if ( sev::change ( _speed, 0.0 ) ) {
      emit speedChanged ();
    }
  }
}

} // namespace snc::device::qt::axes
