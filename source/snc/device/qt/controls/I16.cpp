/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "I16.hpp"
#include <snc/device/qt/ItemModel_T.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemModel_T<
    snc::device::qt::controls::i16::Control >;

namespace snc::device::qt::controls
{

I16::I16 ( QObject * qparent_n, snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

I16::~I16 () = default;

std::size_t
I16::statics_count () const
{
  return info ().statics ()->controls_i16 ().size ();
}

} // namespace snc::device::qt::controls
