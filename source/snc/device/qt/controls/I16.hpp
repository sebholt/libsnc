/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/qt/ItemModel_T.hpp>
#include <snc/device/qt/controls/i16/Control.hpp>

namespace snc::device::qt::controls
{

/// @brief I16 controls model
///
class I16 : public ItemModel_T< i16::Control >
{
  Q_OBJECT

  // -- Types

  using Super = ItemModel_T< i16::Control >;

  // -- Properties

  Q_PROPERTY ( int count READ countInt NOTIFY countChanged )

  public:
  // -- Construction

  I16 ( QObject * qparent_n, snc::device::Info & info_n );

  ~I16 ();

  // -- Get

  Q_INVOKABLE
  QObject *
  getOrCreate ( int index_n )
  {
    return Super::getOrCreate ( index_n );
  }

  Q_INVOKABLE
  QObject *
  getByKey ( const QString & key_n )
  {
    return Super::getByKey ( key_n );
  }

  Q_INVOKABLE
  QObject *
  getByName ( const QString & name_n )
  {
    return Super::getByName ( name_n );
  }

  private:
  std::size_t
  statics_count () const override;
};

} // namespace snc::device::qt::controls
