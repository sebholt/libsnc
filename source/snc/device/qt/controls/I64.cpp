/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "I64.hpp"
#include <snc/device/qt/ItemModel_T.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemModel_T<
    snc::device::qt::controls::i64::Control >;

namespace snc::device::qt::controls
{

I64::I64 ( QObject * qparent_n, snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

I64::~I64 () = default;

std::size_t
I64::statics_count () const
{
  return info ().statics ()->controls_i64 ().size ();
}

} // namespace snc::device::qt::controls
