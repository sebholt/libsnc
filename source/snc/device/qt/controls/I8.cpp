/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "I8.hpp"
#include <snc/device/qt/ItemModel_T.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemModel_T<
    snc::device::qt::controls::i8::Control >;

namespace snc::device::qt::controls
{

I8::I8 ( QObject * qparent_n, snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

I8::~I8 () = default;

std::size_t
I8::statics_count () const
{
  return info ().statics ()->controls_i8 ().size ();
}

} // namespace snc::device::qt::controls
