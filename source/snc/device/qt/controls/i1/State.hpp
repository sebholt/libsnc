/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <QObject>

namespace snc::device::qt::controls::i1
{

/// @brief I1 control state
///
class State : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( bool value READ value NOTIFY valueChanged )

  public:
  // -- Construction

  State ( QObject * qparent_n, int index_n );

  ~State ();

  // -- Index

  int
  index ()
  {
    return _index;
  }

  // -- Value

  bool
  value () const
  {
    return _value;
  }

  Q_SIGNAL
  void
  valueChanged ();

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  int _index = 0;
  bool _value = false;
  snc::device::state::control::handle::I1 _source;
};

} // namespace snc::device::qt::controls::i1
