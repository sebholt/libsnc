/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Control.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/control/i16.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::device::qt::controls::i16
{

Control::Control ( QObject * qparent_n, int index_n )
: Item ( qparent_n, index_n )
, _state ( this, index_n )
{
}

Control::~Control () = default;

void
Control::update_statics ( const snc::device::Info & info_n )
{
  const bool was_valid = isValid ();
  auto & sstats = info_n.statics ()->controls_i16 ();
  if ( index () < static_cast< int > ( sstats.size () ) ) {
    // -- Valid sensor
    _statics = sstats.get ( index () );
    setKey ( QString::fromStdString ( _statics->key () ) );
    setName ( QString::fromStdString ( _statics->name () ) );
    if ( sev::change ( _userSettable, _statics->user_settable () ) ) {
      emit userSettableChanged ();
    }
  } else {
    // -- Invalid sensor
    _statics.reset ();
    setKey ( QString () );
    setName ( QString () );
    if ( sev::change ( _userSettable, false ) ) {
      emit userSettableChanged ();
    }
  }
  // -- Update validity
  if ( isValid () != was_valid ) {
    emit isValidChanged ();
  }

  // -- Update state statics
  _state.update_statics ( info_n );
}

void
Control::update_properties ()
{
  _state.update_properties ();
}

} // namespace snc::device::qt::controls::i16
