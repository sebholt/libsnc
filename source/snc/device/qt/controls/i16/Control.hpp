/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/qt/Item.hpp>
#include <snc/device/qt/controls/i16/State.hpp>

namespace snc::device::qt::controls::i16
{

/// @brief I16 control info
///
class Control : public Item
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( QObject * state READ state CONSTANT )
  Q_PROPERTY ( bool valid READ isValid NOTIFY isValidChanged () )
  Q_PROPERTY (
      bool userSettable READ userSettable NOTIFY userSettableChanged () )

  public:
  // -- Construction

  Control ( QObject * qparent_n, int index_n );

  ~Control ();

  // -- Valid

  bool
  isValid () const
  {
    return _statics.operator bool ();
  }

  Q_SIGNAL
  void
  isValidChanged ();

  // -- User settable

  bool
  userSettable () const
  {
    return _userSettable;
  }

  Q_SIGNAL
  void
  userSettableChanged ();

  // -- State

  auto *
  state ()
  {
    return &_state;
  }

  const auto *
  state () const
  {
    return &_state;
  }

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  bool _userSettable = false;
  snc::device::statics::control::handle::I16 _statics;
  snc::device::qt::controls::i16::State _state;
};

} // namespace snc::device::qt::controls::i16
