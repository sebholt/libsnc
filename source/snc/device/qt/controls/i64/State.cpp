/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "State.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/state/control/i64.hpp>

namespace snc::device::qt::controls::i64
{

State::State ( QObject * qparent_n, int index_n )
: QObject ( qparent_n )
, _index ( index_n )
{
}

State::~State () = default;

void
State::update_statics ( const snc::device::Info & info_n )
{
  _source = info_n.state ().control_i64_shared ( index () );
}

void
State::update_properties ()
{
  if ( _source ) {
    // -- Valid control
    if ( sev::change ( _value, _source->state () ) ) {
      emit valueChanged ();
    }
  } else {
    // -- Invalid control
    if ( sev::change ( _value, 0 ) ) {
      emit valueChanged ();
    }
  }
}

} // namespace snc::device::qt::controls::i64
