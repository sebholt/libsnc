/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "I1.hpp"
#include <snc/device/qt/ItemModel_T.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemModel_T<
    snc::device::qt::sensors::i1::Sensor >;

namespace snc::device::qt::sensors
{

I1::I1 ( QObject * qparent_n, snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

I1::~I1 () = default;

std::size_t
I1::statics_count () const
{
  return info ().statics ()->sensors_i1 ().size ();
}

} // namespace snc::device::qt::sensors
