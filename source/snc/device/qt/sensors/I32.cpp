/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "I32.hpp"
#include <snc/device/qt/ItemModel_T.ipp>
#include <snc/device/statics/statics.hpp>

// -- Instantiation
template class snc::device::qt::ItemModel_T<
    snc::device::qt::sensors::i32::Sensor >;

namespace snc::device::qt::sensors
{

I32::I32 ( QObject * qparent_n, snc::device::Info & info_n )
: Super ( qparent_n, info_n )
{
}

I32::~I32 () = default;

std::size_t
I32::statics_count () const
{
  return info ().statics ()->sensors_i32 ().size ();
}

} // namespace snc::device::qt::sensors
