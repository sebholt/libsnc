/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Sensor.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/sensor/i1.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::device::qt::sensors::i1
{

Sensor::Sensor ( QObject * qparent_n, int index_n )
: Item ( qparent_n, index_n )
, _state ( this, index_n )
{
}

Sensor::~Sensor () = default;

void
Sensor::update_statics ( const snc::device::Info & info_n )
{
  const bool was_valid = isValid ();
  auto & sstats = info_n.statics ()->sensors_i1 ();
  if ( index () < static_cast< int > ( sstats.size () ) ) {
    // -- Valid sensor
    _statics = sstats.get ( index () );
    setKey ( QString::fromStdString ( _statics->key () ) );
    setName ( QString::fromStdString ( _statics->name () ) );
  } else {
    // -- Invalid sensor
    _statics.reset ();
    setKey ( QString () );
    setName ( QString () );
  }
  // -- Update validity
  if ( isValid () != was_valid ) {
    emit isValidChanged ();
  }

  // -- Update state statics
  _state.update_statics ( info_n );
}

void
Sensor::update_properties ()
{
  _state.update_properties ();
}

} // namespace snc::device::qt::sensors::i1
