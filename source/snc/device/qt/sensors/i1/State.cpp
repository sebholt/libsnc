/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "State.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/state/sensor/i1.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::device::qt::sensors::i1
{

State::State ( QObject * qparent_n, int index_n )
: QObject ( qparent_n )
, _index ( index_n )
{
}

State::~State () = default;

void
State::update_statics ( const snc::device::Info & info_n )
{
  _source = info_n.state ().sensor_i1_shared ( index () );
}

void
State::update_properties ()
{
  if ( _source ) {
    // -- Valid sensor
    if ( sev::change ( _value, _source->state () ) ) {
      emit valueChanged ();
    }
  } else {
    // -- Invalid sensor
    if ( sev::change ( _value, false ) ) {
      emit valueChanged ();
    }
  }
}

} // namespace snc::device::qt::sensors::i1
