/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/qt/Item.hpp>
#include <snc/device/qt/sensors/i16/State.hpp>

namespace snc::device::qt::sensors::i16
{

/// @brief I16 sensor info
///
class Sensor : public Item
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( QObject * state READ state CONSTANT )
  Q_PROPERTY ( bool valid READ isValid NOTIFY isValidChanged () )

  public:
  // -- Construction

  Sensor ( QObject * qparent_n, int index_n );

  ~Sensor ();

  // -- Valid

  bool
  isValid () const
  {
    return _statics.operator bool ();
  }

  Q_SIGNAL
  void
  isValidChanged ();

  // -- State

  auto *
  state ()
  {
    return &_state;
  }

  const auto *
  state () const
  {
    return &_state;
  }

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  snc::device::statics::sensor::handle::I16 _statics;
  snc::device::qt::sensors::i16::State _state;
};

} // namespace snc::device::qt::sensors::i16
