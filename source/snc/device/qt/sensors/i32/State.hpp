/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <QObject>

namespace snc::device::qt::sensors::i32
{

/// @brief I32 sensor state
///
class State : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int index READ index CONSTANT )
  Q_PROPERTY ( quint32 value READ value NOTIFY valueChanged )

  public:
  // -- Construction

  State ( QObject * qparent_n, int index_n );

  ~State ();

  // -- Index

  int
  index ()
  {
    return _index;
  }

  // -- Value

  quint32
  value () const
  {
    return _value;
  }

  Q_SIGNAL
  void
  valueChanged ();

  // -- Interface

  void
  update_statics ( const snc::device::Info & info_n );

  void
  update_properties ();

  private:
  // -- Attributes
  int _index = 0;
  quint32 _value = 0;
  snc::device::state::sensor::handle::I32 _source;
};

} // namespace snc::device::qt::sensors::i32
