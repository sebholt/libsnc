/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i1.hpp"
#include <snc/device/statics/control/i1.hpp>

namespace snc::device::state::control
{

void
I1::reset ( const Statics & statics_n )
{
  Control::reset ();
  _state = statics_n.init_state ();
}

} // namespace snc::device::state::control
