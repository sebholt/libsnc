/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/state/control.hpp>

// -- Forward declaration
namespace snc::device::statics::control
{
class I1;
}

namespace snc::device::state::control
{

class I1 : public Control
{
  public:
  // -- Types

  using Statics = snc::device::statics::control::I1;

  // -- Construction

  I1 () = default;

  void
  reset ( const Statics & statics_n );

  // -- State

  bool
  state () const
  {
    return _state;
  }

  void
  set_state ( bool state_n )
  {
    _state = state_n;
  }

  private:
  // -- Attributes
  bool _state = false;
};

} // namespace snc::device::state::control
