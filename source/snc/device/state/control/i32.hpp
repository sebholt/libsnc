/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/state/control.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::device::statics::control
{
class I32;
}

namespace snc::device::state::control
{

class I32 : public Control
{
  public:
  // -- Types

  using Statics = snc::device::statics::control::I32;

  // -- Construction

  I32 () = default;

  void
  reset ( const Statics & statics_n );

  // -- State

  std::uint32_t
  state () const
  {
    return _state;
  }

  void
  set_state ( std::uint32_t state_n )
  {
    _state = state_n;
  }

  private:
  // -- Attributes
  std::uint32_t _state = 0;
};

} // namespace snc::device::state::control
