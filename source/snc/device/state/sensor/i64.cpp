/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i64.hpp"
#include <snc/device/statics/sensor/i64.hpp>

namespace snc::device::state::sensor
{

void
I64::reset ( const Statics & statics_n )
{
  Sensor::reset ();
  _state = statics_n.init_state ();
}

} // namespace snc::device::state::sensor
