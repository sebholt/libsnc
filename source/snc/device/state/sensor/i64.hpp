/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/state/sensor.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::device::statics::sensor
{
class I64;
}

namespace snc::device::state::sensor
{

class I64 : public Sensor
{
  public:
  // -- Types

  using Statics = snc::device::statics::sensor::I64;

  // -- Construction

  I64 () = default;

  void
  reset ( const Statics & statics_n );

  // -- Sensor state

  std::uint64_t
  state () const
  {
    return _state;
  }

  void
  set_state ( std::uint64_t state_n )
  {
    _state = state_n;
  }

  private:
  // -- Attributes
  std::uint64_t _state = 0;
};

} // namespace snc::device::state::sensor
