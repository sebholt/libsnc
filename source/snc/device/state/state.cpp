/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "state.hpp"
#include <sev/mem/align.hpp>
#include <snc/device/statics/statics.hpp>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace snc::device::state
{

namespace
{

template < typename T >
std::size_t
s_aligned_size ( std::size_t num_n )
{
  return sev::mem::aligned_up ( num_n * sizeof ( T ) );
}

template < typename T >
inline std::size_t
s_aligned_items_size ( const snc::device::statics::Statics & statics_n )
{
  using ViewType = typename std::remove_reference< T >::type;
  using StateType = typename ViewType::value_type;
  using StatsType = typename StateType::Statics;

  std::size_t num_items = statics_n.items_by_type< StatsType > ().size ();
  return s_aligned_size< StateType > ( num_items );
}

template < typename T >
void
s_construct_array ( std::byte *& data_n,
                    const snc::device::statics::Statics & statics_n,
                    sev::mem::View< T > & items_n )
{
  // -- Types
  using StateType = T;
  using StatsType = typename StateType::Statics;

  // -- Type checks
  static_assert ( std::is_trivially_copyable< StateType >::value );
  static_assert ( std::is_trivially_destructible< StateType >::value );

  static_assert ( std::is_trivially_copyable< snc::emb::IO_Stats >::value );
  static_assert ( std::is_trivially_destructible< snc::emb::IO_Stats >::value );

  // -- Construct items
  const auto & stats = statics_n.items_by_type< StatsType > ().items ();
  const std::size_t num_items = stats.size ();
  StateType * state_items = reinterpret_cast< StateType * > ( data_n );
  for ( std::size_t ii = 0; ii != num_items; ++ii ) {
    auto state_item = new ( state_items + ii ) StateType ();
    state_item->reset ( *stats[ ii ] );
  }
  items_n.reset ( state_items, num_items );

  // -- Forward data pointer
  data_n += s_aligned_size< StateType > ( num_items );
}

template < typename T >
void
s_reset_items ( const snc::device::statics::Statics & statics_n,
                sev::mem::View< T > & items_n )
{
  // -- Types
  using StateType = T;
  using StatsType = typename StateType::Statics;

  // -- Reset items
  const auto & stats = statics_n.items_by_type< StatsType > ().items ();
  if ( stats.size () != items_n.size () ) {
    throw std::runtime_error ( "unexpected size missmatch" );
  }
  const std::size_t num_items = stats.size ();
  for ( std::size_t ii = 0; ii != num_items; ++ii ) {
    items_n[ ii ].reset ( *stats[ ii ] );
  }
}

} // namespace

struct State::Shared
{
  snc::device::statics::handle::Statics statics;
  std::unique_ptr< std::byte[] > buffer;
};

State::State ()
{
  reset ( snc::device::statics::handle::Statics () );
}

State::State ( const State & state_n )
: State ( state_n.statics () )
{
  assign ( state_n );
}

State::State ( snc::device::statics::handle::Statics statics_n )
{
  reset ( std::move ( statics_n ) );
}

State::~State () = default;

void
State::assign ( const State & state_n )
{
  if ( ( _statics != state_n._statics ) ||
       ( _data.size () != state_n._data.size () ) ) {
    reset ( state_n._statics );
    DEBUG_ASSERT ( state_n._data.size () == _data.size () );
  }

  // Simple byte copy
  std::copy ( state_n._data.begin (), state_n._data.end (), _data.begin () );
}

void
State::reset ( snc::device::statics::handle::Statics statics_n )
{
  _statics = std::move ( statics_n );
  if ( !_statics ) {
    _statics = snc::device::statics::handle::make ();
  }
  const auto & stats = *_statics;

  // -- Compute total size requirements
  std::size_t aligned_size_total = 0;
  std::apply (
      [ &aligned_size_total, &stats ] ( auto &&... items_n ) {
        aligned_size_total =
            ( s_aligned_items_size< decltype ( items_n ) > ( stats ) + ... );
      },
      _items );
  aligned_size_total += s_aligned_size< snc::emb::IO_Stats > ( 1 );

  // -- Allocate buffer
  _shared = std::make_shared< Shared > ();
  _shared->statics = _statics;
  _shared->buffer = std::make_unique< std::byte[] > ( aligned_size_total );
  _data.reset ( _shared->buffer.get (), aligned_size_total );

  // -- Construct items
  {
    std::byte * data = _data.data ();
    std::apply (
        [ &stats, &data ] ( auto &&... items_n ) {
          ( s_construct_array ( data, stats, items_n ), ... );
        },
        _items );
    _io_stats = new ( data ) snc::emb::IO_Stats ();
    data += s_aligned_size< snc::emb::IO_Stats > ( 1 );
    if ( data != _data.end () ) {
      throw std::runtime_error ( "Bad state memory setup" );
    }
  }
}

void
State::reset ()
{
  std::apply (
      [ &stats = *_statics ] ( auto &&... items_n ) {
        ( s_reset_items ( stats, items_n ), ... );
      },
      _items );
  _io_stats->reset ();
}

bool
State::all_axes_queues_empty () const
{
  for ( auto & axis : axes () ) {
    if ( !axis.steps_queue_empty () ) {
      return false;
    }
  }
  return true;
}

bool
State::all_axes_aligned () const
{
  for ( auto & axis : axes () ) {
    if ( !axis.is_aligned () ) {
      return false;
    }
  }
  return true;
}

void
State::acquire_position_current ( sev::lag::Vector3d & pos_n ) const
{
  for ( std::size_t ii = 0; ii != 3; ++ii ) {
    pos_n[ ii ] = axes ()[ ii ].kinematics_current ().position ();
  }
}

void
State::acquire_position_latest ( sev::lag::Vector3d & pos_n ) const
{
  for ( std::size_t ii = 0; ii != 3; ++ii ) {
    pos_n[ ii ] = axes ()[ ii ].kinematics_latest ().position ();
  }
}

} // namespace snc::device::state
