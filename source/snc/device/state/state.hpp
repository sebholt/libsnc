/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/lag/vector3.hpp>
#include <sev/mem/view.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/state/axis.hpp>
#include <snc/device/state/control/i1.hpp>
#include <snc/device/state/control/i16.hpp>
#include <snc/device/state/control/i32.hpp>
#include <snc/device/state/control/i64.hpp>
#include <snc/device/state/control/i8.hpp>
#include <snc/device/state/sensor/i1.hpp>
#include <snc/device/state/sensor/i16.hpp>
#include <snc/device/state/sensor/i32.hpp>
#include <snc/device/state/sensor/i64.hpp>
#include <snc/device/state/sensor/i8.hpp>
#include <snc/device/state/stepper_controls.hpp>
#include <snc/emb/io_stats.hpp>
#include <cstddef>
#include <memory>
#include <tuple>

namespace snc::device::state
{

/// @brief Device state
///
class State
{
  public:
  // -- Types

  using Items_Tuple = std::tuple< sev::mem::View< sensor::I1 >,
                                  sev::mem::View< sensor::I8 >,
                                  sev::mem::View< sensor::I16 >,
                                  sev::mem::View< sensor::I32 >,
                                  sev::mem::View< sensor::I64 >,
                                  sev::mem::View< control::I1 >,
                                  sev::mem::View< control::I8 >,
                                  sev::mem::View< control::I16 >,
                                  sev::mem::View< control::I32 >,
                                  sev::mem::View< control::I64 >,
                                  sev::mem::View< Axis >,
                                  sev::mem::View< Stepper_Controls > >;

  // -- Construction

  State ();

  State ( const State & state_n );

  State ( snc::device::statics::handle::Statics statics_n );

  ~State ();

  // -- Assigment

  void
  assign ( const State & state_n );

  State &
  operator= ( const State & state_n )
  {
    assign ( state_n );
    return *this;
  }

  // -- Setup

  /// @brief Set statics and reset state to the statics initial state
  void
  reset ( snc::device::statics::handle::Statics statics_n );

  /// @brief Reset state to the statics initial state
  void
  reset ();

  // -- Statics

  const snc::device::statics::handle::Statics &
  statics () const
  {
    return _statics;
  }

  // -- Items tuple

  const Items_Tuple &
  items () const
  {
    return _items;
  }

  Items_Tuple &
  items ()
  {
    return _items;
  }

  template < typename T >
  const auto &
  items_by_type () const
  {
    return reinterpret_cast< const sev::mem::View< const T > & > (
        std::get< sev::mem::View< T > > ( _items ) );
  }

  template < typename T >
  auto &
  items_by_type ()
  {
    return std::get< sev::mem::View< T > > ( _items );
  }

  template < typename T >
  std::shared_ptr< const T >
  item_shared ( std::size_t index_n ) const
  {
    const auto & items = items_by_type< T > ();
    if ( index_n < items.size () ) {
      return std::shared_ptr< const T > ( _shared, &items[ index_n ] );
    }
    return {};
  }

  template < typename T >
  std::shared_ptr< T >
  item_shared_writeable ( std::size_t index_n )
  {
    const auto & items = items_by_type< T > ();
    if ( index_n < items.size () ) {
      return std::shared_ptr< T > ( _shared, &items[ index_n ] );
    }
    return {};
  }

  // -- Sensors: I1

  const sev::mem::View< const sensor::I1 > &
  sensors_i1 () const
  {
    return items_by_type< sensor::I1 > ();
  }

  const sev::mem::View< sensor::I1 > &
  sensors_i1_writeable ()
  {
    return items_by_type< sensor::I1 > ();
  }

  sensor::handle::I1
  sensor_i1_shared ( std::size_t index_n ) const
  {
    return item_shared< sensor::I1 > ( index_n );
  }

  sensor::handle::I1_Writeable
  sensor_i1_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< sensor::I1 > ( index_n );
  }

  // -- Sensors: I8

  const sev::mem::View< const sensor::I8 > &
  sensors_i8 () const
  {
    return items_by_type< sensor::I8 > ();
  }

  const sev::mem::View< sensor::I8 > &
  sensors_i8_writeable ()
  {
    return items_by_type< sensor::I8 > ();
  }

  sensor::handle::I8
  sensor_i8_shared ( std::size_t index_n ) const
  {
    return item_shared< sensor::I8 > ( index_n );
  }

  sensor::handle::I8_Writeable
  sensor_i8_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< sensor::I8 > ( index_n );
  }

  // -- Sensors: I16

  const sev::mem::View< const sensor::I16 > &
  sensors_i16 () const
  {
    return items_by_type< sensor::I16 > ();
  }

  const sev::mem::View< sensor::I16 > &
  sensors_i16_writeable ()
  {
    return items_by_type< sensor::I16 > ();
  }

  sensor::handle::I16
  sensor_i16_shared ( std::size_t index_n ) const
  {
    return item_shared< sensor::I16 > ( index_n );
  }

  sensor::handle::I16_Writeable
  sensor_i16_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< sensor::I16 > ( index_n );
  }

  // -- Sensors: I32

  const sev::mem::View< const sensor::I32 > &
  sensors_i32 () const
  {
    return items_by_type< sensor::I32 > ();
  }

  const sev::mem::View< sensor::I32 > &
  sensors_i32_writeable ()
  {
    return items_by_type< sensor::I32 > ();
  }

  sensor::handle::I32
  sensor_i32_shared ( std::size_t index_n ) const
  {
    return item_shared< sensor::I32 > ( index_n );
  }

  sensor::handle::I32_Writeable
  sensor_i32_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< sensor::I32 > ( index_n );
  }

  // -- Sensors: I64

  const sev::mem::View< const sensor::I64 > &
  sensors_i64 () const
  {
    return items_by_type< sensor::I64 > ();
  }

  const sev::mem::View< sensor::I64 > &
  sensors_i64_writeable ()
  {
    return items_by_type< sensor::I64 > ();
  }

  sensor::handle::I64
  sensor_i64_shared ( std::size_t index_n ) const
  {
    return item_shared< sensor::I64 > ( index_n );
  }

  sensor::handle::I64_Writeable
  sensor_i64_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< sensor::I64 > ( index_n );
  }

  // -- Controls: I1

  const sev::mem::View< const control::I1 > &
  controls_i1 () const
  {
    return items_by_type< control::I1 > ();
  }

  const sev::mem::View< control::I1 > &
  controls_i1_writeable ()
  {
    return items_by_type< control::I1 > ();
  }

  control::handle::I1
  control_i1_shared ( std::size_t index_n ) const
  {
    return item_shared< control::I1 > ( index_n );
  }

  control::handle::I1_Writeable
  control_i1_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< control::I1 > ( index_n );
  }

  // -- Controls: I8

  const sev::mem::View< const control::I8 > &
  controls_i8 () const
  {
    return items_by_type< control::I8 > ();
  }

  const sev::mem::View< control::I8 > &
  controls_i8_writeable ()
  {
    return items_by_type< control::I8 > ();
  }

  control::handle::I8
  control_i8_shared ( std::size_t index_n ) const
  {
    return item_shared< control::I8 > ( index_n );
  }

  control::handle::I8_Writeable
  control_i8_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< control::I8 > ( index_n );
  }

  // -- Controls: I16

  const sev::mem::View< const control::I16 > &
  controls_i16 () const
  {
    return items_by_type< control::I16 > ();
  }

  const sev::mem::View< control::I16 > &
  controls_i16_writeable ()
  {
    return items_by_type< control::I16 > ();
  }

  control::handle::I16
  control_i16_shared ( std::size_t index_n ) const
  {
    return item_shared< control::I16 > ( index_n );
  }

  control::handle::I16_Writeable
  control_i16_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< control::I16 > ( index_n );
  }

  // -- Controls: I32

  const sev::mem::View< const control::I32 > &
  controls_i32 () const
  {
    return items_by_type< control::I32 > ();
  }

  const sev::mem::View< control::I32 > &
  controls_i32_writeable ()
  {
    return items_by_type< control::I32 > ();
  }

  control::handle::I32
  control_i32_shared ( std::size_t index_n ) const
  {
    return item_shared< control::I32 > ( index_n );
  }

  control::handle::I32_Writeable
  control_i32_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< control::I32 > ( index_n );
  }

  // -- Controls: I64

  const sev::mem::View< const control::I64 > &
  controls_i64 () const
  {
    return items_by_type< control::I64 > ();
  }

  const sev::mem::View< control::I64 > &
  controls_i64_writeable ()
  {
    return items_by_type< control::I64 > ();
  }

  control::handle::I64
  control_i64_shared ( std::size_t index_n ) const
  {
    return item_shared< control::I64 > ( index_n );
  }

  control::handle::I64_Writeable
  control_i64_writeable_shared ( std::size_t index_n )
  {
    return item_shared_writeable< control::I64 > ( index_n );
  }

  // -- Axes

  const sev::mem::View< Axis > &
  axes ()
  {
    return items_by_type< Axis > ();
  }

  const sev::mem::View< const Axis > &
  axes () const
  {
    return items_by_type< Axis > ();
  }

  handle::Axis
  axis_shared ( std::size_t index_n ) const
  {
    return item_shared< Axis > ( index_n );
  }

  handle::Axis_Writeable
  axis_shared_writeable ( std::size_t index_n )
  {
    return item_shared_writeable< Axis > ( index_n );
  }

  // -- Controls stepper

  const sev::mem::View< Stepper_Controls > &
  steppers_controls ()
  {
    return items_by_type< Stepper_Controls > ();
  }

  const sev::mem::View< const Stepper_Controls > &
  steppers_controls () const
  {
    return items_by_type< Stepper_Controls > ();
  }

  handle::Stepper_Controls
  stepper_controls_shared ( std::size_t index_n ) const
  {
    return item_shared< Stepper_Controls > ( index_n );
  }

  handle::Stepper_Controls_Writeable
  stepper_controls_shared_writeable ( std::size_t index_n )
  {
    return item_shared_writeable< Stepper_Controls > ( index_n );
  }

  // -- Axes derived values

  bool
  all_axes_queues_empty () const;

  bool
  all_axes_aligned () const;

  /// TODO: Remove function
  void
  acquire_position_current ( sev::lag::Vector3d & pos_n ) const;

  /// TODO: Remove function
  void
  acquire_position_latest ( sev::lag::Vector3d & pos_n ) const;

  // -- IO statistics

  snc::emb::IO_Stats &
  io_stats ()
  {
    return *_io_stats;
  }

  const snc::emb::IO_Stats &
  io_stats () const
  {
    return *_io_stats;
  }

  private:
  // -- Attributes
  snc::device::statics::handle::Statics _statics;
  sev::mem::View< std::byte > _data;
  // -- Data views
  Items_Tuple _items;
  snc::emb::IO_Stats * _io_stats = nullptr;

  // -- Data buffer
  struct Shared;
  std::shared_ptr< Shared > _shared;
};

} // namespace snc::device::state
