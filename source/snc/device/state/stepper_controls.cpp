/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_controls.hpp"

namespace snc::device::state
{

Stepper_Controls::Stepper_Controls () = default;

void
Stepper_Controls::reset ( const Statics & stats_n [[maybe_unused]] )
{
  _is_enabled = false;
  _steps_queue_length = 0;
  _steps_queue_ticks = 0;
}

} // namespace snc::device::state
