/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

// -- Forward declaration
namespace snc::device::statics
{
class Stepper_Controls;
}

namespace snc::device::state
{

class Stepper_Controls
{
  public:
  // -- Types

  using Statics = snc::device::statics::Stepper_Controls;

  // -- Construction

  Stepper_Controls ();

  // -- Setup

  /// @brief Reset state
  void
  reset ( const Statics & stats_n );

  // -- Enabled state

  bool
  is_enabled () const
  {
    return _is_enabled;
  }

  void
  set_is_enabled ( bool flag_n )
  {
    _is_enabled = flag_n;
  }

  // -- Steps queue length

  std::uint_fast32_t
  steps_queue_length () const
  {
    return _steps_queue_length;
  }

  void
  set_steps_queue_length ( std::uint_fast32_t value_n )
  {
    _steps_queue_length = value_n;
  }

  bool
  steps_queue_empty () const
  {
    return ( steps_queue_length () == 0 );
  }

  // -- Steps queue ticks

  std::uint_fast32_t
  steps_queue_ticks () const
  {
    return _steps_queue_ticks;
  }

  void
  set_steps_queue_ticks ( std::uint_fast32_t value_n )
  {
    _steps_queue_ticks = value_n;
  }

  private:
  // -- Attributes
  bool _is_enabled = false;

  // -- Step queue state
  std::uint_fast32_t _steps_queue_length = 0;
  std::uint_fast32_t _steps_queue_ticks = 0;
};

} // namespace snc::device::state
