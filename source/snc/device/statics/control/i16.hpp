/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/item.hpp>
#include <cstdint>

namespace snc::device::statics::control
{

/// @brief 16 bit control
///
class I16 : public Item
{
  public:
  // -- Types

  class Emb
  {
    public:
    // -- Switch index

    std::uint_fast32_t
    control_index () const
    {
      return _control_index;
    }

    void
    set_control_index ( std::uint_fast32_t index_n )
    {
      _control_index = index_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _control_index = Item::invalid_index_fast32;
  };

  class Est
  {
    public:
    // -- Switch index

    std::uint_fast32_t
    control_index () const
    {
      return _control_index;
    }

    void
    set_control_index ( std::uint_fast32_t index_n )
    {
      _control_index = index_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _control_index = Item::invalid_index_fast32;
  };

  // -- Construction

  I16 ( std::uint_fast32_t index_n = 0 );

  ~I16 ();

  // -- Init state

  std::uint16_t
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( std::uint16_t state_n )
  {
    _init_state = state_n;
  }

  // -- Init value

  std::uint16_t
  init_value () const
  {
    return _init_value;
  }

  void
  set_init_value ( std::uint16_t state_n )
  {
    _init_value = state_n;
  }

  // -- User settable

  bool
  user_settable () const
  {
    return _user_settable;
  }

  void
  set_user_settable ( bool flag_n )
  {
    _user_settable = flag_n;
  }

  // -- Embedded device control

  Emb &
  emb ()
  {
    return _emb;
  }

  const Emb &
  emb () const
  {
    return _emb;
  }

  // -- EStepper device control

  Est &
  est ()
  {
    return _est;
  }

  const Est &
  est () const
  {
    return _est;
  }

  private:
  // -- Attributes
  std::uint16_t _init_state = 0;
  std::uint16_t _init_value = 0;
  bool _user_settable = false;
  Emb _emb;
  Est _est;
};

} // namespace snc::device::statics::control
