/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "i32.hpp"

namespace snc::device::statics::control
{

I32::I32 ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

I32::~I32 () = default;

} // namespace snc::device::statics::control
