/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "item.hpp"

namespace snc::device::statics
{

Item::Item ( std::uint_fast32_t index_n )
: _index ( index_n )
{
}

Item::~Item () = default;

} // namespace snc::device::statics
