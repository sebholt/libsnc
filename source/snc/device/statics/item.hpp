/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>
#include <limits>
#include <string>
#include <string_view>

namespace snc::device::statics
{

/// @brief Abstract item base class
///
class Item
{
  public:
  // -- Statics

  static constexpr std::uint_fast32_t invalid_index_fast32 =
      std::numeric_limits< std::uint_fast32_t >::max ();

  // -- Construction

  Item ( std::uint_fast32_t index_n = 0 );

  ~Item ();

  // -- Index

  std::uint_fast32_t
  index () const
  {
    return _index;
  }

  void
  set_index ( std::uint_fast32_t index_n )
  {
    _index = index_n;
  }

  // -- Key string

  const std::string &
  key () const
  {
    return _key;
  }

  void
  set_key ( std::string_view key_n )
  {
    _key = key_n;
  }

  // -- Name

  const std::string &
  name () const
  {
    return _name;
  }

  void
  set_name ( std::string_view name_n )
  {
    _name = name_n;
  }

  private:
  // -- Attributes
  std::uint_fast32_t _index = invalid_index_fast32;
  std::string _key;
  std::string _name;
};

} // namespace snc::device::statics
