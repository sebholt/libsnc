/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/emb/config/config.hpp>
#include <string>
#include <string_view>
#include <vector>

namespace snc::device::statics::json
{

class Reader
{
  public:
  // -- Construction

  Reader ();

  ~Reader ();

  // -- Setup

  void
  reset ();

  // -- Statics

  const snc::device::statics::handle::Statics_Writeable &
  statics_writeable () const
  {
    return _statics;
  }

  snc::device::statics::handle::Statics
  statics () const
  {
    return _statics;
  }

  // -- Error state

  bool
  good () const
  {
    return _errors.empty ();
  }

  const std::vector< std::string > &
  errors () const
  {
    return _errors;
  }

  std::string
  error_string () const;

  // -- Parsing

  bool
  parse ( std::string_view json_utf8_n );

  private:
  // -- Attributes
  snc::device::statics::handle::Statics_Writeable _statics;
  std::vector< std::string > _errors;
};

} // namespace snc::device::statics::json
