/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/statics/item.hpp>
#include <cstdint>

namespace snc::device::statics::sensor
{

/// @brief 8 bit sensor
///
class I8 : public Item
{
  public:
  // -- Types

  class Emb
  {
    public:
    // -- Sensor index

    std::uint_fast32_t
    sensor_index () const
    {
      return _sensor_index;
    }

    void
    set_sensor_index ( std::uint_fast32_t index_n )
    {
      _sensor_index = index_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _sensor_index = Item::invalid_index_fast32;
  };

  class Est
  {
    public:
    // -- Sensor index

    std::uint_fast32_t
    sensor_index () const
    {
      return _sensor_index;
    }

    void
    set_sensor_index ( std::uint_fast32_t index_n )
    {
      _sensor_index = index_n;
    }

    private:
    // -- Attributes
    std::uint_fast32_t _sensor_index = Item::invalid_index_fast32;
  };

  // -- Construction and setup

  I8 ( std::uint_fast32_t index_n = 0 );

  ~I8 ();

  // -- Init state

  std::uint8_t
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( std::uint8_t state_n )
  {
    _init_state = state_n;
  }

  // -- Embedded device sensor

  Emb &
  emb ()
  {
    return _emb;
  }

  const Emb &
  emb () const
  {
    return _emb;
  }

  // -- EStepper device sensor

  Est &
  est ()
  {
    return _est;
  }

  const Est &
  est () const
  {
    return _est;
  }

  private:
  // -- Attributes
  std::uint8_t _init_state = 0;
  Emb _emb;
  Est _est;
};

} // namespace snc::device::statics::sensor
