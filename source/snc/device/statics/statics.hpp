/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/statics/items.hpp>
#include <memory>
#include <tuple>

// -- Forward declaration
namespace snc::usb
{
class Device_Query;
}
namespace snc::emb::config
{
class Config;
}
namespace snc::est::config
{
class Config;
}

namespace snc::device::statics
{

class Statics
{
  public:
  // -- Types

  using Items_Tuple = std::tuple< Items< sensor::I1 >,
                                  Items< sensor::I8 >,
                                  Items< sensor::I16 >,
                                  Items< sensor::I32 >,
                                  Items< sensor::I64 >,
                                  Items< control::I1 >,
                                  Items< control::I8 >,
                                  Items< control::I16 >,
                                  Items< control::I32 >,
                                  Items< control::I64 >,
                                  Items< Axis >,
                                  Items< Stepper_Controls > >;

  // -- Construction

  Statics ();

  Statics ( const Statics & statics_n ) = delete;

  ~Statics ();

  // -- Assignment operators

  Statics &
  operator= ( const Statics & statics_n ) = delete;

  // -- EST switch

  bool
  est () const
  {
    return _est;
  }

  void
  set_est ( bool value_n )
  {
    _est = value_n;
  }

  // -- Items tuple

  const Items_Tuple &
  items () const
  {
    return _items;
  }

  Items_Tuple &
  items ()
  {
    return _items;
  }

  template < typename T >
  const auto &
  items_by_type () const
  {
    return std::get< Items< T > > ( _items );
  }

  template < typename T >
  auto &
  items_by_type ()
  {
    return std::get< Items< T > > ( _items );
  }

  // -- Sensors

  const auto &
  sensors_i1 () const
  {
    return items_by_type< sensor::I1 > ();
  }

  auto &
  sensors_i1 ()
  {
    return items_by_type< sensor::I1 > ();
  }

  const auto &
  sensors_i8 () const
  {
    return items_by_type< sensor::I8 > ();
  }

  auto &
  sensors_i8 ()
  {
    return items_by_type< sensor::I8 > ();
  }

  const auto &
  sensors_i16 () const
  {
    return items_by_type< sensor::I16 > ();
  }

  auto &
  sensors_i16 ()
  {
    return items_by_type< sensor::I16 > ();
  }

  const auto &
  sensors_i32 () const
  {
    return items_by_type< sensor::I32 > ();
  }

  auto &
  sensors_i32 ()
  {
    return items_by_type< sensor::I32 > ();
  }

  const auto &
  sensors_i64 () const
  {
    return items_by_type< sensor::I64 > ();
  }

  auto &
  sensors_i64 ()
  {
    return items_by_type< sensor::I64 > ();
  }

  // -- Controls

  const auto &
  controls_i1 () const
  {
    return items_by_type< control::I1 > ();
  }

  auto &
  controls_i1 ()
  {
    return items_by_type< control::I1 > ();
  }

  const auto &
  controls_i8 () const
  {
    return items_by_type< control::I8 > ();
  }

  auto &
  controls_i8 ()
  {
    return items_by_type< control::I8 > ();
  }

  const auto &
  controls_i16 () const
  {
    return items_by_type< control::I16 > ();
  }

  auto &
  controls_i16 ()
  {
    return items_by_type< control::I16 > ();
  }

  const auto &
  controls_i32 () const
  {
    return items_by_type< control::I32 > ();
  }

  auto &
  controls_i32 ()
  {
    return items_by_type< control::I32 > ();
  }

  const auto &
  controls_i64 () const
  {
    return items_by_type< control::I64 > ();
  }

  auto &
  controls_i64 ()
  {
    return items_by_type< control::I64 > ();
  }

  // -- Axes

  const auto &
  axes () const
  {
    return items_by_type< Axis > ();
  }

  auto &
  axes ()
  {
    return items_by_type< Axis > ();
  }

  // -- Control steppers

  const auto &
  steppers_controls () const
  {
    return items_by_type< Stepper_Controls > ();
  }

  auto &
  steppers_controls ()
  {
    return items_by_type< Stepper_Controls > ();
  }

  // -- Speed limit: Total speed

  double
  speed_max () const
  {
    return _speed_max;
  }

  void
  set_speed_max ( double value_n )
  {
    _speed_max = value_n;
  }

  // -- Speed limit: Surface planar

  double
  speed_planar_max () const
  {
    return _speed_planar_max;
  }

  void
  set_speed_planar_max ( double value_n )
  {
    _speed_planar_max = value_n;
  }

  // -- Speed limits: Surface normal

  double
  speed_normal_max () const
  {
    return _speed_normal_max;
  }

  void
  set_speed_normal_max ( double value_n )
  {
    _speed_normal_max = value_n;
  }

  // -- USB device query

  const std::shared_ptr< const snc::usb::Device_Query > &
  usb_device_query () const
  {
    return _usb_device_query;
  }

  void
  set_usb_device_query ( std::shared_ptr< const snc::usb::Device_Query > ref_n )
  {
    _usb_device_query = std::move ( ref_n );
  }

  // -- EMB device config

  const std::shared_ptr< const snc::emb::config::Config > &
  emb_device_config () const
  {
    return _emb_device_config;
  }

  void
  set_emb_device_config (
      std::shared_ptr< const snc::emb::config::Config > ref_n )
  {
    _emb_device_config = std::move ( ref_n );
  }

  // -- EST device config

  const std::shared_ptr< const snc::est::config::Config > &
  est_device_config () const
  {
    return _est_device_config;
  }

  void
  set_est_device_config (
      std::shared_ptr< const snc::est::config::Config > ref_n )
  {
    _est_device_config = std::move ( ref_n );
  }

  // -- Derived values

  std::uint_fast32_t
  acquire_step_slice_usecs_min () const;

  std::uint_fast32_t
  acquire_step_slice_usecs_max () const;

  private:
  // -- Attributes
  bool _est = false;
  // - Items
  Items_Tuple _items;
  // - Speeds
  double _speed_max = 0.0;
  double _speed_planar_max = 0.0;
  double _speed_normal_max = 0.0;
  // - External
  std::shared_ptr< const snc::usb::Device_Query > _usb_device_query;
  std::shared_ptr< const snc::emb::config::Config > _emb_device_config;
  std::shared_ptr< const snc::est::config::Config > _est_device_config;
};

} // namespace snc::device::statics
