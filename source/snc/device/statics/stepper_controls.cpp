/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_controls.hpp"

namespace snc::device::statics
{

Stepper_Controls::Stepper_Controls ( std::uint_fast32_t index_n )
: Item ( index_n )
{
}

Stepper_Controls::~Stepper_Controls () = default;

} // namespace snc::device::statics
