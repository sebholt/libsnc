/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/statics/item.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::est::config
{
class Stepper;
}

namespace snc::device::statics
{

namespace stepper_controls
{

/// @brief Axis estepper device settings
///
class Est
{
  public:
  // -- Types

  using Est_Config_Handle = std::shared_ptr< const snc::est::config::Stepper >;

  // -- Embedded axis index

  std::uint_fast32_t
  stepper_index () const
  {
    return _stepper_index;
  }

  void
  set_stepper_index ( std::uint_fast32_t index_n )
  {
    _stepper_index = index_n;
  }

  // -- Emb stepper config

  const Est_Config_Handle &
  stepper_config () const
  {
    return _stepper_config;
  }

  void
  set_stepper_config ( Est_Config_Handle stepper_n )
  {
    _stepper_config = std::move ( stepper_n );
  }

  private:
  // -- Attributes
  std::uint_fast32_t _stepper_index = Item::invalid_index_fast32;
  Est_Config_Handle _stepper_config;
};

} // namespace stepper_controls

/// @brief Axis static settings
///
class Stepper_Controls : public Item
{
  public:
  // -- Construction

  Stepper_Controls ( std::uint_fast32_t index_n = 0 );

  ~Stepper_Controls ();

  // -- Estepper device

  stepper_controls::Est &
  est ()
  {
    return _est;
  }

  const stepper_controls::Est &
  est () const
  {
    return _est;
  }

  private:
  // -- Attributes
  stepper_controls::Est _est;
};

} // namespace snc::device::statics
