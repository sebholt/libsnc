/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "config.hpp"

namespace snc::emb::config
{

Config::Config () {}

void
Config::reset ()
{
  _steppers.clear ();
  _sensors_bool.clear ();
  _controls_bool.clear ();
  _outputs.clear ();
  _freq_gens.clear ();
}

Output &
Config::output_get_or_create ( std::uint8_t index_n )
{
  if ( _outputs.size () <= index_n ) {
    _outputs.reserve ( index_n + 1 );
    while ( _outputs.size () <= index_n ) {
      _outputs.emplace_back ( _outputs.size () );
    }
  }
  return _outputs[ index_n ];
}

} // namespace snc::emb::config
