/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/view.hpp>
#include <snc/emb/config/controls.hpp>
#include <snc/emb/config/freq_gen.hpp>
#include <snc/emb/config/output.hpp>
#include <snc/emb/config/sensors.hpp>
#include <snc/emb/config/stepper.hpp>
#include <cstdint>
#include <vector>

namespace snc::emb::config
{

class Config
{
  public:
  // -- Construction

  Config ();

  void
  reset ();

  // -- Steppers

  std::size_t
  num_steppers () const
  {
    return _steppers.size ();
  }

  std::vector< Stepper > &
  steppers ()
  {
    return _steppers;
  }

  const std::vector< Stepper > &
  steppers () const
  {
    return _steppers;
  }

  // -- Sensors:Bbool

  std::size_t
  num_sensors_bool () const
  {
    return _sensors_bool.size ();
  }

  std::vector< Sensor_Bool > &
  sensors_bool ()
  {
    return _sensors_bool;
  }

  const std::vector< Sensor_Bool > &
  sensors_bool () const
  {
    return _sensors_bool;
  }

  // -- Controls: Bool

  std::size_t
  num_controls_bool () const
  {
    return _controls_bool.size ();
  }

  std::vector< Control_Bool > &
  controls_bool ()
  {
    return _controls_bool;
  }

  const std::vector< Control_Bool > &
  controls_bool () const
  {
    return _controls_bool;
  }

  // -- Outputs

  std::size_t
  num_outputs () const
  {
    return _outputs.size ();
  }

  std::vector< Output > &
  outputs ()
  {
    return _outputs;
  }

  const std::vector< Output > &
  outputs () const
  {
    return _outputs;
  }

  Output &
  output_get_or_create ( std::uint8_t index_n );

  // -- Frequency generators

  std::size_t
  num_freq_gens () const
  {
    return _freq_gens.size ();
  }

  std::vector< Freq_Gen > &
  freq_gens ()
  {
    return _freq_gens;
  }

  const std::vector< Freq_Gen > &
  freq_gens () const
  {
    return _freq_gens;
  }

  private:
  // -- Attributes
  std::vector< Stepper > _steppers;
  std::vector< Sensor_Bool > _sensors_bool;
  std::vector< Control_Bool > _controls_bool;
  std::vector< Output > _outputs;
  std::vector< Freq_Gen > _freq_gens;
};

} // namespace snc::emb::config
