/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::config
{

class Freq_Gen
{
  public:
  // -- Construction

  Freq_Gen () = default;

  void
  reset ();

  // -- Interface

  bool
  in_use () const
  {
    return ( pulse_usecs_total () != 0 );
  }

  // -- Generator index

  std::uint8_t
  generator_index () const
  {
    return _generator_index;
  }

  void
  set_generator_index ( std::uint8_t index_n )
  {
    _generator_index = index_n;
  }

  // -- Pulse timings

  std::uint32_t
  pulse_usecs_total () const
  {
    return _pulse_usecs_total;
  }

  void
  set_pulse_usecs_total ( std::uint32_t value_n )
  {
    _pulse_usecs_total = value_n;
  }

  std::uint32_t
  pulse_usecs_high () const
  {
    return _pulse_usecs_high;
  }

  void
  set_pulse_usecs_high ( std::uint32_t value_n )
  {
    _pulse_usecs_high = value_n;
  }

  private:
  // -- Attributes
  std::uint8_t _generator_index = 0;
  std::uint32_t _pulse_usecs_total = 0;
  std::uint32_t _pulse_usecs_high = 0;
};

} // namespace snc::emb::config
