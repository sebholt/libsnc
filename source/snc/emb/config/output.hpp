/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/type/output.hpp>
#include <cstdint>

namespace snc::emb::config
{

class Output
{
  public:
  // -- Types

  static constexpr std::uint_fast32_t invalid_index_8 = 0xff;

  // -- Construction

  Output ( std::uint8_t index_n )
  : _index ( index_n )
  {
  }

  // -- Output index

  std::uint8_t
  index () const
  {
    return _index;
  }

  void
  set_index ( std::uint8_t index_n )
  {
    _index = index_n;
  }

  // -- Output type

  snc::emb::type::Output
  type () const
  {
    return _type;
  }

  void
  set_type ( snc::emb::type::Output type_n )
  {
    _type = type_n;
  }

  // -- Frequency generator

  std::uint8_t
  freq_gen_index () const
  {
    return _freq_gen_index;
  }

  void
  set_freq_gen_index ( std::uint8_t index_n )
  {
    _freq_gen_index = index_n;
  }

  // -- Stepper or control index

  std::uint8_t
  control_index () const
  {
    return _stepper_index;
  }

  void
  set_control_index ( std::uint8_t index_n )
  {
    _stepper_index = index_n;
  }

  std::uint8_t
  stepper_index () const
  {
    return _stepper_index;
  }

  void
  set_stepper_index ( std::uint8_t index_n )
  {
    _stepper_index = index_n;
  }

  private:
  // -- Attributes
  std::uint8_t _index = invalid_index_8;
  snc::emb::type::Output _type = snc::emb::type::Output::UNUSED;
  std::uint8_t _freq_gen_index = invalid_index_8;
  std::uint8_t _stepper_index = invalid_index_8;
};

} // namespace snc::emb::config
