/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::config
{

class Stepper
{
  public:
  // -- Statics

  static constexpr std::uint_fast32_t steps_queue_capacity_default = 1024;

  static constexpr std::uint_fast32_t step_slice_usecs_min_default = 30;
  static constexpr std::uint_fast32_t step_slice_usecs_max_default = 800;
  static constexpr std::uint_fast32_t steps_queue_hint_usecs_min_default =
      ( 30 * 1000 );
  static constexpr std::uint_fast32_t steps_queue_hint_usecs_max_default =
      ( 50 * 1000 );

  // -- Setup

  void
  reset ()
  {
    *this = Stepper ();
  }

  // -- Steps queue capacity

  std::uint_fast32_t
  steps_queue_capacity () const
  {
    return _steps_queue_capacity;
  }

  void
  set_steps_queue_capacity ( std::uint_fast32_t value_n )
  {
    _steps_queue_capacity = value_n;
  }

  // -- Step slice minimum microseconds

  /// @brief Don't use step slices with a longer duration than this
  ///
  /// A step slice is one section of a step that consists of one
  /// directional step slice (FORWARD or BACKWARD or HOLD) followed by a
  /// number of HOLD slices to increase the total step duration
  std::uint_fast32_t
  step_slice_usecs_min () const
  {
    return _step_slice_usecs_min;
  }

  void
  set_step_slice_usecs_min ( std::uint_fast32_t value_n )
  {
    _step_slice_usecs_min = value_n;
  }

  // -- Step slice maximum microseconds

  std::uint_fast32_t
  step_slice_usecs_max () const
  {
    return _step_slice_usecs_max;
  }

  void
  set_step_slice_usecs_max ( std::uint_fast32_t value_n )
  {
    _step_slice_usecs_max = value_n;
  }

  // -- Step slice minimum microseconds hint

  /// @brief Don't let the device steps length fall below this
  std::uint_fast32_t
  steps_queue_hint_usecs_min () const
  {
    return _steps_queue_hint_usecs_min;
  }

  void
  set_steps_queue_hint_usecs_min ( std::uint_fast32_t value_n )
  {
    _steps_queue_hint_usecs_min = value_n;
  }

  // -- Step slice maximum microseconds hint

  /// @brief Don't fill the device steps queue with more usecs than this
  std::uint_fast32_t
  steps_queue_hint_usecs_max () const
  {
    return _steps_queue_hint_usecs_max;
  }

  void
  set_steps_queue_hint_usecs_max ( std::uint_fast32_t value_n )
  {
    _steps_queue_hint_usecs_max = value_n;
  }

  private:
  // -- Attributes
  std::uint_fast32_t _steps_queue_capacity = steps_queue_capacity_default;

  std::uint_fast32_t _step_slice_usecs_min = step_slice_usecs_min_default;
  std::uint_fast32_t _step_slice_usecs_max = step_slice_usecs_max_default;

  std::uint_fast32_t _steps_queue_hint_usecs_min =
      steps_queue_hint_usecs_min_default;
  std::uint_fast32_t _steps_queue_hint_usecs_max =
      steps_queue_hint_usecs_max_default;
};

} // namespace snc::emb::config
