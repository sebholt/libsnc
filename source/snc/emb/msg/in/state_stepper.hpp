/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "state.hpp"

namespace snc::emb::msg::in
{

class State_Stepper : public State
{
  public:
  State_Stepper ()
  : State ( Type::STATE_STEPPER )
  {
  }

  void
  reset ()
  {
    State::reset ();
    _stepper_index = 0;
    _steps_queue_length = 0;
  }

  std::uint8_t
  stepper_index () const
  {
    return _stepper_index;
  }

  void
  set_stepper_index ( std::uint8_t index_n )
  {
    _stepper_index = index_n;
  }

  std::uint16_t
  steps_queue_length () const
  {
    return _steps_queue_length;
  }

  void
  set_steps_queue_length ( std::uint16_t length_n )
  {
    _steps_queue_length = length_n;
  }

  private:
  // -- Attributes
  std::uint8_t _stepper_index = 0;
  std::uint16_t _steps_queue_length = 0;
};

} // namespace snc::emb::msg::in
