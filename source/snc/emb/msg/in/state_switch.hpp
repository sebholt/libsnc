/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "state.hpp"

namespace snc::emb::msg::in
{

class State_Switch : public State
{
  public:
  State_Switch ()
  : State ( Type::STATE_SWITCH )
  {
  }

  void
  reset ()
  {
    State::reset ();
    _switch_index = 0;
    _switch_state = false;
  }

  std::uint8_t
  switch_index () const
  {
    return _switch_index;
  }

  void
  set_switch_index ( std::uint8_t index_n )
  {
    _switch_index = index_n;
  }

  bool
  switch_state () const
  {
    return _switch_state;
  }

  void
  set_switch_state ( bool flag_n )
  {
    _switch_state = flag_n;
  }

  private:
  // -- Attributes
  std::uint8_t _switch_index = 0;
  bool _switch_state = false;
};

} // namespace snc::emb::msg::in
