/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/fader_16.hpp>
#include <snc/emb/msg/out/fader_32.hpp>
#include <snc/emb/msg/out/fader_8.hpp>
#include <snc/emb/msg/out/generator_frequency.hpp>
#include <snc/emb/msg/out/heartbeat.hpp>
#include <snc/emb/msg/out/output_config.hpp>
#include <snc/emb/msg/out/stepper_sync.hpp>
#include <snc/emb/msg/out/stepper_sync_config.hpp>
#include <snc/emb/msg/out/steps_u16_64.hpp>
#include <snc/emb/msg/out/switch.hpp>
