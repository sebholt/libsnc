/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>

namespace snc::emb::msg::out
{

class Generator_Frequency : public Message
{
  public:
  Generator_Frequency ()
  : Message ( Type::GENERATOR_FREQUENCY )
  {
  }

  void
  reset ()
  {
    _generator_index = 0;
    _pulse_usecs_high = 0;
    _pulse_usecs_total = 0;
  }

  std::uint8_t
  generator_index () const
  {
    return _generator_index;
  }

  void
  set_generator_index ( std::uint8_t index_n )
  {
    _generator_index = index_n;
  }

  std::uint32_t
  pulse_usecs_total () const
  {
    return _pulse_usecs_total;
  }

  void
  set_pulse_usecs_total ( std::uint32_t usecs_n )
  {
    _pulse_usecs_total = usecs_n;
  }

  std::uint32_t
  pulse_usecs_high () const
  {
    return _pulse_usecs_high;
  }

  void
  set_pulse_usecs_high ( std::uint32_t usecs_n )
  {
    _pulse_usecs_high = usecs_n;
  }

  private:
  // -- Attributes
  std::uint8_t _generator_index = 0;
  std::uint32_t _pulse_usecs_total = 0;
  std::uint32_t _pulse_usecs_high = 0;
};

} // namespace snc::emb::msg::out
