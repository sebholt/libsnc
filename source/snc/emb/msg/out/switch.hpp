/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/out/message.hpp>

namespace snc::emb::msg::out
{

class Switch : public Message
{
  // Public methods
  public:
  Switch ()
  : Message ( Type::SWITCH )
  {
  }

  void
  reset ()
  {
    _switch_index = 0;
    _switch_state = false;
  }

  std::uint8_t
  switch_index () const
  {
    return _switch_index;
  }

  void
  set_switch_index ( std::uint8_t switch_index_n )
  {
    _switch_index = switch_index_n;
  }

  bool
  switch_state () const
  {
    return _switch_state;
  }

  void
  set_switch_state ( bool switch_state_n )
  {
    _switch_state = switch_state_n;
  }

  private:
  // -- Attributes
  std::uint8_t _switch_index = 0;
  bool _switch_state = false;
};

} // namespace snc::emb::msg::out
