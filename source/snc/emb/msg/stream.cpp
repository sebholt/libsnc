/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stream.hpp"

namespace snc::emb::msg
{

Stream::Stream () = default;

void
Stream::clear ()
{
  _buffer.clear ();
  _message_uid = 0;
}

} // namespace snc::emb::msg
