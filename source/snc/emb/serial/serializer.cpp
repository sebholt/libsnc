/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "serializer.hpp"
#include <cnce/frog/messages_out.hpp> // embedded
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/mem/serial.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <algorithm>

namespace
{

template < typename LOC, typename EMB >
struct Seri
{
  constexpr Seri ( snc::emb::serial::Serializer::Args & args_n )
  : args ( args_n )
  {
    args.size_written = sizeof ( EMB );
  }

  // -- Interface
  constexpr const LOC &
  loc ()
  {
    return static_cast< const LOC & > ( *args.message );
  }

  constexpr EMB &
  emb ()
  {
    return *reinterpret_cast< EMB * > ( args.buffer );
  }

  // -- Attributes
  snc::emb::serial::Serializer::Args & args;
};

void
seri_null ( snc::emb::serial::Serializer::Args & args_n )
{
  (void)args_n;
}

void
seri_heartbeat ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Heartbeat, cnce::Frog_Message_Out_Heartbeat > seri (
      args_n );

  auto & emsg = seri.emb ();
  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ],
                             seri.loc ().message_uid () );
}

void
seri_steps_u16_64 ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Steps_U16_64, cnce::Frog_Message_Out_Steps_U16_64 >
      seri ( args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.stepper_index = lmsg.stepper_index ();
  emsg.step_type = static_cast< std::uint8_t > ( lmsg.job_type () );
  std::uint8_t * wptr ( &emsg.usecs[ 0 ] );
  for ( std::size_t ii = 0; ii != lmsg.num_values (); ++ii ) {
    sev::mem::set_8le_uint16 ( wptr, lmsg.usecs ( ii ) );
    wptr += 2;
  }
  // Append stop marker on demand
  if ( lmsg.num_values () != lmsg.num_values_max () ) {
    sev::mem::set_8le_uint16 ( wptr, 0 );
  }
}

void
seri_stepper_sync_config ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Stepper_Sync_Config,
        cnce::Frog_Message_Out_Stepper_Sync_Config >
      seri ( args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.sync_group_index = lmsg.sync_group_index ();
  {
    std::uint32_t mask = 1;
    for ( std::size_t ii = 0; ii != 4; ++ii ) {
      std::uint8_t * ptr ( &emsg.stepper_register[ ii ] );
      *ptr = 0;
      for ( std::size_t jj = 0; jj != 8; ++jj ) {
        if ( ( lmsg.stepper_register () & mask ) != 0 ) {
          *ptr |= ( 1 << jj );
        }
        mask = ( mask << 1 );
      }
    }
  }
}

void
seri_stepper_sync ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Stepper_Sync, cnce::Frog_Message_Out_Stepper_Sync >
      seri ( args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.sync_group_index = lmsg.sync_group_index ();
  emsg.sync_type = cnce::FROG_STEPPER_SYNC_NULL;
  switch ( lmsg.sync_type () ) {
  case snc::emb::type::Stepper_Sync::LOCK_INSTALL:
    emsg.sync_type = cnce::FROG_STEPPER_SYNC_LOCK_INSTALL;
    break;
  case snc::emb::type::Stepper_Sync::LOCK_RELEASE:
    emsg.sync_type = cnce::FROG_STEPPER_SYNC_LOCK_RELEASE;
    break;
  default:
    break;
  }
}

void
seri_switch ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Switch, cnce::Frog_Message_Out_Switch > seri (
      args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.switch_index = lmsg.switch_index ();
  emsg.switch_state =
      ( lmsg.switch_state () ? ~uint8_t ( 0 ) : std::uint8_t ( 0 ) );
}

void
seri_fader_8 ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Fader_8, cnce::Frog_Message_Out_Fader_8 > seri (
      args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.fader_index = lmsg.fader_index ();
  emsg.fader_value = lmsg.fader_value ();
}

void
seri_fader_16 ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Fader_16, cnce::Frog_Message_Out_Fader_16 > seri (
      args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.fader_index = lmsg.fader_index ();
  sev::mem::set_8le_uint16 ( &emsg.fader_value[ 0 ], lmsg.fader_value () );
}

void
seri_fader_32 ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Fader_32, cnce::Frog_Message_Out_Fader_32 > seri (
      args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.fader_index = lmsg.fader_index ();
  sev::mem::set_8le_uint32 ( &emsg.fader_value[ 0 ], lmsg.fader_value () );
}

void
seri_output_config ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Output_Config,
        cnce::Frog_Message_Out_Output_Config >
      seri ( args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.output_index = lmsg.output_index ();
  emsg.output_type = cnce::FROG_OUTPUT_UNUSED;
  switch ( lmsg.output_type () ) {
  case snc::emb::type::Output::SWITCH_PLAIN:
    emsg.output_type = cnce::FROG_OUTPUT_SWITCH_PLAIN;
    break;
  case snc::emb::type::Output::SWITCH_FREQUENCY:
    emsg.output_type = cnce::FROG_OUTPUT_SWITCH_FREQUENCY;
    break;
  case snc::emb::type::Output::STEPPER_DIRECTION:
    emsg.output_type = cnce::FROG_OUTPUT_STEPPER_DIRECTION;
    break;
  case snc::emb::type::Output::STEPPER_CLOCK:
    emsg.output_type = cnce::FROG_OUTPUT_STEPPER_CLOCK;
    break;
  default:
    break;
  }
  emsg.stepper_switch_index = lmsg.stepper_switch_index ();
  emsg.freq_gen_index = lmsg.freq_gen_index ();
}

void
seri_generator_frequency ( snc::emb::serial::Serializer::Args & args_n )
{
  Seri< snc::emb::msg::out::Generator_Frequency,
        cnce::Frog_Message_Out_Generator_Frequency >
      seri ( args_n );

  auto & emsg = seri.emb ();
  auto & lmsg = seri.loc ();

  emsg.init_header ();
  sev::mem::set_8le_uint16 ( &emsg.message_uid[ 0 ], lmsg.message_uid () );
  emsg.generator_index = lmsg.generator_index ();
  sev::mem::set_8le_uint32 ( &emsg.pulse_usecs_total[ 0 ],
                             lmsg.pulse_usecs_total () );
  sev::mem::set_8le_uint32 ( &emsg.pulse_usecs_high[ 0 ],
                             lmsg.pulse_usecs_high () );
}

} // namespace

namespace snc::emb::serial
{

Serializer::Serializer ()
{
  _message_metas.fill ( { 0, &seri_null } );
  {
    using Type = snc::emb::msg::out::Type;
    register_message_type< Type::HEARTBEAT, cnce::Frog_Message_Out_Heartbeat > (
        &seri_heartbeat );
    register_message_type< Type::STEPS_U16_64,
                           cnce::Frog_Message_Out_Steps_U16_64 > (
        &seri_steps_u16_64 );
    register_message_type< Type::STEPPER_SYNC_CONFIG,
                           cnce::Frog_Message_Out_Stepper_Sync_Config > (
        &seri_stepper_sync_config );
    register_message_type< Type::STEPPER_SYNC,
                           cnce::Frog_Message_Out_Stepper_Sync > (
        &seri_stepper_sync );
    register_message_type< Type::SWITCH, cnce::Frog_Message_Out_Switch > (
        &seri_switch );
    register_message_type< Type::FADER_8, cnce::Frog_Message_Out_Fader_8 > (
        &seri_fader_8 );
    register_message_type< Type::FADER_16, cnce::Frog_Message_Out_Fader_16 > (
        &seri_fader_16 );
    register_message_type< Type::FADER_32, cnce::Frog_Message_Out_Fader_32 > (
        &seri_fader_32 );
    register_message_type< Type::OUTPUT_CONFIG,
                           cnce::Frog_Message_Out_Output_Config > (
        &seri_output_config );
    register_message_type< Type::GENERATOR_FREQUENCY,
                           cnce::Frog_Message_Out_Generator_Frequency > (
        &seri_generator_frequency );
  }
}

template < auto EID, typename EMT >
inline void
Serializer::register_message_type ( Seri_Func func_n )
{
  static_assert (
      std::is_same_v< decltype ( EID ), snc::emb::msg::out::Type > );
  constexpr std::size_t message_type = static_cast< std::size_t > ( EID );
  static_assert ( message_type < std::tuple_size< Message_Metas >::value );
  _message_metas[ message_type ] = { sizeof ( EMT ), func_n };
}

void
Serializer::install_output_tile ()
{
  DEBUG_ASSERT ( !_tiles_free.is_empty () );
  _output_tile = _tiles_free.front ();
  _tiles_free.pop_front ();
  _output_data = _output_tile->data ();
  _output_capacity = _output_tile->capacity ();
  DEBUG_ASSERT ( _output_capacity >= 512 );
}

void
Serializer::release_output_tile ()
{
  const std::size_t size_written =
      std::distance ( _output_tile->data (), _output_data );
  _output_tile->set_size ( size_written );
  if ( size_written == 0 ) {
    _tiles_free.push_back ( _output_tile );
  } else {
    _tiles_filled.push_back ( _output_tile );
  }

  // Reset references
  _output_tile = nullptr;
  _output_data = nullptr;
  _output_capacity = 0;
}

bool
Serializer::serialize_begin ()
{
  if ( _tiles_free.is_empty () ) {
    return false;
  }
  install_output_tile ();
  return true;
}

bool
Serializer::serialize ( const snc::emb::msg::Message & msg_n )
{
  const std::size_t message_type = msg_n.message_type ();
  DEBUG_ASSERT ( message_type < _message_metas.size () );
  const Message_Meta metas = _message_metas[ message_type ];

  // -- Write to tile directly
  if ( metas.size_max <= _output_capacity ) {
    Args args{ 0, _output_data, &msg_n };
    metas.func ( args );
    _output_data += args.size_written;
    _output_capacity -= args.size_written;
    return true;
  }

  // -- Split message between two tiles
  // There must be another free tile that we can use
  if ( _tiles_free.is_empty () ) {
    return false;
  }

  // Serializer to buffer
  Args args{ 0, _sbuffer.data (), &msg_n };
  metas.func ( args );

  // Part sizes
  const std::size_t size_a = std::min ( _output_capacity, args.size_written );
  const std::size_t size_b = args.size_written - size_a;
  auto src_begin = _sbuffer.begin ();
  auto src_middle = src_begin + size_a;
  auto src_end = src_middle + size_b;

  // Write first part
  std::copy ( src_begin, src_middle, _output_data );
  _output_data += size_a;
  _output_capacity -= size_a;

  // Swap tiles
  release_output_tile ();
  install_output_tile ();

  // Write second part
  std::copy ( src_middle, src_end, _output_data );
  _output_data += size_b;
  _output_capacity -= size_b;

  return true;
}

void
Serializer::serialize_end ()
{
  release_output_tile ();
}

} // namespace snc::emb::serial
