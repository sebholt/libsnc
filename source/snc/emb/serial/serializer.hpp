/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/byte_array.hpp>
#include <sev/mem/list/se/list.hpp>
#include <snc/emb/msg/message.hpp>
#include <snc/emb/msg/out/message.hpp>
#include <snc/utility/data_tile.hpp>
#include <cstdint>
#include <vector>

namespace snc::emb::serial
{

class Serializer
{
  public:
  // -- Types

  struct Args
  {
    std::size_t size_written;
    std::byte * const buffer;
    const snc::emb::msg::Message * const message;
  };

  /// @brief Function that serializes the message into the given buffer
  using Seri_Func = void ( * ) ( Serializer::Args & args_n );
  using Seri_Buffer = std::array< std::byte, 256 + 64 >;

  struct Message_Meta
  {
    std::size_t size_max = 0;
    Seri_Func func = nullptr;
  };

  using Message_Metas = std::array< Message_Meta, 10 >;

  // -- Construction

  Serializer ();

  // -- Free tiles

  void
  free_tile_push ( snc::utility::Data_Tile * tile_n )
  {
    _tiles_free.push_back ( tile_n );
  }

  // -- Filled tiles

  bool
  filled_tile_available () const
  {
    return !_tiles_filled.is_empty ();
  }

  snc::utility::Data_Tile *
  filled_tile_pop ()
  {
    snc::utility::Data_Tile * res = _tiles_filled.front ();
    _tiles_filled.pop_front_not_empty ();
    return res;
  }

  // -- Serialization

  bool
  serialize_begin ();

  bool
  serialize ( const snc::emb::msg::Message & msg_n );

  void
  serialize_end ();

  // -- Utility
  private:
  template < auto EID, typename EMT >
  void
  register_message_type ( Seri_Func func_n );

  void
  install_output_tile ();

  void
  release_output_tile ();

  void
  serialize_split ( const snc::emb::msg::Message & msg_n );

  private:
  // -- Attributes
  sev::mem::list::se::List< snc::utility::Data_Tile > _tiles_free;
  sev::mem::list::se::List< snc::utility::Data_Tile > _tiles_filled;

  snc::utility::Data_Tile * _output_tile = nullptr;
  std::byte * _output_data = nullptr;
  std::size_t _output_capacity = 0;

  Message_Metas _message_metas;
  Seri_Buffer _sbuffer;
};

} // namespace snc::emb::serial
