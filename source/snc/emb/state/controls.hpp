/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::emb::state
{

class Control_Bool
{
  public:
  Control_Bool ( bool state_n = false )
  : _state ( state_n )
  {
  }

  void
  reset ()
  {
    _state = false;
  }

  bool
  state () const
  {
    return _state;
  }

  void
  set_state ( bool flag_n )
  {
    _state = flag_n;
  }

  private:
  // -- Attributes
  bool _state = false;
};

} // namespace snc::emb::state
