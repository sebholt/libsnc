/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/type/step.hpp>

namespace snc::emb
{

class Step_Splitter
{
  public:
  // -- Construction

  Step_Splitter ();

  Step_Splitter ( snc::emb::type::Step job_type_n, std::uint_fast32_t usecs_n );

  Step_Splitter ( snc::emb::type::Step job_type_n,
                  std::uint_fast32_t usecs_n,
                  std::uint_fast32_t usecs_max_n );

  ~Step_Splitter ();

  std::uint_fast32_t
  usecs_max () const
  {
    return _usecs_max;
  }

  void
  set_usecs_max ( std::uint_fast32_t usecs_max_n );

  void
  reset ();

  void
  setup_step ( snc::emb::type::Step job_type_n, std::uint_fast32_t usecs_n );

  bool
  is_finished () const
  {
    return ( _num_chunks_remain == 0 );
  }

  snc::emb::type::Step
  job_type () const
  {
    return _job_type;
  }

  std::uint_fast32_t
  acquire_usecs_next ();

  std::uint_fast32_t
  usecs_remaining () const
  {
    return ( ( _num_chunks_remain * _chunk_usecs ) + _chunk_usecs_extra );
  }

  private:
  // -- Attributes
  snc::emb::type::Step _job_type = snc::emb::type::Step::INVALID;
  bool _first = true;
  std::uint_fast32_t _num_chunks_remain = 0;
  std::uint_fast32_t _chunk_usecs = 0;
  std::uint_fast32_t _chunk_usecs_extra = 0;
  std::uint_fast32_t _usecs_max = 0;
};

inline void
Step_Splitter::setup_step ( snc::emb::type::Step job_type_n,
                            std::uint_fast32_t usecs_n )
{
  _job_type = job_type_n;
  _first = true;

  _num_chunks_remain = ( usecs_n / _usecs_max );
  if ( ( usecs_n % _usecs_max ) != 0 ) {
    ++_num_chunks_remain;
  }
  _chunk_usecs = ( usecs_n / _num_chunks_remain );
  _chunk_usecs_extra = ( usecs_n % _num_chunks_remain );
}

inline std::uint_fast32_t
Step_Splitter::acquire_usecs_next ()
{
  if ( _num_chunks_remain == 0 ) {
    return 0;
  }

  std::uint_fast32_t res = 0;
  if ( _first ) {
    _first = false;
    _job_type = snc::emb::type::Step::HOLD;
  }
  res = _chunk_usecs;
  if ( _chunk_usecs_extra == _num_chunks_remain ) {
    --_chunk_usecs_extra;
    ++res;
  }
  --_num_chunks_remain;
  if ( _num_chunks_remain == 0 ) {
    _job_type = snc::emb::type::Step::INVALID;
  }
  return res;
}

} // namespace snc::emb
