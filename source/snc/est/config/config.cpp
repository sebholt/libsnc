/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "config.hpp"

namespace snc::est::config
{

Config::Config () = default;

Config::~Config () = default;

void
Config::clear ()
{
  _sensors.i1.clear ();
  _sensors.i8.clear ();
  _sensors.i16.clear ();
  _sensors.i32.clear ();
  _sensors.i64.clear ();
  _controls.i1.clear ();
  _controls.i8.clear ();
  _controls.i16.clear ();
  _controls.i32.clear ();
  _controls.i64.clear ();
  _steppers.clear ();
}

} // namespace snc::est::config
