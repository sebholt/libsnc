/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/view.hpp>
#include <snc/est/config/controls.hpp>
#include <snc/est/config/sensors.hpp>
#include <snc/est/config/stepper.hpp>
#include <cstdint>
#include <vector>

namespace snc::est::config
{

/// @brief EStepper device configuration
///
class Config
{
  public:
  // -- Types

  struct Sensors
  {
    std::vector< Sensor_I1 > i1;
    std::vector< Sensor_I8 > i8;
    std::vector< Sensor_I16 > i16;
    std::vector< Sensor_I32 > i32;
    std::vector< Sensor_I64 > i64;
  };

  struct Controls
  {
    std::vector< Control_I1 > i1;
    std::vector< Control_I8 > i8;
    std::vector< Control_I16 > i16;
    std::vector< Control_I32 > i32;
    std::vector< Control_I64 > i64;
  };

  // -- Construction

  Config ();

  ~Config ();

  void
  clear ();

  // -- Sensors

  auto &
  sensors ()
  {
    return _sensors;
  }

  const auto &
  sensors () const
  {
    return _sensors;
  }

  // -- Controls

  auto &
  controls ()
  {
    return _controls;
  }

  const auto &
  controls () const
  {
    return _controls;
  }

  // -- Steppers

  auto &
  steppers ()
  {
    return _steppers;
  }

  const auto &
  steppers () const
  {
    return _steppers;
  }

  private:
  // -- Attributes
  Sensors _sensors;
  Controls _controls;
  std::vector< Stepper > _steppers;
};

} // namespace snc::est::config
