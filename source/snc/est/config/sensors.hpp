/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::est::config
{

class Sensor_I1
{
  public:
  // -- Construction

  Sensor_I1 () = default;

  // -- Initial sensor state

  bool
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( bool state_n )
  {
    _init_state = state_n;
  }

  private:
  // -- Attributes
  bool _init_state = false;
};

class Sensor_I8
{
  public:
  // -- Construction

  Sensor_I8 () = default;

  // -- Initial sensor state

  std::uint8_t
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( std::uint8_t state_n )
  {
    _init_state = state_n;
  }

  private:
  // -- Attributes
  std::uint8_t _init_state = 0;
};

class Sensor_I16
{
  public:
  // -- Construction

  Sensor_I16 () = default;

  // -- Initial sensor state

  std::uint16_t
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( std::uint16_t state_n )
  {
    _init_state = state_n;
  }

  private:
  // -- Attributes
  std::uint16_t _init_state = 0;
};

class Sensor_I32
{
  public:
  // -- Construction

  Sensor_I32 () = default;

  // -- Initial sensor state

  std::uint32_t
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( std::uint32_t state_n )
  {
    _init_state = state_n;
  }

  private:
  // -- Attributes
  std::uint32_t _init_state = 0;
};

class Sensor_I64
{
  public:
  // -- Construction

  Sensor_I64 () = default;

  // -- Initial sensor state

  std::uint64_t
  init_state () const
  {
    return _init_state;
  }

  void
  set_init_state ( std::uint64_t state_n )
  {
    _init_state = state_n;
  }

  private:
  // -- Attributes
  std::uint64_t _init_state = 0;
};

} // namespace snc::est::config
