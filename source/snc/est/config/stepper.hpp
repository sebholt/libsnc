/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::est::config
{

/// @brief EStepper stepper configuration
///
class Stepper
{
  public:
  // -- Types
  enum class Type
  {
    MOTOR,
    CONTROLS
  };

  // -- Construction

  Stepper () = default;

  ~Stepper () = default;

  // -- Type

  Type
  type () const
  {
    return _type;
  }

  void
  set_type ( Type value_n )
  {
    _type = value_n;
  }

  // -- Steps queue capacity

  std::uint_fast32_t
  steps_queue_capacity () const
  {
    return _steps_queue_capacity;
  }

  void
  set_steps_queue_capacity ( std::uint_fast32_t value_n )
  {
    _steps_queue_capacity = value_n;
  }

  // -- Tick frequency

  double
  tick_frequency () const
  {
    return _tick_frequency;
  }

  void
  set_tick_frequency ( double value_n )
  {
    _tick_frequency = value_n;
  }

  private:
  // -- Attributes
  Type _type = Type::MOTOR;
  std::uint_fast32_t _steps_queue_capacity = 0;
  double _tick_frequency = 0;
};

} // namespace snc::est::config
