/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "step_splitter.hpp"
#include <sev/math/numbers.hpp>

namespace snc::est
{

/// @brief 16 bit maximum value
static const std::uint_fast64_t ticks_max_limit = 65535;
static const std::uint_fast64_t ticks_max_default = ticks_max_limit;

Step_Splitter::Step_Splitter ()
: _ticks_max ( ticks_max_default )
{
}

void
Step_Splitter::set_ticks_max ( std::uint_fast64_t ticks_max_n )
{
  sev::math::assign_smaller< std::uint_fast64_t > ( ticks_max_n,
                                                    ticks_max_limit );
  _ticks_max = ticks_max_n;
}

void
Step_Splitter::reset ()
{
  _step_type = estepper::segment::Type::INVALID;
  _size = 0;
  _index = 0;
  _chunk_ticks = 0;
  _chunk_ticks_extra = 0;
}

void
Step_Splitter::setup_step ( estepper::segment::Type step_type_n,
                            estepper::segment::Type step_type_sleep_n,
                            std::uint_fast64_t ticks_n )
{
  _step_type = step_type_n;
  _step_type_sleep = step_type_sleep_n;
  _size = ( ticks_n / _ticks_max );
  if ( ( ticks_n % _ticks_max ) != 0 ) {
    ++_size;
  }
  _index = 0;
  _chunk_ticks = ( ticks_n / _size );
  _chunk_ticks_extra = ( ticks_n % _size );

  compose_step ();
}

void
Step_Splitter::next ()
{
  ++_index;
  compose_step ();
}

inline void
Step_Splitter::compose_step ()
{
  _step.set_type ( ( _index == 0 ) ? _step_type : _step_type_sleep );
  _step.set_ticks ( _chunk_ticks );
  if ( _index < _chunk_ticks_extra ) {
    ++_step.ticks_ref ();
  }
}

} // namespace snc::est
