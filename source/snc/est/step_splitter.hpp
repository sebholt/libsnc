/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/segment/types.hpp>
#include <snc/est/type_step.hpp>
#include <cstdint>

namespace snc::est
{

/// @brief Splits a long step into multiple smaller ones
///
class Step_Splitter
{
  public:
  // -- Construction

  Step_Splitter ();

  ~Step_Splitter () = default;

  // -- Ticks max

  std::uint_fast64_t
  ticks_max () const
  {
    return _ticks_max;
  }

  void
  set_ticks_max ( std::uint_fast64_t ticks_max_n );

  // -- Setup

  void
  reset ();

  void
  setup_step ( estepper::segment::Type step_type_n,
               estepper::segment::Type step_type_sleep_n,
               std::uint_fast64_t ticks_n );

  // -- Result

  std::uint_fast64_t
  size () const
  {
    return _size;
  }

  Type_Step
  step () const
  {
    return _step;
  }

  void
  next ();

  bool
  done () const
  {
    return ( _index >= _size );
  }

  private:
  void
  compose_step ();

  private:
  // -- Attributes
  estepper::segment::Type _step_type = estepper::segment::Type::INVALID;
  estepper::segment::Type _step_type_sleep = estepper::segment::Type::INVALID;
  std::uint_fast64_t _ticks_max = 0;
  std::uint_fast64_t _size = 0;
  std::uint_fast64_t _index = 0;
  std::uint_fast64_t _chunk_ticks = 0;
  std::uint_fast64_t _chunk_ticks_extra = 0;
  Type_Step _step;
};

} // namespace snc::est
