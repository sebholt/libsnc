/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_data_composer.hpp"
#include <estepper/out/messages.hpp>
#include <sev/assert.hpp>
#include <sev/mem/serial.hpp>

namespace snc::est
{

void
Stepper_Data_Composer::serialize_message (
    std::array< std::uint8_t, 256 > & buffer_n ) const
{
  static_assert ( sizeof ( std::uint16_t ) == 2 );
  std::uint8_t carry_bytes = 2 + _size * sizeof ( std::uint16_t );
  buffer_n[ 0 ] = carry_bytes;
  buffer_n[ 1 ] = estepper::out::STEPPER_DATA;
  buffer_n[ 2 ] = _stepper_index;
  {
    std::uint16_t const * rptr = _words.data ();
    std::uint8_t * wptr = &buffer_n[ 3 ];
    for ( decltype ( _size ) ii = 0; ii != _size; ++ii ) {
      sev::mem::set_8le_uint16 ( wptr, *rptr );
      ++rptr;
      wptr += sizeof ( std::uint16_t );
    }
    DEBUG_ASSERT ( wptr <= ( buffer_n.data () + buffer_n.size () ) );
  }
}

} // namespace snc::est
