/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/segment/types.hpp>
#include <sev/assert.hpp>
#include <array>
#include <cstdint>

namespace snc::est
{

// -- Forward declaration
class Stepper_Segment_Pusher;

/// @brief Stepper data composer base class
///
class Stepper_Data_Composer
{
  public:
  // -- Types

  /// @brief 255 carry bytes - type byte - stepper index byte
  ///        (255 - 2) / sizeof(std::uint16_t)
  static constexpr std::size_t word_count = 126;

  // -- Construction

  Stepper_Data_Composer ( std::uint8_t stepper_index_n )
  : _stepper_index ( stepper_index_n )
  {
  }

  ~Stepper_Data_Composer () = default;

  void
  reset ()
  {
    _size = 0;
  }

  // -- Stepper index

  std::uint8_t
  stepper_index () const
  {
    return _stepper_index;
  }

  // -- Size

  std::size_t
  size () const
  {
    return _size;
  }

  std::size_t
  size_free () const
  {
    return _words.size () - _size;
  }

  // -- Items

  std::uint16_t
  get ( std::size_t index_n ) const
  {
    return _words[ index_n ];
  }

  std::uint16_t const *
  data () const
  {
    return _words.data ();
  }

  auto
  begin () const
  {
    return _words.begin ();
  }

  auto
  end () const
  {
    return _words.begin () + size ();
  }

  // -- Serialization

  void
  serialize_message ( std::array< std::uint8_t, 256 > & buffer_n ) const;

  protected:
  friend class Stepper_Segment_Pusher;
  // -- Push

  void
  push_word ( std::uint16_t word_n )
  {
    DEBUG_ASSERT ( _size < _words.size () );
    _words[ _size ] = word_n;
    ++_size;
  }

  void
  push_bytes ( std::uint8_t byte0_n, std::uint8_t byte1_n )
  {
    push_word ( std::uint_fast16_t ( byte0_n ) |
                ( std::uint_fast16_t ( byte1_n ) << 8 ) );
  }

  // -- Back item

  std::uint16_t &
  back ()
  {
    return _words[ _size - 1 ];
  }

  std::uint16_t
  back () const
  {
    return _words[ _size - 1 ];
  }

  private:
  // -- Attributes
  std::uint8_t const _stepper_index = 0xff;
  std::size_t _size = 0;
  std::array< std::uint16_t, word_count > _words;
};

/// @brief Stepper data composer interface for controls stepper
///
class Stepper_Controls_Composer : public Stepper_Data_Composer
{
  public:
  // -- Construction

  Stepper_Controls_Composer ( std::uint8_t stepper_index_n )
  : Stepper_Data_Composer ( stepper_index_n )
  {
  }

  ~Stepper_Controls_Composer () = default;

  // -- Interface

  /// @brief Requires two words
  std::size_t
  i1_words () const
  {
    return 2;
  }

  void
  push_i1 ( std::uint8_t control_index_n, bool value_n )
  {
    push_bytes ( estepper::segment::CTL_1, control_index_n );
    push_word ( value_n ? 0xff : 0x00 );
  }

  /// @brief Requires two words
  std::size_t
  i8_words () const
  {
    return 2;
  }

  void
  push_i8 ( std::uint8_t control_index_n, std::uint8_t value_n )
  {
    push_bytes ( estepper::segment::CTL_8, control_index_n );
    push_word ( value_n );
  }

  /// @brief Requires two words
  std::size_t
  i16_words () const
  {
    return 2;
  }

  void
  push_i16 ( std::uint8_t control_index_n, std::uint16_t value_n )
  {
    push_bytes ( estepper::segment::CTL_16, control_index_n );
    push_word ( value_n );
  }

  /// @brief Requires three words
  std::size_t
  i32_words () const
  {
    return 3;
  }

  void
  push_i32 ( std::uint8_t control_index_n, std::uint32_t value_n )
  {
    push_bytes ( estepper::segment::CTL_32, control_index_n );
    push_word ( value_n >> 0 );
    push_word ( value_n >> 16 );
  }

  /// @brief Requires five words
  std::size_t
  i64_words () const
  {
    return 5;
  }

  void
  push_i64 ( std::uint8_t control_index_n, std::uint64_t value_n )
  {
    push_bytes ( estepper::segment::CTL_64, control_index_n );
    push_word ( value_n >> 0 );
    push_word ( value_n >> 16 );
    push_word ( value_n >> 32 );
    push_word ( value_n >> 48 );
  }
};

/// @brief Pushes segment words with word count and segment setup on destruction
///
/// A Stepper_Segment_Pusher must not outlive the Stepper_Data_Composer passed
/// in the constructor because it holds a view into the composer data.
class Stepper_Segment_Pusher
{
  public:
  Stepper_Segment_Pusher ( Stepper_Data_Composer & composer_n,
                           estepper::segment::Type segment_type_n )
  : _composer ( composer_n )
  , _segment_type ( segment_type_n )
  {
    // Start with an invalid segment header.
    // This will trigger an error in the estepper device if the deconstructor
    // wasn't called properly.
    _composer.push_word ( estepper::segment::INVALID );
    _segment_head = &_composer.back ();
  }

  ~Stepper_Segment_Pusher ()
  {
    // Setup the segment head
    *_segment_head = std::uint_fast16_t ( _segment_type ) |
                     ( std::uint_fast16_t ( _size ) << 8 );
  }

  std::size_t
  size_free () const
  {
    return _composer.size_free ();
  }

  void
  push ( std::uint16_t word_n )
  {
    _composer.push_word ( word_n );
    ++_size;
  }

  private:
  Stepper_Data_Composer & _composer;
  estepper::segment::Type const _segment_type;
  std::uint8_t _size = 0;
  std::uint16_t * _segment_head = nullptr;
};

} // namespace snc::est
