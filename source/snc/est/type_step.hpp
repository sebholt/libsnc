/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/segment/types.hpp>
#include <cstdint>

namespace snc::est
{

/// @brief A step consisting of type and 16 bit tick count
///
class Type_Step
{
  public:
  // -- Construction

  Type_Step () = default;

  Type_Step ( estepper::segment::Type type_n, std::uint16_t ticks_n )
  : _type ( type_n )
  , _ticks ( ticks_n )
  {
  }

  ~Type_Step () = default;

  void
  reset ()
  {
    _type = estepper::segment::Type::INVALID;
    _ticks = 0;
  }

  // -- Segment type

  estepper::segment::Type
  type () const
  {
    return _type;
  }

  void
  set_type ( estepper::segment::Type type_n )
  {
    _type = type_n;
  }

  // -- Ticks

  std::uint16_t
  ticks () const
  {
    return _ticks;
  }

  std::uint16_t &
  ticks_ref ()
  {
    return _ticks;
  }

  void
  set_ticks ( std::uint16_t ticks_n )
  {
    _ticks = ticks_n;
  }

  private:
  // -- Attributes
  estepper::segment::Type _type = estepper::segment::Type::INVALID;
  std::uint16_t _ticks = 0;
};

} // namespace snc::est
