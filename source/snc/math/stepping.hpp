/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cmath>

namespace snc
{

inline double
speed_accelerate_over_length ( double accel_mm_ss_n, double length_mm_n )
{
  const double sqval = ( accel_mm_ss_n * length_mm_n ) * 2.0;
  if ( sqval > 0.0 ) {
    return std::sqrt ( sqval );
  }
  return 0.0;
}

inline double
speed_accelerate_over_length ( double speed_mm_s_n,
                               double accel_mm_ss_n,
                               double length_mm_n )
{
  const double sqval = ( ( accel_mm_ss_n * length_mm_n ) * 2.0 ) +
                       ( speed_mm_s_n * speed_mm_s_n );
  if ( sqval > 0.0 ) {
    return std::sqrt ( sqval );
  }
  return 0.0;
}

inline double
acceleration_length_for_speed ( double speed_target_mm_s_n,
                                double accel_mm_ss_n )
{
  return 0.5 * ( speed_target_mm_s_n * speed_target_mm_s_n ) / accel_mm_ss_n;
}

} // namespace snc
