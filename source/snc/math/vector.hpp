/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector.hpp>
#include <sev/math/numbers.hpp>
#include <cmath>

namespace snc
{

// Function declarations

template < std::size_t DIM >
double
fit_vector_into_maximum_limits_scale (
    const sev::lag::Vector< double, DIM > & vdir_n,
    const sev::lag::Vector< double, DIM > & vlimits_n );

template < std::size_t DIM >
void
fit_vector_into_maximum_limits (
    sev::lag::Vector< double, DIM > & vdir_n,
    const sev::lag::Vector< double, DIM > & vlimits_n );

// Function inline definitions

template < std::size_t DIM >
inline double
fit_vector_into_maximum_limits_scale (
    const sev::lag::Vector< double, DIM > & vdir_n,
    const sev::lag::Vector< double, DIM > & vlimits_n )
{
  // Used to down scaled the vector.
  // This is in place to deal with rounding errros
  // in the later axis limit compare
  const double scale_fix ( 1.0 - 1.0e-9 );

  double res_scale ( 0.0 );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    double dir_len ( std::fabs ( vdir_n[ ii ] ) );
    if ( dir_len > 1.0e-9 ) {
      const double vscale ( ( vlimits_n[ ii ] / dir_len ) * scale_fix );
      bool within_axis_limits ( true );
      {
        sev::lag::Vector< double, DIM > vdir_scaled ( vdir_n );
        vdir_scaled *= vscale;
        // Check if the scaled vector is within all axis limits
        for ( std::size_t jj = 0; jj != DIM; ++jj ) {
          if ( std::fabs ( vdir_scaled[ jj ] ) > vlimits_n[ jj ] ) {
            within_axis_limits = false;
            break;
          }
        }
      }
      if ( within_axis_limits ) {
        sev::math::assign_larger< double > ( res_scale, vscale );
      }
    }
  }
  return res_scale;
}

template < std::size_t DIM >
inline void
fit_vector_into_maximum_limits (
    sev::lag::Vector< double, DIM > & vdir_n,
    const sev::lag::Vector< double, DIM > & vlimits_n )
{
  const double vscale (
      fit_vector_into_maximum_limits_scale ( vdir_n, vlimits_n ) );
  vdir_n *= vscale;
}

} // namespace snc
