/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "evaluator_cubic_d.hpp"
#include <sev/math/numbers.hpp>

namespace snc::mpath::curve
{

template < std::size_t DIM >
const std::size_t Evaluator_Cubic_D< DIM >::num_legs_approx;

template < std::size_t DIM >
const std::size_t Evaluator_Cubic_D< DIM >::num_legs_max;

template < std::size_t DIM >
Evaluator_Cubic_D< DIM >::Evaluator_Cubic_D (
    const sev::lag::Vector< double, DIM > & pos_begin_n,
    const snc::mpath::elem::Curve_Cubic_D< DIM > & curve_n )
{
  _pos_begin = pos_begin_n;

  // -- Points
  _points = curve_n.points ();

  // -- Point deltas
  _deltas[ 2 ] = _points[ 2 ];
  _deltas[ 2 ] -= _points[ 1 ];

  _deltas[ 1 ] = _points[ 1 ];
  _deltas[ 1 ] -= _points[ 0 ];

  _deltas[ 0 ] = _points[ 0 ];
  _deltas[ 0 ] -= _pos_begin;

  // -- Approximated length
  _length_approx = calc_length_approx ( num_legs_approx );
}

template < std::size_t DIM >
double
Evaluator_Cubic_D< DIM >::calc_length_approx ( std::size_t num_legs_n )
{
  if ( num_legs_n == 0 ) {
    return 0.0;
  }

  double len = 0.0;
  const double index_div = num_legs_n;
  const std::size_t index_end = ( num_legs_n + 1 );
  sev::lag::Vector< double, DIM > pprev ( _pos_begin );
  for ( std::size_t ii = 1; ii != index_end; ++ii ) {
    sev::lag::Vector< double, DIM > pnow;
    calc_pos ( pnow, double ( ii ) / index_div );
    // Inverted direction. We are only interested in the absolute
    // length.
    pprev -= pnow;
    len += sev::lag::magnitude ( pprev );
    pprev = pnow; // keep curve position for next iteration
  }
  return len;
}

template < std::size_t DIM >
void
Evaluator_Cubic_D< DIM >::calc_pos ( sev::lag::Vector< double, DIM > & res_n,
                                     double tval_n )
{
  sev::lag::Vector< double, DIM > vpos2[ 2 ];
  {
    sev::lag::Vector< double, DIM > vpos1[ 3 ];
    for ( std::size_t ii = 0; ii != 3; ++ii ) {
      vpos1[ ii ] = _deltas[ ii ] * tval_n;
    }
    vpos1[ 0 ] += _pos_begin;
    vpos1[ 1 ] += _points[ 0 ];
    vpos1[ 2 ] += _points[ 1 ];

    vpos2[ 1 ] = vpos1[ 2 ] - vpos1[ 1 ];
    vpos2[ 1 ] *= tval_n;
    vpos2[ 1 ] += vpos1[ 1 ];

    vpos2[ 0 ] = vpos1[ 1 ] - vpos1[ 0 ];
    vpos2[ 0 ] *= tval_n;
    vpos2[ 0 ] += vpos1[ 0 ];
  }

  vpos2[ 1 ] -= vpos2[ 0 ];
  vpos2[ 1 ] *= tval_n;
  vpos2[ 1 ] += vpos2[ 0 ];
  res_n = vpos2[ 1 ];
}

template < std::size_t DIM >
void
Evaluator_Cubic_D< DIM >::calc_end_pos (
    sev::lag::Vector< double, DIM > & res_n )
{
  res_n = _points[ 2 ];
}

template < std::size_t DIM >
void
Evaluator_Cubic_D< DIM >::calc_tangent (
    sev::lag::Vector< double, DIM > & res_n, double tval_n )
{
  sev::lag::Vector< double, DIM > vpos2[ 2 ];
  {
    sev::lag::Vector< double, DIM > vpos1[ 3 ];
    for ( std::size_t ii = 0; ii != 3; ++ii ) {
      vpos1[ ii ] = _deltas[ ii ] * tval_n;
    }
    vpos1[ 0 ] += _pos_begin;
    vpos1[ 1 ] += _points[ 0 ];
    vpos1[ 2 ] += _points[ 1 ];

    vpos2[ 1 ] = vpos1[ 2 ] - vpos1[ 1 ];
    vpos2[ 1 ] *= tval_n;
    vpos2[ 1 ] += vpos1[ 1 ];

    vpos2[ 0 ] = vpos1[ 1 ] - vpos1[ 0 ];
    vpos2[ 0 ] *= tval_n;
    vpos2[ 0 ] += vpos1[ 0 ];
  }

  vpos2[ 1 ] -= vpos2[ 0 ];
  res_n = sev::lag::normalized ( vpos2[ 1 ] );
}

template < std::size_t DIM >
double
Evaluator_Cubic_D< DIM >::calc_length ()
{
  std::size_t num_legs ( std::ceil ( _length_approx * 1.5 ) );
  sev::math::assign_larger ( num_legs, num_legs_approx );
  sev::math::assign_smaller ( num_legs, num_legs_max );
  return calc_length_approx ( num_legs );
}

template < std::size_t DIM >
void
Evaluator_Cubic_D< DIM >::calc_extents (
    sev::lag::Vector< double, DIM > & min_n,
    sev::lag::Vector< double, DIM > & max_n )
{
  // Minimum
  min_n = _points[ 0 ];
  for ( std::size_t ii = 1; ii != _points.size (); ++ii ) {
    const sev::lag::Vector< double, DIM > & vpos ( _points[ ii ] );
    for ( std::size_t jj = 0; jj != DIM; ++jj ) {
      sev::math::assign_smaller< double > ( min_n[ jj ], vpos[ jj ] );
    }
  }
  // Maximum
  max_n = _points[ 0 ];
  for ( std::size_t ii = 1; ii != _points.size (); ++ii ) {
    const sev::lag::Vector< double, DIM > & vpos ( _points[ ii ] );
    for ( std::size_t jj = 0; jj != DIM; ++jj ) {
      sev::math::assign_larger< double > ( max_n[ jj ], vpos[ jj ] );
    }
  }
}

// -- Instantiation

template class Evaluator_Cubic_D< 1 >;
template class Evaluator_Cubic_D< 2 >;
template class Evaluator_Cubic_D< 3 >;
template class Evaluator_Cubic_D< 4 >;
template class Evaluator_Cubic_D< 5 >;
template class Evaluator_Cubic_D< 6 >;

} // namespace snc::mpath::curve
