/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/curve_evaluator_d.hpp>
#include <snc/mpath/elem/curve_linear_d.hpp>

namespace snc::mpath::curve
{

/// @brief Abstract base class for curve evaluators
///
template < std::size_t DIM >
class Evaluator_Linear_D : public Curve_Evaluator_D< DIM >
{
  public:
  // -- Construction

  /// @brief Zero length curve at origin
  Evaluator_Linear_D ();

  Evaluator_Linear_D (
      const sev::lag::Vector< double, DIM > & pos_begin_n,
      const snc::mpath::elem::Curve_Linear_D< DIM > & curve_n );

  // -- Abstract interface

  void
  calc_pos ( sev::lag::Vector< double, DIM > & res_n, double tval_n ) override;

  void
  calc_end_pos ( sev::lag::Vector< double, DIM > & res_n ) override;

  void
  calc_tangent ( sev::lag::Vector< double, DIM > & res_n,
                 double tval_n ) override;

  double
  calc_length () override;

  void
  calc_extents ( sev::lag::Vector< double, DIM > & min_n,
                 sev::lag::Vector< double, DIM > & max_n ) override;

  private:
  // -- Attributes
  using Curve_Evaluator_D< DIM >::_length_approx;
  using Curve_Evaluator_D< DIM >::_pos_begin;

  sev::lag::Vector< double, DIM > _pos_end;
  sev::lag::Vector< double, DIM > _delta;
  sev::lag::Vector< double, DIM > _tangent;
};

} // namespace snc::mpath::curve
