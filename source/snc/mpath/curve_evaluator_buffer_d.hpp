/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/curve_evaluator_d.hpp>
#include <snc/mpath/elem/curve_d.hpp>
#include <cstddef>
#include <cstdint>
#include <new>
#include <utility>

namespace snc::mpath
{

/// Element buffer sizes
const std::size_t curve_evaluator_buffer_block_size = 8;
const std::size_t curve_evaluator_buffer_d1_size =
    9 * curve_evaluator_buffer_block_size;
const std::size_t curve_evaluator_buffer_d2_size =
    16 * curve_evaluator_buffer_block_size;
const std::size_t curve_evaluator_buffer_d3_size =
    23 * curve_evaluator_buffer_block_size;
const std::size_t curve_evaluator_buffer_d4_size =
    30 * curve_evaluator_buffer_block_size;
const std::size_t curve_evaluator_buffer_d5_size =
    37 * curve_evaluator_buffer_block_size;
const std::size_t curve_evaluator_buffer_d6_size =
    44 * curve_evaluator_buffer_block_size;

/// @brief Curve evaluator buffer base class with buffer size template argument
///
template < std::size_t BSIZE >
class Curve_Evaluator_Buffer_Sized
{
  public:
  // -- Types
  static const std::size_t buffer_size = BSIZE;

  protected:
  // -- Interface

  std::uint8_t *
  buffer ()
  {
    return &_buffer[ 0 ];
  }

  const std::uint8_t *
  buffer () const
  {
    return &_buffer[ 0 ];
  }

  void *
  evaluator_anon () const
  {
    return _evaluator;
  }

  void
  set_evaluator_anon ( void * eval_n )
  {
    _evaluator = eval_n;
  }

  private:
  // -- Attributes
  void * _evaluator = nullptr;
  alignas ( 8 ) std::uint8_t _buffer[ BSIZE ];
};

/// @brief Helper class for fixed buffer sizes
///
template < std::size_t DIM >
class Curve_Evaluator_Buffer_DN;

template <>
class Curve_Evaluator_Buffer_DN< 1 >
: public Curve_Evaluator_Buffer_Sized< curve_evaluator_buffer_d1_size >
{
};

template <>
class Curve_Evaluator_Buffer_DN< 2 >
: public Curve_Evaluator_Buffer_Sized< curve_evaluator_buffer_d2_size >
{
};

template <>
class Curve_Evaluator_Buffer_DN< 3 >
: public Curve_Evaluator_Buffer_Sized< curve_evaluator_buffer_d3_size >
{
};

template <>
class Curve_Evaluator_Buffer_DN< 4 >
: public Curve_Evaluator_Buffer_Sized< curve_evaluator_buffer_d4_size >
{
};

template <>
class Curve_Evaluator_Buffer_DN< 5 >
: public Curve_Evaluator_Buffer_Sized< curve_evaluator_buffer_d5_size >
{
};

template <>
class Curve_Evaluator_Buffer_DN< 6 >
: public Curve_Evaluator_Buffer_Sized< curve_evaluator_buffer_d6_size >
{
};

/// @brief A buffer large enough for any snc::mpath::curve::Evluator_D < DIM >
/// instance
///
template < std::size_t DIM >
class Curve_Evaluator_Buffer_D : public Curve_Evaluator_Buffer_DN< DIM >
{
  public:
  // -- Types
  using Curve_Evaluator_Buffer_DN< DIM >::buffer_size;

  // -- Construction and setup

  /// @brief Constructs an invalid snc::mpath::Curve_Evaluator_D
  ///
  Curve_Evaluator_Buffer_D ();

  /// @brief Creates a new evaluator from the given curve
  ///
  Curve_Evaluator_Buffer_D (
      const sev::lag::Vector< double, DIM > & pos_begin_n,
      const snc::mpath::elem::Curve_D< DIM > & curve_n )
  {
    new_from_curve ( pos_begin_n, curve_n );
  }

  /// @brief Copies the evaluator
  ///
  Curve_Evaluator_Buffer_D (
      const Curve_Evaluator_Buffer_D< DIM > & ebuffer_n ) = delete;

  /// @brief Copies the evaluator
  ///
  Curve_Evaluator_Buffer_D ( Curve_Evaluator_Buffer_D< DIM > && ebuffer_n ) =
      delete;

  /// @brief Constructs an invalid snc::mpath::Curve_Evaluator_D
  ///
  void
  reset ();

  // -- Accessors

  snc::mpath::Curve_Evaluator_D< DIM > &
  evaluator ()
  {
    return *static_cast< snc::mpath::Curve_Evaluator_D< DIM > * > (
        evaluator_anon () );
  }

  const snc::mpath::Curve_Evaluator_D< DIM > &
  evaluator () const
  {
    return *static_cast< const snc::mpath::Curve_Evaluator_D< DIM > * > (
        evaluator_anon () );
  }

  /// @brief Constructs a T Evaluator with the given args
  ///
  void
  new_from_curve ( const sev::lag::Vector< double, DIM > & pos_begin_n,
                   const snc::mpath::elem::Curve_D< DIM > & curve_n );

  Curve_Evaluator_Buffer_D< DIM > &
  operator= ( const Curve_Evaluator_Buffer_D< DIM > & ebuffer_n ) = delete;

  Curve_Evaluator_Buffer_D< DIM > &
  operator= ( Curve_Evaluator_Buffer_D< DIM > && ebuffer_n ) = delete;

  private:
  // -- Utility
  using Curve_Evaluator_Buffer_DN< DIM >::buffer;
  using Curve_Evaluator_Buffer_DN< DIM >::evaluator_anon;
  using Curve_Evaluator_Buffer_DN< DIM >::set_evaluator_anon;

  template < class T, typename... Args >
  void
  private_new ( Args &&... args_n )
  {
    // Compile time buffer size check
    static_assert ( sizeof ( T ) <= buffer_size, "Buffer size too small" );
    // Create new element and store base casted pointer
    set_evaluator_anon ( static_cast< snc::mpath::Curve_Evaluator_D< DIM > * > (
        new ( buffer () ) T ( std::forward< Args > ( args_n )... ) ) );
  }
};

// -- Types
typedef Curve_Evaluator_Buffer_D< 1 > Curve_Evaluator_Buffer_D1;
typedef Curve_Evaluator_Buffer_D< 2 > Curve_Evaluator_Buffer_D2;
typedef Curve_Evaluator_Buffer_D< 3 > Curve_Evaluator_Buffer_D3;
typedef Curve_Evaluator_Buffer_D< 4 > Curve_Evaluator_Buffer_D4;
typedef Curve_Evaluator_Buffer_D< 5 > Curve_Evaluator_Buffer_D5;
typedef Curve_Evaluator_Buffer_D< 6 > Curve_Evaluator_Buffer_D6;

} // namespace snc::mpath
