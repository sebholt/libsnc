/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <cstdint>

namespace snc::mpath
{

/// @brief Abstract base class for curve evaluators
///
template < std::size_t DIM >
class Curve_Evaluator_D
{
  public:
  // -- Construction

  Curve_Evaluator_D ();

  // -- Static interface

  double
  length_approx () const
  {
    return _length_approx;
  }

  const sev::lag::Vector< double, DIM > &
  pos_begin () const
  {
    return _pos_begin;
  }

  // -- Abstract interface

  /// @brief Calculate curve position at tval_n in pos()
  ///
  virtual void
  calc_pos ( sev::lag::Vector< double, DIM > & res_n, double tval_n ) = 0;

  /// @brief Calculate the precise end position ( tval_n = 1.0 )
  ///
  virtual void
  calc_end_pos ( sev::lag::Vector< double, DIM > & res_n ) = 0;

  /// @brief Calculate curve tangent at tval_n in tangent()
  ///
  virtual void
  calc_tangent ( sev::lag::Vector< double, DIM > & res_n, double tval_n ) = 0;

  /// @brief Calculate a curve length more precise than length_approx()
  ///
  virtual double
  calc_length () = 0;

  /// @brief Calculate the curve extents (bounding box)
  ///
  virtual void
  calc_extents ( sev::lag::Vector< double, DIM > & min_n,
                 sev::lag::Vector< double, DIM > & max_n ) = 0;

  protected:
  // -- Attributes
  double _length_approx;
  sev::lag::Vector< double, DIM > _pos_begin;
};

template < std::size_t DIM >
Curve_Evaluator_D< DIM >::Curve_Evaluator_D ()
{
}

} // namespace snc::mpath
