/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>
#include <cstdint>

namespace snc::mpath::elem
{

class Curve : public snc::mpath::Element
{
  protected:
  // -- Construction

  Curve ( std::uint_fast32_t elem_type_n, std::uint_fast32_t size_n )
  : snc::mpath::Element ( elem_type_n, size_n )
  {
  }
};

} // namespace snc::mpath::elem
