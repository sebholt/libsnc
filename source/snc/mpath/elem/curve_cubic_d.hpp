/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "curve_d.hpp"
#include <sev/lag/vector3.hpp>
#include <array>
#include <cstdint>

namespace snc::mpath::elem
{

template < std::size_t DIM >
class Curve_Cubic_D : public snc::mpath::elem::Curve_D< DIM >
{
  protected:
  // -- Construction

  Curve_Cubic_D ( std::uint_fast32_t elem_type_n )
  : snc::mpath::elem::Curve_D< DIM > ( elem_type_n,
                                       sizeof ( Curve_Cubic_D< DIM > ) )
  {
    for ( auto & pnt : _point ) {
      pnt.fill ( 0.0 );
    }
  }

  public:
  // -- Points

  const std::array< sev::lag::Vector< double, DIM >, 3 > &
  points () const
  {
    return _point;
  }

  const sev::lag::Vector< double, DIM > &
  point ( std::size_t index_n ) const
  {
    return _point[ index_n ];
  }

  void
  set_point ( std::size_t index_n,
              const sev::lag::Vector< double, DIM > & vec_n )
  {
    _point[ index_n ] = vec_n;
  }

  // -- Abstract interface

  void
  translate ( const sev::lag::Vector< double, DIM > & delta_n );

  private:
  // -- Attributes
  std::array< sev::lag::Vector< double, DIM >, 3 > _point;
};

template < std::size_t DIM >
void
Curve_Cubic_D< DIM >::translate (
    const sev::lag::Vector< double, DIM > & delta_n )
{
  for ( auto & point : _point ) {
    point += delta_n;
  }
}

template < std::size_t DIM, std::uint_fast32_t ELEM_TYPE >
class Curve_Cubic_DT : public Curve_Cubic_D< DIM >
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type = ELEM_TYPE;

  // -- Construction

  Curve_Cubic_DT ()
  : Curve_Cubic_D< DIM > ( class_elem_type )
  {
  }
};

} // namespace snc::mpath::elem
