/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "curve.hpp"
#include <sev/lag/vector_class.hpp>
#include <sev/mem/flags.hpp>

namespace snc::mpath::elem
{

template < std::size_t DIM >
class Curve_D : public snc::mpath::elem::Curve
{
  protected:
  // -- Construction

  Curve_D ( std::uint_fast32_t elem_type_n, std::uint_fast32_t size_n )
  : snc::mpath::elem::Curve ( elem_type_n, size_n )
  {
  }
};

template < std::size_t DIM >
void
curve_translate_d ( Curve_D< DIM > & curve_n,
                    const sev::lag::Vector< double, DIM > & delta_n );

template < typename T, std::size_t DIM >
void
curve_translate ( Curve_D< DIM > & curve_n,
                  const sev::lag::Vector< double, DIM > & delta_n )
{
  static_cast< T & > ( curve_n ).translate ( delta_n );
}

} // namespace snc::mpath::elem
