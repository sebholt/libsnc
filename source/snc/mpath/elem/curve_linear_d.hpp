/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "curve_d.hpp"
#include <sev/lag/vector_class.hpp>

namespace snc::mpath::elem
{

template < std::size_t DIM >
class Curve_Linear_D : public snc::mpath::elem::Curve_D< DIM >
{
  protected:
  // -- Construction

  Curve_Linear_D ( std::uint_fast32_t elem_type_n )
  : snc::mpath::elem::Curve_D< DIM > ( elem_type_n,
                                       sizeof ( Curve_Linear_D< DIM > ) )
  , _pos_end ( sev::lag::init::zero )
  {
  }

  Curve_Linear_D ( std::uint_fast32_t elem_type_n,
                   const sev::lag::Vector< double, DIM > & pos_end_n )
  : snc::mpath::elem::Curve_D< DIM > ( elem_type_n,
                                       sizeof ( Curve_Linear_D< DIM > ) )
  , _pos_end ( pos_end_n )
  {
  }

  public:
  // -- End position

  const sev::lag::Vector< double, DIM > &
  pos_end () const
  {
    return _pos_end;
  }

  void
  set_pos_end ( const sev::lag::Vector< double, DIM > & vec_n )
  {
    _pos_end = vec_n;
  }

  // -- Abstract interface

  void
  translate ( const sev::lag::Vector< double, DIM > & delta_n );

  private:
  // -- Attributes
  sev::lag::Vector< double, DIM > _pos_end;
};

template < std::size_t DIM >
void
Curve_Linear_D< DIM >::translate (
    const sev::lag::Vector< double, DIM > & delta_n )
{
  _pos_end += delta_n;
}

template < std::size_t DIM, std::uint_fast32_t ELEM_TYPE >
class Curve_Linear_DT : public Curve_Linear_D< DIM >
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type = ELEM_TYPE;

  // -- Construction

  Curve_Linear_DT ()
  : Curve_Linear_D< DIM > ( class_elem_type )
  {
  }

  Curve_Linear_DT ( const sev::lag::Vector< double, DIM > & pos_end_n )
  : Curve_Linear_D< DIM > ( class_elem_type, pos_end_n )
  {
  }
};

} // namespace snc::mpath::elem
