/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/value64.hpp>
#include <snc/mpath/element.hpp>
#include <cstdint>

namespace snc::mpath::elem
{

class Fader : public snc::mpath::Element
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type =
      snc::mpath::elem::Type::FADER;

  // -- Construction

  Fader ()
  : snc::mpath::Element ( class_elem_type, sizeof ( Fader ) )
  {
  }

  // -- Fader bits

  std::uint8_t
  fader_bits () const
  {
    return _fader_bits;
  }

  void
  set_fader_bits ( std::uint8_t value_n )
  {
    _fader_bits = value_n;
  }

  // -- Fader index

  std::uint8_t
  fader_index () const
  {
    return _fader_index;
  }

  void
  set_fader_index ( std::uint8_t value_n )
  {
    _fader_index = value_n;
  }

  // -- Fader value

  const sev::mem::Value64 &
  fader_value () const
  {
    return _fader_value;
  }

  sev::mem::Value64 &
  fader_value_ref ()
  {
    return _fader_value;
  }

  private:
  // -- Attributes
  std::uint8_t _fader_bits = 0;
  std::uint8_t _fader_index = 0;
  sev::mem::Value64 _fader_value = sev::mem::Value64 ( std::uint64_t ( 0 ) );
};

} // namespace snc::mpath::elem
