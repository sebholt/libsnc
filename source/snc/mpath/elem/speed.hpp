/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>
#include <snc/mpath/speed.hpp>
#include <cstdint>

namespace snc::mpath::elem
{

class Speed : public snc::mpath::Element
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type =
      snc::mpath::elem::Type::SPEED;

  // -- Construction

  Speed ()
  : snc::mpath::Element ( class_elem_type, sizeof ( Speed ) )
  {
  }

  Speed ( const snc::mpath::Speed & speed_n )
  : snc::mpath::Element ( class_elem_type, sizeof ( Speed ) )
  , _speed ( speed_n )
  {
  }

  // -- Speed

  snc::mpath::Speed &
  speed ()
  {
    return _speed;
  }

  const snc::mpath::Speed &
  speed () const
  {
    return _speed;
  }

  private:
  // -- Attributes
  snc::mpath::Speed _speed;
};

} // namespace snc::mpath::elem
