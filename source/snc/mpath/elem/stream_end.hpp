/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>
#include <cstdint>

namespace snc::mpath::elem
{

class Stream_End : public snc::mpath::Element
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type =
      snc::mpath::elem::Type::STREAM_END;

  // -- Construction

  Stream_End ()
  : snc::mpath::Element ( class_elem_type, sizeof ( Stream_End ) )
  {
  }
};

} // namespace snc::mpath::elem
