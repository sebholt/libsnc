/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::mpath::elem
{

/// @brief Element type namespace struct
struct Type
{
  /// @brief Invalid element type
  static constexpr std::uint_fast32_t INVALID = 0;

  /// @brief End of stream marker
  static constexpr std::uint_fast32_t STREAM_END = 1;
  /// @brief End of curve stream marker
  static constexpr std::uint_fast32_t STREAM_CURVE_FINISH = 2;

  static constexpr std::uint_fast32_t FADER = 3;
  static constexpr std::uint_fast32_t SLEEP = 4;
  static constexpr std::uint_fast32_t SPEED_DB_VALUE = 5;
  static constexpr std::uint_fast32_t SPEED = 6;

  // Element list end
  static constexpr std::uint_fast32_t LIST_END = 7;
};

} // namespace snc::mpath::elem
