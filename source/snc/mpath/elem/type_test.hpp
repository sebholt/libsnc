/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/d1/elem_type.hpp>
#include <snc/mpath/d2/elem_type.hpp>
#include <snc/mpath/d3/elem_type.hpp>
#include <snc/mpath/d4/elem_type.hpp>
#include <snc/mpath/d5/elem_type.hpp>
#include <snc/mpath/d6/elem_type.hpp>
#include <snc/mpath/element.hpp>

namespace snc::mpath::elem
{

/// @return True if the element type is valid
///
template < std::size_t DIM >
bool
type_is_valid ( std::uint_fast32_t elem_type_n );

/// @return True if the element is a ::mpath::element::Curve_D < DIM >
///
template < std::size_t DIM >
bool
type_is_curve ( std::uint_fast32_t elem_type_n );

template < std::size_t DIM >
inline bool
is_curve ( const Element & elem_n )
{
  return type_is_curve< DIM > ( elem_n.elem_type () );
}

// -- Specializations

template <>
inline bool
type_is_valid< 1 > ( std::uint_fast32_t elem_type_n )
{
  return d1::elem::type_is_valid ( elem_type_n );
}

template <>
inline bool
type_is_curve< 1 > ( std::uint_fast32_t elem_type_n )
{
  return d1::elem::type_is_curve ( elem_type_n );
}

template <>
inline bool
type_is_valid< 2 > ( std::uint_fast32_t elem_type_n )
{
  return d2::elem::type_is_valid ( elem_type_n );
}

template <>
inline bool
type_is_curve< 2 > ( std::uint_fast32_t elem_type_n )
{
  return d2::elem::type_is_curve ( elem_type_n );
}

template <>
inline bool
type_is_valid< 3 > ( std::uint_fast32_t elem_type_n )
{
  return d3::elem::type_is_valid ( elem_type_n );
}

template <>
inline bool
type_is_curve< 3 > ( std::uint_fast32_t elem_type_n )
{
  return d3::elem::type_is_curve ( elem_type_n );
}

template <>
inline bool
type_is_valid< 4 > ( std::uint_fast32_t elem_type_n )
{
  return d4::elem::type_is_valid ( elem_type_n );
}

template <>
inline bool
type_is_curve< 4 > ( std::uint_fast32_t elem_type_n )
{
  return d4::elem::type_is_curve ( elem_type_n );
}

template <>
inline bool
type_is_valid< 5 > ( std::uint_fast32_t elem_type_n )
{
  return d5::elem::type_is_valid ( elem_type_n );
}

template <>
inline bool
type_is_curve< 5 > ( std::uint_fast32_t elem_type_n )
{
  return d5::elem::type_is_curve ( elem_type_n );
}

template <>
inline bool
type_is_valid< 6 > ( std::uint_fast32_t elem_type_n )
{
  return d6::elem::type_is_valid ( elem_type_n );
}

template <>
inline bool
type_is_curve< 6 > ( std::uint_fast32_t elem_type_n )
{
  return d6::elem::type_is_curve ( elem_type_n );
}

} // namespace snc::mpath::elem
