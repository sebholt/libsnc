/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/elem/type.hpp>
#include <cstdint>

namespace snc::mpath
{

class Element
{
  public:
  // -- Types
  static constexpr std::uint_fast32_t class_elem_type = 0;

  // -- Construction

  /// @brief Creates an invalid element
  Element () = default;

  protected:
  Element ( std::uint_fast32_t elem_type_n, std::uint_fast32_t size_n )
  : _elem_type ( elem_type_n )
  , _elem_size ( size_n )
  {
  }

  public:
  // -- Accessors

  /// @brief The element type
  ///
  std::uint_fast32_t
  elem_type () const
  {
    return _elem_type;
  }

  /// @brief The element size in bytes
  ///
  std::uint_fast32_t
  elem_size () const
  {
    return _elem_size;
  }

  /// @brief Returns true if the element type is not INVALID
  ///
  bool
  is_valid () const
  {
    return ( _elem_type != elem::Type::INVALID );
  }

  private:
  // -- Attributes
  std::uint_fast32_t _elem_type = class_elem_type;
  std::uint_fast32_t _elem_size = 0;
};

} // namespace snc::mpath
