/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element.hpp>
#include <algorithm>
#include <cstddef>
#include <cstdint>
#include <new>
#include <utility>

namespace snc::mpath
{

/// Element buffer sizes
constexpr std::size_t element_buffer_block_size = 8;
constexpr std::size_t element_buffer_d1_size = 5 * element_buffer_block_size;
constexpr std::size_t element_buffer_d2_size = 8 * element_buffer_block_size;
constexpr std::size_t element_buffer_d3_size = 11 * element_buffer_block_size;
constexpr std::size_t element_buffer_d4_size = 14 * element_buffer_block_size;
constexpr std::size_t element_buffer_d5_size = 17 * element_buffer_block_size;
constexpr std::size_t element_buffer_d6_size = 20 * element_buffer_block_size;

/// @brief Element buffer base class with buffer size template argument
///
template < std::size_t BSIZE >
class Element_Buffer_Sized
{
  public:
  // -- Statics

  static constexpr std::size_t buffer_size = BSIZE;

  // -- Accessors

  /// @brief Element accessor
  Element &
  elem ()
  {
    return *reinterpret_cast< Element * > ( buffer () );
  }

  /// @brief Element accessor
  const Element &
  elem () const
  {
    return *reinterpret_cast< const Element * > ( buffer () );
  }

  /// @brief Element type
  std::uint_fast32_t
  elem_type () const
  {
    return elem ()->elem_type ();
  }

  protected:
  // -- Utility

  std::byte *
  buffer ()
  {
    return &_buffer[ 0 ];
  }

  const std::byte *
  buffer () const
  {
    return &_buffer[ 0 ];
  }

  private:
  // -- Attributes
  alignas ( 8 ) std::byte _buffer[ BSIZE ];
};

/// @brief Helper class for fixed buffer sizes
///
template < std::size_t DIM >
class Element_Buffer_DN;

template <>
class Element_Buffer_DN< 1 >
: public Element_Buffer_Sized< element_buffer_d1_size >
{
};

template <>
class Element_Buffer_DN< 2 >
: public Element_Buffer_Sized< element_buffer_d2_size >
{
};

template <>
class Element_Buffer_DN< 3 >
: public Element_Buffer_Sized< element_buffer_d3_size >
{
};

template <>
class Element_Buffer_DN< 4 >
: public Element_Buffer_Sized< element_buffer_d4_size >
{
};

template <>
class Element_Buffer_DN< 5 >
: public Element_Buffer_Sized< element_buffer_d5_size >
{
};

template <>
class Element_Buffer_DN< 6 >
: public Element_Buffer_Sized< element_buffer_d6_size >
{
};

/// @brief A buffer that can hold any snc::mpath::Element subclass for the
/// given dimension
///
template < std::size_t DIM >
class Element_Buffer_D : public Element_Buffer_DN< DIM >
{
  public:
  // -- Construction

  /// @brief Constructs an invalid snc::mpath::Element
  ///
  Element_Buffer_D () { make< Element > (); }

  /// @brief Creates a new element ot type T and returns it in the pointer
  ///
  template < class T, typename... Args >
  explicit Element_Buffer_D ( T ** elem_n, Args &&... args_n )
  {
    acquire ( elem_n, std::forward< Args > ( args_n )... );
  }

  /// @brief Copies the element
  ///
  Element_Buffer_D ( const Element_Buffer_D & ebuffer_n )
  {
    copy ( ebuffer_n.elem () );
  }

  /// @brief Copies the element
  ///
  Element_Buffer_D ( const Element & element_n ) { copy ( element_n ); }

  // -- Setup

  void
  reset ()
  {
    make< Element > ();
  }

  // -- Assignment operators

  Element_Buffer_D< DIM > &
  operator= ( const Element_Buffer_D< DIM > & ebuffer_n )
  {
    copy ( ebuffer_n.elem () );
    return *this;
  }

  Element_Buffer_D< DIM > &
  operator= ( const Element & elem_n )
  {
    copy ( elem_n );
    return *this;
  }

  // -- Accessors

  using Element_Buffer_DN< DIM >::buffer_size;
  using Element_Buffer_DN< DIM >::elem;
  using Element_Buffer_DN< DIM >::elem_type;

  // -- Element creation

  /// @brief Constructs a T element with the given args
  ///
  template < class T, typename... Args >
  T &
  make ( Args &&... args_n )
  {
    // Compile time buffer size check
    static_assert ( sizeof ( T ) <= buffer_size, "Buffer size too small" );
    // Create new element and store base casted pointer
    new ( buffer () ) T ( std::forward< Args > ( args_n )... );
    return static_cast< T & > ( elem () );
  }

  /// @brief Constructs a T element with the given args
  ///
  template < class T, typename... Args >
  void
  acquire ( T ** elem_n, Args &&... args_n )
  {
    *elem_n = &make< T > ( std::forward< Args > ( args_n )... );
  }

  /// @brief Copy constructs a derived element from an anonymous reference
  ///
  Element &
  copy ( const Element & elem_n )
  {
    std::copy_n ( reinterpret_cast< const std::byte * > ( &elem_n ),
                  elem_n.elem_size (),
                  buffer () );
    return elem ();
  }

  private:
  using Element_Buffer_DN< DIM >::buffer;
};

} // namespace snc::mpath
