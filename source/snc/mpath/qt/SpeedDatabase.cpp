/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "SpeedDatabase.hpp"

namespace
{
const QString _name_rapid ( "Rapid" );
const QString _name_material ( "Material" );
const QString _name_material_planar ( "Material planar" );
const QString _name_material_normal ( "Material normal" );
} // namespace

namespace snc::mpath::qt
{

SpeedDatabase::SpeedDatabase ( QObject * qparent_n )
: QAbstractListModel ( qparent_n )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::NAME, "name" );
  _roleNames.insert ( ExtraRole::SPEED, "speed" );
}

SpeedDatabase::~SpeedDatabase () = default;

void
SpeedDatabase::update ( const Speed_Database & speeds_n )
{
  for ( auto speed_id_n : { Speed_Db_Id::RAPID,
                            Speed_Db_Id::MATERIAL,
                            Speed_Db_Id::MATERIAL_PLANAR,
                            Speed_Db_Id::MATERIAL_NORMAL } ) {
    changeSpeed ( speed_id_n, speeds_n.speed ( speed_id_n ) );
  }
}

bool
SpeedDatabase::changeSpeed ( Speed_Db_Id speed_id_n, double value_n )
{
  if ( !_speeds.change_speed ( speed_id_n, value_n ) ) {
    return false;
  }
  {
    auto midx = index ( static_cast< int > ( speed_id_n ), 0 );
    emit dataChanged ( midx, midx, QVector< int >{ ExtraRole::SPEED } );
  }
  return true;
}

QHash< int, QByteArray >
SpeedDatabase::roleNames () const
{
  return _roleNames;
}

int
SpeedDatabase::rowCount ( const QModelIndex & parent_n ) const
{
  if ( parent_n.isValid () ) {
    return 0;
  }
  return num_speed_ids;
}

bool
SpeedDatabase::dataIndexValid ( const QModelIndex & index_n ) const
{
  return ( !index_n.parent ().isValid () && //
           ( index_n.column () == 0 ) &&    //
           ( index_n.row () >= 0 ) &&       //
           ( static_cast< std::size_t > ( index_n.row () ) < num_speed_ids ) );
}

Qt::ItemFlags
SpeedDatabase::flags ( const QModelIndex & index_n ) const
{
  Qt::ItemFlags res = Super::flags ( index_n );
  if ( dataIndexValid ( index_n ) ) {
    res |= Qt::ItemIsEditable;
  }
  return res;
}

QVariant
SpeedDatabase::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( !dataIndexValid ( index_n ) ) {
    return QVariant ();
  }

  switch ( role_n ) {
  case ExtraRole::NAME:
    switch ( static_cast< Speed_Db_Id > ( index_n.row () ) ) {
    case Speed_Db_Id::RAPID:
      return QVariant::fromValue ( _name_rapid );
    case Speed_Db_Id::MATERIAL:
      return QVariant::fromValue ( _name_material );
    case Speed_Db_Id::MATERIAL_PLANAR:
      return QVariant::fromValue ( _name_material_planar );
    case Speed_Db_Id::MATERIAL_NORMAL:
      return QVariant::fromValue ( _name_material_normal );
    default:
      break;
    }
    break;

  case ExtraRole::SPEED:
    return QVariant::fromValue (
        _speeds.speed ( static_cast< Speed_Db_Id > ( index_n.row () ) ) );

  default:
    break;
  }

  return QVariant ();
}

bool
SpeedDatabase::setData ( const QModelIndex & index_n,
                         const QVariant & value_n,
                         int role_n )
{
  if ( !dataIndexValid ( index_n ) ) {
    return Super::setData ( index_n, value_n, role_n );
  }
  bool changed = _speeds.change_speed (
      static_cast< Speed_Db_Id > ( index_n.row () ), value_n.toDouble () );
  if ( changed ) {
    emit dataChanged ( index_n, index_n, QVector< int >{ ExtraRole::SPEED } );
  }
  return changed;
}

} // namespace snc::mpath::qt
