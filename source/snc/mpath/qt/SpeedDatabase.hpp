/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/speed_database.hpp>
#include <QAbstractListModel>

namespace snc::mpath::qt
{

class SpeedDatabase : public QAbstractListModel
{
  Q_OBJECT

  // -- Types
  public:
  using Super = QAbstractListModel;

  enum ExtraRole
  {
    NAME = Qt::UserRole,
    SPEED
  };

  // -- Construction

  SpeedDatabase ( QObject * qparent_n = nullptr );

  ~SpeedDatabase ();

  // -- Speeds

  Speed_Database const &
  speeds () const
  {
    return _speeds;
  }

  void
  update ( const Speed_Database & speeds_n );

  bool
  changeSpeed ( Speed_Db_Id speed_id_n, double value_n );

  // -- Model interface

  QHash< int, QByteArray >
  roleNames () const override;

  int
  rowCount ( const QModelIndex & parent_n = QModelIndex () ) const override;

  Qt::ItemFlags
  flags ( const QModelIndex & index_n ) const override;

  QVariant
  data ( const QModelIndex & index_n, int role_n ) const override;

  bool
  setData ( const QModelIndex & index_n,
            const QVariant & value_n,
            int role_n = Qt::EditRole ) override;

  private:
  // -- Utility
  bool
  dataIndexValid ( const QModelIndex & index_n ) const;

  private:
  // -- Attributes
  Speed_Database _speeds;
  QHash< int, QByteArray > _roleNames;
};

} // namespace snc::mpath::qt
