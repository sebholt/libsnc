/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <array>
#include <cstdint>

namespace snc::mpath
{

/// @brief Speed type identifier
enum class Speed_Db_Id : std::uint_fast8_t
{
  RAPID,

  MATERIAL,
  MATERIAL_PLANAR,
  MATERIAL_NORMAL,

  LIST_END
};

static constexpr std::size_t num_speed_ids =
    static_cast< std::size_t > ( Speed_Db_Id::LIST_END );

/* Database that maps speed ids to values
 *
 */
class Speed_Database
{
  public:
  // -- Types

  using Speeds_List = std::array< double, num_speed_ids >;

  // -- Construction and setup

  Speed_Database ();

  void
  reset ();

  // -- Speeds

  double
  speed ( Speed_Db_Id id_n ) const
  {
    if ( auto index = static_cast< std::size_t > ( id_n );
         index < _speeds.size () ) {
      return _speeds[ index ];
    }
    return 0.0;
  }

  void
  set_speed ( Speed_Db_Id id_n, double speed_n );

  bool
  change_speed ( Speed_Db_Id id_n, double speed_n );

  const Speeds_List &
  speeds () const
  {
    return _speeds;
  }

  void
  sanitized_maximum_speeds ();

  void
  crop_speeds ( double speed_min_n, double speed_max_n );

  void
  crop_planar_speeds ( double speed_min_n, double speed_max_n );

  void
  crop_normal_speeds ( double speed_min_n, double speed_max_n );

  // -- Comparison operators

  bool
  operator== ( const Speed_Database & speed_db_n ) const;

  bool
  operator!= ( const Speed_Database & speed_db_n ) const;

  protected:
  // -- Utility

  double &
  speed_ref ( Speed_Db_Id id_n )
  {
    return _speeds[ static_cast< std::size_t > ( id_n ) ];
  }

  private:
  // -- Attributes
  Speeds_List _speeds;
};

} // namespace snc::mpath
