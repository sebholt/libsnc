/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <array>
#include <cstdint>

namespace snc::mpath_feed
{

/// @brief Speed and acceleration limits for an axis
///
class Axis_Limits
{
  // Public attributes
  public:
  double speed_reversible;
  double speed_max;
  double accel_max;
};

/// @brief Array of axis limits
///
template < std::size_t DIM >
using Axes_Limits_D = std::array< Axis_Limits, DIM >;

} // namespace snc::mpath_feed
