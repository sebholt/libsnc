/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/cargo/element_d.hpp>
#include <snc/mpath_feed/cargo/steps.hpp>
