/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_feed/cargo/event.hpp>
#include <cstdint>

namespace snc::mpath_feed::cargo
{

template < std::size_t DIM >
class Element_D : public Event
{
  public:
  // -- Types
  static const Cargo_Type class_cargo_type = Cargo_Type::ELEMENT;
  static const Type_Int etype = static_cast< Type_Int > ( class_cargo_type );

  // -- Construction

  Element_D ( Pool * cargo_pool_n )
  : Event ( etype, class_cargo_type, cargo_pool_n )
  {
  }

  void
  reset ()
  {
    Event::reset ();
    _ebuffer.reset ();
  }

  // -- Element buffer

  snc::mpath::Element_Buffer_D< DIM > &
  ebuffer ()
  {
    return _ebuffer;
  }

  const snc::mpath::Element_Buffer_D< DIM > &
  ebuffer () const
  {
    return _ebuffer;
  }

  // -- Element

  snc::mpath::Element &
  elem ()
  {
    return ebuffer ().elem ();
  }

  const snc::mpath::Element &
  elem () const
  {
    return ebuffer ().elem ();
  }

  std::uint_fast32_t
  elem_type () const
  {
    return elem ().elem_type ();
  }

  private:
  // -- Attributes
  /// @brief Machine path element
  snc::mpath::Element_Buffer_D< DIM > _ebuffer;
};

// -- Types

using Element_D1 = Element_D< 1 >;
using Element_D2 = Element_D< 2 >;
using Element_D3 = Element_D< 3 >;
using Element_D4 = Element_D< 4 >;
using Element_D5 = Element_D< 5 >;
using Element_D6 = Element_D< 6 >;

} // namespace snc::mpath_feed::cargo
