/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "event.hpp"
#include <sev/assert.hpp>
#include <snc/mpath_feed/cargo/pool.hpp>

namespace snc::mpath_feed::cargo
{

Event::Event ( Type_Int event_type_n,
               Cargo_Type cargo_type_n,
               Pool * cargo_pool_n )
: sev::event::Event ( event_type_n )
, _cargo_type ( cargo_type_n )
, _cargo_pool ( cargo_pool_n )
{
}

void
Event::release_to_cargo_pool ()
{
  DEBUG_ASSERT ( _cargo_pool != nullptr );
  if ( _cargo_pool != nullptr ) {
    _cargo_pool->cargo_release ( this );
  }
}

} // namespace snc::mpath_feed::cargo
