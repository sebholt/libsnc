/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <cstdint>

namespace snc::mpath_feed::cargo
{

// -- Forward declaration
class Pool;

/// @brief Cargo types
enum class Cargo_Type
{
  BSTEPS,
  CSTEPS,
  ELEMENT,
  LIST_END
};

/// @brief Abstract cargo base events
///
/// Cargo events get passed from station to station.
///
class Event : public sev::event::Event
{
  // -- Construction
  protected:
  Event ( Type_Int event_type_n, Cargo_Type cargo_type_n, Pool * cargo_pool_n );

  public:
  // -- Accessors

  /// @brief Cargo type
  Cargo_Type
  cargo_type () const
  {
    return _cargo_type;
  }

  // -- Cargo pool

  Pool *
  cargo_pool () const
  {
    return _cargo_pool;
  }

  /// @brief Release event back to cargo pool
  void
  release_to_cargo_pool ();

  private:
  // -- Private attributes
  Cargo_Type _cargo_type;
  Pool * _cargo_pool;
};

} // namespace snc::mpath_feed::cargo
