/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pool.hpp"
#include <sev/assert.hpp>
#include <sev/event/pool_base.hpp>

namespace snc::mpath_feed::cargo
{

Pool::Pool ()
: _notifier ( [] () {} )
{
}

bool
Pool::all_home () const
{
  bool res = false;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _epool_tracker.all_home ();
  }
  return res;
}

void
Pool::cargo_release ( Event * cargo_n )
{
  if ( cargo_n == nullptr ) {
    return;
  }

  auto * epool = cargo_n->pool ();
  DEBUG_ASSERT ( epool != nullptr );
  if ( epool != nullptr ) {
    std::lock_guard< std::mutex > lock ( _mutex );
    epool->casted_reset_release_virtual ( cargo_n );
    // Notify bit notifier if the event pool was empty or is full
    if ( ( epool->size () == 1 ) || ( epool->all_home () ) ) {
      _notifier ();
    }
  }
}

void
Pool::set_notifier ( const std::function< void () > & notifier_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _notifier = notifier_n;
}

void
Pool::clear_notifier ()
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _notifier = [] () {};
}

} // namespace snc::mpath_feed::cargo
