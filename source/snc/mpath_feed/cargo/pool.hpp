/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool_tracker.hpp>
#include <snc/mpath_feed/cargo/event.hpp>
#include <functional>
#include <mutex>

namespace snc::mpath_feed::cargo
{

/// @brief Cargo pool
///
class Pool
{
  public:
  // -- Construction

  Pool ();

  // -- Interface

  virtual void
  cargo_release ( Event * cargo_n );

  bool
  all_home () const;

  const std::function< void () > &
  notifier () const
  {
    return _notifier;
  }

  // -- Notifier

  void
  set_notifier ( const std::function< void () > & notifier_n );

  void
  clear_notifier ();

  protected:
  // -- Protected attributes
  mutable std::mutex _mutex;
  sev::event::Pool_Tracker _epool_tracker;
  std::function< void () > _notifier;
};

} // namespace snc::mpath_feed::cargo
