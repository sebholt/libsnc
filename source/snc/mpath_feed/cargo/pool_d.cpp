/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pool_d.hpp"
#include <sev/assert.hpp>

namespace snc::mpath_feed::cargo
{

template < std::size_t DIM >
Pool_D< DIM >::Pool_D ()
: _epool_bstep ( _epool_tracker )
, _epool_cstep ( _epool_tracker )
, _epool_elem ( _epool_tracker )
{
}

template < std::size_t DIM >
Pool_D< DIM >::~Pool_D ()
{
  clear ();
}

template < std::size_t DIM >
void
Pool_D< DIM >::clear ()
{
  _epool_bstep.clear ();
  _epool_cstep.clear ();
  _epool_elem.clear ();
}

template < std::size_t DIM >
std::size_t
Pool_D< DIM >::num_bsteps ()
{
  std::size_t res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _epool_bstep.size ();
  }
  return res;
}

template < std::size_t DIM >
std::size_t
Pool_D< DIM >::num_csteps ()
{
  std::size_t res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _epool_cstep.size ();
  }
  return res;
}

template < std::size_t DIM >
std::size_t
Pool_D< DIM >::num_elem ()
{
  std::size_t res ( 0 );
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    res = _epool_elem.size ();
  }
  return res;
}

template < std::size_t DIM >
void
Pool_D< DIM >::allocate_bsteps ( std::size_t num_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _epool_bstep.ensure_minimum_size ( num_n, this );
}

template < std::size_t DIM >
void
Pool_D< DIM >::allocate_csteps ( std::size_t num_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _epool_cstep.ensure_minimum_size ( num_n, this );
}

template < std::size_t DIM >
void
Pool_D< DIM >::allocate_elem ( std::size_t num_n )
{
  std::lock_guard< std::mutex > lock ( _mutex );
  _epool_elem.ensure_minimum_size ( num_n, this );
}

template < std::size_t DIM >
BSteps< DIM > *
Pool_D< DIM >::acquire_bsteps ()
{
  BSteps< DIM > * res = nullptr;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _epool_bstep.try_pop ( &res );
  }
  return res;
}

template < std::size_t DIM >
CSteps< DIM > *
Pool_D< DIM >::acquire_csteps ()
{
  CSteps< DIM > * res = nullptr;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _epool_cstep.try_pop ( &res );
  }
  return res;
}

template < std::size_t DIM >
Element_D< DIM > *
Pool_D< DIM >::acquire_elem ()
{
  Element_D< DIM > * res = nullptr;
  {
    std::lock_guard< std::mutex > lock ( _mutex );
    _epool_elem.try_pop ( &res );
  }
  return res;
}

// -- Instantiation

template class Pool_D< 1 >;
template class Pool_D< 2 >;
template class Pool_D< 3 >;
template class Pool_D< 4 >;
template class Pool_D< 5 >;
template class Pool_D< 6 >;

} // namespace snc::mpath_feed::cargo
