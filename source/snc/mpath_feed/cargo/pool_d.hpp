/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/mpath_feed/cargo/cargos.hpp>
#include <snc/mpath_feed/cargo/pool.hpp>
#include <cstddef>

namespace snc::mpath_feed::cargo
{

/// @brief Cargo pool for DIM dimensions
///
template < std::size_t DIM >
class Pool_D : public Pool
{
  public:
  // -- Construction

  Pool_D ();

  ~Pool_D ();

  void
  clear ();

  // -- Available event count

  std::size_t
  num_bsteps ();

  std::size_t
  num_csteps ();

  std::size_t
  num_elem ();

  // -- Allocation

  void
  allocate_bsteps ( std::size_t num_n );

  void
  allocate_csteps ( std::size_t num_n );

  void
  allocate_elem ( std::size_t num_n );

  // -- Event acquire

  BSteps< DIM > *
  acquire_bsteps ();

  CSteps< DIM > *
  acquire_csteps ();

  Element_D< DIM > *
  acquire_elem ();

  private:
  sev::event::Pool< BSteps< DIM > > _epool_bstep;
  sev::event::Pool< CSteps< DIM > > _epool_cstep;
  sev::event::Pool< Element_D< DIM > > _epool_elem;
};

} // namespace snc::mpath_feed::cargo
