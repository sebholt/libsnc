/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "feed.hpp"
#include <sev/assert.hpp>
#include <snc/mpath_feed/feed_control.hpp>
#include <stdexcept>

namespace snc::mpath_feed
{

Feed::Feed ( const sev::logt::Reference & log_parent_n,
             sev::thread::Tracker * thread_tracker_n,
             const std::vector< Station_Contructor_Handle > & station_cons_n )
: _log ( log_parent_n, "Feed" )
{
  // Event preallocation
  _epools.pool< control_event::out::Job > ().set_size ( 1 );
  _epools.pool< control_event::out::Job_Abort > ().set_size ( 1 );

  // -- Control connection
  auto evq_control = sev::event::queue_io::Reference_2::created ();
  _control_connection.connect ( evq_control.link_a () );
  _control_connection.set_incoming_notifier (
      [ &ea = _event_accu ] () { ea.set ( LEvent::CONTROL_IN ); } );
  _control_connection.set_push_notifier (
      [ &ea = _event_accu ] () { ea.set ( LEvent::CONTROL_OUT ); } );

  // -- Cargo conection
  auto cargo_queue = sev::event::queue::Reference::created ();
  _cargo_connection_in.set_incoming_notifier (
      [ &ea = _event_accu ] () { ea.set ( LEvent::CARGO_IN ); } );
  _cargo_connection_in.connect ( cargo_queue );

  // Control thread
  _control_thread = std::make_unique< Feed_Control > ( _log,
                                                       thread_tracker_n,
                                                       station_cons_n,
                                                       evq_control.link_b (),
                                                       cargo_queue );
}

Feed::~Feed () = default;

void
Feed::session_begin (
    const std::function< void () > & process_request_callback_n )
{
  _event_accu.set_callback ( process_request_callback_n );
}

void
Feed::session_end ()
{
  _event_accu.clear_callback ();
}

void
Feed::job_begin ( const Job_Handle & job_n )
{
  if ( _job_processing ) {
    return;
  }

  _log.cat ( sev::logt::FL_DEBUG_0, "Begin." );

  // Init state
  _job_processing = true;
  _abort_state.clear ();

  // Send job event to controller
  {
    auto & epool = _epools.pool< control_event::out::Job > ();
    if ( epool.is_empty () ) {
      throw std::runtime_error ( "No Job event available" );
    }
    {
      auto * event = epool.pop_not_empty ();
      event->set_job ( job_n );
      _control_connection.push ( event );
    }
  }
}

void
Feed::job_end ()
{
  if ( !_job_processing ) {
    return;
  }

  DEBUG_ASSERT ( is_finished_job_aborting () );
  _log.cat ( sev::logt::FL_DEBUG_0, "End." );
  _abort_state.clear ();
  _job_processing = false;
}

void
Feed::job_abort ()
{
  if ( !is_job_processing () || is_job_aborting () ) {
    return;
  }

  _log.cat ( sev::logt::FL_DEBUG_0, "Abort." );
  _abort_state.set ( Abort_State::ABORT );
  request_event_processing ();
}

void
Feed::request_event_processing ()
{
  _event_accu.set ( LEvent::LOOP_AGAIN );
}

void
Feed::process_events ()
{
  //_log.cat (
  //    sev::logt::FL_DEBUG_0, sev::logt::FL_DEBUG_0, "process_events" );

  // -- Fetch events
  sev::mem::Flags_Fast32 loop_events = _event_accu.fetch_and_clear ();

  // -- Controll events
  if ( loop_events.test_any ( LEvent::CONTROL_IN | LEvent::CONTROL_OUT ) ) {
    _control_connection.feed_queue ();
    process_control_events ();
  }

  // -- Cargo processing
  if ( loop_events.test_any ( LEvent::CARGO_IN ) ) {
    _cargo_connection_in.read_from_queue ();
  }

  if ( !_abort_state.is_empty () ) {
    process_control_abort ();
  }
}

inline void
Feed::process_control_events ()
{
  // Release returned events
  _control_connection.out ().release_all_with_reset ();

  // Process new events
  _control_connection.in ().process_all (
      [ this ] ( const sev::event::Event & event_n ) {
        DEBUG_ASSERT ( event_n.type () == event::Signal::etype );
        switch ( event_n.type () ) {
        case control_event::in::Job_Abort_Done::etype:
          // Debug
          _log.cat ( sev::logt::FL_DEBUG_0,
                     "Incoming controller event: Job abort done." );
          DEBUG_ASSERT (
              _abort_state.test_any ( Abort_State::JOB_ABORT_SENT ) );
          _abort_state.set ( Abort_State::JOB_ABORT_ACKED );
          request_event_processing ();
          break;
        default:
          DEBUG_ASSERT ( false );
          break;
        }
      } );
}

void
Feed::process_control_abort ()
{
  // Shut down feed first
  if ( !_abort_state.test_any ( Abort_State::JOB_ABORT_SENT ) ) {
    auto & epool = _epools.pool< control_event::out::Job_Abort > ();
    if ( !epool.is_empty () ) {
      // Debug
      _log.cat ( sev::logt::FL_DEBUG_0, "Sending control event: Job abort." );
      _control_connection.push ( epool.pop_not_empty () );
      _abort_state.set ( Abort_State::JOB_ABORT_SENT );
    }
  }

  // Return unprocessed cargo
  {
    auto channel ( _cargo_connection_in.channel () );
    while ( !channel.is_empty () ) {
      // Release cargo from cargo events
      auto * cargo = static_cast< cargo::Event * > ( channel.pop_not_empty () );
      cargo->release_to_cargo_pool ();
    }
  }
}

} // namespace snc::mpath_feed
