/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/queue/connection_in.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/mem/view.hpp>
#include <sev/thread/tracker.hpp>
#include <snc/mpath_feed/control_events.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed/station_constructor.hpp>
#include <vector>

namespace snc::mpath_feed
{

// -- Forward declaration
class Feed_Control;

/// @brief Container class for mpath stream to embedded device message
/// generation
///
class Feed
{
  public:
  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t CONTROL_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t CONTROL_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t CARGO_IN = ( 1 << 2 );
    static constexpr std::uint_fast32_t LOOP_AGAIN = ( 1 << 3 );
  };

  struct Abort_State
  {
    static constexpr std::uint_fast32_t ABORT = ( 1 << 0 );
    static constexpr std::uint_fast32_t JOB_ABORT_SENT = ( 1 << 1 );
    static constexpr std::uint_fast32_t JOB_ABORT_ACKED = ( 1 << 2 );
  };

  // -- Construction

  Feed ( const sev::logt::Reference & log_parent_n,
         sev::thread::Tracker * thread_tracker_n,
         const std::vector< Station_Contructor_Handle > & station_cons_n );

  ~Feed ();

  // -- Session control

  void
  session_begin ( const std::function< void () > & process_request_callback_n );

  void
  session_end ();

  // -- Job session control

  void
  job_begin ( const Job_Handle & job_n );

  void
  job_end ();

  void
  job_abort ();

  // -- Job session state

  bool
  is_job_processing () const
  {
    return _job_processing;
  }

  bool
  is_job_aborting () const
  {
    return !_abort_state.is_empty ();
  }

  bool
  is_finished_job_aborting () const
  {
    return _abort_state.test_any ( Abort_State::JOB_ABORT_ACKED );
  }

  // -- Central processing

  /// @brief Processes internale events
  ///
  /// Muste be called when process_request_callback_n from begin_walking() was
  /// triggered.
  void
  process_events ();

  void
  request_event_processing ();

  // -- Cargo interface

  sev::event::queue::Connection_In &
  cargo_connection_in ()
  {
    return _cargo_connection_in;
  }

  private:
  // -- Utility

  void
  process_control_events ();

  void
  process_control_abort ();

  private:
  // -- Logging
  sev::logt::Context _log;
  // -- State registers
  bool _job_processing = false;
  sev::mem::Flags_Fast8 _abort_state;
  sev::event::bit_accus_async::Callback _event_accu;

  // -- Cargo queue connection
  sev::event::queue::Connection_In _cargo_connection_in;

  // -- Control thread events
  sev::event::Tracker_Pools< control_event::out::Job,
                             control_event::out::Job_Abort >
      _epools;
  // -- Control thread connection
  sev::event::queue_io::Connection_2 _control_connection;

  /// @brief Control thread
  std::unique_ptr< Feed_Control > _control_thread;
};

} // namespace snc::mpath_feed
