/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "feed_control.hpp"
#include <sev/math/exponents.hpp>

namespace snc::mpath_feed
{

Feed_Control::Feed_Control (
    const sev::logt::Reference & log_parent_n,
    sev::thread::Tracker * thread_tracker_n,
    const std::vector< Station_Contructor_Handle > & station_cons_n,
    sev::event::queue_io::Link_2 front_link_n,
    const sev::event::queue::Reference & cargo_queue_n )
: _log ( log_parent_n, "Control" )
, _thread ( "MPath-Feed: Control", thread_tracker_n )
{
  // Control events
  _epools.pool< control_event::in::Job_Abort_Done > ().set_size ( 1 );

  // Control connection
  _control_connection.set_incoming_notifier (
      [ &ea = _event_waiter ] () { ea.set ( LEvent::CONTROL_IN ); } );
  _control_connection.set_push_notifier (
      [ &le = _loop_events ] () { le.set ( LEvent::CONTROL_OUT ); } );
  _control_connection.connect ( front_link_n );

  _stations.cargo_queue = cargo_queue_n;
  _stations.constructors = station_cons_n;

  // Start thread
  _thread.start ( [ this ] () { this->operator() (); } );
}

Feed_Control::~Feed_Control ()
{
  if ( _thread.is_running () ) {
    _event_waiter.set ( LEvent::THREAD_ABORT );

    _log.cat ( sev::logt::FL_DEBUG_0, "Thread join begin." );
    _thread.join ();
    _log.cat ( sev::logt::FL_DEBUG_0, "Thread join done." );
  }
}

void
Feed_Control::operator() ()
{
  _log.cat ( sev::logt::FL_DEBUG_0, "Main loop begin." );

  while ( !_loop_events.test_any_unset ( LEvent::THREAD_ABORT ) ) {
    // Wait for events
    _loop_events.set (
        _event_waiter.fetch_and_clear_wait ( _loop_events.is_empty () ) );

    // Control event processing
    control_events ();

    // Stations processing
    stations_process ();

    // Abort processing
    job_abort_process ();
  }

  _log.cat ( sev::logt::FL_DEBUG_0, "Main loop done." );
}

void
Feed_Control::job_abort_process ()
{
  // Aborting?
  if ( _job_abort_state.is_empty () ) {
    return;
  }

  // Check if all events have returned
  if ( !_epools.tracker ().all_home () || //
       !_control_connection.all_empty () ) {
    return;
  }

  // Check if all stations have stopped
  if ( !_stations.control.empty () ) {
    for ( auto & station : _stations.control ) {
      if ( !station->ready_to_end () ) {
        return;
      }
    }

    // Destroy stations
    if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Job abort: Destroying stations.";
    }
    _stations.control.clear ();
  }

  // Check if the abort was acknowledged
  if ( !_job_abort_state.test_any ( Abort_State::ABORT_ACKED ) ) {
    if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Sending control signal: Job abort done";
    }
    _control_connection.push (
        _epools.pool< control_event::in::Job_Abort_Done > ().acquire () );
    _job_abort_state.set ( Abort_State::ABORT_ACKED );
    return;
  }

  // Clear job abort state
  _job_abort_state.clear ();
  return;
}

void
Feed_Control::control_events ()
{
  if ( !_loop_events.test_any ( LEvent::CONTROL_IN | LEvent::CONTROL_OUT ) ) {
    return;
  }

  // Feed control connection queue
  _control_connection.feed_queue (
      _loop_events.flags (), LEvent::CONTROL_IN, LEvent::CONTROL_OUT );

  // Release returned events
  _control_connection.out ().release_all_with_reset ();

  // Process new events
  _control_connection.in ().process_all (
      [ this ] ( const sev::event::Event & event_n ) {
        switch ( event_n.type () ) {
        case control_event::out::Job::etype:
          control_event_job ( event_n );
          break;
        case control_event::out::Job_Abort::etype:
          control_event_job_abort ( event_n );
          break;
        default:
          DEBUG_ASSERT ( false );
        }
      } );
}

void
Feed_Control::control_event_job ( const sev::event::Event & event_n )
{
  auto & cevent = static_cast< const control_event::out::Job & > ( event_n );

  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming Control event: Job.";
  }

  // Create station controls
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Job start: Creating stations.";
  }
  if ( !_stations.constructors.empty () ) {
    const std::function< void () > cback = [ &ea = _event_waiter ] () {
      ea.set ( LEvent::STATION_IO );
    };
    Station_Control * station_prev = nullptr;
    _stations.control.reserve ( _stations.constructors.size () );
    for ( auto & station_con : _stations.constructors ) {
      auto station = std::make_unique< Station_Control > (
          _log, station_con, station_prev, _thread.tracker (), cback );
      station_prev = station.get ();
      _stations.control.push_back ( std::move ( station ) );
    }

    // Set cargo out connection
    _stations.control.back ()->set_cargo_sink_queue ( _stations.cargo_queue );
  }

  // Start station threads
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Job start: Starting station threads.";
  }
  for ( auto & station : _stations.control ) {
    station->start_thread ( cevent.job () );
  }
}

void
Feed_Control::control_event_job_abort ( const sev::event::Event & event_n
                                        [[maybe_unused]] )
{
  if ( auto logo = _log.ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming Control event: Job abort.";
  }

  if ( !_job_abort_state.test_any_set ( Abort_State::ABORT ) ) {
    // Register abort in stations
    for ( auto & station : _stations.control ) {
      station->abort ();
    }
  }
}

void
Feed_Control::stations_process ()
{
  // Process station events
  if ( _loop_events.test_any_unset ( LEvent::STATION_IO ) ) {
    for ( auto & station : _stations.control ) {
      station->events ();
    }
  }

  for ( auto & station : _stations.control ) {
    station->process ();
  }
}

} // namespace snc::mpath_feed
