/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accus_async/condition.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/thread/thread.hpp>
#include <snc/mpath_feed/control_events.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed/station_constructor.hpp>
#include <snc/mpath_feed/station_control.hpp>
#include <memory>
#include <vector>

namespace snc::mpath_feed
{

/// @brief Feed thread
///
class Feed_Control
{
  private:
  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t STATION_IO = ( 1 << 0 );
    static constexpr std::uint_fast32_t CONTROL_IN = ( 1 << 1 );
    static constexpr std::uint_fast32_t CONTROL_OUT = ( 1 << 2 );
    static constexpr std::uint_fast32_t THREAD_ABORT = ( 1 << 3 );
  };

  struct Abort_State
  {
    static constexpr std::uint8_t ABORT = ( 1 << 0 );
    static constexpr std::uint8_t ABORT_ACKED = ( 1 << 1 );
  };

  public:
  // -- Construction

  Feed_Control (
      const sev::logt::Reference & log_parent_n,
      sev::thread::Tracker * thread_tracker_n,
      const std::vector< Station_Contructor_Handle > & station_cons_n,
      sev::event::queue_io::Link_2 front_link_n,
      const sev::event::queue::Reference & cargo_queue_n );

  ~Feed_Control ();

  private:
  // -- Utility

  /// @brief Thread main loop
  void
  operator() ();

  void
  job_abort_process ();

  void
  control_events ();

  void
  control_event_job ( const sev::event::Event & event_n );

  void
  control_event_job_abort ( const sev::event::Event & event_n );

  void
  stations_process ();

  private:
  // -- Attributes
  sev::logt::Context _log;

  // -- Event loop
  sev::mem::Flags_Fast8 _job_abort_state;
  sev::mem::Flags_Fast32 _loop_events;
  sev::event::bit_accus_async::Condition _event_waiter;

  // -- Control queue connection
  sev::event::Tracker_Pools< control_event::in::Job_Abort_Done > _epools;
  sev::event::queue_io::Connection_2 _control_connection;

  // -- Stations
  struct
  {
    sev::event::queue::Reference cargo_queue;
    std::vector< Station_Contructor_Handle > constructors;
    std::vector< std::unique_ptr< Station_Control > > control;
  } _stations;

  sev::thread::Thread _thread;
};

} // namespace snc::mpath_feed
