/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "job.hpp"

namespace snc::mpath_feed
{

Job::Job () {}

Job::~Job () {}

void
Job::reset ()
{
  _name.clear ();
  _stream.reset ();
}

} // namespace snc::mpath_feed
