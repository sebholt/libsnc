/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "job_d.hpp"

namespace snc::mpath_feed
{

template < std::size_t DIM >
Job_D< DIM >::Job_D ()
: _begin_position ( sev::lag::init::zero )
{
}

template < std::size_t DIM >
Job_D< DIM >::Job_D ( const Job & job_n )
: Job ( job_n )
{
}

template < std::size_t DIM >
Job_D< DIM >::~Job_D ()
{
}

template < std::size_t DIM >
void
Job_D< DIM >::reset ()
{
  Job::reset ();
  _begin_position.fill ( 0.0 );
}

// -- Instantiation

template class Job_D< 1 >;
template class Job_D< 2 >;
template class Job_D< 3 >;
template class Job_D< 4 >;
template class Job_D< 5 >;
template class Job_D< 6 >;

} // namespace snc::mpath_feed
