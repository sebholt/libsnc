/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_constructor_d.hpp"
#include "station_d.hpp"

namespace snc::mpath_feed::reader
{

template < std::size_t DIM >
Station_Constructor_D< DIM >::Station_Constructor_D ()
: Station_Constructor ( "Reader" )
{
}

template < std::size_t DIM >
std::unique_ptr< Station >
Station_Constructor_D< DIM >::make ( const Station_Context & context_n )
{
  auto res = std::make_unique< Station_D< DIM > > ( context_n );
  res->set_name ( name () );
  return res;
}

// -- Instantiation

template class Station_Constructor_D< 1 >;
template class Station_Constructor_D< 2 >;
template class Station_Constructor_D< 3 >;
template class Station_Constructor_D< 4 >;
template class Station_Constructor_D< 5 >;
template class Station_Constructor_D< 6 >;

} // namespace snc::mpath_feed::reader
