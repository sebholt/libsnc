/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath_feed/station_constructor.hpp>

namespace snc::mpath_feed::reader
{

/// @brief Specialized station constructor
///
template < std::size_t DIM >
class Station_Constructor_D : public Station_Constructor
{
  public:
  // -- Construction

  Station_Constructor_D ();

  // --- Virtual interface

  std::unique_ptr< Station >
  make ( const Station_Context & context_n ) override;
};

} // namespace snc::mpath_feed::reader
