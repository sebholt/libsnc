/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "stepper_ring_d.hpp"
#include <snc/mpath_feed/cargo/pool_d.hpp>
#include <snc/mpath_feed/station.hpp>

namespace snc::mpath_feed::speeder
{

/// @brief Stepper station
///
template < std::size_t DIM >
class Station_D : public snc::mpath_feed::Station
{
  public:
  // -- Types

  /// @brief Process mode
  enum class PMode : std::uint_fast8_t
  {
    NONE,
    FORWARD,
    STEP,
    FINISH_FORWARD
  };

  /// @brief Level for FORWARD mode
  enum class Forward_Level : std::uint_fast8_t
  {
    FORWARD
  };

  /// @brief Level for STEP mode
  enum class Step_Level : std::uint_fast8_t
  {
    FEED,
    CALC_STEPS,
    BACK_READ_STEPS
  };

  /// @brief Level for FINISH_FORWARD mode
  enum class FF_Level : std::uint_fast8_t
  {
    BEGIN,
    CALC_STEPS,
    BACK_READ_STEPS,
    BACK_READ_ELEM
  };

  // -- Construction

  Station_D ( const Station_Context & context_n,
              const Axes_Limits_D< DIM > & axes_limits_n,
              const sev::lag::Vector< double, DIM > & axis_step_length_n );

  virtual ~Station_D ();

  // -- Abstract interface

  void
  open ( const Job_Handle & job_n,
         std::function< void () > const & notifier_n ) override;

  void
  abort () override;

  void
  close () override;

  Token
  read_next () override;

  bool
  front_feed ( cargo::Event * cargo_n ) override;

  cargo::Event *
  back_read () override;

  private:
  // -- Utility

  bool
  read_next_pmode ( Token & res_n );

  void
  front_release ();

  void
  front_feed_eval ();

  void
  front_feed_elem ();

  void
  back_read_forward_elem ( cargo::Event *& res_n );

  bool
  back_read_forward_steps ( cargo::Event *& res_n );

  void
  curve_request_finish ();

  void
  speeder_feed ();

  void
  speeder_run ();

  /// @brief Returns a casted cargo pointer
  ///
  const cargo::Element_D< DIM > *
  cargo_front_elem () const
  {
    return static_cast< const cargo::Element_D< DIM > * > ( _cargo_front );
  }

  private:
  // -- Attributes
  // States
  bool _is_open = false;
  bool _is_aborting = false;
  bool _stream_end_reached = false;
  PMode _pmode = PMode::NONE;
  Forward_Level _forward_level = Forward_Level::FORWARD;
  Step_Level _step_level = Step_Level::FEED;
  FF_Level _ff_level = FF_Level::BEGIN;

  /// @brief Current front source cargo
  cargo::Event * _cargo_front = nullptr;
  cargo::CSteps< DIM > * _cargo_back_csteps = nullptr;

  // Speeds
  Stepper_Ring_D< DIM > _step_speeder;

  snc::mpath_feed::cargo::Pool_D< DIM > _cargo_pool;

  snc::utility::Debug_File _debug_file_steps;
  snc::utility::Debug_File _debug_file_cargo_out;
};

} // namespace snc::mpath_feed::speeder
