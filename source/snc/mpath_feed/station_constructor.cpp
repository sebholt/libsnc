/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_constructor.hpp"

namespace snc::mpath_feed
{

Station_Constructor::Station_Constructor ( sev::unicode::View name_n )
{
  if ( name_n.is_valid () ) {
    set_name ( name_n );
  }
}

void
Station_Constructor::set_name ( sev::unicode::View span_n )
{
  span_n.get ( _name );
}

} // namespace snc::mpath_feed
