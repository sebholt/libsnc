/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/station.hpp>
#include <memory>
#include <string>

namespace snc::mpath_feed
{

/// @brief Abstract station constructor
///
class Station_Constructor
{
  public:
  // -- Construction

  Station_Constructor ( sev::unicode::View station_name_n = {} );

  // -- Station name

  const std::string &
  name () const
  {
    return _name;
  }

  void
  set_name ( sev::unicode::View span_n );

  // -- Virtual interface

  /// @brief Create a station
  ///
  virtual std::unique_ptr< Station >
  make ( const Station_Context & context_n ) = 0;

  private:
  // -- Attributes
  std::string _name;
};

using Station_Contructor_Handle = std::shared_ptr< Station_Constructor >;

} // namespace snc::mpath_feed
