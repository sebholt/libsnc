/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/reference.hpp>

namespace snc::mpath_feed
{

/// @brief Abstract cargo stream station interface - Context
///
class Station_Context
{
  public:
  // -- Construction

  Station_Context ( const sev::logt::Reference & log_parent_n )
  : _log_parent ( log_parent_n )
  {
  }

  // -- Accessors

  const sev::logt::Reference &
  log_parent () const
  {
    return _log_parent;
  }

  private:
  // -- Attributes
  sev::logt::Reference _log_parent;
};

} // namespace snc::mpath_feed
