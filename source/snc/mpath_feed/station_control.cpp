/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_control.hpp"
#include <snc/mpath_feed/station_processor.hpp>

namespace snc::mpath_feed
{

Station_Control::Station_Control (
    const sev::logt::Reference & log_parent_n,
    const Station_Contructor_Handle & station_constructor_n,
    Station_Control * station_prev_n,
    sev::thread::Tracker * thread_tracker_n,
    const std::function< void () > & event_callback_n )
: _log ( log_parent_n,
         sev::string::cat ( "Station[", station_constructor_n->name (), "]" ) )
, _station_constructor ( station_constructor_n )
, _station_prev ( station_prev_n )
{
  _epools.pool< event::Signal > ().set_size ( 2 );

  auto queue = sev::event::queue_io::Reference_2::created ();

  // Setup queue connection
  _connection.set_incoming_notifier ( event_callback_n );
  _connection.set_push_notifier ( event_callback_n );
  _connection.connect ( queue.link_a () );

  _thread = std::make_unique< Station_Processor > (
      _log, thread_tracker_n, _station_constructor, queue.link_b () );

  // Connect to previous station
  if ( _station_prev != nullptr ) {
    auto queue = sev::event::queue::Reference::created ();
    _station_prev->set_cargo_sink_queue ( queue );
    set_cargo_source_queue ( queue );
  }
}

Station_Control::~Station_Control () = default;

void
Station_Control::set_cargo_source_queue (
    const sev::event::queue::Reference & queue_n )
{
  if ( _thread ) {
    _thread->set_source_queue ( queue_n );
  }
}

void
Station_Control::set_cargo_sink_queue (
    const sev::event::queue::Reference & queue_n )
{
  if ( _thread ) {
    _thread->set_sink_queue ( queue_n );
  }
}

void
Station_Control::start_thread ( const Job_Handle & job_n )
{
  if ( !_thread ) {
    return;
  }
  _state.clear ();
  _state.set ( Station_Control::State::RUNNING );
  _thread->start ( job_n );
}

void
Station_Control::join_thread ()
{
  if ( !_thread ) {
    return;
  }
  _log.cat ( sev::logt::FL_DEBUG_0, "Join thread begin." );
  _thread->join ();
  _log.cat ( sev::logt::FL_DEBUG_0, "Join thread done." );
}

void
Station_Control::abort ()
{
  if ( !_state.is_empty () ) {
    _state.set ( Station_Control::State::STREAM_ABORT );
  }
}

bool
Station_Control::ready_to_end () const
{
  return _state.is_empty () && _connection.all_empty () &&
         _epools.tracker ().all_home ();
}

void
Station_Control::send_signal ( event::Signal_Type signal_n )
{
  auto * event = _epools.pool< event::Signal > ().acquire ();
  event->set_signal ( signal_n );
  _connection.push ( event );
}

void
Station_Control::events ()
{
  // Feed queues
  _connection.feed_queue ();

  // Release returned events
  _connection.out ().release_all_with_reset ();

  // Process new events
  _connection.in ().process_all ( [ this ] (
                                      const sev::event::Event & event_n ) {
    DEBUG_ASSERT ( event_n.type () == event::Signal::etype );
    auto & cevent = static_cast< const event::Signal & > ( event_n );
    switch ( cevent.signal () ) {
    case event::Signal_Type::STREAM_END: {
      _log.cat ( sev::logt::FL_DEBUG_0, "Thread signal: Stream end" );
      _state.set ( Station_Control::State::THREAD_ABORT );
    } break;

    case event::Signal_Type::THREAD_ABORT_DONE: {
      _log.cat ( sev::logt::FL_DEBUG_0, "Thread signal: Thread abort done" );
      DEBUG_ASSERT ( _state.test_any ( Station_Control::State::THREAD_ABORT ) );
      _state.set ( Station_Control::State::THREAD_ABORT_DONE );
    } break;

    default:
      DEBUG_ASSERT ( false );
      break;
    }
  } );
}

void
Station_Control::process ()
{
  if ( _state.is_empty () ) {
    return;
  }

  using State = Station_Control::State;

  if ( !_state.test_any ( State::THREAD_ABORT ) ) {
    if ( _state.test_any ( State::STREAM_ABORT ) &&
         !_state.test_any ( State::STREAM_ABORT_SENT ) ) {
      _log.cat ( sev::logt::FL_DEBUG_0, "Signal to thread: Stream abort" );
      send_signal ( event::Signal_Type::STREAM_ABORT );
      _state.set ( State::STREAM_ABORT_SENT );
    }
  }

  if ( ( _station_prev == nullptr ) || _station_prev->_state.is_empty () ) {
    if ( _state.test_any ( State::THREAD_ABORT ) &&
         !_state.test_any ( State::THREAD_ABORT_SENT ) ) {
      _log.cat ( sev::logt::FL_DEBUG_0, "Signal to thread: Thread abort" );
      send_signal ( event::Signal_Type::THREAD_ABORT );
      _state.set ( State::THREAD_ABORT_SENT );
    }
  }

  if ( _state.test_any ( State::THREAD_ABORT_DONE ) ) {
    if ( _connection.all_empty () && _epools.tracker ().all_home () ) {
      join_thread ();
      _state.clear ();
    }
  }
}

} // namespace snc::mpath_feed
