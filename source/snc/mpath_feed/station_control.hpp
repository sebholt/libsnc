/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/queue/reference.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/thread/thread.hpp>
#include <snc/mpath_feed/events.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed/station_constructor.hpp>
#include <memory>

namespace snc::mpath_feed
{

// -- Forward declaration
class Station_Processor;

/// @brief Station thread controller
///
class Station_Control
{
  public:
  // -- Types
  struct LEvent
  {
    static constexpr std::uint_fast32_t QUEUE_INCOME = ( 1 << 0 );
  };

  struct State
  {
    static constexpr std::uint_fast32_t RUNNING = ( 1 << 1 );
    static constexpr std::uint_fast32_t STREAM_ABORT = ( 1 << 2 );
    static constexpr std::uint_fast32_t STREAM_ABORT_SENT = ( 1 << 3 );

    static constexpr std::uint_fast32_t THREAD_ABORT = ( 1 << 4 );
    static constexpr std::uint_fast32_t THREAD_ABORT_SENT = ( 1 << 5 );
    static constexpr std::uint_fast32_t THREAD_ABORT_DONE = ( 1 << 6 );
  };

  // -- Construction

  Station_Control ( const sev::logt::Reference & log_parent_n,
                    const Station_Contructor_Handle & station_constructor_n,
                    Station_Control * station_prev_n,
                    sev::thread::Tracker * thread_tracker_n,
                    const std::function< void () > & event_callback_n );

  ~Station_Control ();

  // -- Initialization

  void
  set_cargo_source_queue ( const sev::event::queue::Reference & queue_n );

  void
  set_cargo_sink_queue ( const sev::event::queue::Reference & queue_n );

  // -- Thread session interface

  void
  start_thread ( const Job_Handle & job_n );

  void
  abort ();

  // -- Control interface

  bool
  ready_to_end () const;

  void
  events ();

  void
  process ();

  private:
  // -- Utility

  void
  join_thread ();

  void
  send_signal ( event::Signal_Type signal_n );

  private:
  // -- Attributes
  sev::logt::Context _log;
  sev::mem::Flags_Fast8 _state;
  Station_Contructor_Handle _station_constructor;
  Station_Control * _station_prev = nullptr;
  sev::event::Tracker_Pools< event::Signal > _epools;
  sev::event::queue_io::Connection_2 _connection;
  std::unique_ptr< Station_Processor > _thread;
};

} // namespace snc::mpath_feed
