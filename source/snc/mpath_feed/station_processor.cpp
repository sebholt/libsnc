/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "station_processor.hpp"
#include <sev/math/exponents.hpp>
#include <sev/string/utility.hpp>
#include <stdexcept>

namespace snc::mpath_feed
{

Station_Processor::Station_Processor (
    const sev::logt::Reference & log_parent_n,
    sev::thread::Tracker * thread_tracker_n,
    const Station_Contructor_Handle & station_constructor_n,
    sev::event::queue_io::Link_2 front_link_n )
: _log ( log_parent_n, "T" )
, _station_constructor ( station_constructor_n )
, _thread ( sev::string::cat ( "MPath-Feed-", station_constructor_n->name () ),
            thread_tracker_n )
{
  // -- Preallocate events
  _epools.pool< event::Signal > ().set_size ( 2 );

  // -- Cargo queue connections
  _source_connection.set_incoming_notifier (
      [ &ea = _event_waiter ] () { ea.set ( LEvent::CARGO_IN ); } );
  _sink_connection.set_push_notifier (
      [ &le = _loop_events ] () { le.set ( LEvent::CARGO_OUT ); } );

  // -- Control connection
  _control_connection.set_incoming_notifier (
      [ &ea = _event_waiter ] () { ea.set ( LEvent::CONTROL_IN ); } );
  _control_connection.set_push_notifier (
      [ &le = _loop_events ] () { le.set ( LEvent::CONTROL_OUT ); } );
  _control_connection.connect ( front_link_n );
}

Station_Processor::~Station_Processor ()
{
  join ();
}

void
Station_Processor::set_source_queue (
    const sev::event::queue::Reference & queue_n )
{
  _source_connection.connect ( queue_n );
}

void
Station_Processor::set_sink_queue (
    const sev::event::queue::Reference & queue_n )
{
  _sink_connection.connect ( queue_n );
}

void
Station_Processor::start ( const Job_Handle & job_n )
{
  if ( _thread.is_running () ) {
    return;
  }

  DEBUG_ASSERT ( _control_connection.is_connected () );
  _log.cat ( sev::logt::FL_DEBUG_0, "Opening station" );
  _job = job_n;
  // Start thread
  _thread.start ( [ this ] () { this->operator() (); } );
}

void
Station_Processor::join ()
{
  if ( _thread.is_running () ) {
    _thread.join ();
  }
}

void
Station_Processor::operator() ()
{
  // Create and open station
  {
    _log.cat ( sev::logt::FL_DEBUG_0, "Opening station" );
    _station = _station_constructor->make ( Station_Context ( _log ) );
    if ( !_station ) {
      throw std::runtime_error ( "Station construction failed" );
    }
    _station->open (
        _job, [ &ea = _event_waiter ] () { ea.set ( LEvent::STATION ); } );
  }

  // Request initial station poll
  _loop_events.set ( LEvent::STATION );

  _log.cat ( sev::logt::FL_DEBUG_0, "Main loop begin." );
  while ( true ) {

    // -- Wait for events
    _loop_events.set (
        _event_waiter.fetch_and_clear_wait ( _loop_events.is_empty () ) );
    _loop_events.unset ( LEvent::STATION | LEvent::LOOP_AGAIN );

    // -- Controll event processing
    if ( _loop_events.test_any ( LEvent::CONTROL_IN | LEvent::CONTROL_OUT ) ) {
      _control_connection.feed_queue (
          _loop_events, LEvent::CONTROL_IN, LEvent::CONTROL_OUT );
      process_control_events ();
    }

    // -- Read incoming cargo queue
    if ( _loop_events.test_any_unset ( LEvent::CARGO_IN ) ) {
      _source_connection.read_from_queue ();
    }

    // -- Write outgoing cargo queue
    if ( _loop_events.test_any_unset ( LEvent::CARGO_OUT ) ) {
      _sink_connection.feed_into_queue ();
    }

    // -- Central processing
    process_station ();

    // -- Shut down
    if ( !_abort_state.is_empty () ) {
      if ( process_shut_down () ) {
        break;
      }
    }
  }
  _log.cat ( sev::logt::FL_DEBUG_0, "Main loop done." );

  // Clean up
  _abort_state.clear ();
  _station_done.clear ();
  _station.reset ();
  _job.reset ();
}

bool
Station_Processor::process_shut_down ()
{
  if ( !_station_done.test_any ( Station_Done::DONE_ACKED ) ||
       !_epools.tracker ().all_home () || !_source_connection.is_empty () ||
       !_sink_connection.is_empty () || !_control_connection.all_empty () ) {
    return false;
  }

  if ( !_abort_state.test_any_set ( Abort_State::ABORT_ACKED ) ) {
    // Send abort done event
    _log.cat ( sev::logt::FL_DEBUG_0, "Signal to control: Thread abort done" );
    control_signal_send ( event::Signal_Type::THREAD_ABORT_DONE );
    return false;
  }

  return true;
}

void
Station_Processor::process_station ()
{
  // Regular station processing
  if ( _station_done.is_empty () ) {
    do {
      Station::Token token ( _station->read_next () );
      switch ( token ) {
      case Station::Token::FRONT_FEED:
        if ( station_front_feed () ) {
          continue;
        }
        break;
      case Station::Token::BACK_READ:
        station_back_read ();
        continue;
        break;
      case Station::Token::DONE:
        // Station done
        _station_done.set ( Station_Done::DONE );
        // Close station
        _station->close ();
        break;
      default:
        break;
      }

      // Leave loop by default
      break;
    } while ( true );
  }

  // Finished station processing
  if ( !_station_done.is_empty () ) {
    // Check if end of stream was forwarded to the controller
    if ( !_station_done.test_any_set ( Station_Done::DONE_ACKED ) ) {
      _log.cat ( sev::logt::FL_DEBUG_0, "Signal to control: Stream end" );
      control_signal_send ( event::Signal_Type::STREAM_END );
    }

    // Return unprocessed cargo
    {
      auto channel = _source_connection.channel ();
      while ( !channel.is_empty () ) {
        // Release cargo from cargo events
        auto * cargo =
            static_cast< cargo::Event * > ( channel.pop_not_empty () );
        cargo->release_to_cargo_pool ();
      }
    }
  }
}

inline bool
Station_Processor::station_front_feed ()
{
  // Return station event from source buffer
  auto channel = _source_connection.channel ();
  if ( !channel.is_empty () ) {
    cargo::Event * cargo = static_cast< cargo::Event * > ( channel.front () );
    if ( _station->front_feed ( cargo ) ) {
      // Cargo passed to station
      // Pop event and accept more events
      channel.pop_not_empty ();
      request_processing ();
      return true;
    }
  }
  return false;
}

inline void
Station_Processor::station_back_read ()
{
  // Read station cargo item
  cargo::Event * cargo = _station->back_read ();
  // Forward cargo to sink
  if ( cargo != nullptr ) {
    // Write to buffer
    _sink_connection.push ( cargo );
  }
}

void
Station_Processor::process_control_events ()
{
  // Release returned events
  _control_connection.out ().release_all_with_reset ();

  // Process new events
  _control_connection.in ().process_all (
      [ this ] ( const sev::event::Event & event_n ) {
        DEBUG_ASSERT ( event_n.type () == event::Signal::etype );
        auto & cevent = static_cast< const event::Signal & > ( event_n );
        switch ( cevent.signal () ) {
        case event::Signal_Type::STREAM_ABORT:
          // Log
          _log.cat ( sev::logt::FL_DEBUG_0, "Control signal: Stream abort" );
          _station->abort ();
          request_processing ();
          break;
        case event::Signal_Type::THREAD_ABORT:
          // Log
          _log.cat ( sev::logt::FL_DEBUG_0, "Control signal: Thread abort" );
          _abort_state.set ( Abort_State::ABORT );
          request_processing ();
          break;
        default:
          DEBUG_ASSERT ( false );
          break;
        }
      } );
}

void
Station_Processor::control_signal_send ( event::Signal_Type signal_n )
{
  auto * event = _epools.pool< event::Signal > ().acquire ();
  event->set_signal ( signal_n );
  _control_connection.push ( event );
}

inline void
Station_Processor::request_processing ()
{
  _loop_events.set ( LEvent::LOOP_AGAIN );
}

} // namespace snc::mpath_feed
