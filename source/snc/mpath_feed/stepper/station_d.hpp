/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_feed/cargo/pool_d.hpp>
#include <snc/mpath_feed/station.hpp>
#include <snc/mpath_feed/stepper/curve_stepper_d.hpp>

namespace snc::mpath_feed::stepper
{

/// @brief Stepper station
///
template < std::size_t DIM >
class Station_D : public Station
{
  public:
  // -- Types

  /// @brief Process mode
  enum class PMode : std::uint_fast8_t
  {
    NONE,
    STEP,
    FORWARD
  };

  /// @brief Level for STEP mode
  enum class Step_Level : std::uint_fast8_t
  {
    CALC_STEPS,
    BACK_READ_STEPS
  };

  /// @brief Level for FORWARD mode
  enum class Forward_Level : std::uint_fast8_t
  {
    EVAL,
    BACK_READ_STEPS,
    BACK_READ_ELEM
  };

  // -- Construction

  Station_D ( const Station_Context & context_n,
              const sev::lag::Vector< double, DIM > & step_lengths_n );

  virtual ~Station_D ();

  // -- Abstract interface

  void
  open ( const Job_Handle & job_n,
         std::function< void () > const & notifier_n ) override;

  void
  abort () override;

  void
  close () override;

  Token
  read_next () override;

  bool
  front_feed ( cargo::Event * cargo_n ) override;

  cargo::Event *
  back_read () override;

  private:
  // -- Utility

  void
  clear_pmode ();

  /// @return True to try gagain
  bool
  read_next_pmode ( Station::Token & res_n );

  void
  front_release ();

  void
  front_feed_eval ();

  void
  front_feed_elem ();

  void
  back_read_forward_elem ( cargo::Event *& res_n );

  bool
  back_read_forward_steps ( cargo::Event *& res_n );

  void
  curve_request_finish ();

  void
  curve_step ();

  /// @brief Returns a casted cargo pointer
  ///
  const cargo::Element_D< DIM > *
  cargo_front_elem () const
  {
    return static_cast< const cargo::Element_D< DIM > * > ( _cargo_front );
  }

  private:
  // -- Attributes
  // States
  bool _is_open = false;
  bool _is_aborting = false;
  bool _stream_end_reached = false;
  PMode _pmode = PMode::NONE;
  Step_Level _step_level = Step_Level::CALC_STEPS;
  Forward_Level _forward_level = Forward_Level::EVAL;
  /// @brief Current front source cargo
  cargo::Event * _cargo_front = nullptr;
  /// @brief Current steps tile
  cargo::BSteps< DIM > * _cargo_back_bsteps = nullptr;
  Curve_Stepper_D< DIM > _curve_stepper;
  sev::lag::Vector< double, DIM > _step_lengths;

  snc::mpath_feed::cargo::Pool_D< DIM > _cargo_pool;
};

} // namespace snc::mpath_feed::stepper
