/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "generator.hpp"
#include <sev/assert.hpp>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
Generator< DIM >::Generator ( const Context_D< DIM > & context_n,
                              const sev::logt::Reference & log_parent_n,
                              sev::unicode::View log_name_n )
: _log ( log_parent_n, log_name_n )
, _context ( context_n )
{
}

template < std::size_t DIM >
Generator< DIM >::~Generator ()
{
}

template < std::size_t DIM >
snc::mpath_feed::cargo::Event *
Generator< DIM >::cargo_release ()
{
  DEBUG_ASSERT ( _cargo != nullptr );
  snc::mpath_feed::cargo::Event * res = _cargo;
  _cargo = nullptr;
  return res;
}

template < std::size_t DIM >
void
Generator< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route begin: Job name: " << job_n->name ();
  }
}

template < std::size_t DIM >
void
Generator< DIM >::route_abort ( bool forced_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route abort: Forced: " << forced_n;
  }
}

template < std::size_t DIM >
void
Generator< DIM >::route_end ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Route end." );
}

template < std::size_t DIM >
void
Generator< DIM >::cargo_process_begin ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Cargo process begin." );
}

template < std::size_t DIM >
void
Generator< DIM >::cargo_process_end ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Cargo process end." );
}

template < std::size_t DIM >
void
Generator< DIM >::cargo_process_finish ()
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Cargo process finish." );
}

// -- Instantiation

template class Generator< 1 >;
template class Generator< 2 >;
template class Generator< 3 >;
template class Generator< 4 >;
template class Generator< 5 >;
template class Generator< 6 >;

} // namespace snc::mpath_feed_emb::gen
