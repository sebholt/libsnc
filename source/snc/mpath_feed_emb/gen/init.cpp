/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "init.hpp"
#include <sev/assert.hpp>
#include <sev/string/utility.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
Init< DIM >::Init ( const Context_D< DIM > & context_n,
                    const sev::logt::Reference & log_parent_n )
: Super ( context_n, log_parent_n, sev::string::cat ( "Init<", DIM, ">" ) )
{
}

template < std::size_t DIM >
Init< DIM >::~Init ()
{
}

template < std::size_t DIM >
void
Init< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  Super::route_begin ( job_n );

  _message_submitted = false;
  Super::set_process_token ( Generator_Token::PROCESS );
}

template < std::size_t DIM >
void
Init< DIM >::route_abort ( bool forced_n )
{
  Super::route_abort ( forced_n );

  if ( forced_n ) {
    _message_submitted = true;
    // Forced break releases cargo immediately
    Super::set_cargo ( nullptr );
    Super::set_process_token ( Generator_Token::FINISH );
  }
}

template < std::size_t DIM >
void
Init< DIM >::route_end ()
{
  Super::route_end ();
}

template < std::size_t DIM >
void
Init< DIM >::cargo_process_begin ()
{
  Super::cargo_process_begin ();

  Super::set_process_token ( _message_submitted ? Generator_Token::FINISH
                                                : Generator_Token::PROCESS );
}

template < std::size_t DIM >
void
Init< DIM >::cargo_process_end ()
{
  Super::cargo_process_end ();
}

template < std::size_t DIM >
void
Init< DIM >::cargo_process_finish ()
{
  Super::cargo_process_finish ();

  Super::set_process_token ( Generator_Token::FINISH );
}

template < std::size_t DIM >
bool
Init< DIM >::cargo_feed ( snc::mpath_feed::cargo::Event * cargo_n )
{
  DEBUG_ASSERT ( false );
  (void)cargo_n;
  return false;
}

template < std::size_t DIM >
snc::mpath_feed::cargo::Event *
Init< DIM >::cargo_release ()
{
  if ( _message_submitted ) {
    Super::set_process_token ( Generator_Token::FINISH );
  }
  return Super::cargo_release ();
}

template < std::size_t DIM >
void
Init< DIM >::acquire_router_messages ( Stream_Request * request_n )
{
  if ( !_message_submitted ) {
    auto & message =
        request_n->stream
            .template emplace< snc::emb::msg::out::Stepper_Sync_Config > ();
    message.set_sync_group_index (
        Super::context ().stepper_sync_group_index () );
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      message.register_stepper ( Super::context ().axes_mapping ().map ( ii ) );
    }
    _message_submitted = true;
    // Debug
    Super::log ().cat ( sev::logt::FL_DEBUG_0, "Init submitted" );
  }

  Super::set_process_token ( _message_submitted ? Generator_Token::FINISH
                                                : Generator_Token::PROCESS );
}

// -- Instantiation

template class Init< 1 >;
template class Init< 2 >;
template class Init< 3 >;
template class Init< 4 >;
template class Init< 5 >;
template class Init< 6 >;

} // namespace snc::mpath_feed_emb::gen
