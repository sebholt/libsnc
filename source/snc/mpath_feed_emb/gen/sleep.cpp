/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "sleep.hpp"
#include <sev/assert.hpp>
#include <sev/math/exponents.hpp>
#include <sev/string/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/config/stepper.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <snc/mpath/elem/sleep.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
Sleep< DIM >::Sleep ( const Context_D< DIM > & context_n,
                      const sev::logt::Reference & log_parent_n )
: Super ( context_n, log_parent_n, sev::string::cat ( "Sleep<", DIM, ">" ) )
{
  for ( std::size_t ii = 0; ii != _axis.size (); ++ii ) {
    const auto & dsaxis = *Super::context ().axis_statics ( ii );
    _axis[ ii ].step_splitter.set_usecs_max (
        dsaxis.emb ().stepper_config ()->step_slice_usecs_max () );
    _axis[ ii ].num_usecs_queued_min =
        dsaxis.emb ().stepper_config ()->steps_queue_hint_usecs_min ();
    _axis[ ii ].num_usecs_queued_max =
        dsaxis.emb ().stepper_config ()->steps_queue_hint_usecs_max ();
  }
}

template < std::size_t DIM >
Sleep< DIM >::~Sleep ()
{
}

template < std::size_t DIM >
void
Sleep< DIM >::route_begin ( const snc::mpath_feed::Job_Handle & job_n )
{
  Super::route_begin ( job_n );

  _stage = Stage::DONE;
  _next_axis_index = 0;
}

template < std::size_t DIM >
void
Sleep< DIM >::route_abort ( bool forced_n )
{
  Super::route_abort ( forced_n );

  if ( forced_n ) {
    _stage = Stage::DONE;
    _next_axis_index = 0;
    // Forced break releases cargo immediately
    Super::set_cargo ( nullptr );
    Super::set_process_token ( Generator_Token::FINISH );
  }
}

template < std::size_t DIM >
void
Sleep< DIM >::route_end ()
{
  Super::route_end ();
}

template < std::size_t DIM >
void
Sleep< DIM >::cargo_process_begin ()
{
  Super::cargo_process_begin ();

  Super::set_process_token ( Generator_Token::CARGO_FEED );
}

template < std::size_t DIM >
void
Sleep< DIM >::cargo_process_end ()
{
  Super::cargo_process_end ();
}

template < std::size_t DIM >
void
Sleep< DIM >::cargo_process_finish ()
{
  Super::cargo_process_finish ();

  Super::set_process_token ( Generator_Token::FINISH );
}

template < std::size_t DIM >
bool
Sleep< DIM >::cargo_feed ( snc::mpath_feed::cargo::Event * cargo_n )
{
  if ( cargo_n->cargo_type () != snc::mpath_feed::cargo::Cargo_Type::ELEMENT ) {
    return false;
  }
  if ( ( (snc::mpath_feed::cargo::Element_D< DIM > *)cargo_n )->elem_type () !=
       snc::mpath::elem::Type::SLEEP ) {
    return false;
  }

  Super::set_cargo ( cargo_n );

  // Reset state
  _stage = Stage::SYNC_LOCK_BEGIN;
  _next_axis_index = 0;

  std::uint_fast32_t num_usecs;
  // Acquire microseconds
  {
    typedef const snc::mpath_feed::cargo::Element_D< DIM > & CType;
    CType ccargo = static_cast< CType > ( *Super::cargo () );
    const snc::mpath::Element & elem = ccargo.elem ();
    DEBUG_ASSERT ( elem.elem_type () == snc::mpath::elem::Type::SLEEP );
    const snc::mpath::elem::Sleep & esleep (
        static_cast< const snc::mpath::elem::Sleep & > ( elem ) );
    num_usecs = esleep.usecs ();
  }
  // Fix microseconds
  {
    std::uint_fast32_t num_usecs_min =
        Super::device_statics ()->acquire_step_slice_usecs_min ();
    if ( num_usecs < num_usecs_min ) {
      // Debug
      if ( auto logo = Super::log ().ostr ( sev::logt::FL_WARNING ) ) {
        logo << "Requested usec count " << num_usecs << " is too small. Using "
             << num_usecs_min << " usecs.";
      }
      num_usecs = num_usecs_min;
    }
  }
  for ( auto & item : _axis ) {
    item.step_splitter.setup_step ( snc::emb::type::Step::HOLD, num_usecs );
  }

  Super::set_process_token ( Generator_Token::PROCESS );

  return true;
}

template < std::size_t DIM >
snc::mpath_feed::cargo::Event *
Sleep< DIM >::cargo_release ()
{
  if ( _stage == Stage::DONE ) {
    Super::set_process_token ( Generator_Token::FINISH );
  }
  return Super::cargo_release ();
}

template < std::size_t DIM >
void
Sleep< DIM >::acquire_router_messages ( Stream_Request * request_n )
{
  if ( _stage != Stage::DONE ) {
    _request = request_n;
    while ( true ) {
      bool generated = false;
      switch ( _stage ) {
      case Stage::DONE:
        break;
      case Stage::SYNC_LOCK_BEGIN:
        generated = generate_sync_lock ( request_n, true );
        if ( generated ) {
          _stage = Stage::FIRST_STEPS;
        }
        break;
      case Stage::FIRST_STEPS:
        generated = generate_next_axis_steps ( request_n );
        if ( generated && ( _next_axis_index == 0 ) ) {
          // All axes got fed once
          _stage = Stage::SYNC_LOCK_RELEASE;
        }
        break;
      case Stage::SYNC_LOCK_RELEASE:
        generated = generate_sync_lock ( request_n, false );
        if ( generated ) {
          _stage = Stage::REMAINING_STEPS;
        }
        break;
      case Stage::REMAINING_STEPS:
        generated = generate_next_axis_steps ( request_n );
        if ( axes_finished () ) {
          _stage = Stage::DONE;
        }
        break;
      default:
        DEBUG_ASSERT ( false );
        break;
      }
      if ( !generated ) {
        break;
      }
    }
    _request = 0;
  }
  if ( _stage == Stage::DONE ) {
    Super::set_process_token ( ( Super::cargo () != nullptr )
                                   ? Generator_Token::CARGO_RELEASE
                                   : Generator_Token::FINISH );
  } else {
    Super::set_process_token ( Generator_Token::DEVICE_PROGRESS_WAIT );
  }
}

template < std::size_t DIM >
inline bool
Sleep< DIM >::axes_finished () const
{
  for ( auto & axis : _axis ) {
    if ( !axis.step_splitter.is_finished () ) {
      return false;
    }
  }
  return true;
}

template < std::size_t DIM >
inline bool
Sleep< DIM >::generate_sync_lock ( Stream_Request * request_n, bool install_n )
{
  bool slots_free = true;
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    const auto mindex = Super::context ().axes_mapping ().map ( ii );
    if ( _request->axis_tracker[ mindex ].queue_slots_free () == 0 ) {
      slots_free = false;
      break;
    }
  }
  if ( slots_free ) {
    auto & message =
        request_n->stream
            .template emplace< snc::emb::msg::out::Stepper_Sync > ();
    message.set_sync_group_index (
        Super::context ().stepper_sync_group_index () );
    if ( install_n ) {
      message.set_sync_type ( snc::emb::type::Stepper_Sync::LOCK_INSTALL );
    } else {
      message.set_sync_type ( snc::emb::type::Stepper_Sync::LOCK_RELEASE );
    }
    // Update tracker
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      const auto mindex = Super::context ().axes_mapping ().map ( ii );
      _request->axis_tracker[ mindex ].track ( 1 );
    }
  }
  return slots_free;
}

template < std::size_t DIM >
bool
Sleep< DIM >::generate_next_axis_steps ( Stream_Request * request_n )
{
  bool generated = false;
  {
    const auto mindex =
        Super::context ().axes_mapping ().map ( _next_axis_index );
    Axis & axis = _axis[ _next_axis_index ];
    auto & tracker = _request->axis_tracker[ mindex ];
    if ( ( tracker.queue_slots_free () != 0 ) &&
         ( tracker.queue_usecs () < axis.num_usecs_queued_max ) ) {
      snc::emb::Step_Splitter & step_splitter = axis.step_splitter;
      if ( !step_splitter.is_finished () ) {
        // Acquire message
        auto & message =
            request_n->stream
                .template emplace< snc::emb::msg::out::Steps_U16_64 > ();
        message.set_stepper_index ( mindex );
        DEBUG_ASSERT ( step_splitter.job_type () !=
                       snc::emb::type::Step::INVALID );
        DEBUG_ASSERT ( step_splitter.job_type () <
                       snc::emb::type::Step::LIST_END );
        message.set_job_type ( step_splitter.job_type () );
        message.push_usecs ( step_splitter.acquire_usecs_next () );

        // Update tracker
        tracker.track ( message );
        generated = true;
      }

      // Increment axis index
      ++_next_axis_index;
      _next_axis_index %= DIM;
    }
  }
  return generated;
}

// -- Instantiation

template class Sleep< 1 >;
template class Sleep< 2 >;
template class Sleep< 3 >;
template class Sleep< 4 >;
template class Sleep< 5 >;
template class Sleep< 6 >;

} // namespace snc::mpath_feed_emb::gen
