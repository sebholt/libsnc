/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/step_splitter.hpp>
#include <snc/mpath_feed_emb/gen/generator.hpp>
#include <algorithm>
#include <array>
#include <cstdint>

namespace snc::mpath_feed_emb::gen
{

template < std::size_t DIM >
class Sleep : public Generator< DIM >
{
  // -- Types
  private:
  using Super = Generator< DIM >;
  using Stream_Request = typename Super::Stream_Request;

  public:
  enum class Stage
  {
    DONE,
    SYNC_LOCK_BEGIN,
    FIRST_STEPS,
    SYNC_LOCK_RELEASE,
    REMAINING_STEPS
  };

  struct Axis
  {
    snc::emb::Step_Splitter step_splitter;
    std::uint_fast32_t num_usecs_queued_min;
    std::uint_fast32_t num_usecs_queued_max;
  };

  // -- Construction

  Sleep ( const Context_D< DIM > & context_n,
          const sev::logt::Reference & log_parent_n );

  ~Sleep ();

  // -- Generator interface

  void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n ) override;

  void
  route_abort ( bool forced_n ) override;

  void
  route_end () override;

  void
  cargo_process_begin () override;

  void
  cargo_process_end () override;

  void
  cargo_process_finish () override;

  bool
  cargo_feed ( snc::mpath_feed::cargo::Event * tile_n ) override;

  snc::mpath_feed::cargo::Event *
  cargo_release () override;

  void
  acquire_router_messages ( Stream_Request * request_n ) override;

  private:
  // -- Utility

  bool
  axes_finished () const;

  bool
  generate_sync_lock ( Stream_Request * request_n, bool install_n );

  bool
  generate_next_axis_steps ( Stream_Request * request_n );

  private:
  // -- Attributes
  Stage _stage = Stage::DONE;

  Stream_Request * _request;
  std::uint_fast32_t _next_axis_index = 0;
  std::array< Axis, DIM > _axis;
};

} // namespace snc::mpath_feed_emb::gen
