/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed_emb/context_d.hpp>
#include <snc/mpath_feed_emb/gen/control_bool.hpp>
#include <snc/mpath_feed_emb/gen/generator.hpp>
#include <snc/mpath_feed_emb/gen/init.hpp>
#include <snc/mpath_feed_emb/gen/sleep.hpp>
#include <snc/mpath_feed_emb/gen/steps.hpp>
#include <snc/mpath_feed_emb/stream_read_breaker_d.hpp>
#include <snc/mpath_feed_emb/stream_request_d.hpp>
#include <snc/utility/debug_file.hpp>

namespace snc::mpath_feed_emb
{

/// @brief Transforms MPath elements and Cargo steps into
///        embedded device commands
///
///
/// --- Regular processing
///
/// - begin()
///  - loop
///   - messages_acquire_begin()
///    - read_next()
///    - messages_acquire_process()
///    - cargo_feed() / cargo_release()
///   - messages_acquire_end()
/// - end()
template < std::size_t DIM >
class Muxer_D
{
  public:
  // -- Types

  using Stream_Request = snc::mpath_feed_emb::Stream_Request_D< DIM >;

  enum class Token
  {
    NONE,
    CARGO_FEED,
    CARGO_RELEASE,
    PROCESS
  };

  struct Abort_State
  {
    static constexpr std::uint_fast32_t ENABLED = ( 1 << 0 );
    static constexpr std::uint_fast32_t FORCED = ( 1 << 1 );
    static constexpr std::uint_fast32_t IFEED = ( 1 << 2 );
    static constexpr std::uint_fast32_t IFEED_DONE = ( 1 << 3 );
  };

  // -- Construction

  Muxer_D ( const Context_D< DIM > & context_n,
            const sev::logt::Reference & log_parent_n );

  ~Muxer_D ();

  // -- Accessors

  const Context_D< DIM > &
  context () const
  {
    return _context;
  }

  // -- Walk runtime

  void
  route_begin ( const snc::mpath_feed::Job_Handle & job_n );

  void
  route_end ();

  /// @brief Switch to break walk mode
  ///
  void
  route_abort ( bool forced_n );

  // -- Router messages acquiring

  void
  messages_acquire_begin ( Stream_Request * request_n );

  void
  messages_acquire_process ();

  void
  messages_acquire_end ();

  // -- Processing during message acquiring

  // @brief Processing request token
  //
  Token
  read_next () const
  {
    return _process_token;
  }

  // - Cargo feed / release

  bool
  cargo_feed ( snc::mpath_feed::cargo::Event * tile_n );

  snc::mpath_feed::cargo::Event *
  cargo_release ();

  // -- State

  bool
  is_running () const
  {
    return _is_running;
  }

  /// @brief No generator running
  bool
  is_idling () const
  {
    return ( _gen_current == nullptr );
  }

  bool
  is_breaking () const
  {
    return !_abort_state.is_empty ();
  }

  bool
  is_forced_breaking () const
  {
    return _abort_state.test_any ( Abort_State::FORCED );
  }

  bool
  is_finished_aborting () const
  {
    return ( is_idling () &&
             _abort_state.test_any ( Abort_State::IFEED_DONE ) );
  }

  private:
  // -- Utility

  void
  set_process_token ( Token token_n );

  /// @return True if the cargo was accepted
  bool
  cargo_feed_generators ( snc::mpath_feed::cargo::Event * cargo_n );

  void
  release_current_generator ();

  void
  internal_stream_start ();

  void
  internal_cargo_feed ();

  void
  internal_cargo_release ();

  private:
  // -- Logging
  sev::logt::Context _log;

  // -- State
  bool _is_running = false;
  sev::mem::Flags_Fast32 _abort_state;
  Token _process_token = Token::NONE;
  Stream_Request * _sequence_request = nullptr;
  snc::mpath_feed::cargo::Event * _cargo_feed = nullptr;
  gen::Generator< DIM > * _gen_current = nullptr;

  // -- Context
  const Context_D< DIM > _context;

  // -- Generators
  std::array< gen::Generator< DIM > *, 4 > _generators;
  gen::Steps< DIM > _gen_stepper;
  gen::Sleep< DIM > _gen_sleep;
  gen::Control_Bool< DIM > _gen_control_bool;
  gen::Init< DIM > _gen_init;

  // -- Internal break stream
  snc::mpath_feed::cargo::Element_D< DIM > _cargo_elem_break;
  Stream_Read_Breaker_D< DIM > _stream_breaker;

  // -- Debugging
  snc::utility::Debug_File _debug_file_cargo_feed;
};

} // namespace snc::mpath_feed_emb
