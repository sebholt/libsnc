/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stream_read_breaker_d.hpp"
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <chrono>
#include <utility>

namespace snc::mpath_feed_emb
{

template < std::size_t DIM >
Stream_Read_Breaker_D< DIM >::Stream_Read_Breaker_D (
    snc::device::statics::handle::Statics device_statics_n )
: _device_statics ( std::move ( device_statics_n ) )
{
}

template < std::size_t DIM >
void
Stream_Read_Breaker_D< DIM >::setup ( bool stop_cutter_n )
{
  _pstate.set ( PState::CUTTER_STOP, stop_cutter_n );
  _pstate.set ( PState::CUTTER_SLEEP, stop_cutter_n );
}

template < std::size_t DIM >
bool
Stream_Read_Breaker_D< DIM >::open ()
{
  if ( is_open () ) {
    return true;
  }

  set_state_good ();
  return is_open ();
}

template < std::size_t DIM >
void
Stream_Read_Breaker_D< DIM >::close ()
{
  if ( !is_open () ) {
    return;
  }

  _pstate.clear ();
  set_state_closed ();
}

template < std::size_t DIM >
const snc::mpath::Element *
Stream_Read_Breaker_D< DIM >::read ()
{
  DEBUG_ASSERT ( is_good () );

  if ( _pstate.test_any ( PState::CUTTER_STOP | PState::CUTTER_SLEEP ) ) {
    // Cutter statics
    const auto ctl_bool =
        _device_statics->controls_i1 ().get_by_key ( "cutter" );

    // Cutter stop
    if ( _pstate.test_any_unset ( PState::CUTTER_STOP ) ) {
      if ( ctl_bool ) {
        auto & elem = _elem_variant.emplace< snc::mpath::elem::Fader > ();
        elem.set_fader_bits ( 1 );
        elem.set_fader_index ( ctl_bool->emb ().control_index () );
        elem.fader_value_ref ().set_uint8 ( 0, ctl_bool->emb ().inverted () );
        return &elem;
      }
    }

    // Cutter cool down wait
    if ( _pstate.test_any_unset ( PState::CUTTER_SLEEP ) ) {
      const auto dur = ctl_bool->cool_down_duration ();
      if ( dur.count () != 0 ) {
        auto dur_us = std::chrono::ceil< std::chrono::microseconds > ( dur );
        return &_elem_variant.emplace< snc::mpath::elem::Sleep > (
            dur_us.count () );
      }
    }
  }

  // Stream end
  set_state_finished ();
  return &_elem_variant.emplace< snc::mpath::elem::Stream_End > ();
}

// -- Instantiation

template class Stream_Read_Breaker_D< 1 >;
template class Stream_Read_Breaker_D< 2 >;
template class Stream_Read_Breaker_D< 3 >;
template class Stream_Read_Breaker_D< 4 >;
template class Stream_Read_Breaker_D< 5 >;
template class Stream_Read_Breaker_D< 6 >;

} // namespace snc::mpath_feed_emb
