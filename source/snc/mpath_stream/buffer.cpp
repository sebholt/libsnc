/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "buffer.hpp"
#include <sev/assert.hpp>
#include <algorithm>

namespace snc::mpath_stream
{

Buffer::Buffer ()
{
  _rails.set_rail_capacity ( default_rail_capacity );
  _rails.set_pool_capacity ( default_pool_capacity );
}

void
Buffer::clear ()
{
  _rails.clear ();
}

} // namespace snc::mpath_stream
