/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Statistics.hpp"

namespace snc::mpath_stream::qt
{

Statistics::Statistics ( QObject * qparent_n )
: QObject ( qparent_n )
, _axes ( this )
{
}

Statistics::~Statistics () = default;

void
Statistics::clear ()
{
  _axes.setDimensions ( 0 );
  set_length ( 0.0 );
}

void
Statistics::reset ()
{
  _axes.reset ();
  set_length ( 0.0 );
}

void
Statistics::update ( const snc::mpath_stream::Statistics_D2 & stats_n )
{
  updateImpl ( stats_n );
}

void
Statistics::update ( const snc::mpath_stream::Statistics_D3 & stats_n )
{
  updateImpl ( stats_n );
}

void
Statistics::update ( const snc::mpath_stream::Statistics_D4 & stats_n )
{
  updateImpl ( stats_n );
}

template < std::size_t DIM >
void
Statistics::updateImpl (
    const snc::mpath_stream::Statistics_D< DIM > & stats_n )
{
  // -- Rebuild axes
  _axes.ensure_dimensions_min ( DIM );

  // -- Setup axes
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    auto & axis = _axes[ ii ];
    axis.setMinMax ( stats_n.curve_bbox[ 0 ][ ii ],
                     stats_n.curve_bbox[ 1 ][ ii ] );
  }
  // -- Setup values
  set_length ( stats_n.length );
}

void
Statistics::set_length ( double length_n )
{
  if ( _length == length_n ) {
    return;
  }
  _length = length_n;
  emit lengthChanged ();
}

} // namespace snc::mpath_stream::qt
