/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_stream/statistics_d.hpp>
#include <QObject>
#include <cstdint>
#include <memory>

namespace snc::mpath_stream::qt
{

class StatisticsAxis : public QObject
{
  Q_OBJECT

  // -- Types

  Q_PROPERTY ( double min READ min NOTIFY minChanged )
  Q_PROPERTY ( double max READ max NOTIFY maxChanged )
  Q_PROPERTY ( double length READ length NOTIFY lengthChanged )

  public:
  // -- Construction

  StatisticsAxis ( QObject * qparent_n = nullptr );

  ~StatisticsAxis ();

  // -- Setup

  void
  reset ();

  void
  setMinMax ( double min_n, double max_n );

  // -- Min

  double
  min () const
  {
    return _min;
  }

  Q_SIGNAL
  void
  minChanged ();

  // -- Max

  double
  max () const
  {
    return _max;
  }

  Q_SIGNAL
  void
  maxChanged ();

  // -- Length

  double
  length () const
  {
    return _length;
  }

  Q_SIGNAL
  void
  lengthChanged ();

  private:
  // -- Attributes
  double _min = 0.0;
  double _max = 0.0;
  double _length = 0.0;
};

} // namespace snc::mpath_stream::qt
