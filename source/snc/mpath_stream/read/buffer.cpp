/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "buffer.hpp"
#include <sev/assert.hpp>
#include <snc/mpath/elem/stream_end.hpp>

namespace
{
const snc::mpath::elem::Stream_End _elem_stream_end;
}

namespace snc::mpath_stream::read
{

Buffer::Buffer () {}

Buffer::Buffer ( const snc::mpath_stream::Buffer * buffer_n )
: _buffer ( buffer_n )
{
}

void
Buffer::set_buffer ( const snc::mpath_stream::Buffer * buffer_n )
{
  close ();
  _buffer = buffer_n;
}

bool
Buffer::open ()
{
  if ( is_open () ) {
    return true;
  }
  if ( _buffer == nullptr ) {
    return false;
  }
  _it_read = _buffer->begin ();
  _it_end = _buffer->end ();
  set_state_good ();
  return is_open ();
}

void
Buffer::close ()
{
  if ( !is_open () ) {
    return;
  }
  _it_read.reset ();
  _it_end.reset ();
  set_state_closed ();
}

const snc::mpath::Element *
Buffer::read ()
{
  DEBUG_ASSERT ( is_open () );

  if ( _it_read != _it_end ) {
    auto * res = _it_read.item ();
    ++_it_read;
    if ( _it_read == _it_end ) {
      set_state_finished ();
    }
    return res;
  }

  set_state_finished ();
  return &_elem_stream_end;
}

} // namespace snc::mpath_stream::read
