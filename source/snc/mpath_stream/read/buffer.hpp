/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/read.hpp>

namespace snc::mpath_stream::read
{

class Buffer : public snc::mpath_stream::Read
{
  public:
  // -- Construction

  Buffer ();

  Buffer ( const snc::mpath_stream::Buffer * buffer_n );

  // -- Buffer

  const snc::mpath_stream::Buffer *
  buffer () const
  {
    return _buffer;
  }

  void
  set_buffer ( const snc::mpath_stream::Buffer * buffer_n );

  // -- Stream interface

  bool
  open () override;

  void
  close () override;

  const snc::mpath::Element *
  read () override;

  private:
  // -- Attributes
  snc::mpath_stream::Buffer::const_Iterator _it_read;
  snc::mpath_stream::Buffer::const_Iterator _it_end;
  const snc::mpath_stream::Buffer * _buffer = nullptr;
};

} // namespace snc::mpath_stream::read
