/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "statistics_d.hpp"

namespace snc::mpath_stream
{

template < std::size_t DIM >
Statistics_D< DIM >::Statistics_D ()
{
  curve_pos_begin.fill ( 0.0 );
  curve_pos_end.fill ( 0.0 );
  curve_bbox[ 0 ].fill ( 0.0 );
  curve_bbox[ 1 ].fill ( 0.0 );
}

template < std::size_t DIM >
void
Statistics_D< DIM >::reset ()
{
  num_elements = 0;
  num_elements_curve = 0;
  length = 0.0;
  curve_pos_begin.fill ( 0.0 );
  curve_pos_end.fill ( 0.0 );
  curve_bbox[ 0 ].fill ( 0.0 );
  curve_bbox[ 1 ].fill ( 0.0 );
}

// -- Instantiation

template class Statistics_D< 1 >;
template class Statistics_D< 2 >;
template class Statistics_D< 3 >;
template class Statistics_D< 4 >;
template class Statistics_D< 5 >;
template class Statistics_D< 6 >;

} // namespace snc::mpath_stream
