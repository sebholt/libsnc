/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/mpath_stream/buffer.hpp>
#include <snc/mpath_stream/write.hpp>

namespace snc::mpath_stream::write
{

class Buffer : public snc::mpath_stream::Write
{
  public:
  // -- Construction

  Buffer ();

  Buffer ( snc::mpath_stream::Buffer * buffer_n );

  // -- Buffer

  snc::mpath_stream::Buffer *
  buffer () const
  {
    return _buffer;
  }

  void
  set_buffer ( snc::mpath_stream::Buffer * buffer_n );

  // --- Abstract interface

  bool
  open () override;

  bool
  is_open () override;

  bool
  write ( const snc::mpath::Element & elem_n ) override;

  void
  close () override;

  private:
  // -- Attributes
  bool _is_open = false;
  snc::mpath_stream::Buffer * _buffer = nullptr;
};

} // namespace snc::mpath_stream::write
