/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "statistics_d.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <snc/mpath/elem/curve_d.hpp>
#include <snc/mpath/elem/type_test.hpp>
#include <snc/mpath/elements_common.hpp>

namespace snc::mpath_stream::write
{

template < std::size_t DIM >
Statistics_D< DIM >::Statistics_D ()
: _vpos_cur ( sev::lag::init::zero )
{
}

template < std::size_t DIM >
bool
Statistics_D< DIM >::open ()
{
  if ( !_is_open ) {
    _is_open = true;
    _stats.reset ();
    _vpos_cur.fill ( 0.0 );
    _first_curve = true;
  }
  return _is_open;
}

template < std::size_t DIM >
bool
Statistics_D< DIM >::is_open ()
{
  return _is_open;
}

template < std::size_t DIM >
void
Statistics_D< DIM >::close ()
{
  _is_open = false;
}

template < std::size_t DIM >
bool
Statistics_D< DIM >::write ( const snc::mpath::Element & elem_n )
{
  if ( !_is_open ) {
    return false;
  }

  if ( snc::mpath::elem::is_curve< DIM > ( elem_n ) ) {
    {
      auto & curve_dim =
          static_cast< const snc::mpath::elem::Curve_D< DIM > & > ( elem_n );

      _eval_buffer.new_from_curve ( _vpos_cur, curve_dim );
      _eval_buffer.evaluator ().calc_end_pos ( _vpos_cur );

      if ( _first_curve ) {
        _first_curve = false;
        // The first curve element is invalid
        // except for it's end point
        _stats.curve_pos_begin = _vpos_cur;
        // Update bounding box
        _stats.curve_bbox[ 0 ] = _vpos_cur;
        _stats.curve_bbox[ 1 ] = _vpos_cur;
      } else {
        _stats.curve_pos_end = _vpos_cur;
        _stats.length += _eval_buffer.evaluator ().calc_length ();
        // Update bounding box
        {
          sev::lag::Vector< double, DIM > vmin;
          sev::lag::Vector< double, DIM > vmax;
          _eval_buffer.evaluator ().calc_extents ( vmin, vmax );
          for ( std::size_t ii = 0; ii != DIM; ++ii ) {
            sev::math::assign_smaller ( _stats.curve_bbox[ 0 ].xr ( ii ),
                                        vmin[ ii ] );
          }
          for ( std::size_t ii = 0; ii != DIM; ++ii ) {
            sev::math::assign_larger ( _stats.curve_bbox[ 1 ].xr ( ii ),
                                       vmax[ ii ] );
          }
        }
      }
    }
    ++_stats.num_elements_curve;
  }
  ++_stats.num_elements;
  return true;
}

// -- Instantiation

template class Statistics_D< 1 >;
template class Statistics_D< 2 >;
template class Statistics_D< 3 >;
template class Statistics_D< 4 >;
template class Statistics_D< 5 >;
template class Statistics_D< 6 >;

} // namespace snc::mpath_stream::write
