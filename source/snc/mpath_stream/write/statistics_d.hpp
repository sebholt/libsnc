/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector.hpp>
#include <snc/mpath/curve_evaluator_buffer_d.hpp>
#include <snc/mpath_stream/statistics_d.hpp>
#include <snc/mpath_stream/write.hpp>

namespace snc::mpath_stream::write
{

template < std::size_t DIM >
class Statistics_D : public snc::mpath_stream::Write
{
  public:
  // -- Construction

  Statistics_D ();

  // -- Accessors

  const snc::mpath_stream::Statistics_D< DIM > &
  stats () const
  {
    return _stats;
  }

  // -- Abstract interface

  bool
  open () override;

  bool
  is_open () override;

  bool
  write ( const snc::mpath::Element & elem_n ) override;

  void
  close () override;

  private:
  // -- Attributes
  bool _is_open = false;
  bool _first_curve = true;
  snc::mpath_stream::Statistics_D< DIM > _stats;
  sev::lag::Vector< double, DIM > _vpos_cur;
  snc::mpath::Curve_Evaluator_Buffer_D< DIM > _eval_buffer;
};

// -- Types

using Statistics_D1 = Statistics_D< 1 >;
using Statistics_D2 = Statistics_D< 2 >;
using Statistics_D3 = Statistics_D< 3 >;
using Statistics_D4 = Statistics_D< 4 >;
using Statistics_D5 = Statistics_D< 5 >;
using Statistics_D6 = Statistics_D< 6 >;

} // namespace snc::mpath_stream::write
