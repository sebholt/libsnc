/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "tee.hpp"
#include <sev/assert.hpp>

namespace snc::mpath_stream::write
{

template < std::size_t TNUM >
Tee< TNUM >::Tee ()
{
  reset ();
}

template < std::size_t TNUM >
void
Tee< TNUM >::reset ()
{
  _ostreams.fill ( nullptr );
}

template < std::size_t TNUM >
bool
Tee< TNUM >::open ()
{
  bool any_open = false;
  bool all_open = true;
  for ( Write * item : _ostreams ) {
    if ( item != nullptr ) {
      if ( item->open () ) {
        any_open = true;
      } else {
        all_open = false;
        break;
      }
    }
  }
  if ( any_open ) {
    if ( all_open ) {
      return true;
    } else {
      close ();
    }
  }
  return false;
}

template < std::size_t TNUM >
bool
Tee< TNUM >::is_open ()
{
  bool any_open = false;
  bool all_open = true;
  for ( Write * item : _ostreams ) {
    if ( item != nullptr ) {
      if ( item->is_open () ) {
        any_open = true;
      } else {
        all_open = false;
        break;
      }
    }
  }
  return ( any_open && all_open );
}

template < std::size_t TNUM >
void
Tee< TNUM >::close ()
{
  for ( Write * item : _ostreams ) {
    if ( item != nullptr ) {
      item->close ();
    }
  }
}

template < std::size_t TNUM >
bool
Tee< TNUM >::write ( const snc::mpath::Element & elem_n )
{
  bool res = true;
  for ( Write * item : _ostreams ) {
    if ( item != nullptr ) {
      if ( !item->write ( elem_n ) ) {
        res = false;
      }
    }
  }
  return res;
}

// -- Instantiation

template class Tee< 1 >;
template class Tee< 2 >;
template class Tee< 3 >;
template class Tee< 4 >;
template class Tee< 5 >;
template class Tee< 6 >;

} // namespace snc::mpath_stream::write
