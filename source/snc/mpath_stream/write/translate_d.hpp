/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector_class.hpp>
#include <snc/mpath/element_buffer_d.hpp>
#include <snc/mpath_stream/write.hpp>

namespace snc::mpath_stream::write
{

template < std::size_t DIM >
class Translate_D : public snc::mpath_stream::Write
{
  public:
  // -- Construction and setup

  Translate_D ();

  void
  reset ();

  // -- Stream target

  snc::mpath_stream::Write *
  stream_target () const
  {
    return _stream_target;
  }

  void
  set_stream_target ( snc::mpath_stream::Write * stream_target_n );

  void
  clear_stream_target ();

  // -- Translation

  const sev::lag::Vector< double, DIM > &
  translation () const
  {
    return _translation;
  }

  void
  set_translation ( const sev::lag::Vector< double, DIM > & pos_n );

  void
  reset_translation ();

  // --- Abstract interface

  bool
  open () override;

  bool
  is_open () override;

  bool
  write ( const snc::mpath::Element & elem_n ) override;

  void
  close () override;

  private:
  // -- Attributes
  bool _is_open = false;
  snc::mpath_stream::Write * _stream_target = nullptr;
  sev::lag::Vector< double, DIM > _translation;
  snc::mpath::Element_Buffer_D< DIM > _elem_buffer;
};

// -- Types

using Translate_D1 = Translate_D< 1 >;
using Translate_D2 = Translate_D< 2 >;
using Translate_D3 = Translate_D< 3 >;
using Translate_D4 = Translate_D< 4 >;
using Translate_D5 = Translate_D< 5 >;
using Translate_D6 = Translate_D< 6 >;

} // namespace snc::mpath_stream::write
