/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "VectorModel.hpp"

namespace snc::qt
{

VectorModelItem::VectorModelItem ( QObject * qparent_n, double value_n )
: QObject ( qparent_n )
, _value ( value_n )
{
}

VectorModelItem::~VectorModelItem () = default;

void
VectorModelItem::setValue ( double value_n )
{
  if ( _value == value_n ) {
    return;
  }
  _value = value_n;
  emit valueChanged ();
}

VectorModel::VectorModel ( QObject * qparent_n )
: QAbstractListModel ( qparent_n )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::axisObject, "axisObject" );
}

VectorModel::~VectorModel () = default;

void
VectorModel::ensure_dimensions_min ( std::size_t dimensions_n )
{
  if ( _items.size () >= dimensions_n ) {
    return;
  }
  // Create items
  beginInsertRows ( QModelIndex (), _items.size (), dimensions_n - 1 );
  _items.reserve ( dimensions_n );
  while ( _items.size () != dimensions_n ) {
    _items.emplace_back ( std::make_unique< VectorModelItem > ( this, 0.0 ) );
  }
  endInsertRows ();
}

void
VectorModel::ensureDimensionsMin ( int dimensions_n )
{
  if ( dimensions_n >= 0 ) {
    ensure_dimensions_min ( dimensions_n );
  }
}

void
VectorModel::ensure_dimensions_max ( std::size_t dimensions_n )
{
  if ( _items.size () <= dimensions_n ) {
    return;
  }
  // Remove items
  beginRemoveRows ( QModelIndex (), dimensions_n, _items.size () - 1 );
  _items.resize ( dimensions_n );
  endRemoveRows ();
}

void
VectorModel::ensureDimensionsMax ( int dimensions_n )
{
  if ( dimensions_n >= 0 ) {
    ensure_dimensions_max ( dimensions_n );
  }
}

void
VectorModel::setDimensions ( int dimensions_n )
{
  ensureDimensionsMin ( dimensions_n );
  ensureDimensionsMax ( dimensions_n );
}

void
VectorModel::update ( sev::mem::View< const double > data_n )
{
  // Adjust model size
  const std::size_t num = data_n.size ();
  ensure_dimensions_min ( num );
  // Update values
  for ( std::size_t ii = 0; ii != num; ++ii ) {
    _items[ ii ]->setValue ( data_n[ ii ] );
  }
}

snc::qt::VectorModelItem *
VectorModel::get ( int index_n ) const
{
  if ( index_n < 0 ) {
    return nullptr;
  }
  const std::size_t idx = index_n;
  if ( idx >= _items.size () ) {
    return nullptr;
  }
  return _items[ idx ].get ();
}

snc::qt::VectorModelItem *
VectorModel::getOrCreate ( int index_n )
{
  if ( index_n < 0 ) {
    return nullptr;
  }
  const std::size_t idx = index_n;
  ensure_dimensions_min ( idx + 1 );
  return _items[ idx ].get ();
}

double
VectorModel::getValue ( int index_n ) const
{
  auto * item = get ( index_n );
  if ( item == nullptr ) {
    return 0.0;
  }
  return item->value ();
}

void
VectorModel::setValue ( int index_n, double value_n )
{
  auto * item = getOrCreate ( index_n );
  if ( item == nullptr ) {
    return;
  }
  item->setValue ( value_n );
}

QHash< int, QByteArray >
VectorModel::roleNames () const
{
  return _roleNames;
}

int
VectorModel::rowCount ( const QModelIndex & parent_n ) const
{
  if ( parent_n.isValid () ) {
    return 0;
  }
  return _items.size ();
}

QVariant
VectorModel::data ( const QModelIndex & index_n,
                    int role_n [[maybe_unused]] ) const
{
  if ( index_n.parent ().isValid () || ( index_n.column () != 0 ) ||
       ( index_n.row () < 0 ) ||
       ( static_cast< std::size_t > ( index_n.row () ) >= _items.size () ) ) {
    return QVariant ();
  }

  switch ( role_n ) {
  case ExtraRole::axisObject:
    return QVariant::fromValue (
        static_cast< QObject * > ( _items[ index_n.row () ].get () ) );
  default:
    break;
  }

  return QVariant ();
}

} // namespace snc::qt
