/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "qml.hpp"
#include <snc/device/qt/Axes.hpp>
#include <snc/device/qt/Controls.hpp>
#include <snc/device/qt/Info.hpp>
#include <snc/device/qt/Sensors.hpp>
#include <snc/device/qt/State.hpp>
#include <snc/device/qt/Statics.hpp>
#include <snc/device/qt/axes/Axis.hpp>
#include <snc/device/qt/controls/I1.hpp>
#include <snc/device/qt/controls/I16.hpp>
#include <snc/device/qt/controls/I32.hpp>
#include <snc/device/qt/controls/I64.hpp>
#include <snc/device/qt/controls/I8.hpp>
#include <snc/device/qt/controls/i1/Control.hpp>
#include <snc/device/qt/controls/i16/Control.hpp>
#include <snc/device/qt/controls/i32/Control.hpp>
#include <snc/device/qt/controls/i64/Control.hpp>
#include <snc/device/qt/controls/i8/Control.hpp>
#include <snc/device/qt/sensors/I1.hpp>
#include <snc/device/qt/sensors/I16.hpp>
#include <snc/device/qt/sensors/I32.hpp>
#include <snc/device/qt/sensors/I64.hpp>
#include <snc/device/qt/sensors/I8.hpp>
#include <snc/device/qt/sensors/i1/Sensor.hpp>
#include <snc/device/qt/sensors/i16/Sensor.hpp>
#include <snc/device/qt/sensors/i32/Sensor.hpp>
#include <snc/device/qt/sensors/i64/Sensor.hpp>
#include <snc/device/qt/sensors/i8/Sensor.hpp>
#include <snc/mpath_stream/qt/Statistics.hpp>
#include <snc/mpath_stream/qt/StatisticsAxes.hpp>
#include <snc/mpath_stream/qt/StatisticsAxis.hpp>
#include <snc/qt/VectorModel.hpp>
#include <snc/settings/qt/Settings.hpp>
#include <snc/svs/ctl/controls/dash/Controls.hpp>
#include <QDir>
#include <QQmlEngine>

namespace snc::qt
{

namespace
{

template < typename T >
inline void
qml_register_type ( const char * name_n )
{
  const char * uri = "SncQ";
  int versionMajor = 1;
  int versionMinor = 0;
  qmlRegisterType< T > ( uri, versionMajor, versionMinor, name_n );
}

template < typename T >
inline void
qml_register_uncreatable ( const char * name_n )
{
  const char * uri = "SncQ";
  int versionMajor = 1;
  int versionMinor = 0;
  qmlRegisterUncreatableType< T > (
      uri, versionMajor, versionMinor, name_n, "C++ only type" );
}

} // namespace

void
qml_register_types ()
{
  qml_register_type< snc::settings::qt::Settings > ( "Settings" );

  qml_register_type< snc::qt::VectorModelItem > ( "VectorModelItem" );
  qml_register_type< snc::qt::VectorModel > ( "VectorModel" );

  qml_register_type< snc::mpath_stream::qt::StatisticsAxis > (
      "MPathStatisticsAxis" );
  qml_register_type< snc::mpath_stream::qt::StatisticsAxes > (
      "MPathStatisticsAxes" );
  qml_register_type< snc::mpath_stream::qt::Statistics > ( "MPathStatistics" );

  // -- Device info
  qml_register_uncreatable< snc::device::qt::Info > ( "DeviceInfo" );
  qml_register_uncreatable< snc::device::qt::Statics > ( "DeviceStatics" );
  qml_register_uncreatable< snc::device::qt::State > ( "DeviceState" );

  qml_register_uncreatable< snc::device::qt::Sensors > ( "DeviceSensors" );

  qml_register_uncreatable< snc::device::qt::sensors::I1 > (
      "DeviceSensorsI1" );
  qml_register_uncreatable< snc::device::qt::sensors::I8 > (
      "DeviceSensorsI8" );
  qml_register_uncreatable< snc::device::qt::sensors::I16 > (
      "DeviceSensorsI16" );
  qml_register_uncreatable< snc::device::qt::sensors::I32 > (
      "DeviceSensorsI32" );
  qml_register_uncreatable< snc::device::qt::sensors::I64 > (
      "DeviceSensorsI64" );

  qml_register_uncreatable< snc::device::qt::sensors::i1::Sensor > (
      "DeviceSensorI1" );
  qml_register_uncreatable< snc::device::qt::sensors::i8::Sensor > (
      "DeviceSensorI8" );
  qml_register_uncreatable< snc::device::qt::sensors::i16::Sensor > (
      "DeviceSensorI16" );
  qml_register_uncreatable< snc::device::qt::sensors::i32::Sensor > (
      "DeviceSensorI32" );
  qml_register_uncreatable< snc::device::qt::sensors::i64::Sensor > (
      "DeviceSensorI64" );

  qml_register_uncreatable< snc::device::qt::Controls > ( "DeviceControls" );

  qml_register_uncreatable< snc::device::qt::controls::I1 > (
      "DeviceControlsI1" );
  qml_register_uncreatable< snc::device::qt::controls::I8 > (
      "DeviceControlsI8" );
  qml_register_uncreatable< snc::device::qt::controls::I16 > (
      "DeviceControlsI16" );
  qml_register_uncreatable< snc::device::qt::controls::I32 > (
      "DeviceControlsI32" );
  qml_register_uncreatable< snc::device::qt::controls::I64 > (
      "DeviceControlsI64" );

  qml_register_uncreatable< snc::device::qt::controls::i1::Control > (
      "DeviceControlI1" );
  qml_register_uncreatable< snc::device::qt::controls::i8::Control > (
      "DeviceControlI8" );
  qml_register_uncreatable< snc::device::qt::controls::i16::Control > (
      "DeviceControlI16" );
  qml_register_uncreatable< snc::device::qt::controls::i32::Control > (
      "DeviceControlI32" );
  qml_register_uncreatable< snc::device::qt::controls::i64::Control > (
      "DeviceControlI64" );

  qml_register_uncreatable< snc::device::qt::Axes > ( "DeviceAxes" );
  qml_register_uncreatable< snc::device::qt::axes::Axis > ( "DeviceAxis" );

  // -- Controls
  qml_register_uncreatable< snc::svs::ctl::controls::dash::i1::List > (
      "ControlsBoolean" );
}

std::vector< QString >
qml_import_paths ()
{
  std::vector< QString > paths;
  paths.emplace_back ( "qrc:///snc/qt/qml_modules" );
  return paths;
}

} // namespace snc::qt
