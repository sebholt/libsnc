import QtQuick.Layouts 1.11

RowLayout {
  id: root
  property var axis
  property real speedMax: axis ? axis.info.speedMax : 0.0

  spacing: 16

  AxisSpeedSlider {
    axis: root.axis
    to: root.speedMax
  }

  AxisSpeedSlider {
    axis: root.axis
    to: -root.speedMax
  }
}
