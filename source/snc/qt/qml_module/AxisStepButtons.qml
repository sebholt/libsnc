import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

GridLayout {
  id: grid

  property var axis
  property Component buttonComponent
  property var valueTextPrecision: 1
  property var values: []
  property var _items: []
  property var _locale: Qt.locale()

  columnSpacing: 6
  rowSpacing: 0
  flow: GridLayout.TopToBottom
  rows: values.length

  Component {
    id: valueComponent
    Label {
      property real value: 0.0
      text: Number(value).toLocaleString (
              grid._locale, "f", grid.valueTextPrecision )
      font.family: "monospace"
    }
  }

  function generateValues(valuesMin, valuesMax, steps) {
    if (steps <= 1 ) {
      values = [valuesMax]
      return
    }
    // Generate values array
    var new_values = []
    var delta = (valuesMax - valuesMin)
    for (var ii = 0; ii < steps; ii++) {
      var amount = ii / (steps - 1)
      new_values.push( valuesMin + amount*delta )
    }
    values = new_values
  }

  function _getvalue(index) {
    return values[values.length-1-index];
  }

  Repeater {
    model: grid.values.length
    AxisStepButton {
      axis: grid.axis
      forward: true
      value: grid._getvalue(index)
    }
  }

  Repeater {
    model: grid.values.length
    AxisStepButton {
      axis: grid.axis
      forward: false
      reverse: true
      value: grid._getvalue(index)
    }
  }

  Repeater {
    model: grid.values.length
    Loader {
      sourceComponent: valueComponent
      Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
      onLoaded: {
        item.value = grid._getvalue(index)
      }
    }
  }
}
