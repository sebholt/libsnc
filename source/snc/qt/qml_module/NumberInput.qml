import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

TextField {
  id: textField

  property var locale: Qt.locale()
  property double value: 0.0
  property double valueRequest: 0.0
  property double valueLatest: 0.0
  property double valueMin: 0.0
  property double valueMax: 10.0
  property int precision: 1

  function valueString(val) {
    return Number(val).toLocaleString(textField.locale, "f", precision )
  }

  selectByMouse: true
  font.family : "monospace"
  horizontalAlignment: TextInput.AlignRight

  placeholderText: qsTr("?")
  text: valueString(value)

  onValueChanged: {
    valueLatest = value
    text = valueString(valueLatest)
  }

  onEditingFinished: {
    try {
      var val = Number.fromLocaleString(textField.locale, text)
    } catch ( err ) {
      val = NaN
    }

    if (isNaN(val)) {
      val = valueMin
    } else if (val < valueMin) {
      val = valueMin
    } else if (val > valueMax) {
      val = valueMax
    }

    valueRequest = val
    valueLatest = val
    text = valueString(valueLatest)
  }
}
