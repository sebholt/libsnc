StatusLight {
  property var sensor

  color: "darkOrange"
  active: sensor && sensor.state.value
}
