import QtQml 2.11
import QtQuick 2.11
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.4

GridLayout {
  id: root
  property var model: []

  columns: 2
  columnSpacing: 6

  Repeater {
    model: root.model
    Label {
      Layout.row: index
      Layout.column: 0
      text: listItem.name
    }
  }

  Repeater {
    model: root.model
    PanelStateSensorI1 {
      Layout.row: index
      Layout.column: 1
      sensor: listItem
    }
  }
}
