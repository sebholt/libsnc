import QtQuick 2.11
import QtQuick.Window 2.2

QtObject {
  property var win
  property var settings
  property rect winRect: win ? Qt.rect(win.x,win.y,win.width,win.height) : Qt.rect(0,0,0,0);
  property int winVisibility: win ? win.visibility : Window.Windowed
  property bool restored: false
  property bool restoreVisibility: true

  onWinRectChanged: storeRect()
  onWinVisibilityChanged: storeVis()

  function addr(key_n) {
    return "window." + key_n
  }

  function restore() {
    if (restored || !settings || !win) {
      return
    }

    win.x = settings.get( addr("x"), Screen.width / 2 - win.minimumWidth / 2 )
    win.width = settings.get( addr("width"), win.minimumWidth )

    win.y = settings.get( addr("y"), Screen.height / 2 - win.minimumHeight / 2 )
    win.height = settings.get( addr("height"), win.minimumHeight )

    if (restoreVisibility) {
      var vis = settings.get( addr("visibility"), "Windowed" )
      if (vis == "Maximized") {
        win.visible = true
        win.visibility = Window.Maximized
      } else if (vis == "Fullscreen") {
        win.visible = true
        win.visibility = Window.FullScreen
      } else {
        win.visible = true
        win.visibility = Window.Windowed
      }
    }

    restored = true
  }

  function storeRect() {
    if (!restored || !settings || !win || ( win.visibility != Window.Windowed )) {
      return;
    }
    settings.set(addr("x"), winRect.x)
    settings.set(addr("y"), winRect.y)
    settings.set(addr("width"), winRect.width)
    settings.set(addr("height"), winRect.height)
  }

  function storeVis() {
    if (!restored || !settings || !win) {
      return;
    }
    switch(winVisibility) {
    case Window.Maximized:
      settings.set(addr("visibility"), "Maximized")
      break;
    case Window.FullScreen:
      settings.set(addr("visibility"), "Fullscreen")
      break;
    default:
      settings.set(addr("visibility"), "Windowed")
      break;
    }
  }
}
