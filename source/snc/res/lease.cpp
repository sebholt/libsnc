/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "lease.hpp"
#include <sev/assert.hpp>
#include <algorithm>
#include <sstream>
#include <stdexcept>

namespace
{
bool
contains ( std::vector< std::uint_fast32_t > const & vector_n,
           std::uint_fast32_t value_n )
{
  auto it = std::find ( vector_n.cbegin (), vector_n.cend (), value_n );
  return ( it != vector_n.cend () );
}
void
push_index ( std::vector< std::uint_fast32_t > & vector_n,
             std::uint_fast32_t value_n )
{
  vector_n.push_back ( value_n );
  // Just for the beauty of it
  std::sort ( vector_n.begin (), vector_n.end () );
}
void
erase_index ( std::vector< std::uint_fast32_t > & vector_n,
              std::uint_fast32_t value_n )
{
  auto it = std::find ( vector_n.cbegin (), vector_n.cend (), value_n );
  if ( it != vector_n.cend () ) {
    vector_n.erase ( it );
  }
}
} // namespace

namespace snc::res
{

Lease::Lease ( snc::svp::office::Clerk * clerk_n )
: _clerk ( clerk_n )
{
}

Lease::~Lease ()
{
  DEBUG_ASSERT ( !is_leasing () );
}

void
Lease::clear_registers ()
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Clearing registers while leasing is not allowed." );
  }

  _controls.i1.clear ();
  _controls.i8.clear ();
  _controls.i16.clear ();
  _controls.i32.clear ();
  _controls.i64.clear ();
  _axes.clear ();
}

bool
Lease::has_control_i1 ( std::uint_fast32_t index_n ) const
{
  return contains ( controls_i1 (), index_n );
}

void
Lease::register_control_i1 ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering control::i1 while leasing is not allowed." );
  }

  if ( has_control_i1 ( index_n ) ) {
    return;
  }
  push_index ( _controls.i1, index_n );
}

bool
Lease::has_control_i8 ( std::uint_fast32_t index_n ) const
{
  return contains ( controls_i8 (), index_n );
}

void
Lease::register_control_i8 ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering control::i8 while leasing is not allowed." );
  }

  if ( has_control_i8 ( index_n ) ) {
    return;
  }
  push_index ( _controls.i8, index_n );
}

bool
Lease::has_control_i16 ( std::uint_fast32_t index_n ) const
{
  return contains ( controls_i16 (), index_n );
}

void
Lease::register_control_i16 ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering control::i16 while leasing is not allowed." );
  }

  if ( has_control_i16 ( index_n ) ) {
    return;
  }
  push_index ( _controls.i16, index_n );
}

bool
Lease::has_control_i32 ( std::uint_fast32_t index_n ) const
{
  return contains ( controls_i32 (), index_n );
}

void
Lease::register_control_i32 ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering control::i32 while leasing is not allowed." );
  }

  if ( has_control_i32 ( index_n ) ) {
    return;
  }
  push_index ( _controls.i32, index_n );
}

bool
Lease::has_control_i64 ( std::uint_fast32_t index_n ) const
{
  return contains ( controls_i64 (), index_n );
}

void
Lease::register_control_i64 ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering control::i64 while leasing is not allowed." );
  }

  if ( has_control_i64 ( index_n ) ) {
    return;
  }
  push_index ( _controls.i64, index_n );
}

bool
Lease::has_axis ( std::uint_fast32_t index_n ) const
{
  return contains ( axes (), index_n );
}

void
Lease::register_axis ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Registering axis while leasing is not allowed." );
  }

  if ( has_axis ( index_n ) ) {
    return;
  }
  push_index ( _axes, index_n );
}

void
Lease::unregister_axis ( std::uint_fast32_t index_n )
{
  if ( is_leasing () ) {
    throw std::runtime_error (
        "Unregistering axis while leasing is not allowed." );
  }

  erase_index ( _axes, index_n );
}

void
Lease::set_is_leasing ( bool flag_n )
{
  _is_leasing = flag_n;
}

std::string
Lease::registrations_string () const
{
  std::ostringstream ostr;
  std::string_view group_sep = "";

  auto print = [ &ostr, &group_sep ] ( Index_Vector const & vector_n,
                                       std::string_view name_n ) {
    if ( !vector_n.empty () ) {
      ostr << group_sep << name_n << " {";
      std::string_view sep = "";
      for ( const auto & idx : vector_n ) {
        ostr << sep << idx;
        sep = ",";
      }
      ostr << "}";
      group_sep = " ";
    }
  };

  // Format
  print ( controls_i1 (), "controls_i1" );
  print ( controls_i8 (), "controls_i8" );
  print ( controls_i16 (), "controls_i16" );
  print ( controls_i32 (), "controls_i32" );
  print ( controls_i64 (), "controls_i64" );
  print ( axes (), "axes" );

  return ostr.str ();
}

} // namespace snc::res
