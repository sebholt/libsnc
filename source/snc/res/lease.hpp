/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>
#include <string>
#include <vector>

namespace snc::svp::office
{
class Clerk;
}
namespace snc::res
{
class Pool;
}

namespace snc::res
{

/// @brief Database of resources that can be leased from a Pool
///        for exclusive use.
class Lease
{
  public:
  // -- Types
  using Index_Vector = std::vector< std::uint_fast32_t >;

  // -- Construction

  Lease ( snc::svp::office::Clerk * clerk_n );

  ~Lease ();

  // -- Setup

  /// @brief Clears all registrations
  void
  clear_registers ();

  // -- Accessors

  snc::svp::office::Clerk *
  clerk () const
  {
    return _clerk;
  }

  bool
  is_leasing () const
  {
    return _is_leasing;
  }

  // -- Control i1 registration

  const Index_Vector &
  controls_i1 () const
  {
    return _controls.i1;
  }

  bool
  has_control_i1 ( std::uint_fast32_t index_n ) const;

  void
  register_control_i1 ( std::uint_fast32_t index_n );

  // -- Control i8 registration

  const Index_Vector &
  controls_i8 () const
  {
    return _controls.i8;
  }

  bool
  has_control_i8 ( std::uint_fast32_t index_n ) const;

  void
  register_control_i8 ( std::uint_fast32_t index_n );

  // -- Control i16 registration

  const Index_Vector &
  controls_i16 () const
  {
    return _controls.i16;
  }

  bool
  has_control_i16 ( std::uint_fast32_t index_n ) const;

  void
  register_control_i16 ( std::uint_fast32_t index_n );

  // -- Control i32 registration

  const Index_Vector &
  controls_i32 () const
  {
    return _controls.i32;
  }

  bool
  has_control_i32 ( std::uint_fast32_t index_n ) const;

  void
  register_control_i32 ( std::uint_fast32_t index_n );

  // -- Control i64 registration

  const Index_Vector &
  controls_i64 () const
  {
    return _controls.i64;
  }

  bool
  has_control_i64 ( std::uint_fast32_t index_n ) const;

  void
  register_control_i64 ( std::uint_fast32_t index_n );

  // -- Axes registration

  const Index_Vector &
  axes () const
  {
    return _axes;
  }

  bool
  has_axis ( std::uint_fast32_t index_n ) const;

  void
  register_axis ( std::uint_fast32_t index_n );

  void
  unregister_axis ( std::uint_fast32_t index_n );

  // -- Information

  std::string
  registrations_string () const;

  private:
  // -- Private interface
  friend class snc::res::Pool;

  void
  set_is_leasing ( bool flag_n );

  private:
  // -- Attributes
  snc::svp::office::Clerk * _clerk = nullptr;
  bool _is_leasing = false;
  struct
  {
    Index_Vector i1;
    Index_Vector i8;
    Index_Vector i16;
    Index_Vector i32;
    Index_Vector i64;
  } _controls;
  Index_Vector _axes;
};

} // namespace snc::res
