/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pool.hpp"
#include <sev/assert.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/res/lease.hpp>
#include <stdexcept>

namespace snc::res
{

Pool::Pool () = default;

Pool::~Pool ()
{
  reset ();
}

void
Pool::reset ()
{
  if ( _stats.total != 0 ) {
    throw std::runtime_error ( "Reset while there are active leases." );
  }

  _statics.reset ();
  _regs.controls.i1.clear ();
  _regs.controls.i8.clear ();
  _regs.controls.i16.clear ();
  _regs.controls.i32.clear ();
  _regs.controls.i64.clear ();
  _regs.axes.clear ();
}

void
Pool::fill ( snc::device::statics::handle::Statics statics_n )
{
  reset ();

  _statics = std::move ( statics_n );
  if ( !_statics ) {
    return;
  }

  // Allocate registers
  _regs.controls.i1.resize ( _statics->controls_i1 ().size () );
  _regs.controls.i8.resize ( _statics->controls_i8 ().size () );
  _regs.controls.i16.resize ( _statics->controls_i16 ().size () );
  _regs.controls.i32.resize ( _statics->controls_i32 ().size () );
  _regs.controls.i64.resize ( _statics->controls_i64 ().size () );
  _regs.axes.resize ( _statics->axes ().size () );
}

bool
Pool::probe ( const Lease & lease_n ) const
{
  // We need to be configured
  if ( !_statics ) {
    return false;
  }
  // Don't allow double leases
  if ( lease_n.is_leasing () ) {
    return false;
  }

  {
    // Check function
    auto check = [ this ] ( Lease::Index_Vector const & indices_n,
                            Leaser_Vector const & leasers_n ) -> bool {
      for ( auto index : indices_n ) {
        // Is the index valid?
        if ( index >= leasers_n.size () ) {
          return false;
        }
        // Is the resource in use?
        if ( leasers_n[ index ].clerk != nullptr ) {
          return false;
        }
      }
      return true;
    };
    // Check controls
    if ( !check ( lease_n.controls_i1 (), _regs.controls.i1 ) ||
         !check ( lease_n.controls_i8 (), _regs.controls.i8 ) ||
         !check ( lease_n.controls_i16 (), _regs.controls.i16 ) ||
         !check ( lease_n.controls_i32 (), _regs.controls.i32 ) ||
         !check ( lease_n.controls_i64 (), _regs.controls.i64 ) ) {
      return false;
    }
    // Check axes
    if ( !check ( lease_n.axes (), _regs.axes ) ) {
      return false;
    }
  }

  // Return success
  return true;
}

bool
Pool::acquire ( Lease & lease_n )
{
  // Perform checks
  if ( !probe ( lease_n ) ) {
    return false;
  }

  {
    // Registration function
    auto reg = [ this, &lease_n ] ( Lease::Index_Vector const & indices_n,
                                    std::uint_fast32_t & stats_control_n,
                                    Leaser_Vector & leasers_n ) {
      _stats.total += indices_n.size ();
      stats_control_n += indices_n.size ();
      for ( std::uint_fast32_t index : indices_n ) {
        auto & clerk = leasers_n[ index ].clerk;
        DEBUG_ASSERT ( clerk == nullptr );
        clerk = lease_n.clerk ();
      }
    };
    // Register controls
    reg ( lease_n.controls_i1 (), _stats.controls.i1, _regs.controls.i1 );
    reg ( lease_n.controls_i8 (), _stats.controls.i8, _regs.controls.i8 );
    reg ( lease_n.controls_i16 (), _stats.controls.i16, _regs.controls.i16 );
    reg ( lease_n.controls_i32 (), _stats.controls.i32, _regs.controls.i32 );
    reg ( lease_n.controls_i64 (), _stats.controls.i64, _regs.controls.i64 );
    // Register axes
    reg ( lease_n.axes (), _stats.axes, _regs.axes );
  }

  // Set lease flag
  lease_n.set_is_leasing ( true );

  // Return success
  return true;
}

void
Pool::release ( Lease & lease_n )
{
  if ( !lease_n.is_leasing () ) {
    return;
  }

  {
    // Unregistration function
    auto unreg = [ this, &lease_n ] ( Lease::Index_Vector const & indices_n,
                                      std::uint_fast32_t & stats_control_n,
                                      Leaser_Vector & leasers_n ) {
      if ( indices_n.empty () ) {
        return;
      }
      // Update statistics
      DEBUG_ASSERT ( _stats.total >= indices_n.size () );
      _stats.total -= indices_n.size ();
      DEBUG_ASSERT ( stats_control_n >= indices_n.size () );
      stats_control_n -= indices_n.size ();
      // Clear the user
      for ( std::uint_fast32_t index : indices_n ) {
        auto & clerk = leasers_n[ index ].clerk;
        DEBUG_ASSERT ( clerk == lease_n.clerk () );
        clerk = nullptr;
      }
    };
    // Unregister axes
    unreg ( lease_n.axes (), _stats.axes, _regs.axes );
    // Unregister controls
    unreg ( lease_n.controls_i64 (), _stats.controls.i64, _regs.controls.i64 );
    unreg ( lease_n.controls_i32 (), _stats.controls.i32, _regs.controls.i32 );
    unreg ( lease_n.controls_i16 (), _stats.controls.i16, _regs.controls.i16 );
    unreg ( lease_n.controls_i8 (), _stats.controls.i8, _regs.controls.i8 );
    unreg ( lease_n.controls_i1 (), _stats.controls.i1, _regs.controls.i1 );
  }

  // Clear lease flag
  lease_n.set_is_leasing ( false );
}

} // namespace snc::res
