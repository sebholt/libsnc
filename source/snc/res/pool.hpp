/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <cstdint>
#include <memory>
#include <vector>

namespace snc::svp::office
{
class Clerk;
}
namespace snc::res
{
class Lease;
}

namespace snc::res
{

/// @brief Database of machine resources for usage tracking.
///        Groups of resources can be leased for exclusive use by
///        a Lease object.
class Pool
{
  public:
  // -- Types

  struct Leaser
  {
    snc::svp::office::Clerk * clerk = nullptr;
  };

  using Leaser_Vector = std::vector< Leaser >;

  struct Stats
  {
    std::uint_fast32_t total = 0;
    struct
    {
      std::uint_fast32_t i1 = 0;
      std::uint_fast32_t i8 = 0;
      std::uint_fast32_t i16 = 0;
      std::uint_fast32_t i32 = 0;
      std::uint_fast32_t i64 = 0;
    } controls;
    std::uint_fast32_t axes = 0;
  };

  // -- Construction

  Pool ();

  ~Pool ();

  // -- Setup

  void
  reset ();

  void
  fill ( snc::device::statics::handle::Statics statics_n );

  // -- Interface

  const auto &
  statics () const
  {
    return _statics;
  }

  bool
  probe ( const Lease & lease_n ) const;

  bool
  acquire ( Lease & lease_n );

  void
  release ( Lease & lease_n );

  // -- Statistics

  const auto &
  stats () const
  {
    return _stats;
  }

  private:
  // - References
  snc::device::statics::handle::Statics _statics;
  // -- Attributes: Statistics
  Stats _stats;
  // -- Attributes: Registers
  struct
  {
    struct
    {
      Leaser_Vector i1;
      Leaser_Vector i8;
      Leaser_Vector i16;
      Leaser_Vector i32;
      Leaser_Vector i64;
    } controls;
    Leaser_Vector axes;
  } _regs;
};

} // namespace snc::res
