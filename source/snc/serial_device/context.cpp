/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "context.hpp"

namespace snc::serial_device
{

Context::Context () = default;

} // namespace snc::serial_device
