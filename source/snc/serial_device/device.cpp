/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device.hpp"
#include <sev/assert.hpp>
#include <utility>

namespace snc::serial_device
{

Device::Device () {}

Device::~Device () {}

void
Device::open ( Context && context_n )
{
  DEBUG_ASSERT ( !_is_open );
  _emb_device_context = std::move ( context_n );
  _is_open = true;
}

void
Device::close ()
{
  _emb_device_context = Context ();
  _is_open = false;
}

} // namespace snc::serial_device
