/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/thread/tracker.hpp>
#include <snc/serial_device/context.hpp>

namespace snc::serial_device
{

class Device
{
  public:
  // -- Construction

  Device ();

  virtual ~Device ();

  // -- Session interface

  virtual void
  open ( Context && context_n );

  virtual void
  close ();

  bool
  is_open () const
  {
    return _is_open;
  }

  const Context &
  emb_device_context () const
  {
    return _emb_device_context;
  }

  private:
  // -- Attributes
  bool _is_open = false;
  Context _emb_device_context;
};

} // namespace snc::serial_device
