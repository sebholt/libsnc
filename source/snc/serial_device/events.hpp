/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/serial_device/stats.hpp>
#include <snc/utility/data_tile.hpp>
#include <cstdint>
#include <string>

namespace snc::serial_device::event
{

template < std::uint_fast32_t ETYPE >
class Base_ : public sev::event::Event
{
  public:
  static constexpr std::uint_fast32_t etype = ETYPE;

  // Protected methods
  protected:
  Base_ ()
  : sev::event::Event ( etype )
  {
  }
};

namespace in
{

/// @brief Event type
struct Type
{
  // Data IO
  static constexpr std::uint_fast32_t DATA = 0;
  // Thread runtime control
  static constexpr std::uint_fast32_t ABORT_STOP_IO = 2;
  static constexpr std::uint_fast32_t ABORT_SHUT_DOWN = 3;
};

class Data : public Base_< Type::DATA >
{
  public:
  void
  reset ()
  {
    Base_< Type::DATA >::reset ();
    _data_tile = nullptr;
  }

  snc::utility::Data_Tile *
  data_tile () const
  {
    return _data_tile;
  }

  void
  set_data_tile ( snc::utility::Data_Tile * tile_n )
  {
    _data_tile = tile_n;
  }

  private:
  // -- Attributes
  snc::utility::Data_Tile * _data_tile = nullptr;
};

class Abort_Stop_IO : public Base_< Type::ABORT_STOP_IO >
{
};

class Abort_Shut_Down : public Base_< Type::ABORT_SHUT_DOWN >
{
};

} // namespace in

namespace out
{

/// @brief Event type
struct Type
{
  // Data IO
  static constexpr std::uint_fast32_t DATA_IN = 0;
  static constexpr std::uint_fast32_t DATA_SENT = 1;
  static constexpr std::uint_fast32_t DEVICE_ERROR = 2;

  // Thread runtime control
  static constexpr std::uint_fast32_t ABORT_STOP_IO_DONE = 3;
  static constexpr std::uint_fast32_t ABORT_SHUT_DOWN_DONE = 4;
};

class Data_In : public Base_< Type::DATA_IN >
{
  public:
  void
  reset ()
  {
    Base_< Type::DATA_IN >::reset ();
    _data_tile = nullptr;
  }

  snc::utility::Data_Tile *
  data_tile () const
  {
    return _data_tile;
  }

  void
  set_data_tile ( snc::utility::Data_Tile * tile_n )
  {
    _data_tile = tile_n;
  }

  private:
  // -- Attributes
  snc::utility::Data_Tile * _data_tile = nullptr;
};

class Data_Sent : public Base_< Type::DATA_SENT >
{
  public:
  void
  reset ()
  {
    Base_< Type::DATA_SENT >::reset ();
    _data_tile = nullptr;
  }

  snc::utility::Data_Tile *
  data_tile () const
  {
    return _data_tile;
  }

  void
  set_data_tile ( snc::utility::Data_Tile * tile_n )
  {
    _data_tile = tile_n;
  }

  private:
  // -- Attributes
  snc::utility::Data_Tile * _data_tile = nullptr;
};

class Device_Error : public Base_< Type::DEVICE_ERROR >
{
  public:
  std::string &
  error_message ()
  {
    return _error_message;
  }

  const std::string &
  error_message () const
  {
    return _error_message;
  }

  private:
  // -- Attributes
  std::string _error_message;
};

class Abort_Stop_IO_Done : public Base_< Type::ABORT_STOP_IO_DONE >
{
};

class Abort_Shutdown_Done : public Base_< Type::ABORT_SHUT_DOWN_DONE >
{
};

} // namespace out
} // namespace snc::serial_device::event
