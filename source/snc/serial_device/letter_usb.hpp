/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/serial_device/letter.hpp>
#include <snc/usb/device_port_id.hpp>

namespace snc::serial_device
{

class Letter_USB : public Letter
{
  public:
  // -- Construction

  Letter_USB ();

  ~Letter_USB () override;

  // -- USB port id

  const snc::usb::Device_Port_Id &
  usb_port_id () const
  {
    return _usb_port_id;
  }

  snc::usb::Device_Port_Id &
  usb_port_id_ref ()
  {
    return _usb_port_id;
  }

  private:
  /// @brief USB port id
  snc::usb::Device_Port_Id _usb_port_id;
};

} // namespace snc::serial_device
