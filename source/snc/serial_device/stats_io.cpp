/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stats_io.hpp"

namespace snc::serial_device
{

void
Stats_IO::reset ()
{
  _in.reset ();
  _out.reset ();
}

void
Stats_IO::begin_frame ( Time_Point tnow_n )
{
  _in.begin_frame ( tnow_n );
  _out.begin_frame ( tnow_n );
}

void
Stats_IO::begin_frame ()
{
  begin_frame ( Clock::now () );
}

void
Stats_IO::end_frame ( Time_Point tnow_n )
{
  _in.end_frame ( tnow_n );
  _out.end_frame ( tnow_n );
}

void
Stats_IO::end_frame ()
{
  end_frame ( Clock::now () );
}

void
Stats_IO::begin_next_frame ()
{
  _in.begin_next_frame ();
  _out.begin_next_frame ();
}

} // namespace snc::serial_device
