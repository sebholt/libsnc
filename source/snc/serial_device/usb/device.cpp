/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device.hpp"
#include <sev/assert.hpp>
#include <snc/serial_device/usb/thread.hpp>

namespace snc::serial_device::usb
{

Device::Device ( const Letter_USB & letter_n )
: _letter ( letter_n )
{
}

Device::~Device () {}

void
Device::open ( Context && context_n )
{
  DEBUG_ASSERT ( !is_open () );
  if ( is_open () ) {
    return;
  }

  snc::serial_device::Device::open ( std::move ( context_n ) );
  _log =
      sev::logt::Context ( emb_device_context ().log_parent, "Emb-Device-USB" );
  _log.cat ( sev::logt::FL_DEBUG_0, "Open: Starting thread." );
  _thread = std::make_unique< Thread > (
      emb_device_context (), _letter.usb_port_id (), _log );
}

void
Device::close ()
{
  if ( !is_open () ) {
    return;
  }

  _log.cat ( sev::logt::FL_DEBUG_0, "Close: Destroy thread begin." );
  _thread.reset ();
  _log.cat ( sev::logt::FL_DEBUG_0, "Close: Destroy thread done." );
  _log = sev::logt::Context ();
  snc::serial_device::Device::close ();
}

} // namespace snc::serial_device::usb
