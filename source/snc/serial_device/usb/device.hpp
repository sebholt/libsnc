/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <snc/serial_device/device.hpp>
#include <snc/serial_device/letter_usb.hpp>

namespace snc::serial_device::usb
{

// -- Forward declaration
class Thread;

class Device : public snc::serial_device::Device
{
  public:
  // -- Construction

  Device ( const Letter_USB & letter_n );

  ~Device ();

  // -- Accessors

  const auto &
  letter () const
  {
    return _letter;
  }

  // -- Open / close interface

  void
  open ( Context && context_n ) override;

  void
  close () override;

  private:
  // -- Attributes
  /// @brief Logging context
  sev::logt::Context _log;
  /// @brief Device letter
  Letter_USB _letter;
  ///@brief Thread instance
  std::unique_ptr< Thread > _thread;
};

} // namespace snc::serial_device::usb
