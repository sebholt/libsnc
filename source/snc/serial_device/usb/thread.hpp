/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <libusb-1.0/libusb.h>
#include <sev/event/bit_accus_async/socket.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/mem/list/se/item.hpp>
#include <sev/mem/list/se/list.hpp>
#include <sev/thread/thread.hpp>
#include <snc/serial_device/context.hpp>
#include <snc/serial_device/events.hpp>
#include <snc/usb/device_port_id.hpp>
#include <snc/usb/device_query.hpp>
#include <snc/utility/data_tile.hpp>
#include <poll.h>
#include <array>
#include <memory>
#include <vector>

namespace snc::serial_device::usb
{

class Thread
{
  public:
  // -- Static variables

  static constexpr std::size_t read_tiles_max = 16;
  static constexpr std::size_t read_tile_size = 512;

  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t EVENTS_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t EVENTS_OUT = ( 1 << 1 );
  };

  struct Socket_Index
  {
    static constexpr std::uint_fast32_t EVENTS_HUB = 0;
    static constexpr std::uint_fast32_t SERIAL_DEVICE_BEGIN = 1;
  };

  struct Abort_Event
  {
    static constexpr std::uint8_t STOP_IO = ( 1 << 0 );
    static constexpr std::uint8_t STOP_IO_DONE = ( 1 << 1 );
    static constexpr std::uint8_t SHUT_DOWN = ( 1 << 2 );
    static constexpr std::uint8_t SHUT_DOWN_SENT = ( 1 << 3 );
  };

  struct LibUSB_Error
  {
    static constexpr std::uint8_t ERROR = ( 1 << 0 );
    static constexpr std::uint8_t ERROR_ACKED = ( 1 << 1 );
  };

  using Read_Tile = snc::utility::Data_Tile_N< read_tile_size >;

  class Transfer_Bind : public sev::mem::list::se::Item
  {
    public:
    // -- Construction

    Transfer_Bind ();
    ~Transfer_Bind ();

    public:
    // -- Attributes
    libusb_transfer * usb_transfer = nullptr;
    snc::utility::Data_Tile * data_tile = nullptr;
  };

  // -- Construction

  Thread ( Context const & context_n,
           snc::usb::Device_Port_Id const & usb_port_id_n,
           sev::logt::Reference const & log_parent_n );

  ~Thread ();

  // -- Interface

  bool
  is_running () const
  {
    return _thread.is_running ();
  }

  void
  operator() ();

  private:
  // -- Utility
  bool
  process_shut_down ();

  void
  acquire_serial_device_pollfds ();

  void
  wait_for_events ();

  void
  process_event_data ( const sev::event::Event & event_n );

  void
  process_event_abort_stop_io ( const sev::event::Event & event_n );

  void
  process_event_abort_shut_down ( const sev::event::Event & event_n );

  void
  process_write_return ();

  void
  process_libusb ();

  void
  process_libusb_read_transfers ();

  void
  process_libusb_write_transfers ();

  void
  process_libusb_error ();

  void
  usb_context_init ();

  void
  usb_context_destroy ();

  bool
  usb_device_open ();

  void
  usb_device_close ();

  void
  usb_abort ();

  void
  usb_abort_transfers ();

  void
  usb_error ();

  static void
  lusb_cb_transfer_in_anon ( libusb_transfer * transfer_n );

  void
  lusb_cb_transfer_in ( libusb_transfer * transfer_n );

  static void
  lusb_cb_transfer_out_anon ( libusb_transfer * transfer_n );

  void
  lusb_cb_transfer_out ( libusb_transfer * transfer_n );

  static void
  lusb_cb_pollfd_added ( int fd_n, short events_n, void * user_data_n );

  static void
  lusb_cb_pollfd_removed ( int fd_n, void * user_data_n );

  void
  log_transfer_error ( std::string_view prefix_n,
                       libusb_transfer * transfer_n );

  private:
  // -- Attributes
  /// @brief Logging context
  sev::logt::Context _log;
  // Loop flags
  sev::mem::Flags_Fast32 _loop_events;
  sev::mem::Flags_Fast8 _abort_state;
  // libusb flags
  bool _libusb_device_good = false;
  bool _libusb_process_events = false;
  bool _libusb_context_timeouts = false;
  sev::mem::Flags_Fast8 _libusb_error;

  // libusb structs
  libusb_context * _usb_context = nullptr;
  libusb_device_handle * _usb_device_handle = nullptr;
  libusb_device * _usb_device = nullptr;

  // Pipe event collectors
  sev::event::bit_accus_async::Socket _event_accu_eio;

  // Event queue connections
  sev::event::queue_io::Connection_2 _connection;

  // Reading structures
  struct Read
  {
    static constexpr std::size_t ut_in_max = 4;
    // Owners
    std::unique_ptr< Read_Tile[] > owner_tiles;
    std::unique_ptr< Transfer_Bind[] > owner_binds;
    // Referencers
    sev::mem::list::se::List< snc::utility::Data_Tile > tiles_free;
    sev::mem::list::se::List< Transfer_Bind > binds_idle;
    sev::mem::list::se::List< Transfer_Bind > binds_submitted;
  } _read;

  // Writing structures
  struct Write
  {
    static constexpr std::size_t ut_out_max = 4;
    // Owners
    std::unique_ptr< Transfer_Bind[] > owner_binds;
    // Referencers
    sev::mem::list::se::List< Transfer_Bind > binds_idle;
    sev::mem::list::se::List< Transfer_Bind > binds_submitted;

    sev::mem::list::se::List< snc::utility::Data_Tile > tiles_pending;
    sev::mem::list::se::List< snc::utility::Data_Tile > tiles_done;
  } _write;

  /// @brief Pollfd structs buffer
  std::vector< pollfd > _pollfds;

  /// @brief Event pools
  sev::event::Tracker_Pools< event::out::Data_In,
                             event::out::Data_Sent,
                             event::out::Device_Error,
                             event::out::Abort_Stop_IO_Done,
                             event::out::Abort_Shutdown_Done >
      _epools;

  /// @brief USB device info
  snc::usb::Device_Port_Id _usb_port_id;

  /// @brief Thread
  sev::thread::Thread _thread;
};

} // namespace snc::serial_device::usb
