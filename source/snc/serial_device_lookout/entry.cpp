/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "entry.hpp"

namespace snc::serial_device_lookout
{

Entry::Entry () = default;

Entry::~Entry () = default;

void
Entry::clear ()
{
  _device_letter.reset ();
  _usb_device_query.reset ();
}

} // namespace snc::serial_device_lookout
