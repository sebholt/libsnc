/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/serial_device/letter.hpp>
#include <snc/usb/device_query.hpp>
#include <memory>

namespace snc::serial_device_lookout
{

class Entry
{
  public:
  // -- Types

  using Letter_Handle = std::shared_ptr< snc::serial_device::Letter >;
  using Query_Handle = std::shared_ptr< const snc::usb::Device_Query >;

  // -- Construction

  Entry ();

  ~Entry ();

  void
  clear ();

  // -- Device letter

  const Letter_Handle &
  device_letter () const
  {
    return _device_letter;
  }

  void
  set_device_letter ( Letter_Handle const & letter_n )
  {
    _device_letter = letter_n;
  }

  // -- USB device query

  const Query_Handle &
  usb_device_query () const
  {
    return _usb_device_query;
  }

  void
  set_usb_device_query ( Query_Handle const & query_n )
  {
    _usb_device_query = query_n;
  }

  private:
  // -- Attributes
  Letter_Handle _device_letter;
  Query_Handle _usb_device_query;
};

} // namespace snc::serial_device_lookout
