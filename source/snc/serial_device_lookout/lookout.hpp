/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <sev/thread/tracker.hpp>
#include <snc/serial_device_lookout/entry.hpp>
#include <snc/serial_device_lookout/events.hpp>
#include <snc/usb/device_query.hpp>
#include <functional>
#include <memory>

namespace snc::serial_device_lookout
{

// -- Forward declarations
class Thread;

class Lookout
{
  public:
  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t EVENT_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t EVENT_OUT = ( 1 << 1 );
    static constexpr std::uint_fast32_t PROCESS = ( 1 << 2 );
  };

  struct Abort_State
  {
    static constexpr std::uint_fast32_t ENABLED = ( 1 << 0 );
    static constexpr std::uint_fast32_t REQUESTED = ( 1 << 1 );
    static constexpr std::uint_fast32_t ACKNOWLEDGED = ( 1 << 2 );
  };

  struct Callbacks
  {
    std::function< void () > stopped;
    std::function< void () > device_new;
    std::function< void () > device_remove;
  };

  // -- Construction

  Lookout ( sev::thread::Tracker * thread_tracker_n,
            const sev::logt::Reference & log_parent_n );

  ~Lookout ();

  // -- Event callback interface

  const std::function< void () > &
  event_callback () const
  {
    return _event_accu.callback ();
  }

  void
  set_event_callback ( const std::function< void () > & callback_n,
                       bool call_on_demand_n = true );

  void
  clear_event_callback ();

  // -- Device queries

  const std::vector< std::shared_ptr< const snc::usb::Device_Query > > &
  usb_device_queries () const
  {
    return _usb_device_queries;
  }

  void
  add_usb_device_query (
      std::shared_ptr< const snc::usb::Device_Query > query_n );

  // -- Start / stop interface

  void
  start ();

  void
  abort ();

  bool
  is_running () const
  {
    return _thread.operator bool ();
  }

  bool
  is_aborting () const
  {
    return !_abort_state.is_empty ();
  }

  // -- Event processing

  void
  events ( const Callbacks & callbacks_n );

  const std::shared_ptr< const Entry > &
  event_entry ()
  {
    return _event_entry;
  }

  // -- Devices

  const std::vector< std::shared_ptr< const Entry > > &
  entries () const
  {
    return _entries;
  }

  private:
  // -- Attributes
  sev::logt::Context _log;
  // - State flags
  sev::mem::Flags_Fast32 _abort_state;
  sev::event::bit_accus_async::Callback _event_accu;

  // - Queries
  std::vector< std::shared_ptr< const snc::usb::Device_Query > >
      _usb_device_queries;
  // - Entries
  std::vector< std::shared_ptr< const Entry > > _entries;

  // - Connection
  sev::event::queue_io::Connection_2 _connection;

  // - Event processing
  std::shared_ptr< const Entry > _event_entry;

  // - Event pools
  sev::event::Tracker_Pools< events::out::Thread_Abort,
                             events::out::USB_Query_Add >
      _epools;

  // - Thread
  sev::thread::Tracker * _thread_tracker = nullptr;
  std::unique_ptr< Thread > _thread;
};

} // namespace snc::serial_device_lookout
