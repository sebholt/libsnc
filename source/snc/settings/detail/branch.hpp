/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/detail/entity.hpp>
#include <memory>
#include <string_view>
#include <unordered_map>

namespace snc::settings::detail
{

// -- Forward declaration
class Node;

class Branch : public Entity
{
  public:
  // -- Types
  using Entry = std::pair< std::unique_ptr< Node >, std::unique_ptr< char[] > >;
  using Map = std::unordered_map< std::string_view, Entry >;

  // -- Construction

  Branch ();

  ~Branch ();

  // -- Map

  const Map &
  map () const
  {
    return _map;
  }

  // -- Access

  const Node *
  get ( std::string_view name_n ) const;

  Node *
  get ( std::string_view name_n );

  Node *
  get_or_create ( std::string_view name_n );

  private:
  Map _map;
};

} // namespace snc::settings::detail
