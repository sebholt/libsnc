/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "entity.hpp"

namespace snc::settings::detail
{

Entity::Entity () {}

Entity::~Entity () {}

} // namespace snc::settings::detail
