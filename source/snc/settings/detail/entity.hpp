/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::settings::detail
{

class Entity
{
  public:
  // -- Types

  // -- Construction

  Entity ();

  ~Entity ();
};

} // namespace snc::settings::detail
