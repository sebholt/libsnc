/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "node.hpp"
#include <snc/settings/detail/array.hpp>
#include <snc/settings/detail/branch.hpp>
#include <snc/settings/detail/leaf.hpp>

namespace snc::settings::detail
{

Node::Node () {}

Node::~Node () {}

Leaf &
Node::leaf ()
{
  if ( !_leaf ) {
    _leaf = std::make_unique< Leaf > ();
  }
  return *_leaf;
}

Array &
Node::array ()
{
  if ( !_array ) {
    _array = std::make_unique< Array > ();
  }
  return *_array;
}

Branch &
Node::branch ()
{
  if ( !_branch ) {
    _branch = std::make_unique< Branch > ();
  }
  return *_branch;
}

} // namespace snc::settings::detail
