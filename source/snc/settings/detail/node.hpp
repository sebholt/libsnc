/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/detail/types.hpp>
#include <snc/settings/value.hpp>
#include <memory>

namespace snc::settings::detail
{

// -- Forward declaration
class Leaf;
class Array;
class Branch;

class Node
{
  public:
  // -- Construction

  Node ();

  ~Node ();

  // -- Leaf

  /// @brief Might return nullptr
  const Leaf *
  leaf () const
  {
    return _leaf.get ();
  }

  /// @brief Creates an instance on demand
  Leaf &
  leaf ();

  // -- Array

  /// @brief Might return nullptr
  const Array *
  array () const
  {
    return _array.get ();
  }

  /// @brief Creates an instance on demand
  Array &
  array ();

  // -- Branch

  /// @brief Might return nullptr
  const Branch *
  branch () const
  {
    return _branch.get ();
  }

  /// @brief Creates an instance on demand
  Branch &
  branch ();

  // -- Plain tree mode

  Plain_Mode
  plain_mode () const
  {
    return _plain_mode;
  }

  void
  set_plain_mode ( Plain_Mode mode_n )
  {
    _plain_mode = mode_n;
  }

  private:
  std::unique_ptr< Leaf > _leaf;
  std::unique_ptr< Array > _array;
  std::unique_ptr< Branch > _branch;
  Plain_Mode _plain_mode = Plain_Mode::NONE;
};

} // namespace snc::settings::detail
