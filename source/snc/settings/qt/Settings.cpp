/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Settings.hpp"
#include <snc/settings/detail/database.hpp>
#include <snc/settings/qt/utility.hpp>
#include <sstream>
#include <utility>

namespace snc::settings::qt
{

Settings::Settings ( QObject * parent_n )
: QObject ( parent_n )
, _db ( std::make_shared< detail::Database > () )
{
}

Settings::Settings ( Database_Reference database_n, QObject * parent_n )
: QObject ( parent_n )
, _db ( std::move ( database_n ) )
{
}

Settings::~Settings () {}

Database_Reference
Settings::database () const
{
  return _db;
}

QVariant
Settings::get ( const QString & address_n, const QVariant & default_n ) const
{
  if ( auto ret = _db->get_value ( address_n.toStdString () ); ret.first ) {
    return make_qvariant ( ret.second );
  }
  return default_n;
}

void
Settings::set ( const QString & address_n, const QVariant & value_n )
{
  _db->set_value ( address_n.toStdString (), make_value ( value_n ) );
}

QString
Settings::jsonExportString () const
{
  std::ostringstream oss;
  _db->json_export_stream ( oss );
  return QString::fromStdString ( oss.str () );
}

bool
Settings::jsonImportString ( const QString & json_n )
{
  return _db->json_import_string ( json_n.toStdString () ).first;
}

} // namespace snc::settings::qt
