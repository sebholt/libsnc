/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/value.hpp>
#include <QVariant>

namespace snc::settings::qt
{

QVariant
make_qvariant ( const Value & value_n );

Value
make_value ( const QVariant & var_n );

} // namespace snc::settings::qt
