/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "settings.hpp"
#include <snc/settings/detail/database.hpp>
#include <sstream>
#include <utility>

namespace snc::settings
{

Settings::Settings ()
: _db ( std::make_shared< detail::Database > () )
{
}

Settings::Settings ( Settings && settings_n ) = default;

Settings::Settings ( Database_Reference database_n )
: _db ( std::move ( database_n ) )
{
}

Settings::~Settings () {}

Settings &
Settings::operator= ( Settings && settings_n ) = default;

Database_Reference
Settings::database () const
{
  return _db;
}

Value
Settings::get ( std::string_view address_n, Value default_n ) const
{
  if ( auto ret = _db->get_value ( address_n ); ret.first ) {
    return std::move ( ret.second );
  }
  return default_n;
}

void
Settings::set ( std::string_view address_n, Value value_n )
{
  _db->set_value ( address_n, std::move ( value_n ) );
}

std::string
Settings::json_export_string () const
{
  std::ostringstream oss;
  _db->json_export_stream ( oss );
  return oss.str ();
}

void
Settings::json_export_stream ( std::ostream & ostream_n ) const
{
  return _db->json_export_stream ( ostream_n );
}

std::pair< bool, std::string >
Settings::json_import_string ( std::string_view json_n )
{
  return _db->json_import_string ( json_n );
}

std::pair< bool, std::string >
Settings::json_import_stream ( std::istream & istream_n )
{
  return _db->json_import_stream ( istream_n );
}

} // namespace snc::settings
