/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/types.hpp>
#include <snc/settings/value.hpp>
#include <iosfwd>
#include <string_view>

namespace snc::settings
{

/** Settings tree
 */
class Settings
{
  public:
  // -- Construction

  Settings ();

  Settings ( const Settings & settings_n ) = delete;

  Settings ( Settings && settings_n );

  /// @brief Creates another view into the database
  Settings ( Database_Reference database_n );

  ~Settings ();

  // -- Assignment operators

  Settings &
  operator= ( const Settings & settings_n ) = delete;

  Settings &
  operator= ( Settings && settings_n );

  // -- Database

  /// @brief Shared database accessor
  Database_Reference
  database () const;

  // -- Reading

  /// @brief Get the value at @a address_n or return default_n.
  Value
  get ( std::string_view address_n, Value default_n = Value () ) const;

  // -- Writing

  void
  set ( std::string_view address_n, Value value_n );

  // -- Json export

  std::string
  json_export_string () const;

  void
  json_export_stream ( std::ostream & ostream_n ) const;

  // -- Json import

  std::pair< bool, std::string >
  json_import_string ( std::string_view json_n );

  std::pair< bool, std::string >
  json_import_stream ( std::istream & istream_n );

  private:
  // -- Implementation
  Database_Reference _db;
};

} // namespace snc::settings
