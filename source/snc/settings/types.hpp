/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <memory>

namespace snc::settings
{

// -- Forward declaration
namespace detail
{
class Database;
}

using Database_Reference = std::shared_ptr< detail::Database >;

} // namespace snc::settings
