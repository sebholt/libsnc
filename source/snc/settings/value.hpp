/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <string_view>

namespace snc::settings
{

/** Value variant
 */
class Value
{
  public:
  // -- Types
  enum class Type
  {
    NONE,
    BOOL,
    DOUBLE,
    STRING
  };
  class Shared;

  // -- Construction

  /// @brief Creates an invalid value
  Value ();

  /// @brief Copy constructor
  Value ( const Value & value_n );

  /// @brief Move constructor
  Value ( Value && value_n );

  /// @brief Boolean value
  Value ( bool value_n );

  /// @brief Double value
  Value ( double value_n );

  /// @brief String value
  Value ( std::string_view value_n );

  /// @brief String value
  Value ( const char * value_n )
  : Value ( std::string_view ( value_n ) )
  {
  }

  ~Value ();

  // -- Assignment operators

  Value &
  operator= ( const Value & value_n );

  Value &
  operator= ( Value && value_n );

  // -- Type

  Type
  type () const
  {
    return _type;
  }

  static std::string_view
  type_name ( Type type_n );

  std::string_view
  type_name () const
  {
    return type_name ( type () );
  }

  bool
  is_none () const
  {
    return ( _type == Type::NONE );
  }

  bool
  is_bool () const
  {
    return ( _type == Type::BOOL );
  }

  bool
  is_double () const
  {
    return ( _type == Type::DOUBLE );
  }

  bool
  is_string () const
  {
    return ( _type == Type::STRING );
  }

  // -- Value

  bool
  as_bool () const;

  double
  as_double () const;

  std::string_view
  as_string () const;

  // -- Comparison operators

  bool
  operator== ( const Value & value_n ) const;

  bool
  operator!= ( const Value & value_n ) const;

  private:
  // -- Storage
  Type _type;
  union
  {
    bool bv;
    double dv;
    Shared * sv;
  } _uni;
};

} // namespace snc::settings
