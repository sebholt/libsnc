/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/logt/context.hpp>
#include <sev/logt/reference.hpp>
#include <sev/mem/ring_fixed.hpp>
#include <QObject>
#include <cstdint>
#include <functional>
#include <vector>

// -- Forward declaration
namespace snc::svp::dash
{
class Panel;
}

namespace snc::svp::dash
{

/// @brief Manages multiple snc::svp::dash::Panel instances
///
class Dash : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY (
      bool sessionRunning READ session_is_running NOTIFY sessionStateChanged )
  Q_PROPERTY (
      bool sessionAborting READ session_is_aborting NOTIFY sessionStateChanged )

  public:
  // -- Construction

  Dash ( sev::logt::Context log_n, QObject * qparent_n = nullptr );

  virtual ~Dash ();

  // -- Accessors

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  const sev::logt::Reference &
  log_parent () const
  {
    return _log;
  }

  // -- Panels list

  const std::vector< Panel * > &
  panels () const
  {
    return _panels;
  }

  template < typename T >
  T *
  panel_by_type () const
  {
    for ( Panel * fctl : _panels ) {
      if ( T * cctl = dynamic_cast< T * > ( fctl ) ) {
        return cctl;
      }
    }
    return nullptr;
  }

  // -- Session state

  bool
  session_is_running () const
  {
    return _session.is_running;
  }

  bool
  session_is_aborting () const
  {
    return _session.is_aborting;
  }

  /// @brief Session is running and not aborting
  bool
  session_is_good () const
  {
    return _session.is_good;
  }

  Q_SIGNAL
  void
  sessionStateChanged ();

  // -- Start and abort

  Q_SLOT
  void
  start ();

  Q_SLOT
  void
  abort ();

  /// @brief Emitted when the session started
  Q_SIGNAL
  void
  started ();

  /// @brief Emitted when the session ended
  Q_SIGNAL
  void
  stopped ();

  // -- Central processing

  bool
  event ( QEvent * event_n ) override;

  protected:
  // -- Session interface

  virtual void
  session_begin ();

  virtual void
  session_abort ();

  virtual bool
  session_is_ready_to_end ();

  virtual void
  session_end ();

  // -- Processing

  virtual void
  process ();

  private:
  // -- Utility

  void
  process_abort ();

  // -- Panel interface

  friend class snc::svp::dash::Panel;

  void
  register_panel ( Panel * panel )
  {
    _panels.push_back ( panel );
  }

  void
  register_processing ( Panel * panel_n );

  void
  register_state_change ( Panel * panel_n );

  void
  register_processing_request ();

  private:
  // -- Attributes
  sev::logt::Context _log;
  // - Session state
  struct
  {
    bool is_running = false;
    bool is_aborting = false;
    bool is_good = false;
  } _session;
  // - Panel lists
  std::vector< Panel * > _panels;
  // -- Synchronous processing
  struct
  {
    bool requested = false;
    sev::mem::Ring_Fixed< Panel * > panels_process;
    sev::mem::Ring_Fixed< Panel * > panels_state;
  } _processing_sync;
};

} // namespace snc::svp::dash
