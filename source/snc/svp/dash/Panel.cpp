/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/frame/settings/dash/Panel.hpp>

namespace snc::svp::dash
{

Panel::Panel ( const Panel_Init & init_n, sev::unicode::View log_name_n )
: QObject ( &init_n.dash () )
, _dash ( init_n.dash () )
, _log ( init_n.dash ().log_parent (), log_name_n )
, _office_io ( *this )
{
  _event_accu.set_callback ( [ this ] () { this->request_processing (); } );

  // Register this Panel at the dash
  _dash.register_panel ( this );
}

Panel::~Panel () = default;

void
Panel::session_begin ()
{
  if ( auto * sp =
           dash ()
               .panel_by_type< snc::svs::frame::settings::dash::Panel > () ) {
    _settings = sp->settings_ptr ();
  } else {
    throw std::runtime_error ( "No settings panel found." );
  }

  _session.is_running = true;
}

void
Panel::session_abort ()
{
  _session.is_aborting = true;
}

void
Panel::session_end ()
{
  _session.is_running = false;
  _session.is_aborting = false;
}

bool
Panel::session_is_ready_to_end () const
{
  return _session.is_aborting && !_office_session.is_running;
}

void
Panel::office_session_begin ()
{
  _office_session.is_running = true;
  _office_session.is_good = true;
  request_processing ();
}

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  // Return invalid factory
  return Clerk_Factory_Handle ();
}

void
Panel::office_session_begin_clerk ()
{
}

void
Panel::office_session_abort ()
{
  _office_session.is_aborting = true;
  _office_session.is_good = false;
  request_processing ();
}

bool
Panel::office_session_is_ready_to_end () const
{
  return _office_session.is_aborting && _office_io.epool_tracker ().all_home ();
}

void
Panel::office_session_end ()
{
  _office_session.is_running = false;
  _office_session.is_aborting = false;
  _office_session.is_good = false;
  _office_io.session_end ();
  request_processing ();
}

void
Panel::office_event ( Office_Event & event_n [[maybe_unused]] )
{
  // Dummy implementation
  DEBUG_ASSERT ( false );
}

void
Panel::request_processing ()
{
  if ( sev::change ( _processing_requested, true ) ) {
    _dash.register_processing ( this );
  }
}

void
Panel::register_state_change ()
{
  if ( sev::change ( _state_changed, true ) ) {
    _dash.register_state_change ( this );
  }
}

void
Panel::state_changed ()
{
}

void
Panel::process ()
{
}

snc::bat::Cell_Post_Init
Panel::clerk_post_init ()
{
  return [ this ] ( snc::bat::Cell * cell_n ) {
    this->_office_io.session_begin ( static_cast< Clerk * > ( cell_n ) );
    this->office_session_begin_clerk ();
  };
}

void
Panel::pick_up_process ()
{
  DEBUG_ASSERT ( _processing_requested );
  _processing_requested = false;
  this->process ();
}

void
Panel::pick_up_state ()
{
  DEBUG_ASSERT ( _state_changed );
  _state_changed = false;
  this->state_changed ();
}

} // namespace snc::svp::dash
