/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/event/event.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::svp::dash
{
class Panel;
} // namespace snc::svp::dash
namespace snc::svp::office
{
class Clerk;
} // namespace snc::svp::office

namespace snc::svp::dash::event
{

/// @brief Communication pair
class Pair
{
  public:
  // -- Types

  using Panel = snc::svp::dash::Panel *;
  using Clerk = snc::svp::office::Clerk *;

  // -- Construction

  constexpr Pair ( Panel panel_n, Clerk clerk_n )
  : _panel ( panel_n )
  , _clerk ( clerk_n )
  {
    DEBUG_ASSERT ( _panel != nullptr );
    DEBUG_ASSERT ( _clerk != nullptr );
  }

  // -- Panel / Clerk

  constexpr Panel
  panel () const
  {
    return _panel;
  }

  constexpr Clerk
  clerk () const
  {
    return _clerk;
  }

  private:
  // -- Attributes
  Panel _panel = nullptr;
  Clerk _clerk = nullptr;
};

/// @brief Office to dash event base type
class In : public sev::event::Event
{
  public:
  // -- Types

  using Pair = snc::svp::dash::event::Pair;

  // -- Construction

  In ( std::uint_fast32_t event_type_n, Pair pair_n )
  : sev::event::Event ( event_type_n )
  , _pair ( pair_n )
  {
  }

  // -- Communication partners

  const Pair &
  pair () const
  {
    return _pair;
  }

  private:
  // -- Attributes
  const Pair _pair;
};

/// @brief Dash to office event base type
class Out : public sev::event::Event
{
  public:
  // -- Types

  using Pair = snc::svp::dash::event::Pair;

  // -- Construction

  Out ( std::uint_fast32_t event_type_n, Pair pair_n )
  : sev::event::Event ( event_type_n )
  , _pair ( pair_n )
  {
  }

  // -- Communication partners

  const Pair &
  pair () const
  {
    return _pair;
  }

  private:
  // -- Attributes
  const Pair _pair;
};

} // namespace snc::svp::dash::event

namespace snc::svp::dash::event::in
{

/// @brief State event template
template < std::uint_fast32_t ETYPE, typename STATE >
class State_T : public In
{
  public:
  // -- Types

  using State = STATE;
  static constexpr auto etype = ETYPE;

  // -- Construction

  State_T ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  // -- Acknowledged dash event count

  std::int_fast32_t
  dash_event_count () const
  {
    return _dash_event_count;
  }

  void
  set_dash_event_count ( std::int_fast32_t num_n )
  {
    _dash_event_count = num_n;
  }

  // -- State

  const State &
  state () const
  {
    return _state;
  }

  void
  set_state ( const State & state_n )
  {
    _state = state_n;
  }

  private:
  // -- Attributes
  std::int_fast32_t _dash_event_count = 0;
  State _state;
};

} // namespace snc::svp::dash::event::in
