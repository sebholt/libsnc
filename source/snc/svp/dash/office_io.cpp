/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "office_io.hpp"
#include <snc/svp/dash/Dash.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/frame/office/dash/Panel.hpp>
#include <stdexcept>

namespace snc::svp::dash
{

Office_IO::Office_IO ( Panel & panel_n )
: _panel ( panel_n )
{
}

void
Office_IO::session_begin ( Clerk * clerk_n )
{
  _clerk = clerk_n;
  _office =
      panel ().dash ().panel_by_type< snc::svs::frame::office::dash::Panel > ();
  if ( _office == nullptr ) {
    throw std::runtime_error ( "Office panel not found." );
  }
}

void
Office_IO::session_end ()
{
  _clerk = nullptr;
  _office = nullptr;
}

void
Office_IO::submit ( Office_Notification * event_n )
{
  if ( session_is_good () ) {
    stats_out_increment ();
    _office->submit_office_event ( event_n );
  } else {
    release ( event_n );
  }
}

void
Office_IO::release ( Office_Notification * event_n )
{
  // Ensure that we own this event
  DEBUG_ASSERT ( epool_tracker ().owned ( event_n ) );
  // Reset release the event
  event_n->pool ()->casted_reset_release_virtual ( event_n );
}

} // namespace snc::svp::dash
