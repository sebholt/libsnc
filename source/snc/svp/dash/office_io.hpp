/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <snc/svp/dash/events.hpp>

// -- Forward declaration
namespace snc::svp::office
{
class Clerk;
}
namespace snc::svs::frame::office::dash
{
class Panel;
}

namespace snc::svp::dash
{

class Office_IO
{
  public:
  // -- Types

  using Clerk = snc::svp::office::Clerk;
  using Office_Event = const snc::svp::dash::event::In;
  using Office_Notification = snc::svp::dash::event::Out;

  // -- Construction and session

  Office_IO ( Panel & panel_n );

  void
  session_begin ( Clerk * clerk_n );

  void
  session_end ();

  bool
  session_is_good () const
  {
    return ( _clerk != nullptr );
  }

  // -- Panel

  Panel &
  panel ()
  {
    return _panel;
  }

  const Panel &
  panel () const
  {
    return _panel;
  }

  // -- Event pool tracker

  sev::event::Pool_Tracker &
  epool_tracker ()
  {
    return _epool_tracker;
  }

  const sev::event::Pool_Tracker &
  epool_tracker () const
  {
    return _epool_tracker;
  }

  // -- Event allocation

  template < typename T, typename... Args >
  void
  allocate ( sev::event::Pool< T > & pool_n,
             std::size_t num_n,
             Args &&... args_n )
  {
    pool_n.set_size ( num_n,
                      Office_Notification::Pair{ &panel (), _clerk },
                      std::forward< Args > ( args_n )... );
  }

  template < typename T, typename... Args >
  T *
  acquire ( sev::event::Pool< T > & pool_n, Args &&... args_n )
  {
    if ( !pool_n.is_empty () ) {
      return pool_n.pop_not_empty ();
    }
    return pool_n.create ( Office_Notification::Pair{ &panel (), _clerk },
                           std::forward< Args > ( args_n )... );
  }

  // -- Outgoing event statistics

  bool
  events_unacknowledged () const
  {
    return ( _stats_out != 0 );
  }

  std::int_fast32_t
  stats_out () const
  {
    return _stats_out;
  }

  void
  stats_out_increment ()
  {
    ++_stats_out;
  }

  void
  stats_out_sub ( std::int_fast32_t delta_n )
  {
    DEBUG_ASSERT ( delta_n <= _stats_out );
    _stats_out -= delta_n;
  }

  // -- Event io

  void
  submit ( Office_Notification * event_n );

  void
  release ( Office_Notification * event_n );

  private:
  Panel & _panel;
  Clerk * _clerk = nullptr;
  snc::svs::frame::office::dash::Panel * _office = nullptr;
  std::int_fast32_t _stats_out = 0;
  sev::event::Pool_Tracker _epool_tracker;
};

} // namespace snc::svp::dash
