/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_init.hpp>

// -- Forward declaration
namespace snc::svp::dash
{
class Panel;
} // namespace snc::svp::dash

namespace snc::svp::office
{

/** Clerk default construction arguments */
class Clerk_Init
{
  public:
  // -- Types

  using Cell_Init = snc::bat::Cell_Init;
  using Panel = snc::svp::dash::Panel;

  // -- Construction

  Clerk_Init ( const Cell_Init & cell_n, Panel * panel_n )
  : _cell ( cell_n )
  , _panel ( panel_n )
  {
  }

  // -- Accessors

  const Cell_Init &
  cell () const
  {
    return _cell;
  }

  Panel *
  panel () const
  {
    return _panel;
  }

  private:
  // -- Attributes
  const Cell_Init & _cell;
  Panel * _panel = nullptr;
};

} // namespace snc::svp::office
