/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/clerk.hpp>
#include <snc/svp/ship/events.hpp>
#include <snc/svp/ship/sailor_factory_abstract.hpp>
#include <snc/svp/ship/sailor_factory_t.hpp>
#include <snc/svs/fac/tracker/office/client.hpp>
#include <snc/svs/frame/ship/office/client_ship_io.hpp>

namespace snc::svp::office
{

/// @brief Ship session aware clerk that manages a number of ship sailors.
///
class Factor : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  using Sailor_Factory_Handle = snc::svp::ship::Sailor_Factory_Handle;
  using Sailor_Picker =
      std::function< void ( Sailor_Factory_Handle && handle_n ) >;
  using Bridge_Notification = snc::svp::ship::event::In;
  using Bridge_Event = const snc::svp::ship::event::Out;
  struct Bridge_Session
  {
    bool is_running = false;
    bool is_aborting = false;
    bool is_good = false;
  };

  // -- Construction
  protected:
  Factor ( const Clerk_Init & init_n,
           sev::unicode::View cell_type_n,
           sev::unicode::View log_name_n );

  Factor ( const Clerk_Init & init_n, sev::unicode::View name_n );

  public:
  ~Factor () override;

  // -- Clients

  const snc::svs::fac::tracker::office::Client &
  tracker () const
  {
    return _tracker;
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Bridge session control interface

  /// @brief Bridge session state
  const Bridge_Session &
  bridge_session () const
  {
    return _bridge_session;
  }

  /// @brief Bridge session begin
  virtual void
  bridge_session_begin ();

  /// @brief Bridge session begin: Sailor factory generation
  ///
  /// Called after bridge_session_begin()
  virtual void
  bridge_session_begin_factory ( Sailor_Picker & pick_n );

  /// @brief Bridge session begin: Sailors are constructed
  ///
  /// Called after bridge_session_begin_factory() when all sailors
  /// were constructed. Allocate sailor events here.
  virtual void
  bridge_session_begin_sailor ();

  /// @brief Bridge session abort
  virtual void
  bridge_session_abort ();

  /// @brief Returns true when the bridge session is ready to end
  virtual bool
  bridge_session_is_ready_to_end () const;

  /// @brief Bridge session end
  virtual void
  bridge_session_end ();

  // -- Bridge event processing

  virtual void
  bridge_event ( Bridge_Event & event_n );

  // -- Bridge notification generation

  virtual void
  bridge_notify ();

  // -- Ship event io

  auto &
  ship_io ()
  {
    return _ship_io;
  }

  const auto &
  ship_io () const
  {
    return _ship_io;
  }

  protected:
  // -- Bridge creation utility

  template < typename ST, typename... Args >
  Sailor_Factory_Handle
  make_sailor_factory ( Args... args_n )
  {
    Sailor_Factory_Handle res =
        std::make_shared< snc::svp::ship::Sailor_Factory_T< ST, Args... > > (
            this, std::move ( args_n )... );
    res->set_post_init ( sailor_post_init () );
    return res;
  }

  private:
  snc::bat::Cell_Post_Init
  sailor_post_init ();

  private:
  // -- Bridge session state
  Bridge_Session _bridge_session;
  // -- Clients
  snc::svs::fac::tracker::office::Client _tracker;
  // -- Ship event io interface
  snc::svs::frame::ship::office::Client_Ship_IO _ship_io;
};

} // namespace snc::svp::office
