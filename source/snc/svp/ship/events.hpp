/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/assert.hpp>
#include <sev/event/event.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
} // namespace snc::svp::office
namespace snc::svp::ship
{
class Sailor;
} // namespace snc::svp::ship

namespace snc::svp::ship::event
{

/// @brief Communication pair
class Pair
{
  public:
  // -- Types
  using Factor = snc::svp::office::Factor *;
  using Sailor = snc::svp::ship::Sailor *;

  // -- Construction

  constexpr Pair ( Factor factor_n, Sailor sailor_n )
  : _factor ( factor_n )
  , _sailor ( sailor_n )
  {
    DEBUG_ASSERT ( _factor != nullptr );
    DEBUG_ASSERT ( _sailor != nullptr );
  }

  // -- Clerk / Sailor

  constexpr Factor
  factor () const
  {
    return _factor;
  }

  constexpr Sailor
  sailor () const
  {
    return _sailor;
  }

  private:
  Factor _factor = nullptr;
  Sailor _sailor = nullptr;
};

/// @brief Office to bridge event base type
class In : public sev::event::Event
{
  public:
  // -- Types

  using Pair = snc::svp::ship::event::Pair;

  // -- Construction

  In ( std::uint_fast32_t event_type_n, Pair pair_n )
  : sev::event::Event ( event_type_n )
  , _pair ( pair_n )
  {
  }

  // -- Communication partners

  const Pair &
  pair () const
  {
    return _pair;
  }

  private:
  // -- Attributes
  const Pair _pair;
};

/// @brief Bridge to office event base type
class Out : public sev::event::Event
{
  public:
  // -- Types

  using Pair = snc::svp::ship::event::Pair;

  // -- Construction

  Out ( std::uint_fast32_t event_type_n, Pair pair_n )
  : sev::event::Event ( event_type_n )
  , _pair ( pair_n )
  {
  }

  // -- Communication partners

  const Pair &
  pair () const
  {
    return _pair;
  }

  private:
  // -- Attributes
  const Pair _pair;
};

} // namespace snc::svp::ship::event

namespace snc::svp::ship::event::out
{

/// @brief State event template
template < std::uint_fast32_t ETYPE, typename PSTATE >
class State_T : public Out
{
  public:
  // -- Types
  using Pilot_State = PSTATE;
  static constexpr auto etype = ETYPE;

  State_T ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  // -- Acknowledged office event count

  std::int_fast32_t
  office_event_count () const
  {
    return _office_event_count;
  }

  void
  set_office_event_count ( std::int_fast32_t num_n )
  {
    _office_event_count = num_n;
  }

  // -- Pilot state

  const Pilot_State &
  state () const
  {
    return _state;
  }

  void
  set_state ( const Pilot_State & state_n )
  {
    _state = state_n;
  }

  private:
  // -- Attributes
  std::int_fast32_t _office_event_count = 0;
  Pilot_State _state;
};

} // namespace snc::svp::ship::event::out
