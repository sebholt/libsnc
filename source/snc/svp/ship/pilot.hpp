/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/svp/ship/sailor.hpp>

namespace snc::svp::ship
{

class Pilot : public Sailor
{
  public:
  // -- Types
  using Super = Sailor;

  // -- Construction

  Pilot ( const Sailor_Init & init_n,
          sev::unicode::View cell_type_n,
          sev::unicode::View log_name_n );

  Pilot ( const Sailor_Init & init_n, sev::unicode::View name_n );

  ~Pilot ();

  // -- Device info

  const snc::device::statics::Statics &
  device_statics () const
  {
    return *_device_info->statics ();
  }

  const snc::device::statics::handle::Statics &
  device_statics_handle () const
  {
    return _device_info->statics ();
  }

  snc::device::state::State const &
  device_state () const
  {
    return _device_info->state ();
  }

  private:
  // -- Device information
  snc::device::handle::Info _device_info;
};

} // namespace snc::svp::ship
