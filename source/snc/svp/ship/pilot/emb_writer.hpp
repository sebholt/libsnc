/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/msg/stream.hpp>

namespace snc::svp::ship::pilot
{

/** @brief Writes messages to the message stream.
 *
 */
class Emb_Writer
{
  public:
  // -- Construction

  Emb_Writer ( snc::emb::msg::Stream * stream_n )
  : _stream ( stream_n )
  {
  }

  // -- Interface

  /// @brief Create a new message at the back
  template < typename T, typename... Args >
  T &
  emplace ( Args &&... args_n ) const
  {
    return _stream->emplace< T > ( std::forward< Args > ( args_n )... );
  }

  /// @brief Create a new message at the back and set @a msg_n to it.
  template < typename T, typename... Args >
  void
  acquire ( T *& msg_n, Args &&... args_n ) const
  {
    return _stream->acquire ( msg_n, std::forward< Args > ( args_n )... );
  }

  /// @brief Create a copy of @a at the back.
  template < typename T >
  T &
  push ( const T & msg_n ) const
  {
    return _stream->push ( msg_n );
  }

  private:
  // -- Attributes
  snc::emb::msg::Stream * _stream = nullptr;
};

} // namespace snc::svp::ship::pilot
