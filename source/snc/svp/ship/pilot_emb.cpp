/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_emb.hpp"
#include <sev/utility.hpp>

namespace snc::svp::ship
{

Pilot_Emb::Pilot_Emb ( const Sailor_Init & init_n,
                       sev::unicode::View cell_type_n,
                       sev::unicode::View log_name_n )
: Super ( init_n, cell_type_n, log_name_n )
, _tracker ( this )
, _serial_device ( this )
{
}

Pilot_Emb::Pilot_Emb ( const Sailor_Init & init_n, sev::unicode::View name_n )
: Pilot_Emb ( init_n, name_n, name_n )
{
}

Pilot_Emb::~Pilot_Emb () = default;

void
Pilot_Emb::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Clients
  _tracker.connect_required ();
  _serial_device.connect_required ();
}

void
Pilot_Emb::device_messages_register ( bool flag_n )
{
  _serial_device.subscribe ( flag_n );
}

void
Pilot_Emb::device_messages_request ()
{
  _serial_device.request_pick_up ();
}

void
Pilot_Emb::device_messages_acquire ( Emb_Writer stream_n [[maybe_unused]] )
{
}

} // namespace snc::svp::ship
