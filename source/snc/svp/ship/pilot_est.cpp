/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_est.hpp"
#include <snc/est/stepper_data_composer.hpp>

namespace snc::svp::ship
{

Pilot_Est::Pilot_Est ( const Sailor_Init & init_n,
                       sev::unicode::View cell_type_n,
                       sev::unicode::View log_name_n )
: Super ( init_n, cell_type_n, log_name_n )
, _tracker ( this )
, _serial_device ( this )
{
}

Pilot_Est::Pilot_Est ( const Sailor_Init & init_n, sev::unicode::View name_n )
: Pilot_Est ( init_n, name_n, name_n )
{
}

Pilot_Est::~Pilot_Est () = default;

void
Pilot_Est::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Clients
  _tracker.connect_required ();
  _serial_device.connect_required ();
}

void
Pilot_Est::device_messages_register ( bool flag_n )
{
  _serial_device.subscribe ( flag_n );
}

void
Pilot_Est::device_messages_request ()
{
  _serial_device.request_pick_up ();
}

void
Pilot_Est::device_messages_generate ()
{
}

void
Pilot_Est::device_stepper_data_push (
    snc::est::Stepper_Data_Composer const & composer_n )
{
  // Track using the specialized tracking interface
  _tracker.track_stepper_data_out ( composer_n );
  // Serialize and write to serial device
  std::array< std::uint8_t, 256 > buffer;
  composer_n.serialize_message ( buffer );
  _serial_device.push_message ( buffer.data () );
}

void
Pilot_Est::device_message_push ( std::uint8_t const * message_n )
{
  _tracker.track_message_out ( message_n );
  _serial_device.push_message ( message_n );
}

} // namespace snc::svp::ship
