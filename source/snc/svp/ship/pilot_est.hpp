/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/pilot.hpp>
#include <snc/svs/fac/serial_device/ship/client_est_out.hpp>
#include <snc/svs/fac/tracker/ship/est/client.hpp>

// -- Forward declaration
namespace snc::est
{
class Stepper_Data_Composer;
}

namespace snc::svp::ship
{

/// @brief EStepper pilot base class
///
class Pilot_Est : public Pilot
{
  public:
  // -- Types
  using Super = Pilot;

  // -- Construction

  Pilot_Est ( const Sailor_Init & init_n,
              sev::unicode::View cell_type_n,
              sev::unicode::View log_name_n );

  Pilot_Est ( const Sailor_Init & init_n, sev::unicode::View name_n );

  ~Pilot_Est ();

  // -- Accessors

  const snc::svs::fac::tracker::ship::est::Client &
  tracker () const
  {
    return _tracker;
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Embedded device message generation registration

  /// @brief Whether or not this pilot is registered for device message pick up
  bool
  device_messages_registered () const
  {
    return _serial_device.subscribed ();
  }

  /// @brief Register this pilot for device message generation
  void
  device_messages_register ( bool flag_n = true );

  /// @brief Unregister this pilot for device message generation
  void
  device_messages_unregister ()
  {
    device_messages_register ( false );
  }

  // -- Embedded device message generation

  /// @brief Request device message pick up at the next possible time
  void
  device_messages_request ();

  /// @brief Reimplement this to generate device messages
  virtual void
  device_messages_generate ();

  /// @brief Specialization of device_message_push for stepper data.
  void
  device_stepper_data_push (
      snc::est::Stepper_Data_Composer const & composer_n );

  /// @brief Pushes a single message to the serial data stream and tracks it
  void
  device_message_push ( std::uint8_t const * message_n );

  private:
  // -- Tracker
  snc::svs::fac::tracker::ship::est::Client _tracker;
  // -- Serial device
  snc::svs::fac::serial_device::ship::Client_Est_Out _serial_device;
};

} // namespace snc::svp::ship
