/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "sailor.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::svp::ship
{

Sailor::Sailor ( const Sailor_Init & init_n,
                 sev::unicode::View cell_type_n,
                 sev::unicode::View log_name_n )
: Super ( init_n.cell (), cell_type_n, log_name_n )
, _office_io ( this, init_n.factor () )
{
}

Sailor::Sailor ( const Sailor_Init & init_n, sev::unicode::View name_n )
: Sailor ( init_n, name_n, name_n )
{
}

Sailor::~Sailor () {}

void
Sailor::cell_session_begin ()
{
  Super::cell_session_begin ();
  _office_io.connect_required ();
  _office_io.session ().begin ();
}

void
Sailor::cell_session_abort ()
{
  Super::cell_session_abort ();
  _office_io.session ().end ();
}

bool
Sailor::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () &&
         office_io ().epool_tracker ().all_home ();
}

void
Sailor::office_event ( Office_Event & event_n )
{
  // Dummy implementation
  (void)event_n;
  DEBUG_ASSERT ( false );
}

void
Sailor::office_notify ()
{
}

} // namespace snc::svp::ship
