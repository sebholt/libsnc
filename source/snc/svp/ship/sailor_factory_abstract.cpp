/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "sailor_factory_abstract.hpp"

namespace snc::svp::ship
{

Sailor_Factory_Abstract::Sailor_Factory_Abstract ( Factor * factor_n )
: _factor ( factor_n )
{
}

Sailor_Factory_Abstract::~Sailor_Factory_Abstract () = default;

} // namespace snc::svp::ship
