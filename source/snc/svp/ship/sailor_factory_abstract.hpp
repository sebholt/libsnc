/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/cell_factory_abstract.hpp>
#include <snc/device/handle.hpp>
#include <memory>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
} // namespace snc::svp::office

namespace snc::svp::ship
{

/// @brief Sailor abstract factory class
///
class Sailor_Factory_Abstract : public snc::bat::Cell_Factory_Abstract
{
  public:
  // -- Types

  using Factor = snc::svp::office::Factor;

  // -- Construction

  Sailor_Factory_Abstract ( Factor * factor_n );

  ~Sailor_Factory_Abstract ();

  // -- Accessors

  Factor *
  factor () const
  {
    return _factor;
  }

  const snc::device::handle::Info_Writeable &
  device_info () const
  {
    return _device_info;
  }

  void
  set_device_info ( const snc::device::handle::Info_Writeable & device_info_n )
  {
    _device_info = device_info_n;
  }

  private:
  // -- Attributes
  Factor * _factor = nullptr;
  snc::device::handle::Info_Writeable _device_info;
};

/// @brief Sailor factory handle
///
using Sailor_Factory_Handle = std::shared_ptr< Sailor_Factory_Abstract >;

} // namespace snc::svp::ship
