/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/actor/dash/events.hpp>
#include <snc/svs/ctl/actor/office/state.hpp>
#include <cstdint>

namespace snc::svs::ctl::actor::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( bool breakHardAvailable READ breakHardAvailable NOTIFY
                   breakHardAvailableChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  void
  office_session_begin_clerk () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- State

  const office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Break hard availability

  bool
  breakHardAvailable () const
  {
    return _qtState.break_hard_available;
  }

  Q_SIGNAL
  void
  breakHardAvailableChanged ();

  // -- Break hard interface

  Q_INVOKABLE
  void
  breakHard ();

  private:
  // -- Attributes
  office::State _state;
  struct
  {
    bool break_hard_available = false;
  } _qtState;

  // -- Office event io
  sev::event::Pool< dash::event::out::Break_Hard > _event_pool_break_hard;
};

} // namespace snc::svs::ctl::actor::dash
