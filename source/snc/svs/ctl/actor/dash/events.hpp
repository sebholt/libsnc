/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/actor/office/state.hpp>

namespace snc::svs::ctl::actor::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, office::State >;

} // namespace snc::svs::ctl::actor::dash::event::in

namespace snc::svs::ctl::actor::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t JOB = 0;
};

using Out = snc::svp::dash::event::Out;

class Break_Hard : public Out
{
  public:
  static constexpr auto etype = Type::JOB;

  Break_Hard ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

} // namespace snc::svs::ctl::actor::dash::event::out
