/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/svs/ctl/actor/dash/events.hpp>
#include <cstdint>

namespace snc::svs::ctl::actor::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Actor" )
, _service ( *this )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.init_connections ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Break_Hard::etype:
    dash_event_break_hard ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::dash_event_break_hard ( Dash_Event & event_n [[maybe_unused]] )
{
  log ().cat ( sev::logt::FL_DEBUG_0, "Break hard event." );
  _service.actors_break_hard ();
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _service.state () );
    _state_dash = _service.state ();
  }
}

void
Clerk::state_update ()
{
  // Notify on demand
  if ( _state_dash != _service.state () ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::ctl::actor::office
