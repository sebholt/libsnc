/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/actor/dash/events.hpp>
#include <snc/svs/ctl/actor/office/service.hpp>
#include <snc/svs/ctl/actor/office/state.hpp>

namespace snc::svs::ctl::actor::office
{

// -- Forward declaration
class Service;

class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Accessors

  auto &
  service ()
  {
    return _service;
  }

  const auto &
  service () const
  {
    return _service;
  }

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_break_hard ( Dash_Event & event_n );

  void
  dash_notify () override;

  private:
  // -- Utility
  friend class snc::svs::ctl::actor::office::Service;

  void
  state_update ();

  private:
  // -- Resources
  Service _service;

  // -- State
  State _state_dash;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::ctl::actor::office
