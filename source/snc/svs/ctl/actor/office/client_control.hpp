/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/ctl/actor/office/client_base.hpp>

namespace snc::svs::ctl::actor::office
{

/** The control client addresses all actors.
 */
class Client_Control : public Client_Base
{
  // -- Types
  private:
  using Super = Client_Base;

  public:
  // -- Construction

  Client_Control ( User * user_n );

  ~Client_Control ();

  // -- Break locks signals

  void
  break_locks_signal_connect () const;

  void
  break_locks_signal_disconnect () const;

  // -- Break locks

  bool
  break_locked () const;

  /// @brief Install the abort break lock
  void
  break_lock_install_abort () const;
};

} // namespace snc::svs::ctl::actor::office
