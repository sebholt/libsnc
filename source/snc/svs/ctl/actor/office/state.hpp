/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::svs::ctl::actor::office
{

class State
{
  public:
  // -- Construction

  State () = default;

  ~State () = default;

  // -- Setup

  void
  reset ()
  {
    _break_hard_available = false;
  }

  // -- Availability

  bool
  break_hard_available () const
  {
    return _break_hard_available;
  }

  void
  set_break_hard_available ( std::uint_fast32_t flag_n )
  {
    _break_hard_available = flag_n;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( _break_hard_available == state_n._break_hard_available );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return ( _break_hard_available != state_n._break_hard_available );
  }

  private:
  // -- Attributes
  bool _break_hard_available;
};

} // namespace snc::svs::ctl::actor::office
