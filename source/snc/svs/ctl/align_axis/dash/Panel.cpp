/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/align_axis/office/clerk.hpp>

namespace snc::svs::ctl::align_axis::dash
{

Panel::Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Align-Axis[", axis_index_n, "]" ) )
, _axis_index ( axis_index_n )
, _event_pool_job ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::align_axis::office::Clerk,
                             std::uint_fast32_t > ( axis_index () );
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();

  office_io ().allocate ( _event_pool_job, 4 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::State::etype:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.available, state ().available () ) ) {
    emit availableChanged ();
  }
  if ( sev::change ( _qtState.aligning, state ().is_aligning () ) ) {
    emit aligningChanged ();
  }
  if ( sev::change ( _qtState.startable, state ().startable () ) ) {
    emit startableChanged ();
  }
  if ( sev::change ( _qtState.stoppable, state ().stoppable () ) ) {
    emit stoppableChanged ();
  }
}

void
Panel::alignBegin ()
{
  request_job ( Job::Command::ALIGN );
}

void
Panel::alignSet ()
{
  request_job ( Job::Command::SET );
}

void
Panel::alignAbort ()
{
  request_job ( Job::Command::ABORT );
}

void
Panel::request_job ( const Job & job_n )
{
  if ( !office_session_is_good () ) {
    return;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_job );
    event->set_job ( job_n );
    office_io ().submit ( event );
  }
}

} // namespace snc::svs::ctl::align_axis::dash
