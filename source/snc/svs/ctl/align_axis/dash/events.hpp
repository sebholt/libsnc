/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/align_axis/job.hpp>
#include <snc/svs/ctl/align_axis/office/state.hpp>

namespace snc::svs::ctl::align_axis::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, office::State >;

} // namespace snc::svs::ctl::align_axis::dash::event::in

namespace snc::svs::ctl::align_axis::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t JOB = 0;
};

using Out = snc::svp::dash::event::Out;

class Job : public Out
{
  public:
  static constexpr auto etype = Type::JOB;

  Job ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  const snc::svs::ctl::align_axis::Job &
  job () const
  {
    return _job;
  }

  void
  set_job ( const snc::svs::ctl::align_axis::Job & job_n )
  {
    _job = job_n;
  }

  private:
  snc::svs::ctl::align_axis::Job _job;
};

} // namespace snc::svs::ctl::align_axis::dash::event::out
