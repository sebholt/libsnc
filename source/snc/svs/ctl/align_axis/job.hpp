/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_align/job.hpp>

namespace snc::svs::ctl::align_axis
{

using Job = snc::svs::fac::axis_align::Job;

} // namespace snc::svs::ctl::align_axis
