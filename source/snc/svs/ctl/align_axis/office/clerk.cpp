/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/svs/ctl/align_axis/dash/events.hpp>
#include <cstdint>
#include <stdexcept>

namespace snc::svs::ctl::align_axis::office
{

Clerk::Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Align-Axis[", axis_index_n, "]" ) )
, _actor ( this )
, _pilot ( this, axis_index_n )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );

  // Setup actor interface
  _actor.set_break_hard_func ( [ this ] () {
    // Clear requests
    _job_request.reset ();
    // Abort pilot
    _pilot_control.break_hard ();
  } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();
  if ( !_pilot.connect () ) {
    log ().cat ( sev::logt::FL_DEBUG_0, "Align service not found" );
  }
  _pilot.session ().set_begin_callback ( [ this ] () { state_update (); } );
  _pilot.session ().set_end_callback ( [ this ] () {
    // Release pilot lock
    _pilot_control.release ();

    _job_request.reset ();
    _state.reset ();
    state_update ();
  } );

  state_update ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::out::Job::etype:
    dash_event_job ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::dash_event_job ( Dash_Event & event_n )
{
  // When do we not accept the request
  if ( !_pilot_control && !_pilot.control_available () ) {
    return;
  }

  const auto & job =
      static_cast< const dash::event::out::Job & > ( event_n ).job ();
  if ( _job_request.cmd () == job.cmd () ) {
    return;
  }

  // Accept job
  _job_request = job;
  request_processing ();
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
    _state_emitted = _state;
  }
}

void
Clerk::process ()
{
  process_request ();
  service_control_auto_release ();
  state_update ();
}

void
Clerk::process_request ()
{
  // Any requests?
  if ( !_job_request.is_valid () ) {
    return;
  }
  // Acquire service control
  if ( !service_control_acquire () ) {
    _job_request.reset ();
    return;
  }

  // Start job
  _pilot_control.start_job ( _job_request );
  _job_request.reset ();
}

bool
Clerk::service_control_acquire ()
{
  // Already locked?
  if ( _pilot_control ) {
    return true;
  }
  if ( !_pilot.is_alignable () ) {
    return false;
  }

  // Acquire service control and activate actor
  _pilot_control = _pilot.control ();
  _actor.go_active ();
  if ( !_pilot_control || !_actor.is_active () ) {
    _pilot_control.release ();
    _actor.go_idle ();
  }

  return _pilot_control;
}

void
Clerk::service_control_auto_release ()
{
  // Check if we control the pilot
  if ( !_pilot_control ) {
    return;
  }
  // Check if the pilot has finished
  if ( _pilot.is_aligning () ) {
    return;
  }

  // Release pilot an go idle
  _pilot_control.release ();
  _actor.go_idle ();

  state_update ();
}

void
Clerk::state_update ()
{
  _state.blocks ().clear ();
  if ( !_pilot.is_connected () ) {
    _state.blocks ().set ( State::Block::PILOT_NOT_AVAILABLE );
  } else {
    _pilot.acquire_state ( _state );
    if ( !_pilot_control && !_pilot.control_available () ) {
      _state.blocks ().set ( State::Block::PILOT_NOT_AVAILABLE );
    } else if ( !_pilot.is_alignable () ) {
      _state.blocks ().set ( State::Block::NOT_ALIGNABLE );
    }
  }

  // Notify on demand
  if ( _state_emitted != _state ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::ctl::align_axis::office
