/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <string>

namespace snc::svs::ctl::autostart::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t DEVICE_STATICS_JSON = 0;
};

using Out = snc::svp::dash::event::Out;

class Device_Statics_Json : public Out
{
  public:
  static constexpr std::uint_fast32_t etype = Type::DEVICE_STATICS_JSON;

  Device_Statics_Json ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _default_device = false;
    _json.clear ();
    _json.shrink_to_fit ();
  }

  bool
  default_device () const
  {
    return _default_device;
  }

  void
  set_default_device ( bool flag_n )
  {
    _default_device = flag_n;
  }

  std::string const &
  json () const
  {
    return _json;
  }

  void
  set_json ( std::string json_n )
  {
    _json = std::move ( json_n );
  }

  private:
  bool _default_device = false;
  ;
  std::string _json;
};

} // namespace snc::svs::ctl::autostart::dash::event::out
