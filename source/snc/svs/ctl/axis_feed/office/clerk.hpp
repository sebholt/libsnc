/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/axis_feed/dash/events.hpp>
#include <snc/svs/ctl/axis_feed/office/service.hpp>
#include <snc/svs/ctl/axis_feed/office/state.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>

namespace snc::svs::ctl::axis_feed::office
{

// -- Forward declaration
class Client;

/// @brief Clerk
///
class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Clerk ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_event ( Dash_Event & event_n ) override;

  void
  dash_event_direction ( Dash_Event & event_n );

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  void
  process_requests ();

  private:
  // -- Service interface
  friend snc::svs::ctl::axis_feed::office::Service;

  void
  state_changed ();

  private:
  // -- State
  State _state_emitted;

  // -- Resources
  Service _service;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::ctl::axis_feed::office
