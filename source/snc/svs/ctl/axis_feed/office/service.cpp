/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svs/ctl/axis_feed/office/clerk.hpp>
#include <snc/svs/ctl/axis_feed/office/client.hpp>

namespace snc::svs::ctl::axis_feed::office
{

Service::Service ( Provider & provider_n, std::uint_fast32_t axis_index_n )
: Super ( provider_n, axis_index_n )
, _pilot ( &provider_n, axis_index_n )
{
}

Service::~Service () = default;

void
Service::init_connections ()
{
  _pilot.connect ();
  _pilot.session ().set_begin_callback ( [ this ] () { session_begin (); } );
  _pilot.session ().set_end_callback ( [ this ] () { session_end (); } );
}

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  // log ().cat ( sev::logt::FL_DEBUG_0, "Session begin." );

  // Setup feed modes
  {
    auto axis_info = _pilot.axis_info ();
    using Mode = snc::svs::fac::axis_feed::Mode;
    _feed_modes_auto.forward.emplace ();
    _feed_modes_auto.forward->set_dir ( Mode::Dir::FORWARD );
    _feed_modes_auto.forward->speed_ref ().set_speed (
        axis_info.statics ()->geo ().speed_max () );
    _feed_modes_auto.forward->speed_ref ().set_accel (
        axis_info.statics ()->geo ().accel_max () );
    _feed_modes_auto.forward->speed_ref ().set_forward ( true );

    _feed_modes_auto.backward.emplace ( *_feed_modes_auto.forward );
    _feed_modes_auto.backward->set_dir ( Mode::Dir::BACKWARD );
    _feed_modes_auto.backward->speed_ref ().set_forward ( false );
  }

  // Notify clients
  state_update ();
  clients_session_begin ();
}

void
Service::session_end ()
{
  // log ().cat ( sev::logt::FL_DEBUG_0, "Session end." );

  // Reset state
  _feed_modes_auto.reset ();
  _feed_modes_user.reset ();
  _state.reset ();
  _requests.reset ();

  // Notify clients
  state_update ();
  clients_session_end ();
}

void
Service::request_direction ( snc::svs::fac::axis_feed::Mode::Dir dir_n )
{
  if ( !_pilot.session_is_good () ) {
    return;
  }

  _requests.reg.set ( Request::DIRECTION );
  _requests.dir = dir_n;

  provider ().request_processing ();
}

void
Service::process ()
{
  process_requests ();
  state_update ();
}

void
Service::process_requests ()
{
  if ( _requests.reg.is_empty () ) {
    return;
  }

  // Acquire pilot control
  auto pilot_control = _pilot.control ();
  if ( !pilot_control ) {
    // Control not available.  Clear requests.
    _requests.reset ();
    return;
  }

  if ( _requests.reg.test_any_unset ( Request::DIRECTION ) ) {
    using Dir = snc::svs::fac::axis_feed::Mode::Dir;
    switch ( _requests.dir ) {
    case Dir::NONE:
      pilot_control.break_normal ();
      break;
    case Dir::FORWARD:
      if ( _feed_modes_user.forward ) {
        pilot_control.set_mode ( *_feed_modes_user.forward );
      } else if ( _feed_modes_auto.forward ) {
        pilot_control.set_mode ( *_feed_modes_auto.forward );
      }
      break;
    case Dir::BACKWARD:
      if ( _feed_modes_user.backward ) {
        pilot_control.set_mode ( *_feed_modes_user.backward );
      } else if ( _feed_modes_auto.backward ) {
        pilot_control.set_mode ( *_feed_modes_auto.backward );
      }
      break;
    }
  }
}

bool
Service::control_acquirable () const
{
  return Super::control_acquirable () && _pilot.session_is_good ();
}

bool
Service::control_set_default_mode (
    const snc::svs::fac::axis_feed::Mode & mode_n )
{
  if ( mode_n.dir_is_none () ) {
    return false;
  }

  if ( mode_n.dir_is_forward () ) {
    _feed_modes_user.forward.emplace ( mode_n );
  } else if ( mode_n.dir_is_backward () ) {
    _feed_modes_user.backward = mode_n;
  }

  return true;
}

void
Service::state_update ()
{
  // Update state
  if ( !_pilot.is_connected () ) {
    _state.blocks ().assign ( State::Block::PILOT_NOT_AVAILABLE );
  } else {
    // Copy pilot state
    auto pstate = _pilot.acquire_state ();
    _state.set_is_active ( pstate.is_active () );
    _state.set_is_feeding ( pstate.is_feeding () );

    // Update blocks
    _state.blocks ().unset ( State::Block::INVALID_STATE );
    _state.blocks ().set ( State::Block::PILOT_NOT_AVAILABLE,
                           !_pilot.control_available () );
  }

  signal_state ().send ();
  provider ().state_changed ();
}

} // namespace snc::svs::ctl::axis_feed::office
