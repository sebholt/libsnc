/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/svs/fac/axis_feed/mode.hpp>
#include <cstdint>

namespace snc::svs::ctl::axis_feed::office
{

class State
{
  public:
  // -- Types

  using Mode = snc::svs::fac::axis_feed::Mode;

  struct Block
  {
    /// @brief State is not valid
    static constexpr std::uint_fast32_t INVALID_STATE = ( 1 << 0 );
    /// @brief Axis is unused
    static constexpr std::uint_fast32_t PILOT_NOT_AVAILABLE = ( 1 << 1 );
  };

  // -- Setup

  /// @brief Reset to construction state
  void
  reset ()
  {
    _is_active = false;
    _is_feeding = false;
    _blocks.assign ( Block::INVALID_STATE );
  }

  // -- Accessors

  auto &
  blocks ()
  {
    return _blocks;
  }

  const auto &
  blocks () const
  {
    return _blocks;
  }

  bool
  available () const
  {
    return _blocks.is_empty ();
  }

  bool
  is_active () const
  {
    return _is_active;
  }

  void
  set_is_active ( bool flag_n )
  {
    _is_active = flag_n;
  }

  bool
  is_feeding () const
  {
    return _is_feeding;
  }

  void
  set_is_feeding ( bool flag_n )
  {
    _is_feeding = flag_n;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( ( _blocks == state_n._blocks ) );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return ( ( _blocks != state_n._blocks ) );
  }

  private:
  // -- Attributes
  bool _is_active = false;
  bool _is_feeding = false;
  sev::mem::Flags_Fast32 _blocks = Block::INVALID_STATE;
};

} // namespace snc::svs::ctl::axis_feed::office
