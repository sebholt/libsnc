/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/axis_move/office/clerk.hpp>

namespace snc::svs::ctl::axis_move::dash
{

Panel::Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Axis[", axis_index_n, "]-Move" ) )
, _axis_index ( axis_index_n )
, _event_pool_break ( office_io ().epool_tracker () )
, _event_pool_dynamics ( office_io ().epool_tracker () )
, _event_pool_steps ( office_io ().epool_tracker () )
, _event_pool_length ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::axis_move::office::Clerk,
                             std::uint_fast32_t > ( axis_index () );
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();

  office_io ().allocate ( _event_pool_break, 2 );
  office_io ().allocate ( _event_pool_dynamics, 4 );
  office_io ().allocate ( _event_pool_steps, 4 );
  office_io ().allocate ( _event_pool_length, 4 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.available, state ().available () ) ) {
    emit availableChanged ();
  }
}

void
Panel::break_motion ()
{
  // Submit event
  office_io ().submit ( office_io ().acquire ( _event_pool_break ) );
}

bool
Panel::move_dynamics ( const snc::svs::fac::axis_move::Dynamics & dyn_n )
{
  // When do we not accept the request?
  if ( !_state.available () //
       || !office_session_is_good () ) {
    return false;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_dynamics );
    event->set_dynamics ( dyn_n );
    office_io ().submit ( event );
  }
  return true;
}

bool
Panel::move_steps ( std::int64_t steps_n )
{
  // When do we not accept the request?
  if ( !_state.available ()          //
       || !office_session_is_good () //
       || ( steps_n == 0 ) ) {
    return false;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_steps );
    event->set_steps ( steps_n );
    office_io ().submit ( event );
  }
  return true;
}

bool
Panel::move_unit_length ( Unit unit_n, double length_n )
{
  // When do we not accept the request?
  if ( !_state.available ()          //
       || !office_session_is_good () //
       || ( length_n == 0.0 ) ) {
    return false;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_length );
    event->set_unit ( unit_n );
    event->set_amount ( length_n );
    office_io ().submit ( event );
  }
  return true;
}

bool
Panel::move_length ( double length_n )
{
  return move_unit_length ( Unit::LENGTH, length_n );
}

bool
Panel::move_radians ( double length_n )
{
  return move_unit_length ( Unit::RADIANS, length_n );
}

bool
Panel::move_degrees ( double length_n )
{
  return move_unit_length ( Unit::DEGREES, length_n );
}

} // namespace snc::svs::ctl::axis_move::dash
