/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/axis_move/office/state.hpp>
#include <snc/svs/fac/axis_move/dynamics.hpp>

namespace snc::svs::ctl::axis_move::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, office::State >;

} // namespace snc::svs::ctl::axis_move::dash::event::in

namespace snc::svs::ctl::axis_move::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t BREAK = 0;
  static constexpr std::uint_fast32_t DYNAMICS = 1;
  static constexpr std::uint_fast32_t STEPS = 2;
  static constexpr std::uint_fast32_t LENGTH = 3;
};

using Out = snc::svp::dash::event::Out;

class Break : public Out
{
  public:
  // -- Types
  static constexpr auto etype = Type::BREAK;

  // -- Construction and reset

  Break ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

class Dynamics : public Out
{
  public:
  // -- Types
  static constexpr auto etype = Type::DYNAMICS;

  // -- Construction and reset

  Dynamics ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _dynamics.reset ();
  }

  // -- Dynamics

  const snc::svs::fac::axis_move::Dynamics &
  dynamics () const
  {
    return _dynamics;
  }

  void
  set_dynamics ( const snc::svs::fac::axis_move::Dynamics & dyn_n )
  {
    _dynamics = dyn_n;
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::Dynamics _dynamics;
};

class Steps : public Out
{
  public:
  // -- Types
  static constexpr auto etype = Type::STEPS;

  // -- Construction and reset

  Steps ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _steps = 0;
  }

  // -- Dynamics

  std::int64_t
  steps () const
  {
    return _steps;
  }

  void
  set_steps ( std::int64_t steps_n )
  {
    _steps = steps_n;
  }

  private:
  // -- Attributes
  std::int64_t _steps = 0;
};

class Length : public Out
{
  public:
  // -- Types

  static constexpr auto etype = Type::LENGTH;

  enum class Unit
  {
    LENGTH,
    RADIANS,
    DEGREES
  };

  // -- Construction and reset

  Length ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _unit = Unit::LENGTH;
    _amount = 0.0;
  }

  // -- Unit

  Unit
  unit () const
  {
    return _unit;
  }

  void
  set_unit ( Unit unit_n )
  {
    _unit = unit_n;
  }

  // -- Amount

  double
  amount () const
  {
    return _amount;
  }

  void
  set_amount ( double amount_n )
  {
    _amount = amount_n;
  }

  private:
  // -- Attributes
  Unit _unit = Unit::LENGTH;
  double _amount = 0.0;
};

} // namespace snc::svs::ctl::axis_move::dash::event::out
