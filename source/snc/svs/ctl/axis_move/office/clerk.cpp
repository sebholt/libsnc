/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/lag/math.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <snc/device/state/axis.hpp>
#include <snc/device/statics/axis.hpp>
#include <cmath>
#include <cstdint>

namespace snc::svs::ctl::axis_move::office
{

Clerk::Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Axis[", axis_index_n, "]-Move" ) )
, _actor ( this )
, _pilot ( this, axis_index_n )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );

  _pilot.session ().set_begin_callback ( [ this ] () {
    _axis.info = _pilot.axis_info ();
    if ( _axis.info.statics () ) {
      // Initialize dynamics
      _axis.length_per_step = _axis.info.statics ()->geo ().length_per_step ();
      _axis.speed_max = _axis.info.statics ()->geo ().speed_max ();
      _axis.accel_max = _axis.info.statics ()->geo ().accel_max ();

      {
        snc::svs::fac::axis_move::Dynamics dyn = _state.dynamics ();
        dyn.set_speed ( _axis.speed_max );
        dyn.set_accel ( _axis.accel_max );
        _state.set_dynamics ( dyn );
      }
    }

    state_update ();
  } );

  _pilot.session ().set_end_callback ( [ this ] () {
    _actor.go_idle ();
    _pilot_control.release ();

    _axis.info.reset ();
    _axis.length_per_step = 0.0;
    _axis.speed_max = 0.0;
    _axis.accel_max = 0.0;

    _state.reset ();
    _requests.clear ();

    state_update ();
  } );

  _actor.set_break_hard_func ( [ this ] () {
    // Clear step requests
    _requests.clear ();
    // Break pilot
    _pilot_control.break_hard ();
  } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();
  _pilot.connect ();
  state_update ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  if ( _axis.info.statics () ) {
    switch ( event_n.type () ) {
    case dash::event::out::Type::BREAK:
      dash_event_break ( event_n );
      break;
    case dash::event::out::Type::DYNAMICS:
      dash_event_dynamics ( event_n );
      break;
    case dash::event::out::Type::STEPS:
      dash_event_steps ( event_n );
      break;
    case dash::event::out::Type::LENGTH:
      dash_event_length ( event_n );
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }

    // Process requests later in process()
    request_processing ();
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event_break ( Dash_Event & event_n [[maybe_unused]] )
{
  _requests.clear ();
  _requests.reg.set ( Request::BREAK );
  request_processing ();
}

void
Clerk::dash_event_dynamics ( Dash_Event & event_n )
{
  auto & cevent = static_cast< const dash::event::out::Dynamics & > ( event_n );

  {
    snc::svs::fac::axis_move::Dynamics dyn = cevent.dynamics ();
    dyn.set_speed ( std::min ( dyn.speed (), _axis.speed_max ) );
    dyn.set_accel ( std::min ( dyn.speed (), _axis.accel_max ) );

    if ( _state.dynamics () == dyn ) {
      return;
    }
    _state.set_dynamics ( dyn );
    state_update ();
  }

  _requests.reg.set ( Request::DYNAMICS );
}

void
Clerk::dash_event_steps ( Dash_Event & event_n )
{
  auto & cevent = static_cast< const dash::event::out::Steps & > ( event_n );

  _requests.reg.set ( Request::STEPS | Request::DYNAMICS );
  _requests.reg.unset ( Request::BREAK );
  _requests.steps += cevent.steps ();
}

void
Clerk::dash_event_length ( Dash_Event & event_n )
{
  if ( _axis.length_per_step == 0.0 ) {
    return;
  }

  std::int_fast64_t steps = 0;
  {
    auto & cevent = static_cast< const dash::event::out::Length & > ( event_n );
    switch ( cevent.unit () ) {
    case Unit::LENGTH:
      steps = std::round ( cevent.amount () / _axis.length_per_step );
      break;
    case Unit::RADIANS:
      steps = std::round ( cevent.amount () / _axis.length_per_step );
      break;
    case Unit::DEGREES:
      steps = std::round ( sev::lag::degrees_to_radians ( cevent.amount () ) /
                           _axis.length_per_step );
      break;
    }
  }
  if ( steps == 0 ) {
    return;
  }

  _requests.reg.set ( Request::STEPS | Request::DYNAMICS );
  _requests.reg.unset ( Request::BREAK );
  _requests.steps += steps;
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
    _state_emitted = _state;
  }
}

void
Clerk::process ()
{
  process_requests ();
  service_control_auto_release ();
  state_update ();
}

void
Clerk::process_requests ()
{
  // Any requests?
  if ( _requests.reg.is_empty () ) {
    return;
  }
  // Acquire pilot lock
  if ( !service_control_acquire () ) {
    _requests.clear ();
    return;
  }

  // Process requests
  if ( _requests.reg.test_any_unset ( Request::STEPS ) ) {
    _pilot_control.set_target_delta ( _requests.steps );
    _requests.steps = 0;
  }
  if ( _requests.reg.test_any_unset ( Request::DYNAMICS ) ) {
    _pilot_control.set_dynamics ( _state.dynamics () );
  }
  if ( _requests.reg.test_any_unset ( Request::BREAK ) ) {
    _pilot_control.break_hard ();
  }
}

bool
Clerk::service_control_acquire ()
{
  // Already connected?
  if ( _pilot_control ) {
    return true;
  }
  if ( !_axis.info.is_manually_movable () || _axis.info.is_unaligned () ) {
    return false;
  }

  // Acquire service control and activate actor
  _pilot_control = _pilot.control ();
  _actor.go_active ();
  if ( !_pilot_control || !_actor.is_active () ) {
    _pilot_control.release ();
    _actor.go_idle ();
  }

  return _pilot_control;
}

void
Clerk::service_control_auto_release ()
{
  // Done?
  if ( !_pilot_control || !_pilot.is_idle () ) {
    return;
  }

  // Release pilot and go idle
  _pilot_control.release ();
  _actor.go_idle ();

  state_update ();
}

void
Clerk::state_update ()
{
  _state.blocks ().clear ();
  if ( !_axis.info.statics () ) {
    _state.blocks ().set ( State::Block::UNUSED );
  } else if ( !_axis.info.is_manually_movable () ) {
    _state.blocks ().set ( State::Block::NOT_USER_MOVABLE );
  } else if ( _axis.info.is_unaligned () ) {
    _state.blocks ().set ( State::Block::ALIGN_REQUIRED );
  } else if ( !_pilot_control && !_pilot.control_available () ) {
    _state.blocks ().set ( State::Block::PILOT_NOT_AVAILABLE );
  }

  // Notify
  if ( _state_emitted != _state ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::ctl::axis_move::office
