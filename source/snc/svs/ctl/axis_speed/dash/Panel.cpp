/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <sev/lag/math.hpp>
#include <sev/utility.hpp>
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/ctl/axis_speed/office/clerk.hpp>

namespace snc::svs::ctl::axis_speed::dash
{

Panel::Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Axis[", axis_index_n, "]-Speed" ) )
, _axis_index ( axis_index_n )
, _event_pool_break ( office_io ().epool_tracker () )
, _event_pool_speed ( office_io ().epool_tracker () )
, _event_pool_speed_accel ( office_io ().epool_tracker () )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::ctl::axis_speed::office::Clerk,
                             std::uint_fast32_t > ( axis_index () );
}

void
Panel::office_session_begin_clerk ()
{
  Super::office_session_begin_clerk ();

  office_io ().allocate ( _event_pool_break, 2 );
  office_io ().allocate ( _event_pool_speed, 8 );
  office_io ().allocate ( _event_pool_speed_accel, 8 );
}

void
Panel::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case dash::event::in::Type::STATE:
    office_event_state_t< dash::event::in::State > ( event_n, _state );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Panel::state_changed ()
{
  Super::state_changed ();

  if ( sev::change ( _qtState.available, state ().available () ) ) {
    emit availableChanged ();
  }
}

void
Panel::break_motion ()
{
  // Submit event
  office_io ().submit ( office_io ().acquire ( _event_pool_break ) );
}

bool
Panel::move_at_speed ( const snc::svs::fac::axis_move::Speeding & speed_n )
{
  // When do we not accept the request?
  if ( !_state.available () //
       || !office_session_is_good () ) {
    return false;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_speed_accel );
    event->set_speed ( speed_n );
    office_io ().submit ( event );
  }
  return true;
}

bool
Panel::move_at_speed ( double speed_n )
{
  // When do we not accept the request?
  if ( !_state.available () //
       || !office_session_is_good () ) {
    return false;
  }

  // Submit event
  {
    auto * event = office_io ().acquire ( _event_pool_speed );
    event->set_speed ( speed_n );
    office_io ().submit ( event );
  }
  return true;
}

bool
Panel::move_at_rpm ( double speed_n )
{
  return move_at_speed ( speed_n * sev::lag::two_pi_d / 60.0 );
}

} // namespace snc::svs::ctl::axis_speed::dash
