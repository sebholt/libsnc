/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/ctl/axis_speed/dash/events.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>

namespace snc::svs::ctl::axis_speed::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( int axisIndex READ axis_index NOTIFY axisIndexChanged )
  Q_PROPERTY ( bool available READ available NOTIFY availableChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  void
  office_session_begin_clerk () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Axis index

  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  Q_SIGNAL
  void
  axisIndexChanged ();

  // -- State

  const office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Available

  bool
  available () const
  {
    return _state.available ();
  }

  Q_SIGNAL
  void
  availableChanged ();

  // -- Request interface

  void
  break_motion ();

  bool
  move_at_speed ( const snc::svs::fac::axis_move::Speeding & speed_n );

  bool
  move_at_speed ( double speed_n );

  bool
  move_at_rpm ( double speed_n );

  // -- Request interface: Qt wrapper

  Q_INVOKABLE
  void
  breakMotion ()
  {
    break_motion ();
  }

  Q_INVOKABLE
  void
  moveAtSpeed ( double speed_n )
  {
    move_at_speed ( speed_n );
  }

  Q_INVOKABLE
  void
  moveAtRoundsPerMinute ( double speed_n )
  {
    move_at_rpm ( speed_n );
  }

  private:
  // -- State
  std::uint_fast32_t _axis_index = 0;
  office::State _state;
  struct
  {
    bool available = false;
  } _qtState;

  // -- Office event io
  sev::event::Pool< dash::event::out::Break > _event_pool_break;
  sev::event::Pool< dash::event::out::Speed > _event_pool_speed;
  sev::event::Pool< dash::event::out::Speed_Accel > _event_pool_speed_accel;
};

} // namespace snc::svs::ctl::axis_speed::dash
