/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <snc/device/state/state.hpp>
#include <snc/device/statics/statics.hpp>
#include <cstdint>

namespace snc::svs::ctl::axis_speed::office
{

Clerk::Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Axis[", axis_index_n, "]-Speed" ) )
, _actor ( this )
, _pilot ( this, axis_index_n )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );

  _pilot.session ().set_begin_callback ( [ this ] () {
    _axis.info = _pilot.axis_info ();
    if ( _axis.info.statics () ) {
      // Initialize axis limits
      _axis.speed_max = _axis.info.statics ()->geo ().speed_max ();
      _axis.accel_max = _axis.info.statics ()->geo ().accel_max ();

      _requests.speed.set_accel ( _axis.accel_max );
      _requests.speed.set_forward ( true );
    }

    state_update ();
  } );

  _pilot.session ().set_end_callback ( [ this ] () {
    _actor.go_idle ();
    _pilot_control.release ();

    _axis.info.reset ();
    _axis.speed_max = 0.0;
    _axis.accel_max = 0.0;

    _state.reset ();
    _requests.clear ();

    state_update ();
  } );

  // Setup actor interface
  _actor.set_break_hard_func ( [ this ] () {
    // Clear requests
    _requests.clear ();
    // Abort pilot
    _pilot_control.break_hard ();
  } );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();

  _actor.connect_required ();
  _pilot.connect ();
  state_update ();
}

void
Clerk::dash_event ( Dash_Event & event_n )
{
  if ( _axis.info.statics () ) {
    switch ( event_n.type () ) {
    case dash::event::out::Type::BREAK:
      dash_event_break ( event_n );
      break;
    case dash::event::out::Type::SPEED:
      dash_event_speed ( event_n );
      break;
    case dash::event::out::Type::SPEED_ACCEL:
      dash_event_speed_accel ( event_n );
      break;
    default:
      DEBUG_ASSERT ( false );
      break;
    }

    request_processing ();
  }

  dash_io ().notification_set ( Dash_Message::STATE );
}

void
Clerk::dash_event_break ( Dash_Event & event_n [[maybe_unused]] )
{
  _requests.reg.assign ( Request::BREAK );
  request_processing ();
}

void
Clerk::dash_event_speed ( Dash_Event & event_n )
{
  double speed =
      static_cast< const dash::event::out::Speed & > ( event_n ).speed ();

  _requests.reg.assign ( Request::SPEED );
  // Speed
  if ( speed >= 0.0 ) {
    _requests.speed.set_speed ( speed );
    _requests.speed.set_forward ( true );
  } else {
    _requests.speed.set_speed ( -speed );
    _requests.speed.set_forward ( false );
  }
  // Sanitize speed
  _requests.speed.set_speed (
      std::min ( _requests.speed.speed (), _axis.speed_max ) );
  // Acceleration
  _requests.speed.set_accel ( _axis.accel_max );
}

void
Clerk::dash_event_speed_accel ( Dash_Event & event_n )
{
  const auto & speed =
      static_cast< const dash::event::out::Speed_Accel & > ( event_n ).speed ();

  _requests.reg.assign ( Request::SPEED );
  _requests.speed = speed;
  // Sanitize speed and acceleration
  _requests.speed.set_speed (
      std::min ( _requests.speed.speed (), _axis.speed_max ) );
  _requests.speed.set_accel (
      std::min ( _requests.speed.accel (), _axis.accel_max ) );
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _state );
  }
}

void
Clerk::process ()
{
  process_requests ();
  service_control_auto_release ();
  state_update ();
}

void
Clerk::process_requests ()
{
  if ( _requests.reg.is_empty () ) {
    return;
  }
  // Clear requests if the pilot is not available
  if ( !service_control_acquire () ) {
    _requests.clear ();
    return;
  }

  if ( _requests.reg.test_any_unset ( Request::SPEED ) ) {
    _pilot_control.set_speeding ( _requests.speed );
  }
  if ( _requests.reg.test_any_unset ( Request::BREAK ) ) {
    _pilot_control.break_hard ();
  }
}

bool
Clerk::service_control_acquire ()
{
  // Arelady connected?
  if ( _pilot_control ) {
    return true;
  }
  if ( !_axis.info.is_manually_movable () || _axis.info.is_unaligned () ) {
    return false;
  }

  // Acquire service control and activate actor
  _pilot_control = _pilot.control ();
  _actor.go_active ();
  if ( !_pilot_control || !_actor.is_active () ) {
    _pilot_control.release ();
    _actor.go_idle ();
  }

  return _pilot_control;
}

void
Clerk::service_control_auto_release ()
{
  if ( !_pilot_control || !_pilot.is_idle () ) {
    return;
  }

  // Release pilot and go idle
  _pilot_control.release ();
  _actor.go_idle ();

  state_update ();
}

void
Clerk::state_update ()
{
  _state.blocks ().clear ();
  if ( !_axis.info.statics () ) {
    _state.blocks ().set ( State::Block::UNUSED );
  } else if ( !_axis.info.is_manually_movable () ) {
    _state.blocks ().set ( State::Block::NOT_USER_MOVABLE );
  } else if ( _axis.info.is_unaligned () ) {
    _state.blocks ().set ( State::Block::ALIGN_REQUIRED );
  } else if ( !_pilot_control && !_pilot.control_available () ) {
    _state.blocks ().set ( State::Block::PILOT_NOT_AVAILABLE );
  }

  if ( sev::change ( _state_emitted, _state ) ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::ctl::axis_speed::office
