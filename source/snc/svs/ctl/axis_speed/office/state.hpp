/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <cstdint>

namespace snc::svs::ctl::axis_speed::office
{

class State
{
  public:
  // -- Types

  struct Block
  {
    /// @brief State is not valid
    static constexpr std::uint_fast32_t INVALID = ( 1 << 0 );
    /// @brief Axis is unused
    static constexpr std::uint_fast32_t UNUSED = ( 1 << 1 );
    static constexpr std::uint_fast32_t PILOT_NOT_AVAILABLE = ( 1 << 2 );
    static constexpr std::uint_fast32_t NOT_USER_MOVABLE = ( 1 << 3 );
    static constexpr std::uint_fast32_t ALIGN_REQUIRED = ( 1 << 4 );
  };

  // -- Construction

  void
  reset ()
  {
    _blocks.assign ( Block::INVALID );
  }

  // -- Interface

  bool
  available () const
  {
    return _blocks.is_empty ();
  }

  sev::mem::Flags_Fast32 &
  blocks ()
  {
    return _blocks;
  }

  const sev::mem::Flags_Fast32 &
  blocks () const
  {
    return _blocks;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( _blocks == state_n._blocks );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return ( _blocks != state_n._blocks );
  }

  private:
  // -- Attributes
  sev::mem::Flags_Fast32 _blocks = Block::INVALID;
};

} // namespace snc::svs::ctl::axis_speed::office
