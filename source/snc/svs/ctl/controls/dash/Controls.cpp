/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Controls.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::svs::ctl::controls::dash
{

Controls::Controls ( QObject * qparent_n )
: QObject ( qparent_n )
, _i1 ( this )
{
}

Controls::~Controls () = default;

} // namespace snc::svs::ctl::controls::dash
