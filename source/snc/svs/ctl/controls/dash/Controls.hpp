/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/ctl/controls/dash/i1/List.hpp>
#include <QObject>

namespace snc::svs::ctl::controls::dash
{

/*** Controls */
class Controls : public QObject
{
  Q_OBJECT

  public:
  // -- Construction

  Controls ( QObject * qparent_n );

  ~Controls ();

  // -- I1

  auto &
  i1 ()
  {
    return _i1;
  }

  private:
  // -- Attributes
  Panel * _panel = nullptr;
  dash::i1::List _i1;
};

} // namespace snc::svs::ctl::controls::dash
