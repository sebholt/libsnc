/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/ctl/controls/dash/control_setup.hpp>
#include <snc/svs/ctl/controls/office/state.hpp>
#include <snc/svs/ctl/controls/request.hpp>
#include <vector>

namespace snc::svs::ctl::controls::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
  static constexpr std::uint_fast32_t SETUP = 1;
  static constexpr std::uint_fast32_t AVAIL = 2;
};

using namespace snc::svp::dash::event::in;

using In = snc::svp::dash::event::In;

using State = State_T< Type::STATE, office::State >;

class Setup : public In
{
  public:
  static constexpr auto etype = Type::SETUP;

  Setup ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _i1.clear ();
    _i1.shrink_to_fit ();
  }

  const Control_Setup_List &
  i1 () const
  {
    return _i1;
  }

  Control_Setup_List &
  i1_ref ()
  {
    return _i1;
  }

  private:
  Control_Setup_List _i1;
};

class Avail : public In
{
  public:
  static constexpr auto etype = Type::AVAIL;

  Avail ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _index = 0;
    _avail = false;
  }

  bool
  index () const
  {
    return _index;
  }

  void
  set_index ( std::size_t index_n )
  {
    _index = index_n;
  }

  bool
  avail () const
  {
    return _avail;
  }

  void
  set_avail ( bool flag_n )
  {
    _avail = flag_n;
  }

  private:
  std::size_t _index = 0;
  bool _avail = false;
};

} // namespace snc::svs::ctl::controls::dash::event::in

namespace snc::svs::ctl::controls::dash::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t REQUEST = 0;
};

using Out = snc::svp::dash::event::Out;

class Request : public Out
{
  public:
  static constexpr auto etype = Type::REQUEST;

  Request ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    Out::reset ();
    _request.clear ();
  }

  const snc::svs::ctl::controls::Request &
  request () const
  {
    return _request;
  }

  void
  set_request ( const snc::svs::ctl::controls::Request & req_n )
  {
    _request = req_n;
  }

  private:
  snc::svs::ctl::controls::Request _request;
};

} // namespace snc::svs::ctl::controls::dash::event::out
