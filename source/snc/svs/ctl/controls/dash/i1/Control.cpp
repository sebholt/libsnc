/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Control.hpp"
#include <sev/utility.hpp>
#include <snc/svs/ctl/controls/dash/Panel.hpp>

namespace snc::svs::ctl::controls::dash::i1
{

Control::Control ( QObject * qparent_n,
                   Panel * panel_n,
                   const Control_Setup & control_setup_n )
: QObject ( qparent_n )
, _panel ( panel_n )
, _index ( control_setup_n.index )
, _control_index ( control_setup_n.control_index )
, _name ( QString::fromStdString ( control_setup_n.name ) )
{
}

Control::~Control () = default;

void
Control::set_available ( bool flag_n )
{
  if ( sev::change ( _available, flag_n ) ) {
    emit availableChanged ();
  }
}

void
Control::set ( bool state_n )
{
  _panel->submit_request (
      Request ( Request::Type::B1, Request::Action::SET, index (), state_n ) );
}

void
Control::toggle ()
{
  _panel->submit_request (
      Request ( Request::Type::B1, Request::Action::INVERT, index () ) );
}

void
Control::setOn ()
{
  set ( true );
}

void
Control::setOff ()
{
  set ( false );
}

} // namespace snc::svs::ctl::controls::dash::i1
