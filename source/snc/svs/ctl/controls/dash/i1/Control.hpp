/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/ctl/controls/dash/control_setup.hpp>
#include <QObject>

// -- Forward declaration
namespace snc::svs::ctl::controls::dash
{
class Panel;
}

namespace snc::svs::ctl::controls::dash::i1
{

/*** Control */
class Control : public QObject
{
  Q_OBJECT

  // -- Properties

  Q_PROPERTY ( int controlIndex READ control_index CONSTANT )
  Q_PROPERTY ( QString name READ name CONSTANT )
  Q_PROPERTY ( bool available READ available NOTIFY availableChanged )

  public:
  // -- Construction

  Control ( QObject * qparent_n,
            Panel * panel_n,
            const Control_Setup & control_setup_n );

  ~Control ();

  // -- Control index

  std::uint_fast32_t
  index () const
  {
    return _index;
  }

  std::uint_fast32_t
  control_index () const
  {
    return _control_index;
  }

  // -- Name

  const QString &
  name () const
  {
    return _name;
  }

  // -- Available

  bool
  available () const
  {
    return _available;
  }

  void
  set_available ( bool flag_n );

  Q_SIGNAL
  void
  availableChanged ();

  // -- Interface

  Q_INVOKABLE
  void
  set ( bool state_n );

  Q_INVOKABLE
  void
  toggle ();

  Q_INVOKABLE
  void
  setOn ();

  Q_INVOKABLE
  void
  setOff ();

  private:
  // -- Attributes
  Panel * _panel = nullptr;
  std::uint_fast32_t _index = 0;
  std::uint_fast32_t _control_index = 0;
  QString _name;
  bool _available = false;
};

} // namespace snc::svs::ctl::controls::dash::i1
