/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "List.hpp"
#include <sev/assert.hpp>
#include <stdexcept>

namespace snc::svs::ctl::controls::dash::i1
{

List::List ( QObject * qparent_n )
: QAbstractListModel ( qparent_n )
{
  _roleNames = QAbstractListModel::roleNames ();
  _roleNames.insert ( ExtraRole::listItem, "listItem" );
}

List::~List () = default;

QHash< int, QByteArray >
List::roleNames () const
{
  return _roleNames;
}

int
List::rowCount ( const QModelIndex & parent_n ) const
{
  return _list.validParentIndex ( parent_n ) ? _list.size () : 0;
}

QVariant
List::data ( const QModelIndex & index_n, int role_n ) const
{
  if ( _list.validIndex ( index_n ) ) {
    switch ( role_n ) {
    case ExtraRole::listItem:
      return QVariant::fromValue ( _list.at ( index_n.row () ).get () );
    default:
      break;
    }
  }
  return QVariant ();
}

void
List::setup ( Panel * panel_n, const Control_Setup_List & control_setups_n )
{
  // Remove items
  const std::size_t num_old = _list.size ();
  const std::size_t num_new = control_setups_n.size ();

  if ( num_old != 0 ) {
    beginRemoveRows ( QModelIndex (), 0, num_old - 1 );
    _list.clear ();
    endRemoveRows ();
    emit countChanged ();
  }

  if ( num_new != 0 ) {
    beginInsertRows ( QModelIndex (), 0, num_new - 1 );
    for ( auto & setup : control_setups_n ) {
      _list.push ( std::make_shared< Control > ( this, panel_n, setup ) );
    }
    endInsertRows ();
    emit countChanged ();
  }
}

void
List::set_available ( std::size_t index_n, bool flag_n )
{
  if ( index_n < _list.size () ) {
    _list.at ( index_n )->set_available ( flag_n );
  }
}

} // namespace snc::svs::ctl::controls::dash::i1
