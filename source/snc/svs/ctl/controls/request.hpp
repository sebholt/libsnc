/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::ctl::controls
{

/** Control change request
 *
 */
class Request
{
  public:
  // -- Types

  enum class Type : std::uint8_t
  {
    INVALID,
    B1,
    B8,
    B16,
    B32,
    B64
  };

  enum class Action : std::uint8_t
  {
    INVALID,
    SET,
    INVERT
  };

  // -- Construction and setup

  Request () = default;

  Request ( Type type_n,
            Action action_n,
            std::uint_fast32_t index_n,
            std::uint_least64_t value_n = 0 )
  : _type ( type_n )
  , _action ( action_n )
  , _index ( index_n )
  , _value ( value_n )
  {
  }

  /// @brief Reset to default construction state
  void
  clear ()
  {
    _type = Type::INVALID;
    _action = Action::INVALID;
    _index = 0;
    _value = 0;
  }

  // -- Accessors

  Type
  type () const
  {
    return _type;
  }

  Action
  action () const
  {
    return _action;
  }

  std::uint_fast32_t
  index () const
  {
    return _index;
  }

  const std::uint_least64_t &
  value () const
  {
    return _value;
  }

  void
  set_value ( std::uint_least64_t value_n )
  {
    _value = value_n;
  }

  private:
  // -- Attributes
  Type _type = Type::INVALID;
  Action _action = Action::INVALID;
  std::uint_fast32_t _index = 0;
  std::uint_least64_t _value = 0;
};

} // namespace snc::svs::ctl::controls
