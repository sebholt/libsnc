/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/Panel.hpp>

namespace snc::svs::ctl::lookout::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;
};

} // namespace snc::svs::ctl::lookout::dash
