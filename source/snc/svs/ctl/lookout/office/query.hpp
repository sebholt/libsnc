/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/emb/config/config.hpp>
#include <snc/usb/device_query.hpp>
#include <memory>

namespace snc::svs::ctl::lookout
{

class Query
{
  public:
  // -- Construction

  Query ( std::shared_ptr< const snc::emb::config::Config > emb_config_n,
          std::shared_ptr< const snc::usb::Device_Query > usb_query_n );

  // -- Emb-Device configuration

  const std::shared_ptr< const snc::emb::config::Config > &
  emb_config () const
  {
    return _emb_config;
  }

  // -- USB query

  const std::shared_ptr< const snc::usb::Device_Query > &
  usb_query () const
  {
    return _usb_query;
  }

  private:
  std::shared_ptr< const snc::emb::config::Config > _emb_config;
  std::shared_ptr< const snc::usb::Device_Query > _usb_query;
};

} // namespace snc::svs::ctl::lookout
