/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>

namespace snc::svs::ctl::machine_res::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Machine-Resources" )
, _service ( *this )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.init_connections ();
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    _state_emitted = _service.state ();
  }
}

void
Clerk::process ()
{
  _service.process ();
}

void
Clerk::state_changed ()
{
  if ( _state_emitted != _service.state () ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::ctl::machine_res::office
