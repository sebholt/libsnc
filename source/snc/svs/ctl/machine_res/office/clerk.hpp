/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/ctl/machine_res/dash/events.hpp>
#include <snc/svs/ctl/machine_res/office/service.hpp>
#include <snc/svs/ctl/machine_res/office/state.hpp>

namespace snc::svs::ctl::machine_res::office
{

// -- Forward declaration
class Client;

/// @brief Clerk
///
class Clerk : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  /// @brief Front message state
  struct Dash_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk () override;

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Central processing

  void
  process () override;

  // -- Utility

  void
  state_changed ();

  private:
  Service _service;
  State _state_emitted;
  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::ctl::machine_res::office
