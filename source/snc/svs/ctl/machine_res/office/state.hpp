/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::ctl::machine_res::office
{

/// @brief State
///
class State
{
  public:
  // -- Setup

  void
  reset ();

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const;

  bool
  operator!= ( const State & state_n ) const;

  private:
  // -- Attributes
};

} // namespace snc::svs::ctl::machine_res::office
