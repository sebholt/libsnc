/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/axis_align/dash/events.hpp>
#include <snc/svs/fac/axis_align/ship/pilot_emb.hpp>
#include <snc/svs/fac/axis_align/ship/pilot_est.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::office
{

Clerk::Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Factor-Axis-Align[", axis_index_n, "]" ) )
, _service ( *this, axis_index_n )
, _event_pool_dash_state ( dash_io ().epool_tracker () )
, _event_pool_bridge_job ( ship_io ().epool_tracker () )
{
  dash_io ().allocate ( _event_pool_dash_state, 2 );
}

Clerk::~Clerk () = default;

void
Clerk::bridge_session_begin ()
{
  Super::bridge_session_begin ();
  _service.session_begin ();
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  if ( !_service.state ().is_alignable () ) {
    // Axis not alignable.  No pilot required.
    return;
  }

  if ( tracker ().est_device () ) {
    pick_n (
        make_sailor_factory< snc::svs::fac::axis_align::ship::Pilot_Est,
                             std::uint_fast32_t > ( _service.axis_index () ) );
  } else {
    pick_n (
        make_sailor_factory< snc::svs::fac::axis_align::ship::Pilot_Emb,
                             std::uint_fast32_t > ( _service.axis_index () ) );
  }
}

void
Clerk::bridge_session_begin_sailor ()
{
  Super::bridge_session_begin_sailor ();

  // Allocate bridge events
  ship_io ().allocate ( _event_pool_bridge_job, 2 );
}

void
Clerk::bridge_session_abort ()
{
  Super::bridge_session_abort ();
  _service.session_end ();
}

void
Clerk::dash_notify ()
{
  if ( auto sender =
           dash_notifier ( Dash_Message::STATE, _event_pool_dash_state ) ) {
    sender.event->set_dash_event_count ( dash_io ().stats_in_fetch_clear () );
    sender.event->set_state ( _service.state () );
    _state_emitted = _service.state ();
  }
}

void
Clerk::bridge_event ( Bridge_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::out::State::etype:
    bridge_event_state ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Clerk::bridge_event_state ( Bridge_Event & event_n )
{
  auto & cevent = static_cast< const ship::event::out::State & > ( event_n );
  ship_io ().stats_out_sub ( cevent.office_event_count () );
  _service.update_pilot_state ( cevent.state () );
}

void
Clerk::bridge_notify_job ( const Job & job_n )
{
  auto * event = ship_io ().acquire ( _event_pool_bridge_job );
  event->set_job ( job_n );
  ship_io ().submit ( event );
}

void
Clerk::process ()
{
  _service.process ();
}

void
Clerk::state_changed ()
{
  if ( _state_emitted != _service.state () ) {
    dash_io ().notification_set ( Dash_Message::STATE );
  }
}

} // namespace snc::svs::fac::axis_align::office
