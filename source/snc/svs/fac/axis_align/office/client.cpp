/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/axis_align/office/service.hpp>

namespace snc::svs::fac::axis_align::office
{

Client::Client ( User * user_n, std::uint_fast32_t axis_index_n )
: Super ( user_n, axis_index_n )
{
}

Client::~Client () = default;

bool
Client::session_is_good () const
{
  return is_connected () ? service ()->session_is_good () : false;
}

bool
Client::is_alignable () const
{
  return is_connected () ? service ()->state ().is_alignable () : false;
}

bool
Client::is_aligning () const
{
  return is_connected () ? service ()->state ().is_aligning () : false;
}

bool
Client::acquire_state ( State & state_n )
{
  if ( !is_connected () ) {
    return false;
  }
  state_n = service ()->state ();
  return true;
}

bool
Client::Control::start_job ( const Job & job_n )
{
  if ( !is_valid () ) {
    return false;
  }
  return client ()->service ()->control_job ( job_n );
}

bool
Client::Control::begin ()
{
  return start_job ( Job::Command::ALIGN );
}

bool
Client::Control::set ()
{
  return start_job ( Job::Command::SET );
}

bool
Client::Control::break_hard ()
{
  return start_job ( Job::Command::ABORT );
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return connect_to_axis< Service > ( sb_n );
}

} // namespace snc::svs::fac::axis_align::office
