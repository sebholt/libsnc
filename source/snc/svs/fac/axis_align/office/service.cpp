/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/axis_align/office/clerk.hpp>
#include <snc/svs/fac/axis_align/office/client.hpp>

namespace snc::svs::fac::axis_align::office
{

Service::Service ( Provider & provider_n, std::uint_fast32_t axis_index_n )
: Super ( provider_n, axis_index_n )
{
}

Service::~Service () = default;

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  Super::session_begin ();

  // Update state
  if ( axis_info () ) {
    auto & astat = *axis_info ().statics ();
    _state.set_is_alignable (
        astat.align ().alignable () &&
        ( astat.sensors ().low_end () || astat.sensors ().high_end () ) );
  }

  // Notify clients
  state_update ();
  clients_session_begin ();
}

void
Service::session_end ()
{
  Super::session_end ();

  // Update state
  _state.reset ();

  // Notify clients
  state_update ();
  clients_session_end ();
}

void
Service::update_pilot_state ( const ship::State & pilot_state_n )
{
  _state.set_sequence ( pilot_state_n.sequence () );
  _state.set_speed_target ( pilot_state_n.speed_target () );
  state_update ();
}

void
Service::process ()
{
  control_acquirable_change_poll ();
}

bool
Service::control_acquirable () const
{
  return Super::control_acquirable () && //
         _state.is_alignable ();
}

void
Service::control_acquired ( Client_Abstract * client_n )
{
  Super::control_acquired ( client_n );
  state_update ();
}

void
Service::control_released ( Client_Abstract * client_n )
{
  Super::control_released ( client_n );
  state_update ();
}

bool
Service::control_job ( const Job & job_n )
{
  actor ().go_active ();

  provider ().bridge_notify_job ( job_n );

  state_update ();
  return true;
}

void
Service::state_update ()
{
  _state.set_is_aligning ( !provider ().ship_io ().output_satisfied () ||
                           ( _state.sequence () != Sequence::IDLE ) );

  // Set actor idle on demand
  if ( !actor ().is_idle () && !_state.is_aligning () ) {
    actor ().go_idle ();
  }

  signal_state ().send ();
  provider ().state_changed ();
}

} // namespace snc::svs::fac::axis_align::office
