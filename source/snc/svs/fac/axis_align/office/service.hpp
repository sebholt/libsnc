/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_align/job.hpp>
#include <snc/svs/fac/axis_align/office/state.hpp>
#include <snc/svs/fac/axis_align/ship/state.hpp>
#include <snc/svs/fac/base/office/service_axis.hpp>

namespace snc::svs::fac::axis_align::office
{

// -- Forward declaration
class Clerk;
class Client;

/// @brief Service
///
class Service : public snc::svs::fac::base::office::Service_Axis
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Service_Axis;
  using Client = snc::svs::fac::axis_align::office::Client;
  using Provider = snc::svs::fac::axis_align::office::Clerk;

  public:
  // -- Construction

  Service ( Provider & provider_n, std::uint_fast32_t axis_index_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Session

  void
  session_begin () override;

  void
  session_end () override;

  // -- Provider interface

  void
  update_pilot_state ( const ship::State & pilot_state_n );

  void
  process ();

  // -- Accessors

  const auto &
  state () const
  {
    return _state;
  }

  // -- Client control interface
  private:
  friend Client;

  bool
  control_acquirable () const override;

  void
  control_acquired ( Client_Abstract * client_n ) override;

  void
  control_released ( Client_Abstract * client_n ) override;

  bool
  control_job ( const Job & job_n );

  void
  state_update ();

  private:
  State _state;
};

} // namespace snc::svs::fac::axis_align::office
