/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/utility.hpp>
#include <snc/svs/fac/axis_align/types.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::office
{

/** State */
class State
{
  public:
  // -- Setup

  void
  reset ()
  {
    _is_aligning = false;
    _is_alignable = false;
    _sequence = Sequence::IDLE;
    _speed_target = 0.0;
  }

  // -- Aligning state

  bool
  is_aligning () const
  {
    return _is_aligning;
  }

  bool
  set_is_aligning ( bool flag_n )
  {
    return sev::change ( _is_aligning, flag_n );
  }

  // -- Aligning state

  bool
  is_alignable () const
  {
    return _is_alignable;
  }

  void
  set_is_alignable ( bool flag_n )
  {
    _is_alignable = flag_n;
  }

  // -- Aligning sequence

  Sequence
  sequence () const
  {
    return _sequence;
  }

  void
  set_sequence ( Sequence sequence_n )
  {
    _sequence = sequence_n;
  }

  // -- Speed

  double
  speed_target () const
  {
    return _speed_target;
  }

  void
  set_speed_target ( double speed_n )
  {
    _speed_target = speed_n;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( _is_aligning == state_n._is_aligning ) &&
           ( _is_alignable == state_n._is_alignable ) &&
           ( _sequence == state_n._sequence ) &&
           ( _speed_target == state_n.speed_target () );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return !operator== ( state_n );
  }

  private:
  // -- Attributes
  bool _is_aligning = false;
  bool _is_alignable = false;
  Sequence _sequence = Sequence::IDLE;
  double _speed_target = 0.0;
};

} // namespace snc::svs::fac::axis_align::office
