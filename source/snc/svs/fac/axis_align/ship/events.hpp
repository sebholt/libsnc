/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/events.hpp>
#include <snc/svs/fac/axis_align/job.hpp>
#include <snc/svs/fac/axis_align/ship/state.hpp>

namespace snc::svs::fac::axis_align::ship::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t JOB = 0;
};

using In = snc::svp::ship::event::In;

class Job : public In
{
  public:
  static constexpr auto etype = Type::JOB;

  Job ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _job.reset ();
  }

  const snc::svs::fac::axis_align::Job &
  job () const
  {
    return _job;
  }

  void
  set_job ( const snc::svs::fac::axis_align::Job & job_n )
  {
    _job = job_n;
  }

  private:
  snc::svs::fac::axis_align::Job _job;
};
} // namespace snc::svs::fac::axis_align::ship::event::in

namespace snc::svs::fac::axis_align::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::ship::event::out;

/// @brief Pilot state event
using State = State_T< Type::STATE, ship::State >;

} // namespace snc::svs::fac::axis_align::ship::event::out
