/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_est.hpp"
#include <estepper/out/messages.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/sensor/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/est/config/stepper.hpp>
#include <snc/math/stepping.hpp>

namespace snc::svs::fac::axis_align::ship
{

Pilot_Est::Pilot_Est ( const Sailor_Init & init_n,
                       std::uint_fast32_t axis_index_n )
: Super ( init_n, sev::string::cat ( "Axis-Align[", axis_index_n, "]" ) )
, _axis_index ( axis_index_n )
, _axis_statics ( device_statics ().axes ().get ( _axis_index ) )
, _axis_state ( device_state ().axis_shared ( _axis_index ) )
, _stepper_data_composer ( _axis_statics->est ().stepper_index () )
, _event_pool_state ( office_io ().epool_tracker () )
{
  office_io ().allocate ( _event_pool_state, 2 );

  if ( _axis_statics->sensors ().low_end () ) {
    auto index = _axis_statics->sensors ().low_end ()->index ();
    _sensor_state = device_state ().sensor_i1_shared ( index );
  } else if ( _axis_statics->sensors ().high_end () ) {
    auto index = _axis_statics->sensors ().high_end ()->index ();
    _sensor_state = device_state ().sensor_i1_shared ( index );
  }

  // EStepper info
  _est.stepper_index = _axis_statics->est ().stepper_index ();
  _est.direction_reversed = _axis_statics->est ().direction_reversed ();
  _est.steps_queue_capacity =
      _axis_statics->est ().stepper_config ()->steps_queue_capacity ();
  _est.tick_frequency =
      _axis_statics->est ().stepper_config ()->tick_frequency ();

  // Segment types
  {
    const bool sape = _axis_statics->sensors ().high_end ().operator bool ();
    _seg_type.approach = ( sape != _est.direction_reversed )
                             ? estepper::segment::MOTOR_FORWARD
                             : estepper::segment::MOTOR_BACKWARD;
    _seg_type.withdraw = ( sape == _est.direction_reversed )
                             ? estepper::segment::MOTOR_FORWARD
                             : estepper::segment::MOTOR_BACKWARD;
    _step.seg_type = _seg_type.approach;
  }

  // Slow movement speeds
  _speed_reversible = _axis_statics->geo ().speed_reversible ();
  _speed_stoppable = _axis_statics->geo ().speed_stoppable ();

  _length_per_step = _axis_statics->geo ().length_per_step ();
  _accel = _axis_statics->geo ().accel_max ();

  // Keep this
  double const queue_usecs_max = 10 * sev::math::kilo_ui;
  _num_queue_ticks_max = std::ceil (
      ( _est.tick_frequency / sev::math::mega_d ) * queue_usecs_max );

  // The maximum distance to accelerate
  double length_accel = 2.0;
  // The maximum distance to travel with a filled (queue_usecs_max) steps queue
  double length_momentum = 2.0;
  if ( _axis_statics->geo ().rotational () ) {
    length_accel = sev::lag::pi_d / 10.0;
    length_momentum = sev::lag::pi_d / 10.0;
  }

  double speed_max_accel =
      snc::speed_accelerate_over_length ( 0.0, _accel, length_accel );
  double speed_max_momentum =
      length_momentum / ( queue_usecs_max / sev::math::mega_d );
  double speed_max_axis = _axis_statics->geo ().speed_max ();

  _speed_max = speed_max_accel;
  sev::math::assign_smaller< double > ( _speed_max, speed_max_momentum );
  sev::math::assign_smaller< double > ( _speed_max, speed_max_axis );

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
    logo << "Setup:\n";
    logo << "  num_queue_ticks_max " << _num_queue_ticks_max << "\n";
    logo << "  speed_max_accel " << speed_max_accel << "\n";
    logo << "  speed_max_momentum " << speed_max_momentum << "\n";
    logo << "  speed_max_axis " << speed_max_axis << "\n";
    logo << "  speed_max " << _speed_max << "\n";
  }
}

Pilot_Est::~Pilot_Est () = default;

void
Pilot_Est::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::in::Job::etype:
    office_event_job ( event_n );
    break;
  default:
    break;
  }

  office_io ().notification_set ( Office_Message::STATE );
}

void
Pilot_Est::office_event_job ( Office_Event & event_n )
{
  const auto & job =
      static_cast< const ship::event::in::Job & > ( event_n ).job ();

  switch ( job.cmd () ) {
  case Job::Command::INVALID:
    break;

  case Job::Command::ABORT:
    if ( ( _state.sequence () != Sequence::IDLE ) &&
         ( _state.sequence () != Sequence::BREAK ) ) {
      select_sequence ( Sequence::BREAK );
    }
    break;

  case Job::Command::ALIGN:
    if ( _state.sequence () == Sequence::IDLE ) {
      // Start alignment
      _state.set_speed_target ( _speed_max );
      select_sequence ( Sequence::APPROACH );
      tracker ().set_axis_aligned ( _axis_index, false );
      device_messages_register ( true );
    } else {
      // Ignore when we're not IDLE
    }
    break;

  case Job::Command::SET:
    if ( _state.sequence () == Sequence::IDLE ) {
      // Set aligned when movement stopped
      tracker ().set_axis_aligned ( _axis_index, true );
      select_sequence ( Sequence::IDLE );
    } else if ( _state.sequence () != Sequence::BREAK ) {
      // Convert to BREAK job
      select_sequence ( Sequence::BREAK );
    }
    break;
  }
}

void
Pilot_Est::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( _state );
    office_io ().submit ( event );
  }
}

void
Pilot_Est::select_sequence ( Sequence sequence_n )
{
  if ( _state.sequence () != sequence_n ) {
    _state.set_sequence ( sequence_n );
    office_io ().notification_set ( Office_Message::STATE );
  }

  switch ( sequence_n ) {
  case Sequence::IDLE:
    _state.set_speed_target ( 0.0 );
    device_messages_register ( false );
    break;

  case Sequence::APPROACH:
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Approaching at " << _state.speed_target () << " mm/s.";
    }
    _sequence.stepper_enabled = false;
    _sequence.stepper_disabled = false;
    _step.seg_type = _seg_type.approach;
    _step.speed = 0.0;
    break;

  case Sequence::APPROACH_STOP:
    log ().cat ( sev::logt::FL_DEBUG_0, "Approaching stopped." );
    break;

  case Sequence::WITHDRAW:
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Withdrawing at " << _state.speed_target () << " mm/s.";
    }
    _sequence.stepper_enabled = false;
    _sequence.stepper_disabled = false;
    _step.seg_type = _seg_type.withdraw;
    _step.speed = 0.0;
    break;

  case Sequence::WITHDRAW_STOP:
    log ().cat ( sev::logt::FL_DEBUG_0, "Withdrawing stopped." );
    break;

  case Sequence::BREAK:
    _state.set_speed_target ( 0.0 );
    log ().cat ( sev::logt::FL_DEBUG_0, "Breaking." );
    break;
  }
}

void
Pilot_Est::device_messages_generate ()
{
  auto reduce_speed = [ this ] () {
    double speed_target = ( _state.speed_target () / 2.0 );
    sev::math::assign_larger< double > ( speed_target, _speed_stoppable );
    _state.set_speed_target ( speed_target );
  };

  while ( true ) {
    Sequence const seq = _state.sequence ();

    switch ( _state.sequence () ) {
    case Sequence::IDLE:
      break;

    case Sequence::APPROACH:
      if ( _sensor_state->state () ) {
        select_sequence ( Sequence::APPROACH_STOP );
      } else {
        move_feed ();
        enable_stepper_on_demand ();
      }
      break;

    case Sequence::APPROACH_STOP:
      if ( !move_feed () ) {
        disable_stepper_on_demand ();
        if ( !_axis_state->is_enabled () ) {
          if ( _state.speed_target () <= _speed_stoppable ) {
            // Aligned!
            tracker ().set_axis_aligned ( _axis_index, true );
            select_sequence ( Sequence::IDLE );
            log ().cat ( sev::logt::FL_DEBUG_0, "Aligned!" );
          } else {
            // Lower speed and swap direction
            reduce_speed ();
            select_sequence ( Sequence::WITHDRAW );
          }
        }
      }
      break;

    case Sequence::WITHDRAW:
      if ( !_sensor_state->state () ) {
        select_sequence ( Sequence::WITHDRAW_STOP );
      } else {
        move_feed ();
        enable_stepper_on_demand ();
      }
      break;

    case Sequence::WITHDRAW_STOP:
      if ( !move_feed () ) {
        disable_stepper_on_demand ();
        if ( !_axis_state->is_enabled () ) {
          // Lower speed and swap direction
          reduce_speed ();
          select_sequence ( Sequence::APPROACH );
        }
      }
      break;

    case Sequence::BREAK:
      if ( !move_feed () ) {
        disable_stepper_on_demand ();
        if ( !_axis_state->is_enabled () ) {
          log ().cat ( sev::logt::FL_DEBUG_0, "Breaking finished." );
          select_sequence ( Sequence::IDLE );
        }
      }
      break;
    }

    // Break if there were no changes
    if ( seq == _state.sequence () ) {
      break;
    }
  }
}

bool
Pilot_Est::move_feed_splitter ()
{
  bool generate = false;

  switch ( _state.sequence () ) {
  case Sequence::IDLE:
    break;

  case Sequence::APPROACH:
  case Sequence::WITHDRAW:
    // Accelerate on demand
    if ( _step.speed < _state.speed_target () ) {
      _step.speed = snc::speed_accelerate_over_length (
          _step.speed, _accel, _length_per_step );
      sev::math::assign_smaller< double > ( _step.speed,
                                            _state.speed_target () );
    }
    generate = true;
    break;

  case Sequence::APPROACH_STOP:
  case Sequence::WITHDRAW_STOP:
  case Sequence::BREAK:
    // Decelerate until stoppable
    if ( _step.speed > _speed_reversible ) {
      _step.speed = snc::speed_accelerate_over_length (
          _step.speed, -_accel, _length_per_step );
      sev::math::assign_larger< double > ( _step.speed, _speed_reversible );
      generate = true;
    }
    break;
  }

  if ( generate ) {
    // Setup step splitter
    const std::uint_fast32_t ticks =
        std::ceil ( step_ticks_at_speed ( _step.speed ) );
    _step.splitter.setup_step (
        _step.seg_type, estepper::segment::MOTOR_SLEEP, ticks );
  } else {
    // Reset step splitter
    _step.splitter.reset ();
  }
  return generate;
}

bool
Pilot_Est::move_feed ()
{
  bool step_factory_good = true;

  while ( true ) {
    // Reset stepper data composer
    _stepper_data_composer.reset ();

    // Compute initial steps capacity
    std::uint_fast32_t steps_capacity = _est.steps_queue_capacity;
    sev::math::subtract_minimum_zero ( steps_capacity,
                                       _axis_state->steps_queue_length () );
    sev::math::assign_smaller ( steps_capacity,
                                _stepper_data_composer.size_free () );

    // Compute initial ticks capacity
    std::uint_fast32_t ticks_capacity = _num_queue_ticks_max;
    sev::math::subtract_minimum_zero ( ticks_capacity,
                                       _axis_state->steps_queue_ticks () );

    while ( step_factory_good && ( steps_capacity >= 2 ) &&
            ( ticks_capacity != 0 ) ) {
      // Load next step on demand
      if ( _step.splitter.done () ) {
        if ( !move_feed_splitter () ) {
          step_factory_good = false;
          break;
        }
      }
      auto step_first = _step.splitter.step ();
      {
        snc::est::Stepper_Segment_Pusher pusher ( _stepper_data_composer,
                                                  step_first.type () );
        // Push first step
        pusher.push ( step_first.ticks () );
        _step.splitter.next ();
        // Update capacities
        steps_capacity -= 2;
        sev::math::subtract_minimum_zero< std::uint_fast32_t > (
            ticks_capacity, step_first.ticks () );
        while ( ( steps_capacity != 0 ) && ( ticks_capacity != 0 ) ) {
          // Load next step on demand
          if ( _step.splitter.done () ) {
            if ( !move_feed_splitter () ) {
              // Out of steps. Break feeding here.
              step_factory_good = false;
              break;
            }
          }
          if ( _step.splitter.step ().type () != step_first.type () ) {
            // Step type missmatch. Stop pushing.
            break;
          }
          // Push next step
          auto step = _step.splitter.step ();
          pusher.push ( step.ticks () );
          _step.splitter.next ();
          // Update capacities
          --steps_capacity;
          sev::math::subtract_minimum_zero< std::uint_fast32_t > (
              ticks_capacity, step.ticks () );
        }
      }
    }

    // Push stepper data or leave loop
    if ( _stepper_data_composer.size () != 0 ) {
      device_stepper_data_push ( _stepper_data_composer );
    } else {
      break;
    }
  }

  return step_factory_good;
}

void
Pilot_Est::enable_stepper_on_demand ()
{
  if ( sev::change ( _sequence.stepper_enabled, true ) ) {
    std::array< std::uint8_t, 3 > message;
    message[ 0 ] = message.size () - 1;
    message[ 1 ] = estepper::out::STEPPER_ENABLE;
    message[ 2 ] = _est.stepper_index;
    device_message_push ( message.data () );
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
      logo << "Sending stepper " << std::uint_fast32_t ( _est.stepper_index )
           << " enable";
    }
  }
}

void
Pilot_Est::disable_stepper_on_demand ()
{
  if ( _axis_state->steps_queue_length () != 0 ) {
    return;
  }
  if ( sev::change ( _sequence.stepper_disabled, true ) ) {
    std::array< std::uint8_t, 3 > message;
    message[ 0 ] = message.size () - 1;
    message[ 1 ] = estepper::out::STEPPER_DISABLE;
    message[ 2 ] = _est.stepper_index;
    device_message_push ( message.data () );
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
      logo << "Sending stepper " << std::uint_fast32_t ( _est.stepper_index )
           << " disable";
    }
  }
}

} // namespace snc::svs::fac::axis_align::ship
