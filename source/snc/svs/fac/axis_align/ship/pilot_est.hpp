/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/segment/types.hpp>
#include <sev/mem/flags.hpp>
#include <snc/device/state/axis.hpp>
#include <snc/device/state/sensor/i1.hpp>
#include <snc/device/state/state.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/est/step_splitter.hpp>
#include <snc/est/stepper_data_composer.hpp>
#include <snc/svp/ship/pilot_est.hpp>
#include <snc/svs/fac/axis_align/ship/events.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_align::ship
{

class Pilot_Est : public snc::svp::ship::Pilot_Est
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Est;

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot_Est ( const Sailor_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Pilot_Est ();

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  void
  office_event_job ( Office_Event & event_n );

  void
  office_notify () override;

  // -- Embedded device message generation

  void
  device_messages_generate () override;

  private:
  // -- Utility

  void
  select_sequence ( Sequence sequence_n );

  /// @brief Returns true if the step splitter was fed
  bool
  move_feed_splitter ();

  /// @brief Returns true if step data was generated
  bool
  move_feed ();

  void
  enable_stepper_on_demand ();

  void
  disable_stepper_on_demand ();

  double
  step_ticks_at_speed ( double speed_n ) const
  {
    return ( _length_per_step * _est.tick_frequency ) / speed_n;
  }

  double
  speed_at_step_ticks ( double ticks_n ) const
  {
    return ( _length_per_step * _est.tick_frequency ) / ticks_n;
  }

  private:
  // -- Attributes
  const std::uint_fast32_t _axis_index = 0;
  State _state;

  // -- Device information
  snc::device::statics::handle::Axis _axis_statics;
  snc::device::state::handle::Axis _axis_state;
  snc::device::state::sensor::handle::I1 _sensor_state;

  struct
  {
    std::uint8_t stepper_index = 0;
    bool direction_reversed = false;
    std::uint_fast32_t steps_queue_capacity = 0;
    double tick_frequency = 0.0;
  } _est;
  struct
  {
    estepper::segment::Type approach;
    estepper::segment::Type withdraw;
  } _seg_type;

  double _speed_reversible = 0.0;
  double _speed_stoppable = 0.0;
  double _speed_max = 0.0;

  double _length_per_step = 0.0;
  double _accel = 0.0;

  std::uint_fast32_t _num_queue_ticks_max = 0;

  // The current sequence state
  struct
  {
    bool stepper_enabled = false;
    bool stepper_disabled = false;
  } _sequence;

  // The current step
  struct
  {
    snc::est::Step_Splitter splitter;
    double speed = 0.0;
    estepper::segment::Type seg_type;
  } _step;

  snc::est::Stepper_Data_Composer _stepper_data_composer;

  // -- Office event io
  sev::event::Pool< ship::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::axis_align::ship
