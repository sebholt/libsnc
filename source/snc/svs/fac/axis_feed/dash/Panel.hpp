/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/fac/axis_feed/office/state.hpp>

namespace snc::svs::fac::axis_feed::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  enum Mode
  {
    NONE,
    FORWARD,
    BACKWARD
  };

  // -- Properties

  Q_ENUM ( Mode );
  Q_PROPERTY ( int axisIndex READ axis_index NOTIFY axisIndexChanged )
  Q_PROPERTY ( int sensorIndex READ sensor_index NOTIFY sensorIndexChanged )
  Q_PROPERTY ( bool active READ active NOTIFY activeChanged )
  Q_PROPERTY ( bool feeding READ feeding NOTIFY feedingChanged )

  Q_PROPERTY ( Mode mode READ mode NOTIFY modeChanged )
  Q_PROPERTY ( bool modeNone READ modeNone NOTIFY modeChanged )
  Q_PROPERTY ( bool modeForward READ modeForward NOTIFY modeChanged )
  Q_PROPERTY ( bool modeBackward READ modeBackward NOTIFY modeChanged )

  // -- Construction

  Panel ( const Panel_Init & init_n,
          std::uint_fast32_t axis_index_n = 0,
          std::uint_fast32_t sensor_index_n = 0 );

  ~Panel ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Axis index

  std::uint_fast32_t
  axis_index () const
  {
    return _axis_index;
  }

  Q_SIGNAL
  void
  axisIndexChanged ();

  // -- Sensor index

  std::uint_fast32_t
  sensor_index () const
  {
    return _sensor_index;
  }

  Q_SIGNAL
  void
  sensorIndexChanged ();

  // -- State

  const snc::svs::fac::axis_feed::office::State &
  state () const
  {
    return _state;
  }

  void
  state_changed () override;

  // -- Active

  bool
  active () const
  {
    return _state.is_active ();
  }

  Q_SIGNAL
  void
  activeChanged ();

  // -- Feeding

  bool
  feeding () const
  {
    return _state.is_feeding ();
  }

  Q_SIGNAL
  void
  feedingChanged ();

  // -- Mode

  Mode
  mode () const
  {
    return _qtState.mode;
  }

  bool
  modeNone () const
  {
    return ( _qtState.mode == Mode::NONE );
  }

  bool
  modeForward () const
  {
    return ( _qtState.mode == Mode::FORWARD );
  }

  bool
  modeBackward () const
  {
    return ( _qtState.mode == Mode::BACKWARD );
  }

  Q_SIGNAL
  void
  modeChanged ();

  private:
  // -- State
  std::uint_fast32_t _axis_index = 0;
  std::uint_fast32_t _sensor_index = 0;
  snc::svs::fac::axis_feed::office::State _state;
  struct
  {
    bool active = false;
    bool feeding = false;
    Mode mode = Mode::NONE;
  } _qtState;
};

} // namespace snc::svs::fac::axis_feed::dash
