/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_move/speeding.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_feed
{

/// @brief Feed mode description
///
class Mode
{
  public:
  // -- Types

  /// @brief Feed direction
  enum class Dir : std::uint8_t
  {
    /// @brief No feeding
    NONE,
    /// @brief Feed until the sensed value turns true
    FORWARD,
    /// @brief Feed as long as the sensed value is true
    BACKWARD
  };

  // -- Setup

  /// @brief Reset to default construction state
  void
  reset ()
  {
    _dir = Dir::NONE;
    _speed.reset ();
  }

  // -- Direction

  const Dir &
  dir () const
  {
    return _dir;
  }

  void
  set_dir ( Dir mode_n )
  {
    _dir = mode_n;
  }

  bool
  dir_is_none () const
  {
    return _dir == Dir::NONE;
  }

  bool
  dir_is_forward () const
  {
    return _dir == Dir::FORWARD;
  }

  bool
  dir_is_backward () const
  {
    return _dir == Dir::BACKWARD;
  }

  // -- Speed

  const snc::svs::fac::axis_move::Speeding &
  speed () const
  {
    return _speed;
  }

  snc::svs::fac::axis_move::Speeding &
  speed_ref ()
  {
    return _speed;
  }

  void
  set_speed ( const snc::svs::fac::axis_move::Speeding & speed_n )
  {
    _speed = speed_n;
  }

  // -- Comparison operators

  bool
  operator== ( const Mode & mode_n ) const
  {
    return ( ( _dir == mode_n._dir ) && ( _speed == mode_n._speed ) );
  }

  bool
  operator!= ( const Mode & mode_n ) const
  {
    return ( ( _dir != mode_n._dir ) || ( _speed != mode_n._speed ) );
  }

  private:
  // -- Attributes
  Dir _dir = Dir::NONE;
  snc::svs::fac::axis_move::Speeding _speed;
};

} // namespace snc::svs::fac::axis_feed
