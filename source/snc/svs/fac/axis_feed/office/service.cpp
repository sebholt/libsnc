/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/axis_feed/office/clerk.hpp>
#include <snc/svs/fac/axis_feed/office/client.hpp>

namespace snc::svs::fac::axis_feed::office
{

Service::Service ( Provider & provider_n, std::uint_fast32_t axis_index_n )
: Super ( provider_n, axis_index_n )
{
  actor ().set_break_hard_func ( [ this ] () {
    // Controller is responsible for breaking
    if ( !controlled () ) {
      control_break_hard ();
    }
  } );
}

Service::~Service () = default;

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  Super::session_begin ();

  // Notify clients
  state_update ();
  clients_session_begin ();
}

void
Service::session_end ()
{
  Super::session_end ();

  // Update state
  _state.reset ();

  // Notify clients
  state_update ();
  clients_session_end ();
}

void
Service::update_pilot_state ( const ship::State & pilot_state_n )
{
  _state.set_is_feeding ( !pilot_state_n.is_idle () );
  state_update ();
}

void
Service::process ()
{
  control_acquirable_change_poll ();
  state_update ();
}

bool
Service::control_break_hard ()
{
  if ( !session_is_good () ) {
    return false;
  }
  return set_mode_break_with_accel (
      axis_info ().statics ()->geo ().accel_max () );
}

bool
Service::control_break_normal ()
{
  return set_mode_break_with_accel ( _state.mode ().speed ().accel () );
}

bool
Service::control_set_mode ( const Mode & mode_n )
{
  return set_mode ( mode_n );
}

bool
Service::set_mode ( const Mode & mode_n )
{
  // Any changes?
  if ( _state.mode () == mode_n ) {
    return true;
  }

  // Activate actor
  if ( actor ().is_idle () && !actor ().go_active () ) {
    return false;
  }

  // Request accepted
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Set mode: { dir: "
         << static_cast< std::uint_fast32_t > ( mode_n.dir () )
         << " speed: " << mode_n.speed ().speed ()
         << " accel: " << mode_n.speed ().accel ()
         << " forward: " << mode_n.speed ().forward () << "}";
  }

  // Update state
  _state.set_mode ( mode_n );
  _state.set_is_active ( true );
  _state.set_mode_acked ( false );
  state_update ();

  provider ().bridge_notify_mode ( mode_n );

  return true;
}

bool
Service::set_mode_break_with_accel ( double accel_n )
{
  // No need to hold when standing
  if ( !_state.is_active () ) {
    return true;
  }

  Mode mode = _state.mode ();
  mode.set_dir ( Mode::Dir::NONE );
  mode.speed_ref ().set_accel ( accel_n );
  return set_mode ( mode );
}

void
Service::state_update ()
{
  // Feed mode change was acknowledged
  _state.set_mode_acked ( provider ().ship_io ().output_satisfied () );

  // Send actor idle
  if ( !actor ().is_idle () ) {
    if ( _state.mode ().dir_is_none () && //
         !_state.is_potentially_feeding () ) {
      actor ().go_idle ();
      _state.set_is_active ( false );
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Actor went idle";
      }
    }
  }

  // Notify
  signal_state ().send ();
  provider ().state_changed ();
}

} // namespace snc::svs::fac::axis_feed::office
