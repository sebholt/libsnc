/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_feed/office/state.hpp>
#include <snc/svs/fac/axis_feed/ship/state.hpp>
#include <snc/svs/fac/base/office/service_axis.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_feed::office
{

// -- Forward declaration
class Clerk;
class Client;

/// @brief Service
///
class Service : public snc::svs::fac::base::office::Service_Axis
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Service_Axis;
  using Provider = snc::svs::fac::axis_feed::office::Clerk;

  public:
  // -- Construction and session

  Service ( Provider & provider_n, std::uint_fast32_t axis_index_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  const auto &
  state () const
  {
    return _state;
  }

  // -- Session interface

  void
  session_begin () override;

  void
  session_end () override;

  // -- Provider interface

  void
  update_pilot_state ( const ship::State & pilot_state_n );

  void
  process ();

  void
  state_update ();

  // -- Client control interface
  private:
  friend snc::svs::fac::axis_feed::office::Client;

  /// @brief Break with maximum deceleration
  bool
  control_break_hard ();

  /// @brief Break with current deceleration
  bool
  control_break_normal ();

  /// @brief Set align mode
  bool
  control_set_mode ( const Mode & mode_n );

  // -- Utility

  bool
  set_mode ( const Mode & mode_n );

  bool
  set_mode_break_with_accel ( double accel_n );

  private:
  State _state;
};

} // namespace snc::svs::fac::axis_feed::office
