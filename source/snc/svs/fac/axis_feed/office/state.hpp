/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_feed/mode.hpp>

namespace snc::svs::fac::axis_feed::office
{

class State
{
  public:
  // -- Setup

  /// @brief Reset to construction state
  void
  reset ()
  {
    _mode.reset ();
    _is_active = false;
    _is_feeding = false;
    _mode_acked = false;
  }

  // -- Feed mode

  const Mode &
  mode () const
  {
    return _mode;
  }

  void
  set_mode ( const Mode & mode_n )
  {
    _mode = mode_n;
  }

  // -- Pilot activity

  bool
  is_active () const
  {
    return _is_active;
  }

  void
  set_is_active ( bool flag_n )
  {
    _is_active = flag_n;
  }

  // -- Feeding activity

  bool
  is_feeding () const
  {
    return _is_feeding;
  }

  void
  set_is_feeding ( bool flag_n )
  {
    _is_feeding = flag_n;
  }

  // -- Feed mode acknowledged by the pilot

  bool
  mode_acked () const
  {
    return _mode_acked;
  }

  void
  set_mode_acked ( bool flag_n )
  {
    _mode_acked = flag_n;
  }

  bool
  is_potentially_feeding () const
  {
    return ( _is_feeding || !_mode_acked );
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( ( _mode == state_n._mode ) &&
             ( _is_active == state_n._is_active ) &&
             ( _is_feeding == state_n._is_feeding ) &&
             ( _mode_acked == state_n._mode_acked ) );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return ( ( _mode != state_n._mode ) ||
             ( _is_active != state_n._is_active ) ||
             ( _is_feeding != state_n._is_feeding ) ||
             ( _mode_acked != state_n._mode_acked ) );
  }

  private:
  // -- Attributes
  Mode _mode;
  bool _is_active = false;
  bool _is_feeding = false;
  bool _mode_acked = false;
};

} // namespace snc::svs::fac::axis_feed::office
