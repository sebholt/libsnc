/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_emb.hpp"
#include <sev/math/exponents.hpp>
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/math/stepping.hpp>
#include <snc/svs/fac/tracker/ship/emb/local_axis.hpp>
#include <snc/svs/fac/tracker/ship/emb/sailor.hpp>
#include <cmath>

namespace snc::svs::fac::axis_feed::ship
{

Pilot_Emb::Pilot_Emb ( const Sailor_Init & init_n,
                       std::uint_fast32_t axis_index_n,
                       std::uint_fast32_t sensor_index_n )
: Super ( init_n,
          sev::string::cat ( "Axis-Feed[", axis_index_n, "]" ),
          axis_index_n )
, _sensor_index ( sensor_index_n )
, _event_pool_state ( office_io ().epool_tracker () )
{
  office_io ().allocate ( _event_pool_state, 2 );

  if ( _sensor_index < device_statics ().sensors_i1 ().size () ) {
    _sensor = &device_state ().sensors_i1 ()[ _sensor_index ];
  } else {
    _sensor = nullptr;
  }
}

Pilot_Emb::~Pilot_Emb () = default;

void
Pilot_Emb::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::in::Type::MODE:
    office_event_mode ( event_n );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  office_io ().notification_set ( Office_Message::STATE );
}

void
Pilot_Emb::office_event_mode ( Office_Event & event_n )
{
  auto & cevent = static_cast< const ship::event::in::Mode & > ( event_n );
  set_mode ( cevent.mode () );
}

void
Pilot_Emb::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( pilot_state () );
    office_io ().submit ( event );
  }
}

void
Pilot_Emb::pilot_state_changed ()
{
  Super::pilot_state_changed ();
  // Request state event
  office_io ().notification_set ( Office_Message::STATE );
}

bool
Pilot_Emb::device_messages_register_value ()
{
  return Super::device_messages_register_value () || ( !_mode.dir_is_none () );
}

void
Pilot_Emb::set_mode ( const Mode & mode_n )
{
  if ( _mode == mode_n ) {
    return;
  }

  // Accept mode
  _mode = mode_n;

  // Sanitize
  {
    const double smax = axis_statics ()->geo ().speed_max ();
    const double amax = axis_statics ()->geo ().accel_max ();
    _mode.speed_ref ().set_speed ( std::min ( _mode.speed ().speed (), smax ) );
    _mode.speed_ref ().set_accel ( std::min ( _mode.speed ().accel (), amax ) );
  }

  // Update hold speed
  _speed_hold = _mode.speed ();
  _speed_hold.set_speed ( 0.0 );

  // Update state
  load_axis_feed ();
  device_messages_register_update ();
}

void
Pilot_Emb::load_axis_feed ()
{
  bool mode_speed = false;

  // Read sensor state to determine the feed speed
  switch ( _mode.dir () ) {
  case Mode::Dir::NONE:
    break;
  case Mode::Dir::FORWARD:
    if ( _sensor && !_sensor->state () ) {
      mode_speed = true;
    }
    break;
  case Mode::Dir::BACKWARD:
    if ( _sensor && _sensor->state () ) {
      mode_speed = true;
    }
    break;
  }

  if ( sev::change ( _speed_current,
                     mode_speed ? _mode.speed () : _speed_hold ) ) {
    load_speeding ( _speed_current );
  }
}

void
Pilot_Emb::device_messages_acquire ( Emb_Writer stream_n )
{
  load_axis_feed ();

  return Super::device_messages_acquire ( stream_n );
}

} // namespace snc::svs::fac::axis_feed::ship
