/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/device/handle.hpp>
#include <snc/svs/fac/axis_feed/ship/events.hpp>
#include <snc/svs/fac/axis_move/ship/est/pilot_bare.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>

namespace snc::svs::fac::axis_feed::ship
{

class Pilot_Est : public snc::svs::fac::axis_move::ship::est::Pilot_Bare
{
  // -- Types
  private:
  using Super = snc::svs::fac::axis_move::ship::est::Pilot_Bare;

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot_Est ( const Sailor_Init & init_n,
              std::uint_fast32_t axis_index_n,
              std::uint_fast32_t sensor_index_n );

  ~Pilot_Est ();

  // -- Office event processing

  public:
  void
  office_event ( Office_Event & event_n ) override;

  private:
  void
  office_event_mode ( Office_Event & event_n );

  void
  office_notify () override;

  // -- Axis speed pilot interface

  void
  pilot_state_changed () override;

  bool
  device_messages_register_value () override;

  // -- Interface
  public:
  void
  set_mode ( const Mode & mode_n );

  // -- Embedded device message generation

  void
  device_messages_generate () override;

  private:
  // -- Utility

  void
  load_axis_feed ();

  private:
  // -- State
  Mode _mode;
  snc::svs::fac::axis_move::Speeding _speed_current;
  snc::svs::fac::axis_move::Speeding _speed_hold;

  // -- Sensor
  std::uint_fast32_t const _sensor_index = 0;
  snc::device::state::sensor::handle::I1 _sensor;

  // -- Office event io
  sev::event::Pool< ship::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::axis_feed::ship
