/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_move/ship/state.hpp>

namespace snc::svs::fac::axis_feed::ship
{

using State = snc::svs::fac::axis_move::ship::State;

} // namespace snc::svs::fac::axis_feed::ship
