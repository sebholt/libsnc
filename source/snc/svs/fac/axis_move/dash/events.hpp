/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/dash/events.hpp>
#include <snc/svs/fac/axis_move/office/state.hpp>

namespace snc::svs::fac::axis_move::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, snc::svs::fac::axis_move::office::State >;

} // namespace snc::svs::fac::axis_move::dash::event::in

namespace snc::svs::fac::axis_move::dash::event::out
{

} // namespace snc::svs::fac::axis_move::dash::event::out
