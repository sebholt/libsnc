/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pools.hpp>
#include <sev/mem/flags.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/axis_move/dash/events.hpp>
#include <snc/svs/fac/axis_move/dynamics.hpp>
#include <snc/svs/fac/axis_move/office/service.hpp>
#include <snc/svs/fac/axis_move/office/state.hpp>
#include <snc/svs/fac/axis_move/ship/events.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <memory>

namespace snc::svs::fac::axis_move::office
{

/// @brief Clerk
///
class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  struct Dash_Message
  {
    static constexpr std::uint_fast32_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n, std::uint_fast32_t axis_index_n );

  ~Clerk ();

  // -- Bridge session interface

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_begin_sailor () override;

  void
  bridge_session_abort () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_state ( Bridge_Event & event_n );

  // -- Central processing

  void
  process () override;

  void
  process_pilot ();

  private:
  // -- Service interface
  friend snc::svs::fac::axis_move::office::Service;

  void
  bridge_notify_break ();

  void
  bridge_notify_dynamics ( const Dynamics & dynamics_n );

  void
  bridge_notify_target ( std::int64_t pos_n );

  void
  bridge_notify_target_delta ( std::int64_t delta_n );

  void
  bridge_notify_speeding ( const Speeding & speed_n );

  void
  state_changed ();

  private:
  // -- State
  Service _service;
  State _state_emitted;

  // -- Bridge event io
  sev::event::Pools< ship::event::in::Break,
                     ship::event::in::Dynamics,
                     ship::event::in::Target,
                     ship::event::in::Target_Delta,
                     ship::event::in::Speed >
      _bridge_epools;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::fac::axis_move::office
