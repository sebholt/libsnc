/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/axis_move/office/service.hpp>

namespace snc::svs::fac::axis_move::office
{

Client::Client ( User * user_n, std::uint_fast32_t axis_index_n )
: Super ( user_n, axis_index_n )
{
}

Client::~Client () = default;

bool
Client::session_is_good () const
{
  return is_connected () ? service ()->session_is_good () : true;
}

snc::device::handle::Info
Client::device_info_handle () const
{
  return is_connected () ? service ()->device_info_handle ()
                         : snc::device::handle::Info ();
}

snc::device::Axis_Info
Client::axis_info () const
{
  return is_connected () ? service ()->axis_info () : snc::device::Axis_Info ();
}

bool
Client::is_idle () const
{

  return is_connected () ? service ()->is_idle () : true;
}

bool
Client::Control::break_hard ()
{
  return is_valid () ? client ()->service ()->control_break_hard () : false;
}

bool
Client::Control::set_dynamics ( const Dynamics & dyn_n )
{
  return is_valid () ? client ()->service ()->control_dynamics ( dyn_n )
                     : false;
}

bool
Client::Control::set_target ( std::int64_t pos_n )
{
  return is_valid () ? client ()->service ()->control_target ( pos_n ) : false;
}

bool
Client::Control::set_target_delta ( std::int64_t delta_n )
{
  return is_valid () ? client ()->service ()->control_target_delta ( delta_n )
                     : false;
}

bool
Client::Control::set_speeding ( const Speeding & speed_n )
{
  return is_valid () ? client ()->service ()->control_speeding ( speed_n )
                     : false;
}

bool
Client::Control::set_speeding ( double speed_n )
{
  return is_valid () ? client ()->service ()->control_speeding ( speed_n )
                     : false;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return connect_to_axis< Service > ( sb_n );
}

} // namespace snc::svs::fac::axis_move::office
