/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_move/dynamics.hpp>
#include <snc/svs/fac/axis_move/office/state.hpp>
#include <snc/svs/fac/axis_move/ship/state.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <snc/svs/fac/base/office/service_axis.hpp>
#include <cstdint>

namespace snc::svs::fac::axis_move::office
{

// -- Forward declaration
class Clerk;
class Client;

/// @brief Service
///
class Service : public snc::svs::fac::base::office::Service_Axis
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Service_Axis;
  using Provider = snc::svs::fac::axis_move::office::Clerk;

  public:
  // -- Construction

  Service ( Provider & provider_n, std::uint_fast32_t axis_index_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Session

  void
  session_begin () override;

  void
  session_end () override;

  // -- Provider interface

  void
  update_pilot_state ( const ship::State & pilot_state_n );

  void
  process ();

  // -- Accessors

  const auto &
  state () const
  {
    return _state;
  }

  // -- Client state interface

  bool
  is_idle () const;

  // -- Client control interface
  private:
  friend snc::svs::fac::axis_move::office::Client;

  /// @brief Break with maximum deceleration
  bool
  control_break_hard ();

  /// @brief Set the speed and acceleration
  bool
  control_dynamics ( const Dynamics & dyn_n );

  /// @brief Move to target
  bool
  control_target ( std::int64_t pos_n );

  /// @brief Move by some steps
  bool
  control_target_delta ( std::int64_t delta_n );

  /// @brief Move at a given speed
  ///
  /// Overrides dynamics() and target.
  bool
  control_speeding ( const Speeding & speed_n );

  /// @brief Move at the given speed
  ///
  /// Override dynamics() and target.
  bool
  control_speeding ( double speed_n );

  void
  state_update ();

  private:
  State _state;
  ship::State _pilot_state;
};

} // namespace snc::svs::fac::axis_move::office
