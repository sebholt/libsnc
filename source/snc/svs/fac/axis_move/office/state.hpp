/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/axis_move/dynamics.hpp>

namespace snc::svs::fac::axis_move::office
{

class State
{
  public:
  // -- Setup

  /// @brief Reset to construction state
  void
  reset ()
  {
    _dynamics.reset ();
  }

  // -- Dynamics

  const snc::svs::fac::axis_move::Dynamics &
  dynamics () const
  {
    return _dynamics;
  }

  void
  set_dynamics ( const snc::svs::fac::axis_move::Dynamics & dyn_n )
  {
    _dynamics = dyn_n;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const
  {
    return ( _dynamics == state_n._dynamics );
  }

  bool
  operator!= ( const State & state_n ) const
  {
    return !operator== ( state_n );
  }

  private:
  // -- Attributes
  snc::svs::fac::axis_move::Dynamics _dynamics;
};

} // namespace snc::svs::fac::axis_move::office
