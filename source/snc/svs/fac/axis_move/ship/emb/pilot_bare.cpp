/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_bare.hpp"
#include <sev/math/exponents.hpp>
#include <sev/math/numbers.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/math/stepping.hpp>
#include <snc/svs/fac/tracker/ship/emb/local_axis.hpp>
#include <snc/svs/fac/tracker/ship/emb/sailor.hpp>
#include <cmath>

namespace snc::svs::fac::axis_move::ship::emb
{

Pilot_Bare::Pilot_Bare ( const Sailor_Init & init_n,
                         sev::unicode::View cell_type_n,
                         sev::unicode::View log_name_n,
                         std::uint_fast32_t axis_index_n )
: Super ( init_n, cell_type_n, log_name_n )
, _axis_index ( axis_index_n )
{
}

Pilot_Bare::Pilot_Bare ( const Sailor_Init & init_n,
                         sev::unicode::View name_n,
                         std::uint_fast32_t axis_index_n )
: Pilot_Bare ( init_n, name_n, name_n, axis_index_n )
{
}

Pilot_Bare::~Pilot_Bare () = default;

void
Pilot_Bare::cell_session_begin ()
{
  Super::cell_session_begin ();

  if ( _axis_index >= device_statics ().axes ().size () ) {
    throw std::runtime_error (
        sev::string::cat ( "Invalid axis index ",
                           _axis_index,
                           " in ",
                           device_statics ().axes ().size (),
                           " available axes" ) );
  }

  _axis_statics = device_statics ().axes ().get ( _axis_index );
  _axis_state = &device_state ().axes ()[ _axis_index ];

  // Setup step splitter
  _step_splitter.reset ();
  _step_splitter.set_usecs_max (
      _axis_statics->emb ().stepper_config ()->step_slice_usecs_max () );

  // Local copy of constants
  _emb_axis_index = _axis_statics->emb ().stepper_index ();
  if ( _axis_statics->emb ().direction_reversed () ) {
    _emb_step_type_forward = snc::emb::type::Step::BACKWARD;
    _emb_step_type_backward = snc::emb::type::Step::FORWARD;
  } else {
    _emb_step_type_forward = snc::emb::type::Step::FORWARD;
    _emb_step_type_backward = snc::emb::type::Step::BACKWARD;
  }
  _emb_steps_queue_hint_usecs_min =
      _axis_statics->emb ().stepper_config ()->steps_queue_hint_usecs_min ();
  _emb_steps_queue_hint_usecs_max =
      _axis_statics->emb ().stepper_config ()->steps_queue_hint_usecs_max ();

  _dyn_limits.speed_max = _axis_statics->geo ().speed_max ();
  _dyn_limits.accel_max = _axis_statics->geo ().accel_max ();
  _length_per_step = _axis_statics->geo ().length_per_step ();

  _pos_min = _axis_statics->step_index_min ();
  _pos_max = _axis_statics->step_index_max ();

  update_break_ranges ();
}

void
Pilot_Bare::break_motion ()
{
  // When do we not accept the request?
  if ( !_pilot_state.is_active () ) {
    return;
  }

  _pilot_state.set_run_state ( State::Run_State::BREAKING );
  this->pilot_state_changed ();
}

void
Pilot_Bare::load_dynamics ( const Dynamics & dyn_n )
{
  // Sanitize speed
  _speed_target = std::min ( dyn_n.speed (), _dyn_limits.speed_max );

  // Sanitize acceleration
  _accel_target = std::min ( dyn_n.accel (), _dyn_limits.accel_max );

  // Update breaking ranges
  update_break_ranges ();
}

void
Pilot_Bare::load_target ( std::int64_t pos_n )
{
  // Sanitize step position
  _pos_target = std::clamp ( pos_n, _pos_min, _pos_max );

  if ( _pilot_state.is_idle () ) {
    // Acquire current axis state
    _speed_current = 0.0;
    _speed_current_forward = ( _pos_target >= _pos_current );
    _pos_current = _axis_state->step_latest ().position ();

    // Check if we're already there
    if ( _pos_current == _pos_target ) {
      return;
    }
  }

  // Update breaking ranges
  update_break_ranges ();

  // Switch pilot state to active on demand
  if ( !_pilot_state.is_active () ) {
    _pilot_state.set_run_state ( State::Run_State::ACTIVE );
    this->pilot_state_changed ();
  }
}

void
Pilot_Bare::load_target_delta ( std::int64_t delta_n )
{
  std::int64_t pos = _pilot_state.is_idle ()
                         ? _axis_state->step_latest ().position ()
                         : _pos_target;
  load_target ( pos + delta_n );
}

void
Pilot_Bare::load_speeding ( const Speeding & speed_n )
{
  load_dynamics ( speed_n );
  load_target ( speed_n.forward () ? _pos_max : _pos_min );
}

void
Pilot_Bare::pilot_state_changed ()
{
  device_messages_register_update ();
}

void
Pilot_Bare::device_messages_register_update ()
{
  device_messages_register ( this->device_messages_register_value () );
}

bool
Pilot_Bare::device_messages_register_value ()
{
  return !_pilot_state.is_idle ();
}

std::int64_t
Pilot_Bare::break_steps ( double accel_n ) const
{
  // Compute using a higher speed to get a safe number of steps
  const double safe_scale = 1.5;
  const double speed = std::max ( _speed_target, _speed_current ) * safe_scale;
  const double break_length = acceleration_length_for_speed ( speed, accel_n );
  return std::ceil ( break_length / _length_per_step );
}

void
Pilot_Bare::update_break_range_below ( std::int64_t & limit_n,
                                       std::int64_t target_n,
                                       std::int64_t steps_n )
{
  limit_n = target_n - steps_n;
  // Undeflow protection
  if ( limit_n > target_n ) {
    limit_n = std::numeric_limits< std::int64_t >::min ();
  }
}

void
Pilot_Bare::update_break_range_above ( std::int64_t & limit_n,
                                       std::int64_t target_n,
                                       std::int64_t steps_n )
{
  limit_n = target_n + steps_n;
  // Overflow protection
  if ( limit_n < target_n ) {
    limit_n = std::numeric_limits< std::int64_t >::max ();
  }
}

void
Pilot_Bare::update_break_ranges ()
{
  // Target acceleration
  {
    std::int64_t const steps = break_steps ( _accel_target );
    update_break_range_below ( _pos_target_break_lower, _pos_target, steps );
    update_break_range_above ( _pos_target_break_upper, _pos_target, steps );
    update_break_range_above ( _pos_min_break_target, _pos_min, steps );
    update_break_range_below ( _pos_max_break_target, _pos_max, steps );
  }

  // Maximum acceleration
  {
    std::int64_t const steps = break_steps ( _dyn_limits.accel_max );
    update_break_range_above ( _pos_min_break_max, _pos_min, steps );
    update_break_range_below ( _pos_max_break_max, _pos_max, steps );
  }
}

void
Pilot_Bare::device_messages_acquire ( Emb_Writer stream_n )
{
  auto track_local = tracker ().local_axis_tracker ( _axis_index );

  std::uint_fast32_t num_pushable_values = track_local.queue_slots_free ();
  std::uint_fast32_t num_pushable_usecs = 0;
  {
    const std::uint_fast32_t qusecs = track_local.queue_usecs ();
    if ( ( qusecs < _emb_steps_queue_hint_usecs_min ) &&
         ( qusecs < _emb_steps_queue_hint_usecs_max ) ) {
      num_pushable_usecs = _emb_steps_queue_hint_usecs_max;
      num_pushable_usecs -= qusecs;
    }
  }

  snc::emb::msg::out::Steps_U16_64 * message = nullptr;

  while ( ( num_pushable_values != 0 ) && ( num_pushable_usecs != 0 ) ) {
    if ( _step_splitter.is_finished () ) {
      device_messages_step_acquire ();
      if ( _step_splitter.is_finished () ) {
        break;
      }
    }

    // Make new message on demand
    if ( ( message == nullptr ) || message->values_full () ||
         ( message->job_type () != _step_splitter.job_type () ) ) {
      stream_n.acquire ( message );
      message->set_stepper_index ( _emb_axis_index );
      message->set_job_type ( _step_splitter.job_type () );
    }

    const std::uint_fast32_t num_usecs = _step_splitter.acquire_usecs_next ();
    {
      // Update local tracking
      sev::math::subtract_minimum_zero ( num_pushable_usecs, num_usecs );
      --num_pushable_values;
    }
    message->push_usecs ( num_usecs );
  }
}

void
Pilot_Bare::device_messages_step_acquire ()
{
  while ( true ) {
    switch ( _pilot_state.run_state () ) {
    case State::Run_State::IDLE:
      return;

    case State::Run_State::BREAKING:
      // Generate step
      if ( _speed_current_forward ? step_generate_break< true > ()
                                  : step_generate_break< false > () ) {
        return;
      }

      // Finish if we are ready to stop
      if ( _speed_current == 0.0 ) {
        device_messages_step_idle ();
        return;
      }

      break;

    case State::Run_State::ACTIVE:
      // Reverse direction on demand
      device_messages_step_reverse_direction ();
      // Generate step
      if ( _speed_current_forward ? step_generate< true > ()
                                  : step_generate< false > () ) {
        return;
      }

      // Finish if we are ready to stop
      if ( _speed_current == 0.0 ) {
        // Leave if hold was requested or the target is reached
        if ( ( _speed_target == 0.0 ) || ( _pos_target == _pos_current ) ) {
          device_messages_step_idle ();
          return;
        }
      }

      break;
    }
  }
}

void
Pilot_Bare::device_messages_step_idle ()
{
  // Set target position to current postion when going idle
  _pos_target = _pos_current;

  // Set run state
  _pilot_state.set_run_state ( State::Run_State::IDLE );
  this->pilot_state_changed ();
}

template < bool FORWARD >
inline bool
Pilot_Bare::step_generate_speed ( double speed_n )
{
  _speed_current = speed_n;
  if ( _speed_current == 0.0 ) {
    return false;
  }

  _step_splitter.setup_step (
      FORWARD ? _emb_step_type_forward : _emb_step_type_backward,
      std::ceil ( ( _length_per_step / _speed_current ) * sev::math::mega_d ) );
  _pos_current += FORWARD ? 1 : -1;

  return true;
}

void
Pilot_Bare::device_messages_step_reverse_direction ()
{
  // No need to reverse if we're approaching the target.
  if ( _speed_current_forward ? ( _pos_current <= _pos_target )
                              : ( _pos_current >= _pos_target ) ) {
    return;
  }
  // Don't reverse if we're moving.
  if ( _speed_current > 0.0 ) {
    return;
  }

  // Reverse direction!
  _speed_current_forward = !_speed_current_forward;
}

template < bool FORWARD >
bool
Pilot_Bare::step_generate ()
{
  // -- Celerate to target speed
  double speed_next = _speed_current;
  {
    // Accelerate to target speed
    if ( _speed_current < _speed_target ) {
      double const speed_max = snc::speed_accelerate_over_length (
          _speed_current, _accel_target, _length_per_step );
      speed_next = std::min ( speed_max, _speed_target );
    }
    // Decelerate to target speed
    if ( _speed_current > _speed_target ) {
      double const speed_min = snc::speed_accelerate_over_length (
          _speed_current, -_accel_target, _length_per_step );
      speed_next = std::max ( speed_min, _speed_target );

      // Avoid near zero speeds when stopping
      if ( ( _speed_target == 0.0 ) &&
           ( speed_next < stoppable_speed ( _accel_target ) ) ) {
        speed_next = 0.0;
      }
    }
  }

  // -- Reduce speed near border
  step_decelerate_border_approach< FORWARD > ( speed_next );

  // -- Reduce speed near target or break on direction missmatch
  if ( FORWARD ? ( _pos_current <= _pos_target )
               : ( _pos_current >= _pos_target ) ) {
    step_decelerate_target_approach< FORWARD > ( speed_next );
  } else {
    step_decelerate_to_stop ( speed_next, _accel_target );
  }

  // -- Generate step
  return step_generate_speed< FORWARD > ( speed_next );
}

template < bool FORWARD >
bool
Pilot_Bare::step_generate_break ()
{
  double speed_next = _speed_current;

  // -- Decelerate
  step_decelerate_to_stop ( speed_next, _dyn_limits.accel_max );

  // -- Decelerate near border
  step_decelerate_border_approach< FORWARD > ( speed_next );

  // -- Generate step
  return step_generate_speed< FORWARD > ( speed_next );
}

void
Pilot_Bare::step_decelerate_to_stop ( double & speed_n, double accel_n ) const
{
  double speed_max = snc::speed_accelerate_over_length (
      _speed_current, -accel_n, _length_per_step );

  // We don't need to go below this speed.
  if ( speed_max >= stoppable_speed ( accel_n ) ) {
    sev::math::assign_smaller ( speed_n, speed_max );
  } else {
    speed_n = 0.0;
  }
}

template < bool FORWARD >
inline void
Pilot_Bare::step_decelerate_border_approach ( double & speed_n ) const
{
  // Reduce speed if we're in the target acceleration breaking range.
  if ( FORWARD ? ( _pos_current >= _pos_max_break_target )
               : ( _pos_current <= _pos_min_break_target ) ) {
    step_decelerate_border_approach< FORWARD > ( speed_n, _accel_target );
  }

  // Reduce speed if we're in the maximum acceleration breaking range.
  if ( FORWARD ? ( _pos_current >= _pos_max_break_max )
               : ( _pos_current <= _pos_min_break_max ) ) {
    step_decelerate_border_approach< FORWARD > ( speed_n,
                                                 _dyn_limits.accel_max );
  }
}

template < bool FORWARD >
inline void
Pilot_Bare::step_decelerate_border_approach ( double & speed_n,
                                              double accel_n ) const
{
  std::int64_t const steps_to_border =
      FORWARD ? ( _pos_max - _pos_current ) : ( _pos_current - _pos_min );
  if ( steps_to_border > 0 ) {
    double const speed_max = snc::speed_accelerate_over_length (
        accel_n, double ( steps_to_border ) * _length_per_step );
    sev::math::assign_smaller ( speed_n, speed_max );
  } else {
    speed_n = 0.0;
  }
}

template < bool FORWARD >
inline void
Pilot_Bare::step_decelerate_target_approach ( double & speed_n ) const
{
  // Decelerate if we're in the target's breaking range.
  if ( FORWARD ? ( _pos_current < _pos_target_break_lower )
               : ( _pos_current > _pos_target_break_upper ) ) {
    return;
  }

  // Compute minimal achievable speed based on the current speed
  double speed_reduced = snc::speed_accelerate_over_length (
      _speed_current, -_accel_target, _length_per_step );

  // Compute recommended approach speed near target
  std::int64_t const steps_to_target =
      FORWARD ? ( _pos_target - _pos_current ) : ( _pos_current - _pos_target );
  if ( steps_to_target > 0 ) {
    double speed_approach_max = snc::speed_accelerate_over_length (
        _accel_target, double ( steps_to_target ) * _length_per_step );

    // We don't need to be slower than the recommended approach speed
    sev::math::assign_larger ( speed_reduced, speed_approach_max );
  } else {
    // Avoid near zero speeds when stopping
    if ( speed_reduced <= stoppable_speed ( _accel_target ) ) {
      speed_reduced = 0.0;
    }
  }

  // Reduce speed on demand
  sev::math::assign_smaller ( speed_n, speed_reduced );
}

double
Pilot_Bare::stoppable_speed ( double accel_n ) const
{
  // We don't need to go below this speed.
  double stoppale_scale = 0.5;
  double speed_stoppable = snc::speed_accelerate_over_length (
      accel_n, _length_per_step * stoppale_scale );
  return speed_stoppable;
}

} // namespace snc::svs::fac::axis_move::ship::emb
