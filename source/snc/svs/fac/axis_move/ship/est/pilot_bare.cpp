/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_bare.hpp"
#include <estepper/out/messages.hpp>
#include <sev/math/exponents.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/est/config/stepper.hpp>
#include <snc/est/stepper_data_composer.hpp>
#include <snc/math/stepping.hpp>

namespace snc::svs::fac::axis_move::ship::est
{

Pilot_Bare::Pilot_Bare ( const Sailor_Init & init_n,
                         sev::unicode::View cell_type_n,
                         sev::unicode::View log_name_n,
                         std::uint_fast32_t axis_index_n )
: Super ( init_n, cell_type_n, log_name_n )
, _axis_index ( axis_index_n )
, _axis_statics ( device_statics ().axes ().get ( _axis_index ) )
, _axis_state ( device_state ().axis_shared ( _axis_index ) )
{
  // EStepper info
  _est.stepper_index = _axis_statics->est ().stepper_index ();
  _est.direction_reversed = _axis_statics->est ().direction_reversed ();
  _est.steps_queue_capacity =
      _axis_statics->est ().stepper_config ()->steps_queue_capacity ();
  _est.tick_frequency =
      _axis_statics->est ().stepper_config ()->tick_frequency ();

  // Segment types
  if ( _est.direction_reversed ) {
    _seg_type.forward = estepper::segment::MOTOR_BACKWARD;
    _seg_type.backward = estepper::segment::MOTOR_FORWARD;
  } else {
    _seg_type.forward = estepper::segment::MOTOR_FORWARD;
    _seg_type.backward = estepper::segment::MOTOR_BACKWARD;
  }

  // Axis limits
  _dyn_limits.speed_max = _axis_statics->geo ().speed_max ();
  _dyn_limits.accel_max = _axis_statics->geo ().accel_max ();
  _length_per_step = _axis_statics->geo ().length_per_step ();

  double const steps_queue_usecs_min = 30 * sev::math::kilo_ui;
  double const steps_queue_usecs_max = 50 * sev::math::kilo_ui;
  _steps_queue_ticks_min = std::ceil (
      ( _est.tick_frequency / sev::math::mega_d ) * steps_queue_usecs_min );
  _steps_queue_ticks_max = std::ceil (
      ( _est.tick_frequency / sev::math::mega_d ) * steps_queue_usecs_max );

  _pos_min = _axis_statics->step_index_min ();
  _pos_max = _axis_statics->step_index_max ();

  update_break_ranges ();
}

Pilot_Bare::Pilot_Bare ( const Sailor_Init & init_n,
                         sev::unicode::View name_n,
                         std::uint_fast32_t axis_index_n )
: Pilot_Bare ( init_n, name_n, name_n, axis_index_n )
{
}

Pilot_Bare::~Pilot_Bare () = default;

void
Pilot_Bare::break_motion ()
{
  // When do we not accept the request?
  if ( !_pilot_state.is_active () ) {
    return;
  }

  _pilot_state.set_run_state ( State::Run_State::BREAKING );
  this->pilot_state_changed ();
}

void
Pilot_Bare::load_dynamics ( const Dynamics & dyn_n )
{
  // Sanitize speed
  _speed_target = std::min ( dyn_n.speed (), _dyn_limits.speed_max );

  // Sanitize acceleration
  _accel_target = std::min ( dyn_n.accel (), _dyn_limits.accel_max );

  // Update breaking ranges
  update_break_ranges ();
}

void
Pilot_Bare::load_target ( std::int64_t pos_n )
{
  // Sanitize step position
  _pos_target = std::clamp ( pos_n, _pos_min, _pos_max );

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
    logo << "load_target: pos_target: " << _pos_target;
  }

  if ( _pilot_state.is_idle () ) {
    // Acquire current axis state
    _speed_current = 0.0;
    _speed_current_forward = ( _pos_target >= _pos_current );
    _pos_current = _axis_state->step_latest ().position ();

    // Check if we're already there
    if ( _pos_current == _pos_target ) {
      return;
    }
  }

  // Update breaking ranges
  update_break_ranges ();

  // Switch pilot state to active on demand
  if ( !_pilot_state.is_active () ) {
    _pilot_state.set_run_state ( State::Run_State::ACTIVE );
    this->pilot_state_changed ();
  }
}

void
Pilot_Bare::load_target_delta ( std::int64_t delta_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
    logo << "load_target_delta: delta: " << delta_n;
  }

  std::int64_t pos = _pilot_state.is_idle ()
                         ? _axis_state->step_latest ().position ()
                         : _pos_target;
  load_target ( pos + delta_n );
}

void
Pilot_Bare::load_speeding ( const Speeding & speed_n )
{
  load_dynamics ( speed_n );
  load_target ( speed_n.forward () ? _pos_max : _pos_min );
}

void
Pilot_Bare::pilot_state_changed ()
{
  device_messages_register_update ();
}

void
Pilot_Bare::device_messages_register_update ()
{
  device_messages_register ( this->device_messages_register_value () );
}

bool
Pilot_Bare::device_messages_register_value ()
{
  return !_pilot_state.is_idle ();
}

void
Pilot_Bare::update_break_ranges ()
{
  auto break_steps = [ this ] ( double accel_n ) -> std::int64_t {
    // Compute using a higher speed to get a safe number of steps
    const double safe_scale = 1.5;
    const double speed =
        std::max ( _speed_target, _speed_current ) * safe_scale;
    const double break_length =
        snc::acceleration_length_for_speed ( speed, accel_n );
    return std::ceil ( break_length / _length_per_step );
  };

  auto update_break_range_below = [] ( std::int64_t & limit_n,
                                       std::int64_t target_n,
                                       std::int64_t steps_n ) {
    limit_n = target_n - steps_n;
    // Undeflow protection
    if ( limit_n > target_n ) {
      limit_n = std::numeric_limits< std::int64_t >::min ();
    }
  };

  auto update_break_range_above = [] ( std::int64_t & limit_n,
                                       std::int64_t target_n,
                                       std::int64_t steps_n ) {
    limit_n = target_n + steps_n;
    // Overflow protection
    if ( limit_n < target_n ) {
      limit_n = std::numeric_limits< std::int64_t >::max ();
    }
  };

  // Target acceleration
  {
    std::int64_t const steps = break_steps ( _accel_target );
    update_break_range_below ( _pos_target_break_lower, _pos_target, steps );
    update_break_range_above ( _pos_target_break_upper, _pos_target, steps );
    update_break_range_above ( _pos_min_break_target, _pos_min, steps );
    update_break_range_below ( _pos_max_break_target, _pos_max, steps );
  }

  // Maximum acceleration
  {
    std::int64_t const steps = break_steps ( _dyn_limits.accel_max );
    update_break_range_above ( _pos_min_break_max, _pos_min, steps );
    update_break_range_below ( _pos_max_break_max, _pos_max, steps );
  }
}

void
Pilot_Bare::device_messages_generate ()
{
  auto ret = device_messages_feed ();
  // Enable stepper on demand
  if ( ret.first ) {
    device_messsages_stepper_enable ();
  }
  if ( !ret.second ) {
    // Disable stepper on demand
    device_messsages_stepper_disable ();
    // Wait for axis disable acknowledge
    if ( !_axis_state->is_enabled () ) {
      // Send pilot idle on demand
      if ( _pilot_state.run_state () != State::Run_State::IDLE ) {
        _pilot_state.set_run_state ( State::Run_State::IDLE );
        this->pilot_state_changed ();
      }
    }
  }
}

void
Pilot_Bare::device_messsages_stepper_enable ()
{
  if ( sev::change ( _sequence.stepper_enabled, true ) ) {
    _sequence.stepper_disabled = false;
    {
      std::array< std::uint8_t, 3 > message;
      message[ 0 ] = message.size () - 1;
      message[ 1 ] = estepper::out::STEPPER_ENABLE;
      message[ 2 ] = _est.stepper_index;
      device_message_push ( message.data () );
    }
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
      logo << "Sending stepper " << std::uint_fast32_t ( _est.stepper_index )
           << " enable";
    }
  }
}

void
Pilot_Bare::device_messsages_stepper_disable ()
{
  if ( _axis_state->steps_queue_length () != 0 ) {
    return;
  }
  if ( sev::change ( _sequence.stepper_disabled, true ) ) {
    _sequence.stepper_enabled = false;
    {
      std::array< std::uint8_t, 3 > message;
      message[ 0 ] = message.size () - 1;
      message[ 1 ] = estepper::out::STEPPER_DISABLE;
      message[ 2 ] = _est.stepper_index;
      device_message_push ( message.data () );
    }
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
      logo << "Sending stepper " << std::uint_fast32_t ( _est.stepper_index )
           << " disable";
    }
  }
}

std::pair< bool, bool >
Pilot_Bare::device_messages_feed ()
{
  // The return value
  std::pair< bool, bool > res ( false, true );
  bool & steps_generated = res.first;
  bool & step_factory_good = res.second;

  // Stepper data composer
  snc::est::Stepper_Data_Composer stepper_data_composer ( _est.stepper_index );

  // Compute initial ticks capacity
  std::uint_fast64_t ticks_capacity = 0;
  {
    std::uint_fast64_t queue_ticks = _axis_state->steps_queue_ticks ();
    // Only feed when we're below the minimum to encourage multi step feeds
    if ( queue_ticks < _steps_queue_ticks_min ) {
      ticks_capacity = _steps_queue_ticks_max;
      sev::math::subtract_minimum_zero ( ticks_capacity, queue_ticks );
    }
  }

  while ( true ) {
    // Compute initial steps capacity
    std::uint_fast64_t steps_capacity = _est.steps_queue_capacity;
    sev::math::subtract_minimum_zero ( steps_capacity,
                                       _axis_state->steps_queue_length () );
    sev::math::assign_smaller ( steps_capacity,
                                stepper_data_composer.size_free () );

    while ( step_factory_good && ( steps_capacity >= 2 ) &&
            ( ticks_capacity != 0 ) ) {
      // Load next step on demand
      if ( _step_splitter.done () ) {
        if ( !device_messages_feed_splitter () ) {
          step_factory_good = false;
          break;
        }
      }
      auto step_first = _step_splitter.step ();
      {
        snc::est::Stepper_Segment_Pusher pusher ( stepper_data_composer,
                                                  step_first.type () );

        // Push first step
        pusher.push ( step_first.ticks () );
        _step_splitter.next ();
        // Update capacities
        steps_capacity -= 2;
        sev::math::subtract_minimum_zero< std::uint_fast64_t > (
            ticks_capacity, step_first.ticks () );

        // Push subsequent steps if the type matches
        while ( ( steps_capacity != 0 ) && ( ticks_capacity != 0 ) ) {
          // Load next step on demand
          if ( _step_splitter.done () ) {
            if ( !device_messages_feed_splitter () ) {
              // Out of steps. Break feeding here.
              step_factory_good = false;
              break;
            }
          }
          if ( _step_splitter.step ().type () != step_first.type () ) {
            // Step type missmatch. Stop pushing.
            break;
          }
          // Push next step
          auto step = _step_splitter.step ();
          pusher.push ( step.ticks () );
          _step_splitter.next ();
          // Update capacities
          --steps_capacity;
          sev::math::subtract_minimum_zero< std::uint_fast64_t > (
              ticks_capacity, step.ticks () );
        }
      }
    }

    // Push stepper data or leave loop
    if ( stepper_data_composer.size () != 0 ) {
      device_stepper_data_push ( stepper_data_composer );
      // Reset stepper data composer
      stepper_data_composer.reset ();
      steps_generated = true;
    } else {
      // Leave loop
      break;
    }
  }

  return res;
}

bool
Pilot_Bare::device_messages_feed_splitter ()
{
  switch ( _pilot_state.run_state () ) {
  case State::Run_State::IDLE:
    return false;

  case State::Run_State::BREAKING:
    // Generate step
    if ( _speed_current != 0.0 ) {
      return ( _speed_current_forward ? step_generate_break< true > ()
                                      : step_generate_break< false > () );
    }
    return false;

  case State::Run_State::ACTIVE:
    while ( true ) {
      bool stopped =
          ( _speed_current == 0.0 ) &&
          ( ( _speed_target == 0.0 ) || ( _pos_target == _pos_current ) );
      if ( !stopped ) {
        // Generate step
        if ( _speed_current_forward ? step_generate< true > ()
                                    : step_generate< false > () ) {
          return true;
        } else {
          // Reverse direction on demand
          if ( !device_messages_step_reverse_direction () ) {
            return false;
          }
        }
      } else {
        return false;
      }
    }
    return false;
  }

  return false;
}

bool
Pilot_Bare::device_messages_step_reverse_direction ()
{
  // No need to reverse if we're approaching the target.
  if ( _speed_current_forward ? ( _pos_current <= _pos_target )
                              : ( _pos_current >= _pos_target ) ) {
    return false;
  }
  // Don't reverse if we're moving.
  if ( _speed_current > 0.0 ) {
    return false;
  }

  // Reverse direction!
  _speed_current_forward = !_speed_current_forward;
  return true;
}

template < bool FORWARD >
inline bool
Pilot_Bare::step_generate_speed ( double speed_n )
{
  _speed_current = speed_n;
  if ( _speed_current == 0.0 ) {
    return false;
  }

  _step_splitter.setup_step (
      FORWARD ? _seg_type.forward : _seg_type.backward,
      estepper::segment::MOTOR_SLEEP,
      std::ceil ( step_ticks_at_speed ( _speed_current ) ) );
  _pos_current += FORWARD ? 1 : -1;
  return true;
}

template < bool FORWARD >
bool
Pilot_Bare::step_generate ()
{
  // -- Celerate to target speed
  double speed_next = _speed_current;
  {
    // Accelerate to target speed
    if ( _speed_current < _speed_target ) {
      double const speed_max = snc::speed_accelerate_over_length (
          _speed_current, _accel_target, _length_per_step );
      speed_next = std::min ( speed_max, _speed_target );
    }
    // Decelerate to target speed
    if ( _speed_current > _speed_target ) {
      double const speed_min = snc::speed_accelerate_over_length (
          _speed_current, -_accel_target, _length_per_step );
      speed_next = std::max ( speed_min, _speed_target );

      // Avoid near zero speeds when stopping
      if ( ( _speed_target == 0.0 ) &&
           ( speed_next < stoppable_speed ( _accel_target ) ) ) {
        speed_next = 0.0;
      }
    }
  }

  // -- Reduce speed near border
  step_decelerate_border_approach< FORWARD > ( speed_next );

  // -- Reduce speed near target or break on direction missmatch
  if ( FORWARD ? ( _pos_current <= _pos_target )
               : ( _pos_current >= _pos_target ) ) {
    step_decelerate_target_approach< FORWARD > ( speed_next );
  } else {
    step_decelerate_to_stop ( speed_next, _accel_target );
  }

  // -- Generate step
  return step_generate_speed< FORWARD > ( speed_next );
}

template < bool FORWARD >
bool
Pilot_Bare::step_generate_break ()
{
  double speed_next = _speed_current;

  // -- Decelerate
  step_decelerate_to_stop ( speed_next, _dyn_limits.accel_max );

  // -- Decelerate near border
  step_decelerate_border_approach< FORWARD > ( speed_next );

  // -- Generate step
  return step_generate_speed< FORWARD > ( speed_next );
}

void
Pilot_Bare::step_decelerate_to_stop ( double & speed_n, double accel_n ) const
{
  double speed_max = snc::speed_accelerate_over_length (
      _speed_current, -accel_n, _length_per_step );

  // We don't need to go below this speed.
  if ( speed_max >= stoppable_speed ( accel_n ) ) {
    sev::math::assign_smaller ( speed_n, speed_max );
  } else {
    speed_n = 0.0;
  }
}

template < bool FORWARD >
inline void
Pilot_Bare::step_decelerate_border_approach ( double & speed_n ) const
{
  // Reduce speed if we're in the target acceleration breaking range.
  if ( FORWARD ? ( _pos_current >= _pos_max_break_target )
               : ( _pos_current <= _pos_min_break_target ) ) {
    step_decelerate_border_approach< FORWARD > ( speed_n, _accel_target );
  }

  // Reduce speed if we're in the maximum acceleration breaking range.
  if ( FORWARD ? ( _pos_current >= _pos_max_break_max )
               : ( _pos_current <= _pos_min_break_max ) ) {
    step_decelerate_border_approach< FORWARD > ( speed_n,
                                                 _dyn_limits.accel_max );
  }
}

template < bool FORWARD >
inline void
Pilot_Bare::step_decelerate_border_approach ( double & speed_n,
                                              double accel_n ) const
{
  std::int64_t const steps_to_border =
      FORWARD ? ( _pos_max - _pos_current ) : ( _pos_current - _pos_min );
  if ( steps_to_border > 0 ) {
    double const speed_max = snc::speed_accelerate_over_length (
        accel_n, double ( steps_to_border ) * _length_per_step );
    sev::math::assign_smaller ( speed_n, speed_max );
  } else {
    speed_n = 0.0;
  }
}

template < bool FORWARD >
inline void
Pilot_Bare::step_decelerate_target_approach ( double & speed_n ) const
{
  // Decelerate if we're in the target's breaking range.
  if ( FORWARD ? ( _pos_current < _pos_target_break_lower )
               : ( _pos_current > _pos_target_break_upper ) ) {
    return;
  }

  // Compute minimal achievable speed based on the current speed
  double speed_reduced = snc::speed_accelerate_over_length (
      _speed_current, -_accel_target, _length_per_step );

  // Compute recommended approach speed near target
  std::int64_t const steps_to_target =
      FORWARD ? ( _pos_target - _pos_current ) : ( _pos_current - _pos_target );
  if ( steps_to_target > 0 ) {
    double speed_approach_max = snc::speed_accelerate_over_length (
        _accel_target, double ( steps_to_target ) * _length_per_step );

    // We don't need to be slower than the recommended approach speed
    sev::math::assign_larger ( speed_reduced, speed_approach_max );
  } else {
    // Avoid near zero speeds when stopping
    if ( speed_reduced <= stoppable_speed ( _accel_target ) ) {
      speed_reduced = 0.0;
    }
  }

  // Reduce speed on demand
  sev::math::assign_smaller ( speed_n, speed_reduced );
}

double
Pilot_Bare::stoppable_speed ( double accel_n ) const
{
  // We don't need to go below this speed.
  double stoppale_scale = 0.5;
  double speed_stoppable = snc::speed_accelerate_over_length (
      accel_n, _length_per_step * stoppale_scale );
  return speed_stoppable;
}

} // namespace snc::svs::fac::axis_move::ship::est
