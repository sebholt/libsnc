/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/segment/types.hpp>
#include <snc/est/step_splitter.hpp>
#include <snc/svp/ship/pilot_est.hpp>
#include <snc/svs/fac/axis_move/dynamics.hpp>
#include <snc/svs/fac/axis_move/ship/state.hpp>
#include <snc/svs/fac/axis_move/speeding.hpp>
#include <utility>

namespace snc::svs::fac::axis_move::ship::est
{

class Pilot_Bare : public snc::svp::ship::Pilot_Est
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Est;

  struct Dyn_Limits
  {
    double speed_max = 0.0;
    double accel_max = 0.0;
  };

  // -- Construction
  public:
  Pilot_Bare ( const Sailor_Init & init_n,
               sev::unicode::View cell_type_n,
               sev::unicode::View log_name_n,
               std::uint_fast32_t axis_index_n );

  Pilot_Bare ( const Sailor_Init & init_n,
               sev::unicode::View name_n,
               std::uint_fast32_t axis_index_n );

  ~Pilot_Bare ();

  protected:
  // -- Interface: Accessors

  const State &
  pilot_state () const
  {
    return _pilot_state;
  }

  const snc::device::statics::handle::Axis &
  axis_statics () const
  {
    return _axis_statics;
  }

  const snc::device::state::handle::Axis &
  axis_state () const
  {
    return _axis_state;
  }

  const Dyn_Limits &
  dyn_limits () const
  {
    return _dyn_limits;
  }

  const double &
  length_per_step () const
  {
    return _length_per_step;
  }

  const std::int64_t &
  pos_current () const
  {
    return _pos_current;
  }

  const std::int64_t &
  pos_target () const
  {
    return _pos_target;
  }

  const std::int64_t &
  pos_min () const
  {
    return _pos_min;
  }

  const std::int64_t &
  pos_max () const
  {
    return _pos_max;
  }

  // -- Interface: Loading

  void
  break_motion ();

  void
  load_dynamics ( const Dynamics & dyn_n );

  void
  load_target ( std::int64_t pos_n );

  void
  load_target_delta ( std::int64_t pos_n );

  void
  load_speeding ( const Speeding & speed_n );

  // -- Interface: State change

  virtual void
  pilot_state_changed ();

  virtual bool
  device_messages_register_value ();

  /// @brief Set devices messages register to
  /// this->device_messages_register_value();
  void
  device_messages_register_update ();

  // -- Embedded device message generation
  public:
  void
  device_messages_generate () override;

  private:
  void
  device_messsages_stepper_enable ();

  void
  device_messsages_stepper_disable ();

  std::pair< bool, bool >
  device_messages_feed ();

  bool
  device_messages_feed_splitter ();

  bool
  device_messages_step_reverse_direction ();

  template < bool FORWARD >
  bool
  step_generate_speed ( double speed_n );

  template < bool FORWARD >
  bool
  step_generate ();

  template < bool FORWARD >
  bool
  step_generate_break ();

  void
  step_decelerate_to_stop ( double & speed_n, double accel_n ) const;

  template < bool FORWARD >
  void
  step_decelerate_border_approach ( double & speed_n ) const;

  template < bool FORWARD >
  void
  step_decelerate_border_approach ( double & speed_n, double accel_n ) const;

  template < bool FORWARD >
  void
  step_decelerate_target_approach ( double & speed_n ) const;

  double
  stoppable_speed ( double accel_n ) const;

  private:
  // -- Utility

  double
  step_ticks_at_speed ( double speed_n ) const
  {
    return ( _length_per_step * _est.tick_frequency ) / speed_n;
  }

  void
  update_break_ranges ();

  private:
  // -- State
  const std::uint_fast32_t _axis_index = 0;
  State _pilot_state;

  // -- Axis information
  snc::device::statics::handle::Axis _axis_statics;
  snc::device::state::handle::Axis _axis_state;

  snc::est::Step_Splitter _step_splitter;

  // -- Est device
  struct
  {
    std::uint8_t stepper_index = 0;
    bool direction_reversed = false;
    std::uint_fast32_t steps_queue_capacity = 0;
    double tick_frequency = 0.0;
  } _est;

  struct
  {
    estepper::segment::Type forward;
    estepper::segment::Type backward;
  } _seg_type;

  struct
  {
    bool stepper_enabled = false;
    bool stepper_disabled = true;
  } _sequence;

  // -- Current speeds
  bool _speed_current_forward = false;
  double _speed_current = 0.0;
  double _speed_target = 0.0;
  double _accel_target = 0.0;

  // -- Axis limits
  Dyn_Limits _dyn_limits;
  double _length_per_step = 0.0;
  std::uint_fast64_t _steps_queue_ticks_min = 0;
  std::uint_fast64_t _steps_queue_ticks_max = 0;

  // -- Current steps
  std::int64_t _pos_current = 0;
  std::int64_t _pos_target = 0;
  std::int64_t _pos_target_break_lower = 0;
  std::int64_t _pos_target_break_upper = 0;
  // -- Border positions
  std::int64_t _pos_min = 0;
  std::int64_t _pos_max = 0;
  // -- Border breaking ranges
  std::int64_t _pos_min_break_max = 0;
  std::int64_t _pos_max_break_max = 0;
  std::int64_t _pos_min_break_target = 0;
  std::int64_t _pos_max_break_target = 0;
};

} // namespace snc::svs::fac::axis_move::ship::est
