/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_axis.hpp"
#include <snc/svp/office/factor.hpp>

namespace snc::svs::fac::base::office
{

const snc::device::handle::Info Service::device_info_fallback (
    snc::device::handle::make_info ( snc::device::statics::handle::make () ) );

Service::Service ( Provider_Factor & provider_n, sev::unicode::View log_name_n )
: Super ( provider_n, log_name_n )
, _machine_res ( &provider_n )
, _actor ( &provider_n )
, _device_info ( device_info_fallback )
{
}

Service::~Service () = default;

snc::svp::office::Factor &
Service::provider_factor ()
{
  return dynamic_cast< snc::svp::office::Factor & > ( provider_cell () );
}

void
Service::session_begin ()
{
  _session_is_good = true;
  _actor.connect_required ();
  _machine_res.connect_required ();
  _device_info = provider_factor ().tracker ().device_info ();
}

void
Service::session_end ()
{
  control_release ();
  _device_info = device_info_fallback;
  _actor.go_idle ();
  _actor.disconnect ();
  _machine_res.disconnect ();
  _session_is_good = false;
}

bool
Service::control_acquirable () const
{
  return Super::control_acquirable () && //
         _session_is_good &&             //
         _machine_res.resources_probe ();
}

void
Service::control_acquired ( Client_Abstract * client_n )
{
  Super::control_acquired ( client_n );
  _machine_res.resources_acquire ();
}

void
Service::control_released ( Client_Abstract * client_n )
{
  _machine_res.resources_release ();
  Super::control_released ( client_n );
}

} // namespace snc::svs::fac::base::office
