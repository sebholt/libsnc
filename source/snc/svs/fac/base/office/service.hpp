/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/svs/ctl/actor/office/client_actor.hpp>
#include <snc/svs/ctl/machine_res/office/client.hpp>

// -- Forward declaration
namespace snc::svp::office
{
class Factor;
}

namespace snc::svs::fac::base::office
{

/// @brief Factor axis service base class
///
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  public:
  using Provider_Factor = snc::svp::office::Factor;

  static const snc::device::handle::Info device_info_fallback;

  // -- Construction

  Service ( Provider_Factor & provider_n,
            sev::unicode::View log_name_n = "Service" );

  ~Service ();

  // -- Accessors: Device info

  const auto &
  device_info_handle () const
  {
    return _device_info;
  }

  // -- Accessors: Device statics

  const auto &
  device_statics () const
  {
    return *_device_info->statics ();
  }

  const auto &
  device_statics_handle () const
  {
    return _device_info->statics ();
  }

  // -- Accessors: Device state

  const auto &
  device_state () const
  {
    return _device_info->state ();
  }

  // -- Accessors: Machine resources client

  const auto &
  machine_res () const
  {
    return _machine_res;
  }

  auto &
  machine_res ()
  {
    return _machine_res;
  }

  // -- Accessors: Actor

  auto &
  actor ()
  {
    return _actor;
  }

  const auto &
  actor () const
  {
    return _actor;
  }

  Provider_Factor &
  provider_factor ();

  // -- Session

  virtual void
  session_begin ();

  virtual void
  session_end ();

  bool
  session_is_good () const
  {
    return _session_is_good;
  }

  // -- Client control interface

  bool
  control_acquirable () const override;

  protected:
  void
  control_acquired ( Client_Abstract * client_n ) override;

  void
  control_released ( Client_Abstract * client_n ) override;

  private:
  bool _session_is_good = false;
  snc::svs::ctl::machine_res::office::Client _machine_res;
  snc::svs::ctl::actor::office::Client_Actor _actor;
  snc::device::handle::Info _device_info;
};

} // namespace snc::svs::fac::base::office
