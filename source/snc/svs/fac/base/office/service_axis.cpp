/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_axis.hpp"
#include <snc/svp/office/factor.hpp>

namespace snc::svs::fac::base::office
{

Service_Axis::Service_Axis ( Provider_Factor & provider_n,
                             std::uint_fast32_t axis_index_n )
: Super ( provider_n )
, _axis_index ( axis_index_n )
, _machine_startup ( &provider_n )
{
  machine_res ().leases_ref ().register_axis ( axis_index_n );
}

Service_Axis::~Service_Axis () = default;

void
Service_Axis::session_begin ()
{
  Super::session_begin ();
  _axis_info = device_info_handle ()->axis ( axis_index () );
  _machine_startup.connect ();
}

void
Service_Axis::session_end ()
{
  _machine_startup.disconnect ();
  _axis_info.reset ();
  Super::session_end ();
}

bool
Service_Axis::control_acquirable () const
{
  return Super::control_acquirable () && //
         _axis_info &&                   //
         _machine_startup.driver_ready ();
}

} // namespace snc::svs::fac::base::office
