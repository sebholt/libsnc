/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "clerk.hpp"
#include <sev/assert.hpp>
#include <sev/math/numbers.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/controls/ship/i1/pilot_emb.hpp>
#include <snc/svs/fac/controls/ship/i1/pilot_est.hpp>
#include <cstdint>

namespace snc::svs::fac::controls::office
{

Clerk::Clerk ( const Clerk_Init & init_n )
: Super ( init_n, "Factor-Controls" )
, _service ( *this )
{
}

Clerk::~Clerk () = default;

void
Clerk::bridge_session_begin ()
{
  Super::bridge_session_begin ();
  _service.session_begin ();
}

void
Clerk::bridge_session_begin_factory ( Sailor_Picker & pick_n )
{
  // Create i1 pilots
  {
    auto & controls = _service.device_statics ().controls_i1 ();
    const std::size_t num = controls.size ();
    if ( tracker ().est_device () ) {
      for ( std::size_t ii = 0; ii != num; ++ii ) {
        pick_n (
            make_sailor_factory< snc::svs::fac::controls::ship::i1::Pilot_Est,
                                 std::uint_fast32_t > ( ii ) );
      }
    } else {
      for ( std::size_t ii = 0; ii != num; ++ii ) {
        pick_n (
            make_sailor_factory< snc::svs::fac::controls::ship::i1::Pilot_Emb,
                                 std::uint_fast32_t > ( ii ) );
      }
    }
  }
}

void
Clerk::bridge_session_begin_sailor ()
{
  Super::bridge_session_begin_sailor ();
  _service.session_begin_pilots ();
}

void
Clerk::bridge_session_abort ()
{
  Super::bridge_session_abort ();
  _service.session_end ();
}

void
Clerk::process ()
{
  _service.process ();
}

} // namespace snc::svs::fac::controls::office
