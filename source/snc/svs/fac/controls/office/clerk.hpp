/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/controls/office/service.hpp>

namespace snc::svs::fac::controls::office
{

// -- Forward declaration
class Client;

class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk ();

  // -- Bridge session

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_begin_sailor () override;

  void
  bridge_session_abort () override;

  // -- Central processing

  void
  process () override;

  private:
  // -- Service
  Service _service;
};

} // namespace snc::svs::fac::controls::office
