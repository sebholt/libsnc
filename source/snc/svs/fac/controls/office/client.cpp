/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/controls/office/service.hpp>

namespace snc::svs::fac::controls::office
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client ()
{
  Client::disconnect ();
}

bool
Client::session_is_good () const
{
  return is_connected () ? service ()->session_is_good () : false;
}

Client::Stats
Client::stats () const
{
  Stats res;
  if ( is_connected () ) {
    auto & services = service ()->services ();
    res.i1 = services.i1.size ();
  }
  return res;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::fac::controls::office
