/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/utility.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/fac/controls/office/clerk.hpp>
#include <snc/svs/fac/controls/ship/i1/pilot_emb.hpp>
#include <snc/svs/fac/controls/ship/i1/pilot_est.hpp>

namespace snc::svs::fac::controls::office::i1
{

Service::Service ( Provider & provider_n,
                   snc::svp::ship::Pilot * pilot_n,
                   std::uint_fast32_t control_index_n )
: Super ( provider_n, sev::string::cat ( "Service_I1_", control_index_n ) )
, _control_index ( control_index_n )
, _pilot ( pilot_n )
, _ship_io ( provider_n.ship_io () )
, _event_pool_set ( _ship_io.epool_tracker () )
, _event_pool_toggle ( _ship_io.epool_tracker () )
{
  ship_io ().allocate_for ( _pilot, _event_pool_set, 2 );
  ship_io ().allocate_for ( _pilot, _event_pool_toggle, 2 );
  _event_pool_set.set_capacity ( 8 );
  _event_pool_toggle.set_capacity ( 8 );
}

Service::~Service () = default;

void
Service::session_begin ()
{
  Super::session_begin ();
  _control_statics =
      device_info_handle ()->statics ()->controls_i1 ().get ( _control_index );
}

void
Service::session_end ()
{
  _control_statics.reset ();
  Super::session_end ();
}

void
Service::set_value ( bool value_n )
{
  auto * event = ship_io ().acquire_for ( _pilot, _event_pool_set );
  event->set_value ( value_n );
  ship_io ().submit ( event );
}

void
Service::toggle ()
{
  ship_io ().submit ( ship_io ().acquire_for ( _pilot, _event_pool_toggle ) );
}

void
Service::process ()
{
  if ( sev::change ( _control_was_available, control_acquirable () ) ) {
    signal_state ().send ();
  }
}

} // namespace snc::svs::fac::controls::office::i1
