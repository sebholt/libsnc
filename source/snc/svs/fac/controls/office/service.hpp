/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/office/service.hpp>
#include <snc/svs/fac/controls/office/i1/service.hpp>
#include <memory>

namespace snc::svs::fac::controls::office
{

// -- Forward declaration
class Clerk;
class Client;

/// @brief Service
///
class Service : public snc::svs::fac::base::office::Service
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Service;
  using Client = snc::svs::fac::controls::office::Client;
  using Provider = snc::svs::fac::controls::office::Clerk;

  public:
  struct Services
  {
    std::vector< std::unique_ptr< i1::Service > > i1;
  };

  public:
  // -- Construction and session

  Service ( Provider & provider_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Accessors

  const Services &
  services () const
  {
    return _services;
  }

  // -- Bridge session

  void
  session_begin_pilots ();

  void
  session_end () override;

  // -- Central processing

  void
  process ();

  private:
  // -- Services
  Services _services;
};

} // namespace snc::svs::fac::controls::office
