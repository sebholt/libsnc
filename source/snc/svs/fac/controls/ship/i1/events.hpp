/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/events.hpp>
#include <snc/svs/fac/controls/ship/i1/state.hpp>

namespace snc::svs::fac::controls::ship::i1::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t SET = 0;
  static constexpr std::uint_fast32_t TOGGLE = 1;
};

using In = snc::svp::ship::event::In;

class Set : public In
{
  public:
  static constexpr auto etype = Type::SET;

  Set ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }

  void
  reset ()
  {
    In::reset ();
    _value = false;
  }

  bool
  value () const
  {
    return _value;
  }

  void
  set_value ( bool value_n )
  {
    _value = value_n;
  }

  private:
  bool _value = false;
};

class Toggle : public In
{
  public:
  static constexpr auto etype = Type::TOGGLE;

  Toggle ( Pair pair_n )
  : In ( etype, pair_n )
  {
  }
};

} // namespace snc::svs::fac::controls::ship::i1::event::in

namespace snc::svs::fac::controls::ship::i1::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::ship::event::out;

/// @brief Pilot state event
using State = State_T< Type::STATE, State >;

} // namespace snc::svs::fac::controls::ship::i1::event::out
