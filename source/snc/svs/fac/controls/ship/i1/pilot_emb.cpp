/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_emb.hpp"
#include <sev/utility.hpp>
#include <snc/device/state/control/i1.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/msg/out/switch.hpp>

namespace snc::svs::fac::controls::ship::i1
{

Pilot_Emb::Pilot_Emb ( const Sailor_Init & init_n,
                       std::uint_fast32_t control_index_n )
: Super ( init_n, sev::string::cat ( "Controls_I1[", control_index_n, "]" ) )
, _control_index ( control_index_n )
, _event_pool_state ( office_io ().epool_tracker () )
{
  {
    auto ctl_stats = device_statics ().controls_i1 ().get ( _control_index );
    _emb.index = ctl_stats->emb ().control_index ();
    _emb.inverted = ctl_stats->emb ().inverted ();
  }

  _control_state = device_state ().control_i1_shared ( _control_index );

  office_io ().allocate ( _event_pool_state, 4 );
}

Pilot_Emb::~Pilot_Emb () = default;

void
Pilot_Emb::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::i1::event::in::Type::SET: {
    const auto & cevent =
        static_cast< const ship::i1::event::in::Set & > ( event_n );
    request_value ( cevent.value () );
  } break;
  case ship::i1::event::in::Type::TOGGLE:
    request_value ( !control_state ()->state () );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  office_io ().notification_set ( Office_Message::STATE );
}

void
Pilot_Emb::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( _state );
    office_io ().submit ( event );
  }
}

void
Pilot_Emb::request_value ( bool value_n )
{
  _request.is_valid = true;
  _request.value = value_n;

  // Update run state
  device_messages_register ( true );
  device_messages_request ();
  set_run_state ( State::Run_State::BUSY );
}

void
Pilot_Emb::set_run_state ( State::Run_State state_n )
{
  if ( sev::change ( _state.run_state_ref (), state_n ) ) {
    office_io ().notification_set ( Office_Message::STATE );
  }
}

void
Pilot_Emb::device_message ( Emb_Writer stream_n, bool state_n )
{
  auto & message = stream_n.emplace< snc::emb::msg::out::Switch > ();
  message.set_switch_index ( _emb.index );
  message.set_switch_state ( ( state_n != _emb.inverted ) );
}

void
Pilot_Emb::device_messages_acquire ( Emb_Writer stream_n )
{
  // Send device message
  if ( _request.is_valid ) {
    device_message ( stream_n, _request.value );
    _request.reset ();
  }

  // Change run state to idle and notify office immediately
  device_messages_register ( false );
  set_run_state ( State::Run_State::IDLE );
}

} // namespace snc::svs::fac::controls::ship::i1
