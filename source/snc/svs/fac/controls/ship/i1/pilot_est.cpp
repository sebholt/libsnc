/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_est.hpp"
#include <estepper/out/messages.hpp>
#include <sev/utility.hpp>
#include <snc/device/state/control/i1.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::svs::fac::controls::ship::i1
{

Pilot_Est::Pilot_Est ( const Sailor_Init & init_n,
                       std::uint_fast32_t control_index_n )
: Super ( init_n, sev::string::cat ( "Controls_I1[", control_index_n, "]" ) )
, _control_index ( control_index_n )
, _event_pool_state ( office_io ().epool_tracker () )
{
  {
    auto ctl_stats = device_statics ().controls_i1 ().get ( _control_index );
    _est.index = ctl_stats->est ().control_index ();
    _est.inverted = ctl_stats->est ().inverted ();
  }

  _control_state = device_state ().control_i1_shared ( _control_index );

  office_io ().allocate ( _event_pool_state, 4 );
}

Pilot_Est::~Pilot_Est () = default;

void
Pilot_Est::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::i1::event::in::Type::SET: {
    const auto & cevent =
        static_cast< const ship::i1::event::in::Set & > ( event_n );
    request_value ( cevent.value () );
  } break;
  case ship::i1::event::in::Type::TOGGLE:
    request_value ( !control_state ()->state () );
    break;
  default:
    DEBUG_ASSERT ( false );
    break;
  }

  office_io ().notification_set ( Office_Message::STATE );
}

void
Pilot_Est::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( _state );
    office_io ().submit ( event );
  }
}

void
Pilot_Est::request_value ( bool value_n )
{
  _request.is_valid = true;
  _request.value = value_n;

  // Update run state
  device_messages_register ( true );
  device_messages_request ();
  set_run_state ( State::Run_State::BUSY );
}

void
Pilot_Est::set_run_state ( State::Run_State state_n )
{
  if ( sev::change ( _state.run_state_ref (), state_n ) ) {
    office_io ().notification_set ( Office_Message::STATE );
  }
}

void
Pilot_Est::device_messages_generate ()
{
  // Send device message
  if ( _request.is_valid ) {
    {
      std::array< std::uint8_t, 4 > message;
      message[ 0 ] = message.size () - 1;
      message[ 1 ] = estepper::out::CONTROL_1;
      message[ 2 ] = _est.index;
      message[ 3 ] = ( _request.value != _est.inverted ) ? 0xff : 0x00;
      device_message_push ( message.data () );
    }
    _request.reset ();
  }

  // Change run state to idle and notify office immediately
  device_messages_register ( false );
  set_run_state ( State::Run_State::IDLE );
}

} // namespace snc::svs::fac::controls::ship::i1
