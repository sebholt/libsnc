/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

namespace snc::svs::fac::controls::ship::i1
{

/// @brief Pilot_State
///
class State
{
  public:
  // -- Types

  enum class Run_State
  {
    IDLE,
    BUSY
  };

  // -- Setup

  void
  reset ()
  {
    _run_state = Run_State::IDLE;
  }

  // -- Accessors

  const Run_State &
  run_state () const
  {
    return _run_state;
  }

  Run_State &
  run_state_ref ()
  {
    return _run_state;
  }

  bool
  is_idle () const
  {
    return _run_state == Run_State::IDLE;
  }

  bool
  is_busy () const
  {
    return _run_state != Run_State::IDLE;
  }

  private:
  // -- Attributes
  Run_State _run_state = Run_State::IDLE;
};

} // namespace snc::svs::fac::controls::ship::i1
