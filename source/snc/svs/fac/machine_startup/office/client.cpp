/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/machine_startup/office/service.hpp>

namespace snc::svs::fac::machine_startup::office
{

bool
Client::session_is_good () const
{
  return is_connected () ? service ()->state ().is_connected () : false;
}

const State &
Client::state () const
{
  if ( is_connected () ) {
    return service ()->state ();
  }

  static const State cstate;
  return cstate;
}

bool
Client::driver_ready () const
{
  return is_connected () ? service ()
                               ->state ()
                               .startup ()
                               .driver ()
                               .enable_duration_passed ()
                         : false;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::fac::machine_startup::office
