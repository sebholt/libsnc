/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/utility.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/fac/machine_startup/office/clerk.hpp>

namespace snc::svs::fac::machine_startup::office
{

Service::Service ( Provider & provider_n )
: Super ( provider_n )
, _controls ( &provider_n )
, _timer ( provider_n.cell_context ().timer_queue () )
{
  _timer.set_callback ( provider ().processing_request () );
}

Service::~Service () = default;

void
Service::init_connections ()
{
  _controls.connect_required ();
  _controls.session ().set_begin_callback (
      [ this ] () { controls_session_begin (); } );
  _controls.session ().set_end_callback (
      [ this ] () { controls_session_end (); } );
}

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  Super::session_begin ();

  // Update state
  _state.set_is_connected ( true );
  _state.set_is_disconnecting ( false );
  state_updated ();

  // Notify clients
  clients_session_begin ();
}

void
Service::session_abort ()
{
  // Update state
  _state.set_is_disconnecting ( true );
  state_updated ();
}

void
Service::session_end ()
{
  // Notify clients
  clients_session_end ();

  // Update state
  _state.reset ();
  state_updated ();

  Super::session_end ();
}

void
Service::process ()
{
  startup_next ();
}

void
Service::set_device_configured ()
{
  if ( !session_is_good () ) {
    return;
  }

  _state.startup_ref ().set_outputs_configured ( true );
  state_updated ();

  provider ().request_processing ();
}

void
Service::controls_session_begin ()
{
  _level.emplace ();

  // Resources
  auto & statics_bool = _controls.device_statics ().controls_i1 ();

  // Power
  {
    auto & level = _level->power;
    if ( auto scontrol = statics_bool.get_by_key ( "power" ) ) {
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Power control found.";
      }
      level.control.emplace ( &provider (), scontrol->index () );
      if ( !level.control->connect () ) {
        throw std::runtime_error ( "Control service not available." );
      }
      level.warm_up_duration = scontrol->warm_up_duration ();
    } else {
      level.warmed_up = true;
    }
  }

  // Driver
  {
    auto & level = _level->driver;
    if ( auto scontrol = statics_bool.get_by_key ( "driver" ) ) {
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Driver control found.";
      }
      level.control.emplace ( &provider (), scontrol->index () );
      if ( !level.control->connect () ) {
        throw std::runtime_error ( "Control service not available." );
      }
      level.warm_up_duration = scontrol->warm_up_duration ();
    } else {
      level.warmed_up = true;
    }
  }

  // Start in timer
  provider ().request_processing ();
}

void
Service::controls_session_end ()
{
  _level.reset ();
}

void
Service::startup_next ()
{
  if ( !_level || !_state.startup ().outputs_configured () ) {
    return;
  }

  auto tnow = std::chrono::steady_clock::now ();

  // Power
  if ( auto & level = _level->power; !level.warmed_up ) {
    auto & state = _state.startup_ref ().power ();
    if ( !level.enabled ) {
      level.enabled = true;
      level.warm_up_begin = tnow;
      DEBUG_ASSERT ( level.control->control_available () );
      level.control->control ().set_value ( true );
      state.set_enabled_now ();
      state_updated ();
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Power enabled.";
      }
    }
    auto duration_done = ( tnow - level.warm_up_begin );
    auto duration_remain = level.warm_up_duration - duration_done;
    if ( duration_remain.count () > 0 ) {
      _timer.start_single_point ( tnow + duration_remain );
      return;
    }

    level.warmed_up = true;
    state.set_enable_duration_passed ();
    state_updated ();
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Power warm up complete.";
    }
  }

  // Driver
  if ( auto & level = _level->driver; !level.warmed_up ) {
    auto & state = _state.startup_ref ().driver ();
    if ( !level.enabled ) {
      level.enabled = true;
      level.warm_up_begin = tnow;
      DEBUG_ASSERT ( level.control->control_available () );
      level.control->control ().set_value ( true );
      state.set_enabled_now ();
      state_updated ();
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Driver enabled.";
      }
    }
    auto duration_done = ( tnow - level.warm_up_begin );
    auto duration_remain = level.warm_up_duration - duration_done;
    if ( duration_remain.count () > 0 ) {
      _timer.start_single_point ( tnow + duration_remain );
      return;
    }

    level.warmed_up = true;
    state.set_enable_duration_passed ();
    state_updated ();
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Driver warm up complete.";
    }
  }
}

void
Service::state_updated ()
{
  signal_state ().send ();
  provider ().state_changed ();
}

} // namespace snc::svs::fac::machine_startup::office
