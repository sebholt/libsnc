/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/machine_startup/startup_state.hpp>
#include <cstdint>

namespace snc::svs::fac::machine_startup::office
{

/// @brief State
///
class State
{
  public:
  // -- Setup

  void
  reset ();

  // -- Connected

  bool
  is_connected () const
  {
    return _is_connected;
  }

  void
  set_is_connected ( bool flag_n )
  {
    _is_connected = flag_n;
  }

  // -- Disconnecting

  bool
  is_disconnecting () const
  {
    return _is_disconnecting;
  }

  void
  set_is_disconnecting ( bool flag_n )
  {
    _is_disconnecting = flag_n;
  }

  // -- Startup

  const Startup_State &
  startup () const
  {
    return _startup;
  }

  Startup_State &
  startup_ref ()
  {
    return _startup;
  }

  // -- Comparison operators

  bool
  operator== ( const State & state_n ) const;

  bool
  operator!= ( const State & state_n ) const;

  private:
  // -- Attributes
  bool _is_connected = false;
  bool _is_disconnecting = false;
  Startup_State _startup;
};

} // namespace snc::svs::fac::machine_startup::office
