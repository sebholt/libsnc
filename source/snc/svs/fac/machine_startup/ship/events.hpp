/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/ship/events.hpp>
#include <cstdint>

namespace snc::svs::fac::machine_startup::ship::event::in
{

} // namespace snc::svs::fac::machine_startup::ship::event::in

namespace snc::svs::fac::machine_startup::ship::event::out
{

struct Type
{
  static constexpr std::uint_fast32_t DEVICE_CONFIGURED = 0;
};

using Out = snc::svp::ship::event::Out;

class Device_Configured : public Out
{
  public:
  static constexpr auto etype = Type::DEVICE_CONFIGURED;

  Device_Configured ( Pair pair_n )
  : Out ( etype, pair_n )
  {
  }
};

} // namespace snc::svs::fac::machine_startup::ship::event::out
