/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_emb.hpp"
#include <sev/utility.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/config/config.hpp>
#include <snc/emb/msg/out/all.hpp>

namespace snc::svs::fac::machine_startup::ship
{

Pilot_Emb::Pilot_Emb ( const Sailor_Init & init_n )
: Super ( init_n, "Machine_Startup" )
, _event_pool_device_configured ( office_io ().epool_tracker () )
{
  _startup_timer.connect ( cell_context ().timer_queue () );
  _startup_timer.set_callback ( [ this ] () {
    this->request_device_message ( Message_Send_Device::STARTUP );
  } );

  _heartbeat_timer.connect ( cell_context ().timer_queue () );
  _heartbeat_timer.set_duration ( std::chrono::milliseconds ( 250 ) );
  _heartbeat_timer.set_callback ( [ this ] () {
    this->request_device_message ( Message_Send_Device::HEARTBEAT );
  } );

  office_io ().allocate ( _event_pool_device_configured, 1 );
}

Pilot_Emb::~Pilot_Emb () = default;

void
Pilot_Emb::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Initialize device message generation
  _startup_timer.start_single_point_now ();
  _heartbeat_timer.start_periodical_now ();
  device_messages_register ();
}

void
Pilot_Emb::cell_session_abort ()
{
  Super::cell_session_abort ();
  _heartbeat_timer.stop ();
  _startup_timer.stop ();
}

void
Pilot_Emb::office_notify ()
{
  if ( auto * event =
           office_io ().notify_get ( Office_Message::DEVICE_CONFIGURED,
                                     _event_pool_device_configured ) ) {
    office_io ().submit ( event );
  }
}

void
Pilot_Emb::request_device_message ( std::uint8_t message_n )
{
  _message_send_device.set ( message_n );
  device_messages_request ();
}

void
Pilot_Emb::device_messages_acquire ( Emb_Writer stream_n )
{
  auto & mreg = _message_send_device;
  if ( mreg.is_empty () ) {
    return;
  }
  if ( mreg.test_any_unset ( Message_Send_Device::STARTUP ) ) {
    device_messages_acquire_startup ( stream_n );
  }
  if ( mreg.test_any_unset ( Message_Send_Device::HEARTBEAT ) ) {
    stream_n.emplace< snc::emb::msg::out::Heartbeat > ();
  }
}

void
Pilot_Emb::device_messages_acquire_startup ( Emb_Writer stream_n )
{
  if ( sev::change ( _device_configured, true ) ) {
    device_messages_acquire_config_outputs ( stream_n );
    // Notify
    office_io ().notification_set ( Office_Message::DEVICE_CONFIGURED );
  }
}

void
Pilot_Emb::device_messages_acquire_config_outputs ( Emb_Writer stream_n )
{
  const auto & emb_config = *device_statics ().emb_device_config ();

  // Configure frequency generators
  for ( const auto & fg_cfg : emb_config.freq_gens () ) {
    auto & msg = stream_n.emplace< snc::emb::msg::out::Generator_Frequency > ();
    msg.set_generator_index ( fg_cfg.generator_index () );
    msg.set_pulse_usecs_total ( fg_cfg.pulse_usecs_total () );
    msg.set_pulse_usecs_high ( fg_cfg.pulse_usecs_high () );
  }

  // Configure outputs
  for ( const auto & out_cfg : emb_config.outputs () ) {
    auto & msg = stream_n.emplace< snc::emb::msg::out::Output_Config > ();
    msg.set_output_index ( out_cfg.index () );
    msg.set_output_type ( out_cfg.type () );

    switch ( out_cfg.type () ) {
    case snc::emb::type::Output::UNUSED:
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending output["
             << static_cast< std::size_t > ( out_cfg.index () )
             << "] configuration: Type: UNUSED";
      }
      break;

    case snc::emb::type::Output::SWITCH_PLAIN:
      msg.set_stepper_switch_index ( out_cfg.control_index () );
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending output["
             << static_cast< std::size_t > ( out_cfg.index () )
             << "] configuration: control index: "
             << static_cast< std::size_t > ( out_cfg.control_index () )
             << " Type: SWITCH_PLAIN";
      }
      break;

    case snc::emb::type::Output::SWITCH_FREQUENCY:
      msg.set_stepper_switch_index ( out_cfg.control_index () );
      msg.set_freq_gen_index ( out_cfg.freq_gen_index () );
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending output["
             << static_cast< std::size_t > ( out_cfg.index () )
             << "] configuration: control index: "
             << static_cast< std::size_t > ( out_cfg.control_index () )
             << " Type: SWITCH_FREQUENCY fg: " << out_cfg.freq_gen_index ();
      }
      break;

    case snc::emb::type::Output::STEPPER_DIRECTION:
      msg.set_stepper_switch_index ( out_cfg.stepper_index () );
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending output["
             << static_cast< std::size_t > ( out_cfg.index () )
             << "] configuration: stepper index: "
             << static_cast< std::size_t > ( out_cfg.stepper_index () )
             << " Type: STEPPER_DIRECTION";
      }
      break;

    case snc::emb::type::Output::STEPPER_CLOCK:
      msg.set_stepper_switch_index ( out_cfg.stepper_index () );
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending output["
             << static_cast< std::size_t > ( out_cfg.index () )
             << "] configuration: stepper index: "
             << static_cast< std::size_t > ( out_cfg.stepper_index () )
             << " Type: STEPPER_CLOCK";
      }
      break;
    }
  }
}

} // namespace snc::svs::fac::machine_startup::ship
