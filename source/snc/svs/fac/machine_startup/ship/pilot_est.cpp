/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_est.hpp"
#include <estepper/out/messages.hpp>
#include <sev/mem/serial.hpp>
#include <sev/utility.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/control/i16.hpp>
#include <snc/device/statics/control/i32.hpp>
#include <snc/device/statics/control/i64.hpp>
#include <snc/device/statics/control/i8.hpp>
#include <snc/device/statics/statics.hpp>

namespace snc::svs::fac::machine_startup::ship
{

Pilot_Est::Pilot_Est ( const Sailor_Init & init_n )
: Super ( init_n, "Machine_Startup" )
, _event_pool_device_configured ( office_io ().epool_tracker () )
{
  _startup_timer.connect ( cell_context ().timer_queue () );
  _startup_timer.set_callback ( [ this ] () {
    this->request_device_message ( Message_Send_Device::STARTUP );
  } );

  _ping_timer.connect ( cell_context ().timer_queue () );
  _ping_timer.set_duration ( std::chrono::milliseconds ( 200 ) );
  _ping_timer.set_callback ( [ this ] () {
    this->request_device_message ( Message_Send_Device::PING );
  } );

  office_io ().allocate ( _event_pool_device_configured, 1 );
}

Pilot_Est::~Pilot_Est () = default;

void
Pilot_Est::cell_session_begin ()
{
  Super::cell_session_begin ();

  // Initialize device message generation
  _startup_timer.start_single_point_now ();
  _ping_timer.start_periodical_now ();
  device_messages_register ();
  // Start with a ping
  request_device_message ( Message_Send_Device::PING );
}

void
Pilot_Est::cell_session_abort ()
{
  Super::cell_session_abort ();
  _ping_timer.stop ();
  _startup_timer.stop ();
}

void
Pilot_Est::office_notify ()
{
  if ( auto * event =
           office_io ().notify_get ( Office_Message::DEVICE_CONFIGURED,
                                     _event_pool_device_configured ) ) {
    office_io ().submit ( event );
  }
}

void
Pilot_Est::request_device_message ( std::uint8_t message_n )
{
  _message_send_device.set ( message_n );
  device_messages_request ();
}

void
Pilot_Est::device_messages_generate ()
{
  auto & mreg = _message_send_device;
  if ( mreg.is_empty () ) {
    return;
  }
  if ( mreg.test_any_unset ( Message_Send_Device::PING ) ) {
    std::array< std::uint8_t, 2 > message;
    message[ 0 ] = message.size () - 1;
    message[ 1 ] = estepper::out::PING;
    device_message_push ( message.data () );
  }
  if ( mreg.test_any_unset ( Message_Send_Device::STARTUP ) ) {
    device_messages_generate_startup ();
  }
}

void
Pilot_Est::device_messages_generate_startup ()
{
  if ( !sev::change ( _device_configured, true ) ) {
    return;
  }

  // Control 1 init values
  for ( const auto & item : device_statics ().controls_i1 ().items () ) {
    if ( item->init_value () != item->init_state () ) {
      std::uint8_t const est_value =
          ( item->init_value () != item->est ().inverted () ) ? 0xff : 0x00;
      {
        std::array< std::uint8_t, 4 > message;
        message[ 0 ] = message.size () - 1;
        message[ 1 ] = estepper::out::CONTROL_1;
        message[ 2 ] = item->est ().control_index ();
        message[ 3 ] = est_value;
        device_message_push ( message.data () );
      }
      // Log
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending Control_i1[" << item->index ()
             << "] init value. est-control: " << item->est ().control_index ()
             << " est-value: " << std::uint_fast32_t ( est_value );
      }
    }
  }
  // Control 8 init values
  for ( const auto & item : device_statics ().controls_i8 ().items () ) {
    if ( item->init_value () != item->init_state () ) {
      {
        std::array< std::uint8_t, 4 > message;
        message[ 0 ] = message.size () - 1;
        message[ 1 ] = estepper::out::CONTROL_8;
        message[ 2 ] = item->est ().control_index ();
        message[ 3 ] = item->init_value ();
        device_message_push ( message.data () );
      }
      // Log
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending Control_i8[" << item->index ()
             << "] init value. est-control: " << item->est ().control_index ()
             << " est-value: " << std::uint_fast32_t ( item->init_value () );
      }
    }
  }
  // Control 16 init values
  for ( const auto & item : device_statics ().controls_i16 ().items () ) {
    if ( item->init_value () != item->init_state () ) {
      {
        std::array< std::uint8_t, 5 > message;
        message[ 0 ] = message.size () - 1;
        message[ 1 ] = estepper::out::CONTROL_16;
        message[ 2 ] = item->est ().control_index ();
        sev::mem::set_8le_uint16 ( &message[ 3 ], item->init_value () );
        device_message_push ( message.data () );
      }
      // Log
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending Control_i16[" << item->index ()
             << "] init value. est-control: " << item->est ().control_index ()
             << " est-value: " << std::uint_fast32_t ( item->init_value () );
      }
    }
  }
  // Control 32 init values
  for ( const auto & item : device_statics ().controls_i32 ().items () ) {
    if ( item->init_value () != item->init_state () ) {
      {
        std::array< std::uint8_t, 7 > message;
        message[ 0 ] = message.size () - 1;
        message[ 1 ] = estepper::out::CONTROL_32;
        message[ 2 ] = item->est ().control_index ();
        sev::mem::set_8le_uint32 ( &message[ 3 ], item->init_value () );
        device_message_push ( message.data () );
      }
      // Log
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending Control_i32[" << item->index ()
             << "] init value. est-control: " << item->est ().control_index ()
             << " est-value: " << item->init_value ();
      }
    }
  }
  // Control 64 init values
  for ( const auto & item : device_statics ().controls_i64 ().items () ) {
    if ( item->init_value () != item->init_state () ) {
      {
        std::array< std::uint8_t, 11 > message;
        message[ 0 ] = message.size () - 1;
        message[ 1 ] = estepper::out::CONTROL_64;
        message[ 2 ] = item->est ().control_index ();
        sev::mem::set_8le_uint64 ( &message[ 3 ], item->init_value () );
        device_message_push ( message.data () );
      }
      // Log
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
        logo << "Sending Control_i64[" << item->index ()
             << "] init value. est-control: " << item->est ().control_index ()
             << " est-value: " << item->init_value ();
      }
    }
  }

  // Notify
  office_io ().notification_set ( Office_Message::DEVICE_CONFIGURED );
}

} // namespace snc::svs::fac::machine_startup::ship
