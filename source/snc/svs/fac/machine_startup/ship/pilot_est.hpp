/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/mem/flags.hpp>
#include <snc/svp/ship/pilot_est.hpp>
#include <snc/svs/fac/machine_startup/ship/events.hpp>
#include <snc/svs/fac/machine_startup/ship/state.hpp>

namespace snc::svs::fac::machine_startup::ship
{

class Pilot_Est : public snc::svp::ship::Pilot_Est
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Est;

  struct Message_Send_Device
  {
    static constexpr std::uint8_t STARTUP = ( 1 << 0 );
    static constexpr std::uint8_t PING = ( 1 << 1 );
  };

  struct Office_Message
  {
    static constexpr std::uint8_t DEVICE_CONFIGURED = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot_Est ( const Sailor_Init & init_n );

  ~Pilot_Est ();

  // -- Bridge session interface

  void
  cell_session_begin () override;

  void
  cell_session_abort () override;

  // -- Office event processing

  void
  office_notify () override;

  // -- Embedded device message generation

  void
  device_messages_generate () override;

  private:
  // -- Utility

  void
  request_device_message ( std::uint8_t message_n );

  void
  device_messages_generate_startup ();

  private:
  // -- State
  bool _device_configured = false;
  State _pilot_state;

  // -- Requests
  sev::mem::Flags_Fast8 _message_send_device;

  // -- Startup
  sev::event::timer_queue::chrono::Client_Steady _startup_timer;

  // -- Ping
  sev::event::timer_queue::chrono::Client_Steady _ping_timer;

  // -- Office event io
  sev::event::Pool< ship::event::out::Device_Configured >
      _event_pool_device_configured;
};

} // namespace snc::svs::fac::machine_startup::ship
