/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <snc/svs/fac/mpath/dash/events.hpp>
#include <snc/svs/fac/mpath/office/state.hpp>

namespace snc::svs::fac::mpath::dash
{

template < std::size_t DIM >
class Panel_N : public snc::svp::dash::Panel
{
  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Construction

  Panel_N ( const Panel_Init & init_n );

  ~Panel_N ();

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Interface: state

  const snc::svs::fac::mpath::office::State &
  state () const
  {
    return _state;
  }

  private:
  // -- State
  snc::svs::fac::mpath::office::State _state;
};

} // namespace snc::svs::fac::mpath::dash
