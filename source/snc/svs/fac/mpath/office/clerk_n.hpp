/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/mpath/dash/events.hpp>
#include <snc/svs/fac/mpath/office/service_n.hpp>
#include <snc/svs/fac/mpath/office/state.hpp>
#include <snc/svs/fac/mpath/ship/events.hpp>
#include <memory>

namespace snc::svs::fac::mpath::office
{

// -- Forward declaration
template < std::size_t DIM >
class Service_N;

template < std::size_t DIM >
class Clerk_N : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;
  using Service = snc::svs::fac::mpath::office::Service_N< DIM >;

  public:
  struct Dash_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  // -- Construction

  Clerk_N ( const Super::Clerk_Init & init_n );

  ~Clerk_N ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Dash event processing

  void
  dash_notify () override;

  // -- Bridge session interface

  void
  bridge_session_begin () override;

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  void
  bridge_session_begin_sailor () override;

  void
  bridge_session_abort () override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_state ( Bridge_Event & event_n );

  // -- Central processing

  void
  process () override;

  private:
  // -- Service interface
  friend Service;

  void
  bridge_notify_break_hard ();

  void
  bridge_notify_route_begin (
      const std::shared_ptr< snc::mpath_feed::Job_D< DIM > > & mpath_job_n );

  private:
  // -- Resources
  Service _service;

  // -- Bridge event io
  sev::event::Pool< ship::event::in::Job< DIM > > _event_pool_bridge_job;
  sev::event::Pool< ship::event::in::Break > _event_pool_bridge_break;

  // -- Dash event io
  sev::event::Pool< dash::event::in::State > _event_pool_dash_state;
};

} // namespace snc::svs::fac::mpath::office
