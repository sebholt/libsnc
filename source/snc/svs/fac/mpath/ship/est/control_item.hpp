/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/mpath/ship/est/fader.hpp>
#include <cstdint>
#include <variant>

namespace snc::svs::fac::mpath::ship::est
{

using Control_Item = std::variant< std::uint_fast64_t, Fader >;

} // namespace snc::svs::fac::mpath::ship::est
