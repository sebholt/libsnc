/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/value64.hpp>
#include <cstdint>

namespace snc::svs::fac::mpath::ship::est
{

/// @brief Control fader
///
class Fader
{
  public:
  // -- Construction

  Fader () = default;

  Fader ( std::uint8_t bits_n, std::uint8_t index_n, sev::mem::Value64 value_n )
  : _bits ( bits_n )
  , _index ( index_n )
  , _value ( value_n )
  {
  }

  ~Fader () = default;

  // -- Fader bits

  std::uint8_t
  bits () const
  {
    return _bits;
  }

  void
  set_bits ( std::uint8_t value_n )
  {
    _bits = value_n;
  }

  // -- Fader index

  std::uint8_t
  index () const
  {
    return _index;
  }

  void
  set_index ( std::uint8_t value_n )
  {
    _index = value_n;
  }

  // -- Fader value

  const sev::mem::Value64 &
  value () const
  {
    return _value;
  }

  sev::mem::Value64 &
  value_ref ()
  {
    return _value;
  }

  private:
  // -- Attributes
  std::uint8_t _bits = 0;
  std::uint8_t _index = 0;
  sev::mem::Value64 _value = sev::mem::Value64 ( std::uint64_t ( 0 ) );
};

} // namespace snc::svs::fac::mpath::ship::est
