/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "muxer_config.hpp"

namespace snc::svs::fac::mpath::ship::est
{

Muxer_Config::Muxer_Config () = default;

Muxer_Config::~Muxer_Config () = default;

} // namespace snc::svs::fac::mpath::ship::est
