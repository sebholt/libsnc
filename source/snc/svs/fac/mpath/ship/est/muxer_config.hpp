/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/math/exponents.hpp>

namespace snc::svs::fac::mpath::ship::est
{

/// @brief Multiplexing configuration
///
class Muxer_Config
{
  public:
  // -- Construction

  Muxer_Config ();

  ~Muxer_Config ();

  // -- Steps queue microseconds min

  double
  steps_queue_usecs_min () const
  {
    return _steps_queue_usecs_min;
  }

  void
  set_steps_queue_usecs_min ( double value_n )
  {
    _steps_queue_usecs_min = value_n;
  }

  // -- Steps queue microseconds max

  double
  steps_queue_usecs_max () const
  {
    return _steps_queue_usecs_max;
  }

  void
  set_steps_queue_usecs_max ( double value_n )
  {
    _steps_queue_usecs_max = value_n;
  }

  // -- Buffer feed

  double
  buffer_feed_offset_usecs () const
  {
    return _buffer_feed_offset_usecs;
  }

  void
  set_buffer_feed_offset_usecs ( double value_n )
  {
    _buffer_feed_offset_usecs = value_n;
  }

  /// @brief Feed the buffer until there are this many usecs in the buffer or
  /// the steps queue
  double
  buffer_feed_usecs_limit () const
  {
    return _steps_queue_usecs_max + _buffer_feed_offset_usecs;
  }

  private:
  // -- Attributes
  double _steps_queue_usecs_min = 30 * sev::math::kilo_ull;
  double _steps_queue_usecs_max = 50 * sev::math::kilo_ull;
  double _buffer_feed_offset_usecs = 10 * sev::math::kilo_ull;
};

} // namespace snc::svs::fac::mpath::ship::est
