/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "pilot_n.hpp"
#include <estepper/out/messages.hpp>
#include <sev/math/numbers.hpp>
#include <snc/device/state/state.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/control/i16.hpp>
#include <snc/device/statics/control/i32.hpp>
#include <snc/device/statics/control/i64.hpp>
#include <snc/device/statics/control/i8.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/device/statics/stepper_controls.hpp>
#include <snc/est/stepper_data_composer.hpp>
#include <snc/mpath/elem/fader.hpp>
#include <snc/mpath/elem/sleep.hpp>
#include <snc/mpath_feed/cargo/cargos.hpp>
#include <snc/mpath_feed/reader/station_constructor_d.hpp>
#include <snc/mpath_feed/speeder/station_constructor_d.hpp>
#include <snc/mpath_feed/stepper/station_constructor_d.hpp>
#include <algorithm>
#include <cmath>

namespace
{

template < std::size_t DIM >
std::vector< snc::mpath_feed::Station_Contructor_Handle >
make_stations ( const snc::device::statics::handle::Statics & device_statics_n,
                const snc::mpath_feed::Axes_Mapping_D< DIM > & axes_mapping_n )
{
  // Station constructors
  auto con_reader = std::make_shared<
      snc::mpath_feed::reader::Station_Constructor_D< DIM > > ();
  auto con_stepper = std::make_shared<
      snc::mpath_feed::stepper::Station_Constructor_D< DIM > > ();
  auto con_speeder = std::make_shared<
      snc::mpath_feed::speeder::Station_Constructor_D< DIM > > ();
  {
    // Axes step lengths
    sev::lag::Vector< double, DIM > axis_step_length;
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      const auto & dsaxis =
          *device_statics_n->axes ().get ( axes_mapping_n.map ( ii ) );
      axis_step_length[ ii ] = dsaxis.geo ().length_per_step ();
    }
    con_stepper->set_step_lengths ( axis_step_length );
    con_speeder->set_step_lengths ( axis_step_length );

    // Axes limits
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      const auto & dsaxis =
          *device_statics_n->axes ().get ( axes_mapping_n.map ( ii ) );
      auto & alimits ( con_speeder->axes_limits_ref ()[ ii ] );
      alimits.speed_reversible = dsaxis.geo ().speed_reversible ();
      alimits.speed_max = dsaxis.geo ().speed_max ();
      alimits.accel_max = dsaxis.geo ().accel_max ();
    }
  }

  std::vector< snc::mpath_feed::Station_Contructor_Handle > cons;
  cons.push_back ( std::move ( con_reader ) );
  cons.push_back ( std::move ( con_stepper ) );
  cons.push_back ( std::move ( con_speeder ) );
  return cons;
}

} // namespace

namespace snc::svs::fac::mpath::ship::est
{

template < std::size_t DIM >
Pilot_N< DIM >::Pilot_N (
    const Sailor_Init & init_n,
    const snc::mpath_feed::Axes_Mapping_D< DIM > & axes_mapping_n )
: Super ( init_n, "MPath" )
, _axes_mapping ( axes_mapping_n )
, _feed ( log (),
          cell_context ().thread_tracker (),
          make_stations ( device_statics_handle (), axes_mapping_n ) )
, _event_pool_state ( office_io ().epool_tracker () )
{
  _stepper.controls.init ( _stepper.muxer_config,
                           device_statics ().steppers_controls ().get ( 0 ),
                           device_state ().stepper_controls_shared ( 0 ) );
  for ( std::size_t ii = 0; ii != DIM; ++ii ) {
    auto axis_index = _axes_mapping.map ( ii );
    _stepper.axes[ ii ].init ( _stepper.muxer_config,
                               device_statics ().axes ().get ( axis_index ),
                               device_state ().axis_shared ( axis_index ) );
  }

  office_io ().allocate ( _event_pool_state, 4 );
}

template < std::size_t DIM >
Pilot_N< DIM >::~Pilot_N ()
{
}

template < std::size_t DIM >
void
Pilot_N< DIM >::cell_session_begin ()
{
  Super::cell_session_begin ();
  _feed.session_begin ( processing_request_async () );
}

template < std::size_t DIM >
void
Pilot_N< DIM >::cell_session_end ()
{
  _feed.session_end ();
  Super::cell_session_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::cell_session_abort ()
{
  Super::cell_session_abort ();
  mpath_feed_break ( true );
}

template < std::size_t DIM >
bool
Pilot_N< DIM >::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end ();
  // && !_mpath_feed.is_routing ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_event ( Office_Event & event_n )
{
  switch ( event_n.type () ) {
  case ship::event::in::Type::JOB:
    office_event_job ( event_n );
    break;
  case ship::event::in::Type::BREAK:
    office_event_break ( event_n );
    break;
  default:
    break;
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_event_job ( Office_Event & event_n )
{
  auto & cevent =
      static_cast< const ship::event::in::Job< DIM > & > ( event_n );

  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route begin: Job name: " << cevent.mpath_job ()->name ();
  }

  // Reset state
  _state.set_stream_finished ( false );
  _mpath_job = cevent.mpath_job ();

  // Acquire current position
  {
    sev::lag::Vector< double, DIM > pos_cur ( sev::lag::init::zero );
    for ( std::size_t ii = 0; ii != DIM; ++ii ) {
      pos_cur[ ii ] =
          _stepper.axes[ ii ].state ()->kinematics_latest ().position ();
    }
    _mpath_job->set_begin_position ( pos_cur );
  }

  // Start feed
  _is_routing = true;
  _stream_is_finished = false;
  _stepper.enabled = false;
  _stepper.disabled = false;

  _feed.job_begin ( _mpath_job );

  device_messages_register ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_event_break ( Office_Event & event_n [[maybe_unused]] )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Incoming break event.";
  }
  mpath_feed_break ( false );
}

template < std::size_t DIM >
void
Pilot_N< DIM >::office_notify ()
{
  if ( auto * event = office_io ().notify_get ( Office_Message::STATE,
                                                _event_pool_state ) ) {
    event->set_office_event_count ( office_io ().stats_in_fetch_clear () );
    event->set_state ( _state );
    office_io ().submit ( event );
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::process_async ()
{
  _feed.process_events ();
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::process ()
{
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_generate ()
{
  if ( !_stepper.disabled ) {
    device_messages_generate_feed_buffers ();
    device_messages_generate_send_controls ();
    for ( auto & stepper_n : _stepper.axes ) {
      device_messages_generate_send_motor ( stepper_n );
    }
    device_messages_generate_enable_disable ();
  }
  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_generate_feed_buffers ()
{
  auto buffers_satiated = [ this ] () -> bool {
    if ( !_stepper.controls.buffer_satiated () ) {
      return false;
    }
    for ( const auto & axis : _stepper.axes ) {
      if ( !axis.buffer_satiated () ) {
        return false;
      }
    }
    return true;
  };

  while ( !buffers_satiated () ) {
    if ( _feed_steps.cargo == nullptr ) {
      if ( !device_messages_generate_feed_buffer_new () ) {
        break;
      }
    } else {
      device_messages_generate_feed_buffer_steps ();
    }
  }
}

template < std::size_t DIM >
bool
Pilot_N< DIM >::device_messages_generate_feed_buffer_new ()
{
  auto & con = _feed.cargo_connection_in ();
  while ( !con.is_empty () ) {
    bool cargo_accepted = true;
    auto * cargo =
        static_cast< snc::mpath_feed::cargo::Event * > ( con.front () );
    // Evaluate the cargo type
    switch ( cargo->cargo_type () ) {

    case snc::mpath_feed::cargo::Cargo_Type::CSTEPS: {
      // We need this cargo for a while
      using Cargo_Type = snc::mpath_feed::cargo::CSteps< DIM >;
      auto & cargo_csteps = static_cast< Cargo_Type const & > ( *cargo );
      _feed_steps.cargo = cargo;
      _feed_steps.step_next = cargo_csteps.begin ();
      _feed_steps.step_end = cargo_csteps.end ();
    } break;

    case snc::mpath_feed::cargo::Cargo_Type::ELEMENT: {
      using Cargo_Type = snc::mpath_feed::cargo::Element_D< DIM >;
      auto & cargo_elem = static_cast< Cargo_Type const & > ( *cargo );
      snc::mpath::Element const & mpath_elem = cargo_elem.ebuffer ().elem ();
      // Evaluate mpath element type
      switch ( mpath_elem.elem_type () ) {

      case snc::mpath::elem::Type::SLEEP: {
        auto & sleep_elem =
            static_cast< snc::mpath::elem::Sleep const & > ( mpath_elem );
        double const sleep_usecs_d = sleep_elem.usecs ();
        // Push sleep duration to all steppers
        _stepper.controls.push_sleep_usecs ( sleep_usecs_d );
        for ( auto & axis : _stepper.axes ) {
          axis.push_sleep_usecs ( sleep_usecs_d );
        }
      } break;

      case snc::mpath::elem::Type::FADER: {
        auto & fader_elem =
            static_cast< snc::mpath::elem::Fader const & > ( mpath_elem );
        // Push fader to controls buffer
        _stepper.controls.push_fader ( fader_elem );
      } break;

      default:
        // Skip
        cargo_accepted = false;
        break;
      }
    } break;

    default:
      // Skip
      cargo_accepted = false;
      break;
    }

    // Check if we used this cargo
    if ( cargo_accepted ) {
      // Return cargo immediately if we don't need it anymore
      if ( _feed_steps.cargo == nullptr ) {
        con.pop_not_empty ();
        cargo->release_to_cargo_pool ();
      }
      // We sucessfully processed one cargo event
      return true;
    }

    // Skip this unused cargo
    con.pop_not_empty ();
    cargo->release_to_cargo_pool ();
  }

  // No (accepted) cargo event processed
  return false;
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_generate_feed_buffer_steps ()
{
  // TODO: Implement
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_generate_send_controls ()
{
  snc::est::Stepper_Controls_Composer stepper_data_composer (
      _stepper.controls.est ().stepper_index );

  // Compute initial ticks capacity
  std::uint_fast64_t ticks_capacity = 0;
  {
    std::uint_fast64_t queue_ticks =
        _stepper.controls.state ()->steps_queue_ticks ();
    // Only feed when we're below the minimum to encourage multi step
    // feeds
    if ( queue_ticks < _stepper.controls.steps_queue_ticks_min () ) {
      ticks_capacity = _stepper.controls.steps_queue_ticks_max ();
      sev::math::subtract_minimum_zero ( ticks_capacity, queue_ticks );
    }
  }

  while ( true ) {
    // Compute initial steps capacity
    std::uint_fast64_t steps_capacity =
        _stepper.controls.est ().steps_queue_capacity;
    sev::math::subtract_minimum_zero (
        steps_capacity, _stepper.controls.state ()->steps_queue_length () );
    sev::math::assign_smaller ( steps_capacity,
                                stepper_data_composer.size_free () );

    while ( _stepper.controls.item_available () ) {
      if ( _stepper.controls.item_is_fader () ) {
        //
        // Fader item
        //
        bool fader_consumed = true;
        Fader const & fader = _stepper.controls.item_fader ();
        switch ( fader.bits () ) {
        case 1:
          if ( steps_capacity >= stepper_data_composer.i1_words () ) {
            auto & controls_statics = device_statics ().controls_i1 ();
            if ( fader.index () < controls_statics.size () ) {
              auto & statics_est =
                  controls_statics.get ( fader.index () )->est ();
              stepper_data_composer.push_i1 (
                  statics_est.control_index (),
                  ( fader.value ().get_uint8 () != 0 ) !=
                      statics_est.inverted () );
              steps_capacity -= stepper_data_composer.i1_words ();
            } else {
              // Ignore fader with invalid fader index
            }
          } else {
            fader_consumed = false;
          }
          break;
        case 8:
          if ( steps_capacity >= stepper_data_composer.i8_words () ) {
            auto & controls_statics = device_statics ().controls_i8 ();
            if ( fader.index () < controls_statics.size () ) {
              auto & statics_est =
                  controls_statics.get ( fader.index () )->est ();
              stepper_data_composer.push_i8 ( statics_est.control_index (),
                                              fader.value ().get_uint8 () );
              steps_capacity -= stepper_data_composer.i8_words ();
            } else {
              // Ignore fader with invalid fader index
            }
          } else {
            fader_consumed = false;
          }
          break;
        case 16:
          if ( steps_capacity >= stepper_data_composer.i16_words () ) {
            auto & controls_statics = device_statics ().controls_i16 ();
            if ( fader.index () < controls_statics.size () ) {
              auto & statics_est =
                  controls_statics.get ( fader.index () )->est ();
              stepper_data_composer.push_i16 ( statics_est.control_index (),
                                               fader.value ().get_uint16 () );
              steps_capacity -= stepper_data_composer.i16_words ();
            } else {
              // Ignore fader with invalid fader index
            }
          } else {
            fader_consumed = false;
          }
          break;
        case 32:
          if ( steps_capacity >= stepper_data_composer.i32_words () ) {
            auto & controls_statics = device_statics ().controls_i32 ();
            if ( fader.index () < controls_statics.size () ) {
              auto & statics_est =
                  controls_statics.get ( fader.index () )->est ();
              stepper_data_composer.push_i32 ( statics_est.control_index (),
                                               fader.value ().get_uint32 () );
              steps_capacity -= stepper_data_composer.i32_words ();
            } else {
              // Ignore fader with invalid fader index
            }
          } else {
            fader_consumed = false;
          }
          break;
        case 64:
          if ( steps_capacity >= stepper_data_composer.i64_words () ) {
            auto & controls_statics = device_statics ().controls_i64 ();
            if ( fader.index () < controls_statics.size () ) {
              auto & statics_est =
                  controls_statics.get ( fader.index () )->est ();
              stepper_data_composer.push_i64 ( statics_est.control_index (),
                                               fader.value ().get_uint64 () );
              steps_capacity -= stepper_data_composer.i64_words ();
            } else {
              // Ignore fader with invalid fader index
            }
          } else {
            fader_consumed = false;
          }
          break;
        default:
          // Ignore
          break;
        }

        if ( fader_consumed ) {
          // The fader was consumed
          _stepper.controls.item_fader_consume ();
        } else {
          // Leave loop
          break;
        }

      } else {
        //
        // Long step item
        //
        if ( ( steps_capacity >= 2 ) && ( ticks_capacity != 0 ) ) {
          // Load next step
          snc::est::Stepper_Segment_Pusher pusher (
              stepper_data_composer, estepper::segment::Type::CTL_SLEEP );
          // Update capacities (the pusher head consumes one word)
          --steps_capacity;
          // Push steps
          bool all_consumed = false;
          while ( !all_consumed && ( steps_capacity != 0 ) &&
                  ( ticks_capacity != 0 ) ) {
            // Push next step
            auto step = _stepper.controls.item_step_prepare ();
            pusher.push ( step );
            all_consumed = _stepper.controls.item_step_consume ();
            // Update capacities
            --steps_capacity;
            sev::math::subtract_minimum_zero< std::uint_fast64_t > (
                ticks_capacity, step );
          }
        } else {
          // Leave loop
          break;
        }
      }
    }

    // Push stepper data or leave loop
    if ( stepper_data_composer.size () != 0 ) {
      device_stepper_data_push ( stepper_data_composer );
      // Reset stepper data composer
      stepper_data_composer.reset ();
    } else {
      break;
    }
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_generate_send_motor (
    Stepper_Motor & stepper_n )
{
  snc::est::Stepper_Data_Composer stepper_data_composer (
      stepper_n.est ().stepper_index );

  // Compute initial ticks capacity
  std::uint_fast64_t ticks_capacity = 0;
  {
    std::uint_fast64_t queue_ticks = stepper_n.state ()->steps_queue_ticks ();
    // Only feed when we're below the minimum to encourage multi step feeds
    if ( queue_ticks < stepper_n.steps_queue_ticks_min () ) {
      ticks_capacity = stepper_n.steps_queue_ticks_max ();
      sev::math::subtract_minimum_zero ( ticks_capacity, queue_ticks );
    }
  }

  while ( true ) {
    // Compute initial steps capacity
    std::uint_fast64_t steps_capacity = stepper_n.est ().steps_queue_capacity;
    sev::math::subtract_minimum_zero (
        steps_capacity, stepper_n.state ()->steps_queue_length () );
    sev::math::assign_smaller ( steps_capacity,
                                stepper_data_composer.size_free () );

    while ( stepper_n.steps_available () && ( steps_capacity >= 2 ) &&
            ( ticks_capacity != 0 ) ) {
      // Load next step
      auto step_first = stepper_n.prepare_step ();
      {
        snc::est::Stepper_Segment_Pusher pusher ( stepper_data_composer,
                                                  step_first.type () );

        // Push first step
        pusher.push ( step_first.ticks () );
        stepper_n.consume_step ();
        // Update capacities
        steps_capacity -= 2;
        sev::math::subtract_minimum_zero< std::uint_fast64_t > (
            ticks_capacity, step_first.ticks () );

        // Push subsequent steps if they have the same type
        while ( stepper_n.steps_available () && ( steps_capacity != 0 ) &&
                ( ticks_capacity != 0 ) ) {
          // Load next step
          auto step = stepper_n.prepare_step ();
          if ( step.type () != step_first.type () ) {
            // Step type missmatch. Stop pushing.
            break;
          }
          // Push next step
          pusher.push ( step.ticks () );
          stepper_n.consume_step ();
          // Update capacities
          --steps_capacity;
          sev::math::subtract_minimum_zero< std::uint_fast64_t > (
              ticks_capacity, step.ticks () );
        }
      }
    }

    // Push stepper data or leave loop
    if ( stepper_data_composer.size () != 0 ) {
      device_stepper_data_push ( stepper_data_composer );
      // Reset stepper data composer
      stepper_data_composer.reset ();
    } else {
      break;
    }
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::device_messages_generate_enable_disable ()
{
  // Enable
  if ( !_stepper.enabled ) {
    // Wait until the step queues have been filled
    if ( all_queues_filled () ||
         ( !all_queues_empty () && _stream_is_finished ) ) {
      _stepper.enabled = true;
      // Compose the message
      {
        std::array< std::uint8_t, 2 + 1 + DIM > message;
        message[ 0 ] = message.size () - 1;
        message[ 1 ] = estepper::out::STEPPER_ENABLE;
        message[ 2 ] = _stepper.controls.statics ()->est ().stepper_index ();
        for ( std::size_t ii = 0; ii != DIM; ++ii ) {
          std::size_t index = ( 3 + ii );
          DEBUG_ASSERT ( index < message.size () );
          message[ index ] =
              _stepper.axes[ ii ].statics ()->est ().stepper_index ();
        }
        device_message_push ( message.data () );
      }
      // Log
      if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
        logo << "Sending steppers enable";
      }
    }
  }

  // Disable
  if ( _stream_is_finished && !_stepper.disabled ) {
    // Check if all step queues are empty
    if ( all_queues_empty () ) {
      _stepper.disabled = true;
      // Only send the message if we enabled in the first place
      if ( _stepper.enabled ) {
        // Compose the message
        {
          std::array< std::uint8_t, 2 + 1 + DIM > message;
          message[ 0 ] = message.size () - 1;
          message[ 1 ] = estepper::out::STEPPER_DISABLE;
          message[ 2 ] = _stepper.controls.statics ()->est ().stepper_index ();
          for ( std::size_t ii = 0; ii != DIM; ++ii ) {
            std::size_t index = ( 3 + ii );
            DEBUG_ASSERT ( index < message.size () );
            message[ index ] =
                _stepper.axes[ ii ].statics ()->est ().stepper_index ();
          }
          device_message_push ( message.data () );
        }
        // Log
        if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_1 ) ) {
          logo << "Sending steppers disable";
        }
      }
    }
  }
}

template < std::size_t DIM >
void
Pilot_N< DIM >::mpath_feed_break ( bool forced_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Route break: Forced: " << forced_n;
  }

  // Forced abort
  if ( forced_n ) {
    // Abort feed only when forced.
    _feed.job_abort ();
  }
  _feed.request_event_processing ();

  mpath_feed_poll_end ();
}

template < std::size_t DIM >
void
Pilot_N< DIM >::mpath_feed_poll_end ()
{
  // Finished
  // if ( !_mpath_feed.is_finished () ) {
  //  return;
  //}
  // Wait until all axes stopped before finishing
  if ( cell_session ().is_good ) {
    // for ( auto ai : _mpath_feed.context ().axes_mapping ().maps () ) {
    //  if ( !device_state ().axes ().get ( ai ).steps_queue_empty () ) {
    //    return;
    //  }
    //}
  }

  // MPath feed end
  //_mpath_feed.route_end ();
  _mpath_job.reset ();

  // Unregister from device messages
  device_messages_unregister ();

  // Update state
  _state.set_stream_finished ( true );
  office_io ().notification_set ( Office_Message::STATE );
}

template < std::size_t DIM >
bool
Pilot_N< DIM >::all_queues_filled () const
{
  if ( _stepper.controls.state ()->steps_queue_empty () ) {
    return false;
  }
  for ( const auto & axis : _stepper.axes ) {
    if ( axis.state ()->steps_queue_empty () ) {
      return false;
    }
  }
  return true;
}

template < std::size_t DIM >
bool
Pilot_N< DIM >::all_queues_empty () const
{
  if ( !_stepper.controls.state ()->steps_queue_empty () ) {
    return false;
  }
  for ( const auto & axis : _stepper.axes ) {
    if ( !axis.state ()->steps_queue_empty () ) {
      return false;
    }
  }
  return true;
}

// -- Instantiation

template class Pilot_N< 1 >;
template class Pilot_N< 2 >;
template class Pilot_N< 3 >;
template class Pilot_N< 4 >;
template class Pilot_N< 5 >;
template class Pilot_N< 6 >;

} // namespace snc::svs::fac::mpath::ship::est
