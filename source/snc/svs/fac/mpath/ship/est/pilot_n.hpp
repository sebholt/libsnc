/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/flags.hpp>
#include <snc/mpath_feed/axes_mapping_d.hpp>
#include <snc/mpath_feed/cargo/event.hpp>
#include <snc/mpath_feed/feed.hpp>
#include <snc/mpath_feed/job.hpp>
#include <snc/mpath_feed/job_d.hpp>
#include <snc/mpath_feed/step.hpp>
#include <snc/svp/ship/pilot_est.hpp>
#include <snc/svs/fac/mpath/ship/est/muxer_config.hpp>
#include <snc/svs/fac/mpath/ship/est/stepper_controls.hpp>
#include <snc/svs/fac/mpath/ship/est/stepper_motor.hpp>
#include <snc/svs/fac/mpath/ship/events.hpp>

namespace snc::svs::fac::mpath::ship::est
{

template < std::size_t DIM >
class Pilot_N : public snc::svp::ship::Pilot_Est
{
  private:
  // -- Types
  using Super = snc::svp::ship::Pilot_Est;

  struct Office_Message
  {
    static constexpr std::uint8_t STATE = ( 1 << 0 );
  };

  public:
  // -- Construction

  Pilot_N ( const Sailor_Init & init_n,
            const snc::mpath_feed::Axes_Mapping_D< DIM > & axes_mapping_n );

  ~Pilot_N ();

  // -- Cell session interface

  void
  cell_session_begin () override;

  void
  cell_session_end () override;

  void
  cell_session_abort () override;

  bool
  cell_session_is_ready_to_end () const override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  void
  office_event_job ( Office_Event & event_n );

  void
  office_event_break ( Office_Event & event_n );

  void
  office_notify () override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  // -- Embedded device message generation

  void
  device_messages_generate () override;

  private:
  // -- Message composition

  void
  device_messages_generate_feed_buffers ();

  bool
  device_messages_generate_feed_buffer_new ();

  void
  device_messages_generate_feed_buffer_steps ();

  void
  device_messages_generate_send_controls ();

  void
  device_messages_generate_send_motor ( Stepper_Motor & stepper_n );

  void
  device_messages_generate_enable_disable ();

  // -- Utility

  void
  mpath_feed_break ( bool forced_n );

  void
  mpath_feed_poll_end ();

  bool
  all_queues_filled () const;

  bool
  all_queues_empty () const;

  private:
  // -- State
  State _state;

  // -- Resources
  snc::mpath_feed::Axes_Mapping_D< DIM > const _axes_mapping;
  bool _is_routing = false;
  bool _stream_is_finished = false;
  std::shared_ptr< snc::mpath_feed::Job_D< DIM > > _mpath_job;
  snc::mpath_feed::Feed _feed;
  struct
  {
    snc::mpath_feed::cargo::Event * cargo = nullptr;
    snc::mpath_feed::CStep< DIM > const * step_next = nullptr;
    snc::mpath_feed::CStep< DIM > const * step_end = nullptr;
  } _feed_steps;
  struct
  {
    Muxer_Config muxer_config;
    Stepper_Controls controls;
    std::array< Stepper_Motor, DIM > axes;
    bool enabled = false;
    bool disabled = false;
  } _stepper;

  // -- Office event io
  sev::event::Pool< ship::event::out::State > _event_pool_state;
};

} // namespace snc::svs::fac::mpath::ship::est
