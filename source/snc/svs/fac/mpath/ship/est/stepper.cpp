/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper.hpp"
#include <sev/assert.hpp>
#include <sev/math/exponents.hpp>
#include <cmath>

namespace snc::svs::fac::mpath::ship::est
{

Stepper::Stepper () = default;

Stepper::~Stepper () = default;

void
Stepper::init ( Muxer_Config & muxer_config_n )
{
  _muxer_config = &muxer_config_n;
}

std::uint_fast64_t
Stepper::ticks_from_usecs_ceil ( double tick_frequency_n, double usecs_n )
{
  return std::ceil ( ( tick_frequency_n / sev::math::mega_d ) * usecs_n );
}

std::uint16_t
Stepper::compute_step_ticks ( std::uint_fast64_t long_step_ticks_n )
{
  std::uint_fast64_t const step_ticks_max = 65535;

  std::uint_fast64_t step_ticks = long_step_ticks_n;
  if ( step_ticks > step_ticks_max ) {
    if ( step_ticks < ( step_ticks_max * 2 ) ) {
      // Avoid small residues
      step_ticks /= 2;
    } else {
      step_ticks = step_ticks_max;
    }
  }
  DEBUG_ASSERT ( step_ticks <= step_ticks_max );
  return step_ticks;
}

} // namespace snc::svs::fac::mpath::ship::est
