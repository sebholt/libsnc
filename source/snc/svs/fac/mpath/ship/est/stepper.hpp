/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/mpath/ship/est/muxer_config.hpp>
#include <cstdint>

namespace snc::svs::fac::mpath::ship::est
{

/// @brief Stepper base class
///
class Stepper
{
  public:
  // -- Construction

  Stepper ();

  ~Stepper ();

  void
  init ( Muxer_Config & muxer_config_n );

  // -- Accessors

  Muxer_Config const &
  muxer_config () const
  {
    return *_muxer_config;
  }

  protected:
  // -- Utility
  static std::uint_fast64_t
  ticks_from_usecs_ceil ( double tick_frequency_n, double usecs_n );

  static std::uint16_t
  compute_step_ticks ( std::uint_fast64_t long_step_ticks_n );

  private:
  // -- Attributes
  Muxer_Config * _muxer_config = nullptr;
};

} // namespace snc::svs::fac::mpath::ship::est
