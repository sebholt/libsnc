/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_controls.hpp"
#include <estepper/segment/types.hpp>
#include <snc/device/state/stepper_controls.hpp>
#include <snc/device/statics/stepper_controls.hpp>
#include <snc/est/config/stepper.hpp>

namespace snc::svs::fac::mpath::ship::est
{

Stepper_Controls::Stepper_Controls () = default;

Stepper_Controls::~Stepper_Controls () = default;

void
Stepper_Controls::init (
    Muxer_Config & muxer_config_n,
    snc::device::statics::handle::Stepper_Controls const & statics_n,
    snc::device::state::handle::Stepper_Controls const & state_n )
{
  Stepper::init ( muxer_config_n );
  _statics = statics_n;
  _state = state_n;

  // EStepper info
  _est.stepper_index = _statics->est ().stepper_index ();
  _est.steps_queue_capacity =
      _statics->est ().stepper_config ()->steps_queue_capacity ();
  _est.tick_frequency = _statics->est ().stepper_config ()->tick_frequency ();

  // Tick limits
  _steps_queue_ticks_min = ticks_from_usecs_ceil (
      _est.tick_frequency, muxer_config ().steps_queue_usecs_min () );
  _steps_queue_ticks_max = ticks_from_usecs_ceil (
      _est.tick_frequency, muxer_config ().steps_queue_usecs_max () );
  _buffer_feed_ticks_limit = ticks_from_usecs_ceil (
      _est.tick_frequency, muxer_config ().buffer_feed_usecs_limit () );

  // Control item ring buffer
  _ring.set_capacity_block_size ( 256 );
  _ring.set_capacity_shrink_limit ( 4096 );
  _ring.set_capacity ( 256 );
}

bool
Stepper_Controls::buffer_satiated () const
{
  return ( _state->steps_queue_ticks () + _ticks ) >= _buffer_feed_ticks_limit;
}

void
Stepper_Controls::push_sleep_usecs ( double usecs_n )
{
  std::uint_fast64_t const sleep_ticks =
      ticks_from_usecs_ceil ( _est.tick_frequency, usecs_n );
  // Update total tick count
  _ticks += sleep_ticks;
  // Check where to put the sleep ticks
  if ( _ring.empty () ) {
    // Push new sleep ticks item
    _ring.emplace_back ( std::in_place_type_t< std::uint_fast64_t > (),
                         sleep_ticks );
  } else {
    if ( auto & back = _ring.back ();
         std::holds_alternative< std::uint_fast64_t > ( back ) ) {
      // Append to existing sleep ticks item
      std::get< std::uint_fast64_t > ( back ) += sleep_ticks;
    } else {
      // Push new sleep ticks item
      _ring.emplace_back ( std::in_place_type_t< std::uint_fast64_t > (),
                           sleep_ticks );
    }
  }
}

void
Stepper_Controls::push_fader ( snc::mpath::elem::Fader const & fader_n )
{
  // Push new fader item
  _ring.emplace_back ( std::in_place_type_t< Fader > (),
                       fader_n.fader_bits (),
                       fader_n.fader_index (),
                       fader_n.fader_value () );
}

void
Stepper_Controls::item_fader_consume ()
{
  DEBUG_ASSERT ( !_ring.is_empty () );
  DEBUG_ASSERT ( item_is_fader () );
  _ring.pop_front_not_empty ();
}

std::uint16_t
Stepper_Controls::item_step_prepare ()
{
  DEBUG_ASSERT ( !_ring.is_empty () );
  DEBUG_ASSERT ( item_is_step () );
  DEBUG_ASSERT ( std::get< std::uint_fast64_t > ( _ring.front () ) != 0 );
  _step =
      compute_step_ticks ( std::get< std::uint_fast64_t > ( _ring.front () ) );
  return _step;
}

bool
Stepper_Controls::item_step_consume ()
{
  DEBUG_ASSERT ( _ticks != 0 );
  DEBUG_ASSERT ( _step != 0 );
  DEBUG_ASSERT ( _ticks >= _step );
  DEBUG_ASSERT ( !_ring.is_empty () );
  DEBUG_ASSERT ( item_is_step () );
  std::uint_fast64_t const step64 = _step;
  _step = 0;
  _ticks -= step64;
  {
    auto & sfront = std::get< std::uint_fast64_t > ( _ring.front () );
    DEBUG_ASSERT ( sfront >= step64 );
    sfront -= step64;
    if ( sfront == 0 ) {
      // We're done with this long step
      _ring.pop_front_not_empty ();
      return true;
    }
  }
  return false;
}

} // namespace snc::svs::fac::mpath::ship::est
