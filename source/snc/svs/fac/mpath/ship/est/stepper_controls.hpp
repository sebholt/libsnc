/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/ring_resizing.hpp>
#include <snc/device/handle.hpp>
#include <snc/mpath/elem/fader.hpp>
#include <snc/svs/fac/mpath/ship/est/control_item.hpp>
#include <snc/svs/fac/mpath/ship/est/fader.hpp>
#include <snc/svs/fac/mpath/ship/est/stepper.hpp>

namespace snc::svs::fac::mpath::ship::est
{

/// @brief Controls stepper resources
///
class Stepper_Controls : public Stepper
{
  public:
  // -- Types

  struct Est
  {
    std::uint8_t stepper_index = 0;
    std::uint_fast32_t steps_queue_capacity = 0;
    double tick_frequency = 0.0;
  };

  // -- Construction

  Stepper_Controls ();

  ~Stepper_Controls ();

  void
  init ( Muxer_Config & muxer_config_n,
         snc::device::statics::handle::Stepper_Controls const & statics_n,
         snc::device::state::handle::Stepper_Controls const & state_n );

  // -- Accessors

  const auto &
  statics () const
  {
    return _statics;
  }

  const auto &
  state () const
  {
    return _state;
  }

  const auto &
  est () const
  {
    return _est;
  }

  std::uint_fast64_t
  steps_queue_ticks_min () const
  {
    return _steps_queue_ticks_min;
  }

  std::uint_fast64_t
  steps_queue_ticks_max () const
  {
    return _steps_queue_ticks_max;
  }

  std::uint_fast64_t
  buffer_feed_ticks_limit () const
  {
    return _buffer_feed_ticks_limit;
  }

  // -- Feeding interface

  bool
  buffer_satiated () const;

  void
  push_sleep_usecs ( double usecs_n );

  void
  push_fader ( snc::mpath::elem::Fader const & fader_n );

  // -- Control item reading interface

  bool
  item_available () const
  {
    return !_ring.is_empty ();
  }

  bool
  item_is_fader () const
  {
    return std::holds_alternative< Fader > ( _ring.front () );
  }

  bool
  item_is_step () const
  {
    return std::holds_alternative< std::uint_fast64_t > ( _ring.front () );
  }

  // -- Control item reading interface: Fader

  Fader const &
  item_fader () const
  {
    return std::get< Fader > ( _ring.front () );
  }

  void
  item_fader_consume ();

  // -- Control item reading interface: Step

  std::uint16_t
  item_step_prepare ();

  /// @return true if the complete long step was consumed
  bool
  item_step_consume ();

  private:
  snc::device::statics::handle::Stepper_Controls _statics;
  snc::device::state::handle::Stepper_Controls _state;
  Est _est;
  std::uint_fast64_t _steps_queue_ticks_min = 0;
  std::uint_fast64_t _steps_queue_ticks_max = 0;
  std::uint_fast64_t _buffer_feed_ticks_limit = 0;
  sev::mem::Ring_Resizing< Control_Item > _ring;
  std::uint_fast64_t _ticks = 0;
  std::uint16_t _step = 0;
};

} // namespace snc::svs::fac::mpath::ship::est
