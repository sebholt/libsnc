/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_motor.hpp"
#include <estepper/segment/types.hpp>
#include <snc/device/state/axis.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/est/config/stepper.hpp>

namespace snc::svs::fac::mpath::ship::est
{

Stepper_Motor::Stepper_Motor () = default;

Stepper_Motor::~Stepper_Motor () = default;

void
Stepper_Motor::init ( Muxer_Config & muxer_config_n,
                      snc::device::statics::handle::Axis const & statics_n,
                      snc::device::state::handle::Axis const & state_n )
{
  Stepper::init ( muxer_config_n );
  _statics = statics_n;
  _state = state_n;

  // EStepper info
  _est.stepper_index = _statics->est ().stepper_index ();
  _est.direction_reversed = _statics->est ().direction_reversed ();
  _est.steps_queue_capacity =
      _statics->est ().stepper_config ()->steps_queue_capacity ();
  _est.tick_frequency = _statics->est ().stepper_config ()->tick_frequency ();

  // Tick limits
  _steps_queue_ticks_min = ticks_from_usecs_ceil (
      _est.tick_frequency, muxer_config ().steps_queue_usecs_min () );
  _steps_queue_ticks_max = ticks_from_usecs_ceil (
      _est.tick_frequency, muxer_config ().steps_queue_usecs_max () );
  _buffer_feed_ticks_limit = ticks_from_usecs_ceil (
      _est.tick_frequency, muxer_config ().buffer_feed_usecs_limit () );

  // Long step ring buffer
  _ring.set_capacity_block_size ( 256 );
  _ring.set_capacity_shrink_limit ( 4096 );
  _ring.set_capacity ( 256 );
}

void
Stepper_Motor::reset ()
{
  _ring.clear ();
  _ticks = 0;
  _step.reset ();
}

bool
Stepper_Motor::buffer_satiated () const
{
  return ( _state->steps_queue_ticks () + _ticks ) >= _buffer_feed_ticks_limit;
}

void
Stepper_Motor::push_sleep_usecs ( double usecs_n )
{
  std::uint_fast64_t const sleep_ticks =
      ticks_from_usecs_ceil ( _est.tick_frequency, usecs_n );
  // Update total tick count
  _ticks += sleep_ticks;
  // Check where to put the sleep ticks
  if ( _ring.empty () ) {
    // Append new sleep element
    _ring.emplace_back ( estepper::segment::Type::MOTOR_SLEEP, sleep_ticks );
  } else {
    // Append ticks to the last long step
    _ring.back ().ticks_ref () += sleep_ticks;
  }
}

snc::est::Type_Step
Stepper_Motor::prepare_step ()
{
  DEBUG_ASSERT ( _ticks != 0 );
  DEBUG_ASSERT ( !_ring.is_empty () );
  DEBUG_ASSERT ( _ring.front ().ticks () != 0 );
  {
    const auto & sfront = _ring.front ();
    _step.set_type ( sfront.type () );
    _step.set_ticks ( compute_step_ticks ( sfront.ticks () ) );
  }
  return _step;
}

void
Stepper_Motor::consume_step ()
{
  DEBUG_ASSERT ( _ticks != 0 );
  DEBUG_ASSERT ( _step.ticks () != 0 );
  DEBUG_ASSERT ( _ticks >= _step.ticks () );
  DEBUG_ASSERT ( !_ring.is_empty () );
  DEBUG_ASSERT ( _ring.front ().type () == _step.type () );
  std::uint_fast64_t const step_ticks64 = _step.ticks ();
  _step.reset ();
  _ticks -= step_ticks64;
  {
    auto & sfront = _ring.front ();
    DEBUG_ASSERT ( sfront.ticks () >= step_ticks64 );
    sfront.ticks_ref () -= step_ticks64;
    if ( sfront.ticks () != 0 ) {
      // The remaining ticks will we sleep ticks
      sfront.set_type ( estepper::segment::MOTOR_SLEEP );
    } else {
      // We're done with this long step
      _ring.pop_front_not_empty ();
    }
  }
}

} // namespace snc::svs::fac::mpath::ship::est
