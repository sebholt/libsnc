/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/segment/types.hpp>
#include <sev/mem/ring_resizing.hpp>
#include <snc/device/handle.hpp>
#include <snc/est/type_step.hpp>
#include <snc/est/type_step_long.hpp>
#include <snc/svs/fac/mpath/ship/est/stepper.hpp>

namespace snc::svs::fac::mpath::ship::est
{

/// @brief Motor stepper resources
///
class Stepper_Motor : public Stepper
{
  public:
  // -- Types

  struct Est
  {
    std::uint8_t stepper_index = 0;
    bool direction_reversed = false;
    std::uint_fast32_t steps_queue_capacity = 0;
    double tick_frequency = 0.0;
  };

  // -- Construction

  Stepper_Motor ();

  ~Stepper_Motor ();

  void
  init ( Muxer_Config & muxer_config_n,
         snc::device::statics::handle::Axis const & statics_n,
         snc::device::state::handle::Axis const & state_n );

  void
  reset ();

  // -- Accessors

  const auto &
  statics () const
  {
    return _statics;
  }

  const auto &
  state () const
  {
    return _state;
  }

  const auto &
  est () const
  {
    return _est;
  }

  std::uint_fast64_t
  steps_queue_ticks_min () const
  {
    return _steps_queue_ticks_min;
  }

  std::uint_fast64_t
  steps_queue_ticks_max () const
  {
    return _steps_queue_ticks_max;
  }

  std::uint_fast64_t
  buffer_feed_ticks_limit () const
  {
    return _buffer_feed_ticks_limit;
  }

  // -- Feeding interface

  bool
  buffer_satiated () const;

  void
  push_sleep_usecs ( double usecs_n );

  // -- Step reading interface

  bool
  steps_available () const
  {
    return ( _ticks != 0 );
  }

  snc::est::Type_Step
  prepare_step ();

  void
  consume_step ();

  private:
  snc::device::statics::handle::Axis _statics;
  snc::device::state::handle::Axis _state;
  Est _est;
  std::uint_fast64_t _steps_queue_ticks_min = 0;
  std::uint_fast64_t _steps_queue_ticks_max = 0;
  std::uint_fast64_t _buffer_feed_ticks_limit = 0;
  sev::mem::Ring_Resizing< snc::est::Type_Step_Long > _ring;
  std::uint_fast64_t _ticks = 0;
  snc::est::Type_Step _step;
};

} // namespace snc::svs::fac::mpath::ship::est
