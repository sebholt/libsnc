/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/factor.hpp>
#include <snc/svs/fac/serial_device/office/service.hpp>
#include <snc/svs/fac/serial_device/ship/events.hpp>
#include <snc/svs/frame/ship/office/client_session.hpp>

namespace snc::svs::fac::serial_device::office
{

class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n );

  ~Clerk () override;

  // -- Cell session interface

  void
  cell_session_begin () override;

  // -- Bridge session interface

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  // -- Bridge event processing

  void
  bridge_event ( Bridge_Event & event_n ) override;

  void
  bridge_event_device_error ( Bridge_Event & event_n );

  private:
  // -- Attributes
  Service _service;
  snc::svs::frame::ship::office::Client_Session _ship_session;
};

} // namespace snc::svs::fac::serial_device::office
