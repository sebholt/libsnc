/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"

namespace snc::svs::fac::serial_device::office
{

Service::Service ( Provider_Cell & provider_n )
: Super ( provider_n )
{
}

void
Service::set_serial_device ( const Device_Letter_Handle & serial_device_n )
{
  _serial_device = serial_device_n;
}

} // namespace snc::svs::fac::serial_device::office
