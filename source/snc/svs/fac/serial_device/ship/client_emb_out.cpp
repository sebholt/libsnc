/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_emb_out.hpp"
#include <sev/utility.hpp>
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/serial_device/ship/service_emb_out.hpp>

namespace snc::svs::fac::serial_device::ship
{

Client_Emb_Out::Client_Emb_Out ( snc::svp::ship::Pilot_Emb * user_n )
: Super ( user_n )
, _pilot_emb ( user_n )
{
}

Client_Emb_Out::~Client_Emb_Out () = default;

void
Client_Emb_Out::subscribe ( bool flag_n )
{
  if ( !is_connected () ) {
    return;
  }
  if ( sev::change ( _subscribed, flag_n ) ) {
    service ()->subscribe ( _pilot_emb, flag_n );
  }
}

void
Client_Emb_Out::request_pick_up ()
{
  if ( is_connected () ) {
    service ()->request_pick_up ();
  }
}

Service_Emb_Out *
Client_Emb_Out::service () const
{
  return static_cast< Service_Emb_Out * > ( service_abstract () );
}

bool
Client_Emb_Out::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service_Emb_Out * > ( sb_n ) != nullptr );
}

void
Client_Emb_Out::disconnecting ()
{
  subscribe ( false );
  Super::disconnecting ();
}

} // namespace snc::svs::fac::serial_device::ship
