/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/office/client.hpp>

// -- Forward declaration
namespace snc::svp::ship
{
class Pilot_Emb;
}

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Service_Emb_Out;

/// @brief Client
///
class Client_Emb_Out : public snc::svs::fac::base::office::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Client;

  public:
  // -- Construction

  Client_Emb_Out ( snc::svp::ship::Pilot_Emb * user_n );

  ~Client_Emb_Out () override;

  // -- Interface

  bool
  subscribed () const
  {
    return _subscribed;
  }

  void
  subscribe ( bool flag_n = true );

  void
  request_pick_up ();

  // -- Service connection
  protected:
  Service_Emb_Out *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  void
  disconnecting () override;

  private:
  snc::svp::ship::Pilot_Emb * _pilot_emb = nullptr;
  bool _subscribed = false;
};

} // namespace snc::svs::fac::serial_device::ship
