/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_emb_out.hpp"
#include <sev/utility.hpp>
#include <snc/svp/ship/pilot_est.hpp>
#include <snc/svs/fac/serial_device/ship/service_est_out.hpp>

namespace snc::svs::fac::serial_device::ship
{

Client_Est_Out::Client_Est_Out ( snc::svp::ship::Pilot_Est * user_n )
: Super ( user_n )
, _pilot_est ( user_n )
{
}

Client_Est_Out::~Client_Est_Out () = default;

void
Client_Est_Out::subscribe ( bool flag_n )
{
  if ( !is_connected () ) {
    return;
  }
  if ( sev::change ( _subscribed, flag_n ) ) {
    service ()->subscribe ( _pilot_est, flag_n );
  }
}

void
Client_Est_Out::request_pick_up ()
{
  if ( is_connected () ) {
    service ()->request_pick_up ();
  }
}

void
Client_Est_Out::push_message ( std::uint8_t const * message_n )
{
  if ( is_connected () ) {
    service ()->push_message ( message_n );
  }
}

void
Client_Est_Out::push_data ( std::byte const * data_n, std::size_t size_n )
{
  if ( is_connected () ) {
    service ()->push_data ( data_n, size_n );
  }
}

inline Service_Est_Out *
Client_Est_Out::service () const
{
  return static_cast< Service_Est_Out * > ( service_abstract () );
}

bool
Client_Est_Out::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service_Est_Out * > ( sb_n ) != nullptr );
}

void
Client_Est_Out::disconnecting ()
{
  subscribe ( false );
  Super::disconnecting ();
}

} // namespace snc::svs::fac::serial_device::ship
