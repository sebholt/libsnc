/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/office/client.hpp>

// -- Forward declaration
namespace snc::svp::ship
{
class Pilot_Est;
}

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Service_Est_Out;

/// @brief Client
///
class Client_Est_Out : public snc::svs::fac::base::office::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Client;

  public:
  // -- Construction

  Client_Est_Out ( snc::svp::ship::Pilot_Est * user_n );

  ~Client_Est_Out () override;

  // -- Interface

  bool
  subscribed () const
  {
    return _subscribed;
  }

  void
  subscribe ( bool flag_n = true );

  void
  request_pick_up ();

  void
  push_message ( std::uint8_t const * message_n );

  void
  push_data ( std::byte const * data_n, std::size_t size_n );

  // -- Service connection
  protected:
  Service_Est_Out *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  void
  disconnecting () override;

  private:
  snc::svp::ship::Pilot_Est * _pilot_est = nullptr;
  bool _subscribed = false;
};

} // namespace snc::svs::fac::serial_device::ship
