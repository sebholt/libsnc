/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_emb_out.hpp"
#include <snc/svp/ship/pilot_emb.hpp>
#include <snc/svs/fac/serial_device/ship/sailor.hpp>

namespace snc::svs::fac::serial_device::ship
{

Service_Emb_Out::Service_Emb_Out ( Provider & provider_n )
: Super ( provider_n )
{
}

Service_Emb_Out::~Service_Emb_Out () = default;

Service_Emb_Out::Provider &
Service_Emb_Out::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service_Emb_Out::Provider &
Service_Emb_Out::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service_Emb_Out::subscribe ( snc::svp::ship::Pilot_Emb * pilot_n,
                             bool subscribe_n )
{
  DEBUG_ASSERT ( pilot_n != nullptr );
  _pilots.subscribe ( pilot_n, subscribe_n );
  // New subscriptions trigger message generation
  if ( subscribe_n ) {
    provider ().pilot_messages_request ();
  }
}

void
Service_Emb_Out::request_pick_up ()
{
  provider ().pilot_messages_request ();
}

void
Service_Emb_Out::ensure_minimum_capacity ( std::size_t size_n )
{
  _pilots.ensure_minimum_capacity ( size_n );
}

void
Service_Emb_Out::pick_up ( snc::emb::msg::Stream & stream_n )
{
  // Update active pilots list
  _pilots.apply_subscriptions ();

  // Generate messages
  for ( auto * pilot : _pilots.list () ) {
    pilot->device_messages_acquire ( &stream_n );
  }
}

} // namespace snc::svs::fac::serial_device::ship
