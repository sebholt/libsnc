/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_est_out.hpp"
#include <snc/svp/ship/pilot_est.hpp>
#include <snc/svs/fac/serial_device/ship/sailor.hpp>

namespace snc::svs::fac::serial_device::ship
{

Service_Est_Out::Service_Est_Out ( Provider & provider_n )
: Super ( provider_n )
{
}

Service_Est_Out::~Service_Est_Out () = default;

Service_Est_Out::Provider &
Service_Est_Out::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service_Est_Out::Provider &
Service_Est_Out::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service_Est_Out::subscribe ( snc::svp::ship::Pilot_Est * pilot_n,
                             bool subscribe_n )
{
  DEBUG_ASSERT ( pilot_n != nullptr );
  _pilots.subscribe ( pilot_n, subscribe_n );
  // New subscriptions trigger message generation
  if ( subscribe_n ) {
    provider ().pilot_messages_request ();
  }
}

void
Service_Est_Out::request_pick_up ()
{
  provider ().pilot_messages_request ();
}

void
Service_Est_Out::push_message ( std::uint8_t const * message_n )
{
  push_data ( reinterpret_cast< std::byte const * > ( message_n ),
              message_n[ 0 ] + 1 );
}

void
Service_Est_Out::push_data ( std::byte const * data_n, std::size_t size_n )
{
  while ( size_n != 0 ) {
    snc::utility::Data_Tile * tile = nullptr;
    if ( !_tiles_filled.is_empty () ) {
      snc::utility::Data_Tile * tile_back = _tiles_filled.back ();
      if ( tile_back->size_free () != 0 ) {
        tile = tile_back;
      }
    }
    if ( tile == nullptr ) {
      // Allocate a  new tile on demand
      if ( _tiles_free.is_empty () ) {
        _tile_owner.push_back ( std::make_unique< Write_Tile > () );
        _tiles_free.push_back ( _tile_owner.back ().get () );
      }
      // Fetch a tile from the free list
      tile = _tiles_free.front ();
      _tiles_free.pop_front_not_empty ();
      _tiles_filled.push_back ( tile );
    }

    // Write some data to the tile
    std::size_t copy_size = std::min ( size_n, tile->size_free () );
    std::byte * buffer = tile->data () + tile->size ();
    for ( std::size_t ii = 0; ii != copy_size; ++ii ) {
      buffer[ ii ] = data_n[ ii ];
    }
    size_n -= copy_size;
    data_n += copy_size;
    tile->size_increment ( copy_size );
  }
}

void
Service_Est_Out::allocate_data_tiles ( std::size_t num_n )
{
  // Allocate some serial data tiles
  _tile_owner.reserve ( _tile_owner.size () + num_n );
  for ( std::size_t ii = 0; ii != num_n; ++ii ) {
    _tile_owner.push_back ( std::make_unique< Write_Tile > () );
    _tiles_free.push_back ( _tile_owner.back ().get () );
  }
}

void
Service_Est_Out::ensure_minimum_capacity ( std::size_t size_n )
{
  _pilots.ensure_minimum_capacity ( size_n );
}

void
Service_Est_Out::generate ()
{
  // Update active pilots list
  _pilots.apply_subscriptions ();

  // Generate messages
  for ( auto * pilot : _pilots.list () ) {
    pilot->device_messages_generate ();
  }
}

} // namespace snc::svs::fac::serial_device::ship
