/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/list/se/list.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/utility/data_tile.hpp>
#include <snc/utility/subscription_list.hpp>

// -- Forward declaration
namespace snc::svp::ship
{
class Pilot_Est;
}

namespace snc::svs::fac::serial_device::ship
{

// -- Forward declaration
class Sailor;

class Service_Est_Out : public snc::bat::ifc::Service_Abstract
{
  public:
  // -- Static variables

  static constexpr std::size_t write_tiles_max = 16;
  static constexpr std::size_t write_tile_size = 512;

  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Sailor;

  using Write_Tile = snc::utility::Data_Tile_N< write_tile_size >;

  // -- Construction
  public:
  Service_Est_Out ( Provider & provider_n );

  ~Service_Est_Out ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Client interface

  void
  subscribe ( snc::svp::ship::Pilot_Est * pilot_n, bool subscribe_n );

  void
  request_pick_up ();

  void
  push_message ( std::uint8_t const * message_n );

  void
  push_data ( std::byte const * data_n, std::size_t size_n );

  // -- Provider interface

  void
  allocate_data_tiles ( std::size_t num_n );

  void
  ensure_minimum_capacity ( std::size_t size_n );

  void
  generate ();

  void
  free_tile_push ( snc::utility::Data_Tile * tile_n )
  {
    tile_n->set_size ( 0 );
    _tiles_free.push_back ( tile_n );
  }

  bool
  filled_tile_available () const
  {
    return !_tiles_filled.is_empty ();
  }

  snc::utility::Data_Tile *
  filled_tile_pop ()
  {
    snc::utility::Data_Tile * res = _tiles_filled.front ();
    _tiles_filled.pop_front_not_empty ();
    return res;
  }

  private:
  // -- Resources
  std::vector< std::unique_ptr< Write_Tile > > _tile_owner;
  sev::mem::list::se::List< snc::utility::Data_Tile > _tiles_free;
  sev::mem::list::se::List< snc::utility::Data_Tile > _tiles_filled;

  snc::utility::Subscription_List< snc::svp::ship::Pilot_Est > _pilots;
};

} // namespace snc::svs::fac::serial_device::ship
