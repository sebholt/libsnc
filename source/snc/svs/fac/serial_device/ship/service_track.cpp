/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service_track.hpp"
#include <snc/svp/ship/pilot.hpp>
#include <snc/svs/fac/serial_device/ship/sailor.hpp>

namespace snc::svs::fac::serial_device::ship
{

Service_Track::Service_Track ( Provider & provider_n )
: Super ( provider_n )
{
}

Service_Track::~Service_Track () = default;

Service_Track::Provider &
Service_Track::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service_Track::Provider &
Service_Track::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service_Track::set_io_stats ( const snc::serial_device::Stats_IO & stats_n )
{
  if ( _io_stats.callback != nullptr ) {
    _io_stats.callback ( _io_stats.service, stats_n );
  }
}

void
Service_Track::data_in ( const snc::utility::Data_Tile * tile_n )
{
  if ( _data_in.callback != nullptr ) {
    _data_in.callback ( _data_in.service, tile_n );
  }
}

void
Service_Track::emb_out ( snc::emb::msg::Stream & stream_n,
                         std::size_t offset_n )
{
  if ( _emb_out.callback != nullptr ) {
    _emb_out.callback ( _emb_out.service, stream_n, offset_n );
  }
}

void
Service_Track::abort ()
{
  if ( _abort.callback != nullptr ) {
    _abort.callback ( _abort.service );
  }
}

void
Service_Track::clear_callbacks ()
{
  _io_stats.service = nullptr;
  _io_stats.callback = nullptr;

  _data_in.service = nullptr;
  _data_in.callback = nullptr;

  _emb_out.service = nullptr;
  _emb_out.callback = nullptr;

  _abort.service = nullptr;
  _abort.callback = nullptr;
}

void
Service_Track::set_io_stats_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                    IO_Stats_Callback cback_n )
{
  _io_stats.service = service_n;
  _io_stats.callback = cback_n;
}

void
Service_Track::set_data_in_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                   Data_In_Callback cback_n )
{
  _data_in.service = service_n;
  _data_in.callback = cback_n;
}

void
Service_Track::set_emb_out_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                   Emb_Out_Callback cback_n )
{
  _emb_out.service = service_n;
  _emb_out.callback = cback_n;
}

void
Service_Track::set_abort_cback ( snc::bat::ifc::Service_Abstract * service_n,
                                 Abort_Callback cback_n )
{
  _abort.service = service_n;
  _abort.callback = cback_n;
}

} // namespace snc::svs::fac::serial_device::ship
