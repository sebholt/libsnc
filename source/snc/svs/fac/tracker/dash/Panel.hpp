/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/device/qt/Info.hpp>
#include <snc/svp/dash/Panel.hpp>

namespace snc::svs::fac::tracker::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY ( QObject * info READ info CONSTANT )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Accessors

  snc::device::qt::Info *
  info ()
  {
    return &_info;
  }

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Office event processing

  void
  office_event ( Office_Event & event_n ) override;

  // -- Change callbacks

  void
  state_changed () override;

  private:
  // -- Attributes
  snc::device::qt::Info _info;
};

} // namespace snc::svs::fac::tracker::dash
