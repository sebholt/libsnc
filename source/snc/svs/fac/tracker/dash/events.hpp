/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/device/info.hpp>
#include <snc/svp/dash/events.hpp>
#include <cstdint>

namespace snc::svs::fac::tracker::dash::event::in
{

struct Type
{
  static constexpr std::uint_fast32_t STATE = 0;
};

using namespace snc::svp::dash::event::in;

using State = State_T< Type::STATE, snc::device::Info >;

} // namespace snc::svs::fac::tracker::dash::event::in
