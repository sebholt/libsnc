/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/device/statics/statics.hpp>
#include <snc/svs/fac/tracker/office/service.hpp>
#include <stdexcept>

namespace snc::svs::fac::tracker::office
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

snc::device::handle::Info
Client::device_info () const
{
  require_connection ();
  return service ()->device_info ();
}

bool
Client::est_device () const
{
  require_connection ();
  return service ()->device_info ()->statics ()->est ();
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

void
Client::connecting ()
{
  Super::connecting ();
  // Disconnect from state signal by default
  signal_state_disconnect ();
}

} // namespace snc::svs::fac::tracker::office
