/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <snc/svs/fac/tracker/office/clerk.hpp>

namespace snc::svs::fac::tracker::office
{

Service::Service (
    Provider & provider_n,
    const snc::device::statics::handle::Statics & device_statics_n )
: Super ( provider_n )
, _device_info_root ( snc::device::handle::make_info ( device_statics_n ) )
{
}

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::reset_statics ()
{
  set_statics ( snc::device::statics::handle::make () );
}

void
Service::set_statics ( snc::device::statics::handle::Statics statics_n )
{
  if ( provider ().bridge_session ().is_running ) {
    log ().cat (
        sev::logt::FL_WARNING,
        "Setting device statics while bridge session is running denied." );
    return;
  }
  _device_info_root->reset ( statics_n );
  // Notify
  signal_state ().send ();
}

void
Service::reset_device_state ()
{
  _device_info_root->reset_state ();
  // Notify
  signal_state ().send ();
}

void
Service::update_device_state ( const snc::device::Info & device_info_n )
{
  _device_info_root->assign ( device_info_n );
  // Notify
  signal_state ().send ();
}

} // namespace snc::svs::fac::tracker::office
