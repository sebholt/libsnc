/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "axis.hpp"
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/emb/config/config.hpp>
#include <snc/emb/msg/out/all.hpp>
#include <cmath>

namespace snc::svs::fac::tracker::ship::emb
{

Axis::Axis ( const snc::device::statics::Axis & statics_n )
{
  // Selected statics
  _emb_direction_reversed = statics_n.emb ().direction_reversed ();
  _length_per_step = statics_n.geo ().length_per_step ();
  _speed_stoppable = statics_n.geo ().speed_stoppable ();
  // Acquire steps queue capacity
  if ( statics_n.emb ().stepper_config () ) {
    _steps_queue_capacity =
        statics_n.emb ().stepper_config ()->steps_queue_capacity ();
  }
  _steps_track_list.set_capacity ( _steps_queue_capacity * 2 );
}

void
Axis::session_begin ()
{
  _steps_track_list.clear ();

  _steps_queue_length_usecs = 0;
  _device_in_message_uid_acked = 0;
  _step_base_position = 0;

  _step_current_num_peeks = 0;
  _step_current.reset ();
  _step_current_complete.reset ();
  _step_latest.reset ();
}

void
Axis::session_end ()
{
  _steps_track_list.clear ();

  _step_current_num_peeks = 0;
  _step_current.reset ();
  _step_current_complete.reset ();
  _step_latest.reset ();
}

void
Axis::device_out_track_steps (
    const snc::emb::msg::out::Steps_U16_64 & message_n )
{
  switch ( message_n.job_type () ) {

  case snc::emb::type::Step::HOLD:
    _step_latest.num_usecs += message_n.usecs_sum ();
    device_out_track_steps_each ( message_n );
    break;

  case snc::emb::type::Step::BACKWARD: {
    const std::int64_t num_values = message_n.num_values ();
    _step_latest.position -= num_values;
    _step_latest.count += num_values;
    _step_latest.is_valid = true;
    _step_latest.is_forward = false;
    _step_latest.num_usecs = message_n.usecs_sum ();
    device_out_track_steps_each ( message_n );
  } break;

  case snc::emb::type::Step::FORWARD: {
    const std::int64_t num_values = message_n.num_values ();
    _step_latest.position += num_values;
    _step_latest.count += num_values;
    _step_latest.is_valid = true;
    _step_latest.is_forward = true;
    _step_latest.num_usecs = message_n.usecs_sum ();
    device_out_track_steps_each ( message_n );
  } break;

  case snc::emb::type::Step::SWITCH_SET_LOW:
  case snc::emb::type::Step::SWITCH_SET_HIGH: {
    Step_Track strack;
    strack.message_uid = message_n.message_uid ();
    strack.job_type = message_n.job_type ();
    strack.usecs = 0;
    for ( std::size_t ii = 0; ii != message_n.num_values (); ++ii ) {
      _steps_track_list.push_back ( strack );
    }
  } break;

  default:
    DEBUG_ASSERT ( false );
    break;
  }
}

void
Axis::device_out_track_stepper_sync (
    const snc::emb::msg::out::Stepper_Sync & message_n )
{
  // Sync messages use one slot in the step queue
  {
    Step_Track strack;
    strack.message_uid = message_n.message_uid ();
    strack.job_type = snc::emb::type::Step::INVALID;
    strack.usecs = 0;
    _steps_track_list.push_back ( strack );
  }
}

inline void
Axis::device_out_track_steps_each (
    const snc::emb::msg::out::Steps_U16_64 & msg_n )
{
  DEBUG_ASSERT ( msg_n.job_type () != snc::emb::type::Step::INVALID );
  DEBUG_ASSERT ( msg_n.job_type () < snc::emb::type::Step::LIST_END );
  {
    Step_Track strack;
    strack.message_uid = msg_n.message_uid ();
    strack.job_type = msg_n.job_type ();
    for ( std::size_t ii = 0; ii != msg_n.num_values (); ++ii ) {
      strack.usecs = msg_n.usecs ( ii );
      _steps_track_list.push_back ( strack );
      _steps_queue_length_usecs += strack.usecs;
    }
  }
}

void
Axis::device_in_queue_length ( std::size_t device_queue_length_n )
{
  std::size_t num_acked_values = _steps_track_list.size ();
  {
    Steps_Track_List::Spans tiles = _steps_track_list.spans ();

    auto find = [ &num_acked_values,
                  acked_uid = device_in_message_uid_acked () ] (
                    const Steps_Track_List::Span & tile_n ) -> bool {
      auto it_end = tile_n.begin ();
      auto it = tile_n.end ();
      --it_end;
      --it;
      for ( ; it != it_end; --it ) {
        if ( it->message_uid == acked_uid ) {
          return true;
        }
        DEBUG_ASSERT ( num_acked_values != 0 );
        --num_acked_values;
      }
      return false;
    };

    if ( !find ( tiles[ 1 ] ) && !find ( tiles[ 0 ] ) ) {
      return;
    }
  }

  if ( num_acked_values < device_queue_length_n ) {
    // There are more steps in the device than were sent.
    // TODO: Error handling
    return;
  }

  // Untrack all steps that are not in the device anymore
  num_acked_values -= device_queue_length_n;
  for ( ; num_acked_values != 0; --num_acked_values ) {
    device_in_untrack_step ();
  }

  device_in_current_step_forward_peek ();

  if ( _steps_track_list.is_empty () ) {
    DEBUG_ASSERT ( _step_current_num_peeks == 0 );
    _step_current.invalidate ();
    _step_current_complete.invalidate ();
    _step_latest.invalidate ();
  }
}

inline void
Axis::device_in_untrack_step ()
{
  DEBUG_ASSERT ( !_steps_track_list.is_empty () );

  const Step_Track & strack = _steps_track_list.front ();
  const std::uint_fast32_t cstep_usecs = strack.usecs;
  switch ( strack.job_type ) {

  case snc::emb::type::Step::HOLD:
    DEBUG_ASSERT ( _steps_queue_length_usecs >= cstep_usecs );
    sev::math::subtract_minimum_zero ( _steps_queue_length_usecs, cstep_usecs );
    if ( _step_current.is_valid ) {
      if ( _step_current_num_peeks == 0 ) {
        // This hold job hasn't been forward peeked
        _step_current.num_usecs += cstep_usecs;
      } else {
        DEBUG_ASSERT ( _step_current_num_peeks <= _steps_track_list.size () );
        --_step_current_num_peeks;
      }
    } else {
      DEBUG_ASSERT ( _step_current_num_peeks == 0 );
    }
    break;

  case snc::emb::type::Step::BACKWARD:
    // The last step is complete
    _step_current_complete = _step_current;
    // Update current step
    --_step_current.position;
    ++_step_current.count;
    DEBUG_ASSERT ( _steps_queue_length_usecs >= cstep_usecs );
    sev::math::subtract_minimum_zero ( _steps_queue_length_usecs, cstep_usecs );
    DEBUG_ASSERT ( _step_current_num_peeks == 0 );
    {
      _step_current.is_valid = true;
      _step_current.is_forward = false;
      _step_current.num_usecs = cstep_usecs;
    }
    break;

  case snc::emb::type::Step::FORWARD:
    // The last step is complete
    _step_current_complete = _step_current;
    // Update current step
    ++_step_current.position;
    ++_step_current.count;
    DEBUG_ASSERT ( _steps_queue_length_usecs >= cstep_usecs );
    sev::math::subtract_minimum_zero ( _steps_queue_length_usecs, cstep_usecs );
    DEBUG_ASSERT ( _step_current_num_peeks == 0 );
    {
      _step_current.is_valid = true;
      _step_current.is_forward = true;
      _step_current.num_usecs = cstep_usecs;
    }
    break;

  default:
    break;
  }
  _steps_track_list.pop_front_not_empty ();
}

inline void
Axis::device_in_current_step_forward_peek ()
{
  DEBUG_ASSERT ( _step_current_num_peeks <= _steps_track_list.size () );
  if ( !_step_current.is_valid ||
       ( _step_current_num_peeks >= _steps_track_list.size () ) ) {
    return;
  }

  // Accumulate subsequent hold steps
  for ( std::uint_fast32_t ii = _step_current_num_peeks,
                           size = _steps_track_list.size ();
        ii != size;
        ++ii ) {
    const Step_Track & strack = _steps_track_list[ ii ];
    switch ( strack.job_type ) {
    case snc::emb::type::Step::HOLD: {
      // Current step gets extended with a hold job
      _step_current.num_usecs += strack.usecs;
      ++_step_current_num_peeks;
    } break;
    default:
      return;
    }
  }
}

} // namespace snc::svs::fac::tracker::ship::emb
