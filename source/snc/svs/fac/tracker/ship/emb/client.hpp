/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/office/client.hpp>
#include <snc/svs/fac/tracker/ship/emb/local_axis.hpp>
#include <cstdint>

namespace snc::svs::fac::tracker::ship::emb
{

// -- Forward declaration
class Service;

/// @brief Client for device info access
///
class Client : public snc::svs::fac::base::office::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Client;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client () override;

  // -- Interface

  void
  set_axis_aligned ( std::uint_fast32_t axis_index_n, bool aligned_n ) const;

  snc::svs::fac::tracker::ship::emb::Local_Axis
  local_axis_tracker ( std::uint_fast32_t axis_index_n ) const;

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::tracker::ship::emb
