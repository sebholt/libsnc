/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include "axis.hpp"
#include <sev/math/numbers.hpp>
#include <snc/emb/msg/out/steps_u16_64.hpp>
#include <cstdint>

namespace snc::svs::fac::tracker::ship::emb
{

class Local_Axis
{
  public:
  // -- Construction

  Local_Axis () = default;

  Local_Axis ( const Axis & axis_n ) { init ( axis_n ); }

  // -- Setup

  void
  reset ()
  {
    _queue_slots_free = 0;
    _queue_usecs = 0;
  }

  void
  init ( const Axis & axis_n )
  {
    _queue_slots_free = axis_n.steps_queue_length_free ();
    _queue_usecs = axis_n.steps_queue_length_usecs ();
  }

  // -- Accessors

  bool
  queue_full () const
  {
    return ( queue_slots_free () == 0 );
  }

  std::uint_fast32_t
  queue_slots_free () const
  {
    return _queue_slots_free;
  }

  std::uint_fast32_t
  queue_usecs () const
  {
    return _queue_usecs;
  }

  // -- Tracking

  void
  track ( std::uint_fast32_t num_values_n )
  {
    DEBUG_ASSERT ( num_values_n <= _queue_slots_free );
    sev::math::subtract_minimum_zero ( _queue_slots_free, num_values_n );
  }

  void
  track ( std::uint_fast32_t num_values_n, std::uint_fast32_t usecs_n )
  {
    DEBUG_ASSERT ( num_values_n <= _queue_slots_free );
    sev::math::subtract_minimum_zero ( _queue_slots_free, num_values_n );
    _queue_usecs += usecs_n;
  }

  void
  track ( const snc::emb::msg::out::Steps_U16_64 & msg_n )
  {
    track ( msg_n.num_values (), msg_n.usecs_sum () );
  }

  private:
  // -- Attributes
  std::uint_fast32_t _queue_slots_free = 0;
  std::uint_fast32_t _queue_usecs = 0;
};

} // namespace snc::svs::fac::tracker::ship::emb
