/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "axis.hpp"
#include <estepper/segment/types.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/est/config/stepper.hpp>
#include <snc/est/stepper_data_composer.hpp>

namespace snc::svs::fac::tracker::ship::est
{

Axis::Axis ( const sev::logt::Reference & log_parent_n,
             std::uint_fast32_t index_n,
             const snc::device::handle::Info_Writeable & device_info_n )
: Super ( log_parent_n,
          sev::string::cat ( "Axis[", index_n, "]" ),
          index_n,
          device_info_n )
, _axis_statics ( device_statics ()->axes ().get ( index_n ) )
, _axis_state ( device_state_ref ().axis_shared_writeable ( index_n ) )
{
  _est.direction_reversed = _axis_statics->est ().direction_reversed ();
  _est.tick_frequency =
      _axis_statics->est ().stepper_config ()->tick_frequency ();
  _length_per_step = _axis_statics->geo ().length_per_step ();
  _steps_queue.set_capacity (
      _axis_statics->est ().stepper_config ()->steps_queue_capacity () );
}

Axis::~Axis () = default;

void
Axis::set_aligned ( bool flag_n )
{
  if ( flag_n ) {
    std::int64_t bpos = _step_current.position;
    if ( axis_statics ()->sensors ().high_end () ) {
      std::int64_t const pdelta = axis_statics ()->step_index_max ();
      if ( _est.direction_reversed ) {
        bpos += pdelta;
      } else {
        bpos -= pdelta;
      }
    }
    _step_base_position = bpos;
  }
  axis_state ()->set_is_aligned ( flag_n );

  update_state ();
}

void
Axis::device_out_message_stepper_enable ()
{
  axis_state ()->set_is_enabled ( true );
}

void
Axis::device_out_message_stepper_disable ()
{
}

void
Axis::device_out_stepper_data (
    snc::est::Stepper_Data_Composer const & composer_n )
{
  if ( composer_n.size () <= _steps_queue.capacity_free () ) {
    _steps_queue.append_copy_not_full ( composer_n.begin (),
                                        composer_n.end () );
  } else {
    throw std::length_error ( "Steps queue overflow" );
  }

  std::uint16_t const * it_end = composer_n.end ();
  std::uint16_t const * it_cur = composer_n.begin ();
  while ( it_cur != it_end ) {
    // New segment
    std::uint8_t const seg_type = *it_cur;
    std::uint8_t const seg_count = ( *it_cur ) >> 8;
    ++it_cur;
    if ( seg_count != 0 ) {
      std::uint16_t const * it_seg_end = it_cur + seg_count;
      DEBUG_ASSERT ( ( it_end - it_seg_end ) >= 0 );
      switch ( seg_type ) {
      case estepper::segment::MOTOR_SLEEP:
        for ( ; it_cur != it_seg_end; ++it_cur ) {
          _step_latest.num_ticks += *it_cur;
          _steps_queue_length_ticks += *it_cur;
        }
        break;

      case estepper::segment::MOTOR_FORWARD:
        _step_latest.position += seg_count;
        _step_latest.count += seg_count;
        _step_latest.is_valid = true;
        _step_latest.is_backward = false;
        _step_latest.num_ticks = *( it_seg_end - 1 );
        for ( ; it_cur != it_seg_end; ++it_cur ) {
          _steps_queue_length_ticks += *it_cur;
        }
        break;

      case estepper::segment::MOTOR_BACKWARD:
        _step_latest.position -= seg_count;
        _step_latest.count += seg_count;
        _step_latest.is_valid = true;
        _step_latest.is_backward = true;
        _step_latest.num_ticks = *( it_seg_end - 1 );
        for ( ; it_cur != it_seg_end; ++it_cur ) {
          _steps_queue_length_ticks += *it_cur;
        }
        break;
      default:
        throw std::runtime_error ( "Bad new motor stepper segment type" );
      }
    }
  }

  update_state ();
}

void
Axis::device_in_message_stepper_words_processed ( std::uint16_t num_n )
{
  std::size_t num = num_n;
  // Check if the reported count is reasonable
  if ( num > _steps_queue.size () ) {
    num = _steps_queue.size ();
    if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
      logo << "EStepper IN: More words reported as processed (" << num_n
           << ") than there are words in the motor stepper queue ("
           << _steps_queue.size () << ")";
    }
    DEBUG_ASSERT ( false );
  }

  for ( std::size_t ii = 0; ii != num; ++ii ) {
    std::uint16_t const value = _steps_queue.front ();
    _steps_queue.pop_front_not_empty ();
    if ( _segment.count == 0 ) {
      // New segment
      _segment.type = value;
      _segment.count = ( value >> 8 );
      if ( _forward_peek_count != 0 ) {
        --_forward_peek_count;
        DEBUG_ASSERT ( _segment.type == estepper::segment::MOTOR_SLEEP );
      }
    } else {
      // Inside segment
      --_segment.count;
      switch ( _segment.type ) {
      case estepper::segment::MOTOR_SLEEP:
        if ( _forward_peek_count != 0 ) {
          --_forward_peek_count;
        } else {
          _step_current.num_ticks += value;
        }
        // Update steps_queue_length_ticks
        DEBUG_ASSERT ( _steps_queue_length_ticks >= value );
        _steps_queue_length_ticks -= value;
        break;

      case estepper::segment::MOTOR_FORWARD:
        DEBUG_ASSERT ( _forward_peek_count == 0 );
        // The last step is complete
        _step_current_complete = _step_current;
        // Update current step
        ++_step_current.position;
        ++_step_current.count;
        _step_current.is_valid = true;
        _step_current.is_backward = false;
        _step_current.num_ticks = value;
        // Update steps_queue_length_ticks
        DEBUG_ASSERT ( _steps_queue_length_ticks >= value );
        _steps_queue_length_ticks -= value;
        if ( _segment.count == 0 ) {
          forward_peek ();
        }
        break;

      case estepper::segment::MOTOR_BACKWARD:
        DEBUG_ASSERT ( _forward_peek_count == 0 );
        // The last step is complete
        _step_current_complete = _step_current;
        // Update current step
        --_step_current.position;
        ++_step_current.count;
        _step_current.is_valid = true;
        _step_current.is_backward = true;
        _step_current.num_ticks = value;
        // Update steps_queue_length_ticks
        DEBUG_ASSERT ( _steps_queue_length_ticks >= value );
        _steps_queue_length_ticks -= value;
        if ( _segment.count == 0 ) {
          forward_peek ();
        }
        break;

      default:
        throw std::runtime_error ( "Bad processed motor stepper segment type" );
      }
    }
  }

  update_state ();
}

void
Axis::device_in_message_stepper_disabled ()
{
  axis_state ()->set_is_enabled ( false );
  DEBUG_ASSERT ( _steps_queue.is_empty () );
  // Invalidate steps
  _step_current.invalidate ();
  _step_current_complete.invalidate ();
  _step_latest.invalidate ();
  // Update state
  update_state ();
}

void
Axis::forward_peek ()
{
  std::uint8_t seg_type = 0;
  std::uint8_t seg_count = 0;

  // Accumulate subsequent sleep steps
  for ( std::uint_fast32_t ii = 0, size = _steps_queue.size (); ii != size;
        ++ii ) {
    std::uint16_t const value = _steps_queue[ ii ];
    if ( seg_count == 0 ) {
      seg_type = value;
      seg_count = ( value >> 8 );
      DEBUG_ASSERT ( seg_count <= ( size - ii ) );
      if ( seg_type == estepper::segment::MOTOR_SLEEP ) {
        ++_forward_peek_count;
      } else {
        break;
      }
    } else {
      --seg_count;
      ++_forward_peek_count;
      _step_current.num_ticks += value;
    }
  }
}

void
Axis::update_state ()
{
  auto & state = *axis_state ();
  // Steps queue
  state.set_steps_queue_length ( _steps_queue.size () );
  state.set_steps_queue_ticks ( _steps_queue_length_ticks );

  // Step count
  {
    state.step_current ().set_count ( _step_current.count );
    state.step_current_complete ().set_count ( _step_current_complete.count );
    state.step_latest ().set_count ( _step_latest.count );
  }
  // Step position
  {
    std::array< std::int64_t, 3 > step_pos;
    step_pos[ 0 ] = _step_current.position;
    step_pos[ 1 ] = _step_current_complete.position;
    step_pos[ 2 ] = _step_latest.position;
    {
      std::int64_t const base = _step_base_position;
      if ( _est.direction_reversed ) {
        for ( auto & spos : step_pos ) {
          spos = base - spos;
        }
      } else {
        for ( auto & spos : step_pos ) {
          spos = spos - base;
        }
      }
    }
    state.step_current ().set_position ( step_pos[ 0 ] );
    state.step_current_complete ().set_position ( step_pos[ 1 ] );
    state.step_latest ().set_position ( step_pos[ 2 ] );
  }
  // Step speed
  {
    auto steps_per_second = [ tick_freqeuncy = _est.tick_frequency ] (
                                Axis_Step const & step_n ) -> double {
      if ( step_n.is_valid && ( step_n.num_ticks != 0 ) ) {
        return ( tick_freqeuncy / double ( step_n.num_ticks ) );
      }
      return 0.0;
    };

    std::array< double, 3 > speeds;
    speeds[ 0 ] = steps_per_second ( _step_current );
    if ( _step_current.is_backward != _est.direction_reversed ) {
      speeds[ 0 ] = -speeds[ 0 ];
    }
    speeds[ 1 ] = steps_per_second ( _step_current_complete );
    if ( _step_current_complete.is_backward != _est.direction_reversed ) {
      speeds[ 1 ] = -speeds[ 1 ];
    }
    speeds[ 2 ] = steps_per_second ( _step_latest );
    if ( _step_latest.is_backward != _est.direction_reversed ) {
      speeds[ 2 ] = -speeds[ 2 ];
    }
    state.step_current ().set_speed ( speeds[ 0 ] );
    state.step_current_complete ().set_speed ( speeds[ 1 ] );
    state.step_latest ().set_speed ( speeds[ 2 ] );
  }
  // Length position and speed
  {
    std::array< double, 6 > len_vals;
    {
      const auto & step = state.step_current ();
      len_vals[ 0 ] = step.position ();
      len_vals[ 1 ] = step.speed ();
    }
    {
      const auto & step = state.step_current_complete ();
      len_vals[ 2 ] = step.position ();
      len_vals[ 3 ] = step.speed ();
    }
    {
      const auto & step = state.step_latest ();
      len_vals[ 4 ] = step.position ();
      len_vals[ 5 ] = step.speed ();
    }
    // Scale length
    for ( auto & val : len_vals ) {
      val *= _length_per_step;
    }
    {
      auto & kin = state.kinematics_current ();
      kin.set_position ( len_vals[ 0 ] );
      kin.set_speed ( len_vals[ 1 ] );
    }
    {
      auto & kin = state.kinematics_current_complete ();
      kin.set_position ( len_vals[ 2 ] );
      kin.set_speed ( len_vals[ 3 ] );
    }
    {
      auto & kin = state.kinematics_latest ();
      kin.set_position ( len_vals[ 4 ] );
      kin.set_speed ( len_vals[ 5 ] );
    }
  }
}

} // namespace snc::svs::fac::tracker::ship::est
