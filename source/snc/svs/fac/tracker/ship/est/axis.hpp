/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/ring_fixed.hpp>
#include <snc/device/handle.hpp>
#include <snc/svs/fac/tracker/ship/est/axis_step.hpp>
#include <snc/svs/fac/tracker/ship/est/stepper.hpp>
#include <cstdint>

namespace snc::svs::fac::tracker::ship::est
{

/// @brief Axis stepper tracker
///
class Axis : public Stepper
{
  // -- Types
  private:
  using Super = Stepper;
  using Steps_Queue = sev::mem::Ring_Fixed< std::uint16_t >;

  public:
  // -- Construction

  Axis ( const sev::logt::Reference & log_parent_n,
         std::uint_fast32_t index_n,
         const snc::device::handle::Info_Writeable & device_info_n );

  ~Axis () override;

  // -- Axis info

  const snc::device::statics::handle::Axis &
  axis_statics () const
  {
    return _axis_statics;
  }

  const snc::device::state::handle::Axis_Writeable &
  axis_state () const
  {
    return _axis_state;
  }

  // -- Interface

  void
  set_aligned ( bool flag_n );

  // -- Device OUT tracking interface

  void
  device_out_message_stepper_enable () override;

  void
  device_out_message_stepper_disable () override;

  void
  device_out_stepper_data (
      snc::est::Stepper_Data_Composer const & composer_n ) override;

  // -- Device IN tracking interface

  void
  device_in_message_stepper_words_processed ( std::uint16_t num_n ) override;

  void
  device_in_message_stepper_disabled () override;

  private:
  // -- Utility

  void
  forward_peek ();

  void
  update_state ();

  private:
  snc::device::statics::handle::Axis _axis_statics;
  snc::device::state::handle::Axis_Writeable _axis_state;
  double _length_per_step = 0.0;
  struct
  {
    bool direction_reversed = false;
    double tick_frequency = 0.0;
  } _est;
  /// @brief The step queue mirrors the step queue in the estepper device
  Steps_Queue _steps_queue;
  /// @brief Number of ticks in the queue
  std::uint_fast32_t _steps_queue_length_ticks = 0;
  /// @brief Forward peek count
  std::uint_fast32_t _forward_peek_count = 0;
  /// @brief Base step position on an aligned axis
  std::int64_t _step_base_position = 0;
  /// @brief Segment tracking for queue front removal
  struct
  {
    std::uint8_t type = 0;
    std::uint8_t count = 0;
  } _segment;
  /// @brief Step current (acknowledged by the device)
  Axis_Step _step_current;
  /// @brief Step current acknowledged by the device and completed
  Axis_Step _step_current_complete;
  /// @brief Step latest (latest step in the queue)
  Axis_Step _step_latest;
};

} // namespace snc::svs::fac::tracker::ship::est
