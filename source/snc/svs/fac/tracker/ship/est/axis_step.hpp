/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <cstdint>

namespace snc::svs::fac::tracker::ship::est
{

class Axis_Step
{
  // -- Construction and setup
  public:
  Axis_Step () = default;

  void
  reset ()
  {
    is_valid = false;
    is_backward = false;
    num_ticks = 0;
    position = 0;
    count = 0;
  }

  void
  invalidate ()
  {
    is_valid = false;
  }

  public:
  // -- Attributes
  bool is_valid = false;
  bool is_backward = false;
  std::uint_fast32_t num_ticks = 0;
  std::int64_t position = 0;
  std::uint64_t count = 0;
};

} // namespace snc::svs::fac::tracker::ship::est
