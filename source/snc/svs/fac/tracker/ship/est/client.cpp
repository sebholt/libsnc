/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/fac/tracker/ship/est/service.hpp>

namespace snc::svs::fac::tracker::ship::est
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

void
Client::track_stepper_data_out (
    snc::est::Stepper_Data_Composer const & composer_n )
{
  if ( is_connected () ) {
    service ()->device_out_track_stepper_data ( composer_n );
  }
}

void
Client::track_message_out ( std::uint8_t const * message_n )
{
  if ( is_connected () ) {
    service ()->device_out_track_message ( message_n );
  }
}

void
Client::set_axis_aligned ( std::uint_fast32_t axis_index_n,
                           bool aligned_n ) const
{
  if ( is_connected () ) {
    service ()->set_axis_aligned ( axis_index_n, aligned_n );
  }
}

inline Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::fac::tracker::ship::est
