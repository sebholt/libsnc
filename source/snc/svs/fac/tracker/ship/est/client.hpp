/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svs/fac/base/office/client.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::est
{
class Stepper_Data_Composer;
}

namespace snc::svs::fac::tracker::ship::est
{

// -- Forward declaration
class Service;

/// @brief Client for device info access
///
class Client : public snc::svs::fac::base::office::Client
{
  // -- Types
  private:
  using Super = snc::svs::fac::base::office::Client;

  public:
  // -- Construction

  Client ( User * user_n );

  ~Client () override;

  // -- Interface

  void
  track_stepper_data_out ( snc::est::Stepper_Data_Composer const & composer_n );

  void
  track_message_out ( std::uint8_t const * message_n );

  void
  set_axis_aligned ( std::uint_fast32_t axis_index_n, bool aligned_n ) const;

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;
};

} // namespace snc::svs::fac::tracker::ship::est
