/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/math/exponents.hpp>
#include <sev/mem/serial.hpp>
#include <sev/mem/view.hpp>
#include <snc/device/statics/axis.hpp>
#include <snc/device/statics/control/i1.hpp>
#include <snc/device/statics/control/i16.hpp>
#include <snc/device/statics/control/i32.hpp>
#include <snc/device/statics/control/i64.hpp>
#include <snc/device/statics/control/i8.hpp>
#include <snc/device/statics/sensor/i1.hpp>
#include <snc/device/statics/sensor/i16.hpp>
#include <snc/device/statics/sensor/i32.hpp>
#include <snc/device/statics/sensor/i64.hpp>
#include <snc/device/statics/sensor/i8.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/device/statics/stepper_controls.hpp>
#include <snc/est/stepper_data_composer.hpp>
#include <snc/svs/fac/tracker/ship/est/axis.hpp>
#include <snc/svs/fac/tracker/ship/est/sailor.hpp>
#include <snc/svs/fac/tracker/ship/est/stepper.hpp>
#include <snc/svs/fac/tracker/ship/est/stepper_controls.hpp>
#include <snc/utility/data_tile.hpp>
#include <stdexcept>

namespace snc::svs::fac::tracker::ship::est
{

namespace
{
// -- Anonymous callbacks
void
set_io_stats_anon ( snc::bat::ifc::Service_Abstract * service_n,
                    const snc::serial_device::Stats_IO & stats_n )
{
  static_cast< Service * > ( service_n )->set_io_stats ( stats_n );
}

void
data_in_anon ( snc::bat::ifc::Service_Abstract * service_n,
               const snc::utility::Data_Tile * tile_n )
{
  static_cast< Service * > ( service_n )->device_in_process_data ( tile_n );
}

void
abort_anon ( snc::bat::ifc::Service_Abstract * service_n [[maybe_unused]] )
{
}

} // namespace

Service::Service ( Provider & provider_n,
                   const snc::device::handle::Info_Writeable & device_info_n )
: Super ( provider_n )
, _device_info ( device_info_n )
, _serial ( &provider_cell () )
{
  // -- Office event generation timer
  _office_egen_interval =
      std::chrono::microseconds ( sev::math::mega_ull / 60 );
  _office_egen_timer.connect (
      provider_cell ().cell_context ().timer_queue () );
  _office_egen_timer.set_callback ( [ &oio = provider ().office_io () ] () {
    oio.notification_set ( Sailor::Office_Message::DEVICE_STATE );
  } );

  // -- Setup outgoing message handlers
  {
    auto & hdl = _device_out.handlers;
    hdl.fill ( &Service::device_out_message_default );
    hdl[ estepper::out::PING ] = &Service::device_out_message_ping;
    hdl[ estepper::out::CONTROL_1 ] = &Service::device_out_message_control_1;
    hdl[ estepper::out::CONTROL_8 ] = &Service::device_out_message_control_8;
    hdl[ estepper::out::CONTROL_16 ] = &Service::device_out_message_control_16;
    hdl[ estepper::out::CONTROL_32 ] = &Service::device_out_message_control_32;
    hdl[ estepper::out::CONTROL_64 ] = &Service::device_out_message_control_64;
    hdl[ estepper::out::STEPPER_ENABLE ] =
        &Service::device_out_message_stepper_enable;
    hdl[ estepper::out::STEPPER_DISABLE ] =
        &Service::device_out_message_stepper_disable;
    hdl[ estepper::out::STEPPER_DATA ] =
        &Service::device_out_message_stepper_data;
  }

  // -- Setup incoming message handlers
  {
    auto & hdl = _device_in.handlers;
    hdl.fill ( &Service::device_in_message_default );
    hdl[ estepper::in::ERROR ] = &Service::device_in_message_error;
    hdl[ estepper::in::UNKNOWN_MESSAGE_TYPE ] =
        &Service::device_in_message_unknown_message_type;
    hdl[ estepper::in::INCOMPLETE_MESSAGE ] =
        &Service::device_in_message_incomplete_message;
    hdl[ estepper::in::INVALID_CONTROL_INDEX ] =
        &Service::device_in_message_invalid_control_index;
    hdl[ estepper::in::INVALID_STEPPER_INDEX ] =
        &Service::device_in_message_invalid_stepper_index;
    hdl[ estepper::in::STEPPER_BUFFER_OVERFLOW ] =
        &Service::device_in_message_stepper_buffer_overflow;
    hdl[ estepper::in::STEPPER_BAD_SEGMENT ] =
        &Service::device_in_message_stepper_bad_segment;
    hdl[ estepper::in::PONG ] = &Service::device_in_message_pong;
    hdl[ estepper::in::SENSOR_1 ] = &Service::device_in_message_sensor_1;
    hdl[ estepper::in::SENSOR_8 ] = &Service::device_in_message_sensor_8;
    hdl[ estepper::in::SENSOR_16 ] = &Service::device_in_message_sensor_16;
    hdl[ estepper::in::SENSOR_32 ] = &Service::device_in_message_sensor_32;
    hdl[ estepper::in::SENSOR_64 ] = &Service::device_in_message_sensor_64;
    hdl[ estepper::in::CONTROL_1 ] = &Service::device_in_message_control_1;
    hdl[ estepper::in::CONTROL_8 ] = &Service::device_in_message_control_8;
    hdl[ estepper::in::CONTROL_16 ] = &Service::device_in_message_control_16;
    hdl[ estepper::in::CONTROL_32 ] = &Service::device_in_message_control_32;
    hdl[ estepper::in::CONTROL_64 ] = &Service::device_in_message_control_64;
    hdl[ estepper::in::STEPPER_WORDS_PROCESSED ] =
        &Service::device_in_message_stepper_words_processed;
    hdl[ estepper::in::STEPPER_DISABLED ] =
        &Service::device_in_message_stepper_disabled;
  }

  auto resize_map = [] ( auto & map_n,
                         std::uint_fast32_t est_index_n ) -> bool {
    std::uint_fast32_t const max_est_items = 256;
    if ( est_index_n >= max_est_items ) {
      return false;
    }
    if ( est_index_n >= map_n.size () ) {
      map_n.resize ( est_index_n + 1 );
    }
    return true;
  };

  // -- Setup sensor mappings
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_sensors_1;
    for ( const auto & item : device_statics ()->sensors_i1 ().items () ) {
      std::uint_fast32_t est_index = item->est ().sensor_index ();
      if ( resize_map ( map, est_index ) ) {
        auto & imap = map[ est_index ];
        imap.first = device_state_ref ().sensor_i1_writeable_shared ( index );
        imap.second = item->est ().inverted ();
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_sensors_8;
    for ( const auto & item : device_statics ()->sensors_i8 ().items () ) {
      std::uint_fast32_t est_index = item->est ().sensor_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().sensor_i8_writeable_shared ( index );
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_sensors_16;
    for ( const auto & item : device_statics ()->sensors_i16 ().items () ) {
      std::uint_fast32_t est_index = item->est ().sensor_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().sensor_i16_writeable_shared ( index );
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_sensors_32;
    for ( const auto & item : device_statics ()->sensors_i32 ().items () ) {
      std::uint_fast32_t est_index = item->est ().sensor_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().sensor_i32_writeable_shared ( index );
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_sensors_64;
    for ( const auto & item : device_statics ()->sensors_i64 ().items () ) {
      std::uint_fast32_t est_index = item->est ().sensor_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().sensor_i64_writeable_shared ( index );
      }
      ++index;
    }
  }

  // -- Setup control mappings
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_controls_1;
    for ( const auto & item : device_statics ()->controls_i1 ().items () ) {
      std::uint_fast32_t est_index = item->est ().control_index ();
      if ( resize_map ( map, est_index ) ) {
        auto & imap = map[ est_index ];
        imap.first = device_state_ref ().control_i1_writeable_shared ( index );
        imap.second = item->est ().inverted ();
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_controls_8;
    for ( const auto & item : device_statics ()->controls_i8 ().items () ) {
      std::uint_fast32_t est_index = item->est ().control_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().control_i8_writeable_shared ( index );
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_controls_16;
    for ( const auto & item : device_statics ()->controls_i16 ().items () ) {
      std::uint_fast32_t est_index = item->est ().control_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().control_i16_writeable_shared ( index );
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_controls_32;
    for ( const auto & item : device_statics ()->controls_i32 ().items () ) {
      std::uint_fast32_t est_index = item->est ().control_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().control_i32_writeable_shared ( index );
      }
      ++index;
    }
  }
  {
    std::uint_fast32_t index = 0;
    auto & map = _device_in.map_controls_64;
    for ( const auto & item : device_statics ()->controls_i64 ().items () ) {
      std::uint_fast32_t est_index = item->est ().control_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] =
            device_state_ref ().control_i64_writeable_shared ( index );
      }
      ++index;
    }
  }

  // -- Setup stepper mappings
  {
    std::size_t num = device_statics ()->axes ().size ();
    _steppers.axes.reserve ( num );
    for ( std::uint_fast32_t ii = 0; ii != num; ++ii ) {
      _steppers.axes.push_back (
          std::make_shared< Axis > ( log (), ii, device_info () ) );
    }
  }
  {
    std::size_t num = device_statics ()->steppers_controls ().size ();
    _steppers.steppers_controls.reserve ( num );
    for ( std::uint_fast32_t ii = 0; ii != num; ++ii ) {
      _steppers.steppers_controls.push_back (
          std::make_shared< Stepper_Controls > ( log (), ii, device_info () ) );
    }
  }
  {
    auto & map = _steppers.esteppers;
    for ( const auto & item : _steppers.axes ) {
      std::size_t est_index = item->axis_statics ()->est ().stepper_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] = std::shared_ptr< Stepper > (
            item, static_cast< Stepper * > ( item.get () ) );
      }
    }
    for ( const auto & item : _steppers.steppers_controls ) {
      std::size_t est_index = item->stepper_statics ()->est ().stepper_index ();
      if ( resize_map ( map, est_index ) ) {
        map[ est_index ] = std::shared_ptr< Stepper > (
            item, static_cast< Stepper * > ( item.get () ) );
      }
    }
  }
}

Service::~Service () = default;

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

void
Service::session_begin ()
{
  _serial.connect_required ();
  _serial.set_io_stats_cback ( this, &set_io_stats_anon );
  _serial.set_data_in_cback ( this, &data_in_anon );
  _serial.set_abort_cback ( this, &abort_anon );
}

void
Service::session_abort ()
{
  _office_egen_timer.stop ();
}

void
Service::session_end ()
{
}

void
Service::request_device_state_message ()
{
  // Return if generation is scheduled already
  if ( _office_egen_timer.is_running () ) {
    return;
  }

  // Start timer
  auto tnow = std::chrono::steady_clock::now ();
  auto elapsed = tnow - _office_egen_timer.point ();
  if ( elapsed <= _office_egen_interval ) {
    // Wait for the rest of the interval
    auto remain = _office_egen_interval - elapsed;
    _office_egen_timer.start_single_point ( tnow + remain );
  } else {
    // The interval has elapsed already.
    _office_egen_timer.set_point ( tnow );
    provider ().office_io ().notification_set (
        Sailor::Office_Message::DEVICE_STATE );
  }
}

void
Service::set_axis_aligned ( std::size_t axis_index_n, bool flag_n )
{
  if ( axis_index_n < _steppers.axes.size () ) {
    _steppers.axes[ axis_index_n ]->set_aligned ( flag_n );
  }

  // Send device state immediately
  provider ().office_io ().notification_set (
      Sailor::Office_Message::DEVICE_STATE );
}

void
Service::set_io_stats ( const snc::serial_device::Stats_IO & stats_n )
{
  device_state_ref ().io_stats ().set_serial ( stats_n );
  // Request state message
  request_device_state_message ();
}

void
Service::device_out_track_stepper_data (
    snc::est::Stepper_Data_Composer const & composer_n )
{
  std::size_t const stepper_index = composer_n.stepper_index ();
  auto & map = _steppers.esteppers;
  if ( stepper_index < map.size () ) {
    if ( const auto & est = map[ stepper_index ] ) {
      est->device_out_stepper_data ( composer_n );
    }
  }
  request_device_state_message ();
}

void
Service::device_out_track_message ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  if ( carry_bytes == 0 ) {
    // Ignore zero length message
  } else {
    std::size_t const message_type = message_n[ 1 ];
    if ( message_type <= _device_out.handlers.size () ) {
      // Call message handler
      ( this->*_device_out.handlers[ message_type ] ) ( message_n );
    } else {
      // Unknown or bad message type: Call default handler.
      device_out_message_default ( message_n );
    }
  }
  request_device_state_message ();
}

void
Service::device_out_message_default ( std::uint8_t const * message_n )
{
  log ().cat ( sev::logt::FL_WARNING,
               "EStepper OUT: Unhandled message type: ",
               std::uint_fast32_t ( message_n[ 1 ] ) );
}

void
Service::device_out_message_ping ( std::uint8_t const * message_n
                                   [[maybe_unused]] )
{
  if ( _ping.running ) {
    return;
  }
  _ping.running = true;
  _ping.start_time = std::chrono::steady_clock::now ();
}

void
Service::device_out_message_control_1 ( std::uint8_t const * message_n
                                        [[maybe_unused]] )
{
}

void
Service::device_out_message_control_8 ( std::uint8_t const * message_n
                                        [[maybe_unused]] )
{
}

void
Service::device_out_message_control_16 ( std::uint8_t const * message_n
                                         [[maybe_unused]] )
{
}

void
Service::device_out_message_control_32 ( std::uint8_t const * message_n
                                         [[maybe_unused]] )
{
}

void
Service::device_out_message_control_64 ( std::uint8_t const * message_n
                                         [[maybe_unused]] )
{
}

void
Service::device_out_message_stepper_enable ( std::uint8_t const * message_n )
{
  if ( message_n[ 0 ] > 1 ) {
    std::size_t const count = message_n[ 0 ] - 1;
    std::uint8_t const * indices = &message_n[ 2 ];
    for ( std::size_t ii = 0; ii != count; ++ii ) {
      std::size_t const stepper_index = indices[ ii ];
      auto & map = _steppers.esteppers;
      if ( stepper_index < map.size () ) {
        if ( const auto & est = map[ stepper_index ] ) {
          est->device_out_message_stepper_enable ();
        }
      }
    }
  }
}

void
Service::device_out_message_stepper_disable ( std::uint8_t const * message_n )
{
  if ( message_n[ 0 ] > 1 ) {
    std::size_t const count = message_n[ 0 ] - 1;
    std::uint8_t const * indices = &message_n[ 2 ];
    for ( std::size_t ii = 0; ii != count; ++ii ) {
      std::size_t const stepper_index = indices[ ii ];
      auto & map = _steppers.esteppers;
      if ( stepper_index < map.size () ) {
        if ( const auto & est = map[ stepper_index ] ) {
          est->device_out_message_stepper_disable ();
        }
      }
    }
  }
}

void
Service::device_out_message_stepper_data ( std::uint8_t const * message_n
                                           [[maybe_unused]] )
{
  throw std::runtime_error ( "No stepper data tracking on serialized data" );
}

void
Service::device_in_process_data ( const snc::utility::Data_Tile * tile_n )
{
  sev::mem::View< const std::byte > data ( tile_n->data (), tile_n->size () );
  while ( data.size () != 0 ) {
    if ( _device_in.bytes_missing != 0 ) {
      // Read missing message bytes from the tile to the buffer
      std::size_t const bytes_to_copy =
          std::min ( _device_in.bytes_missing, data.size () );
      std::copy_n ( data.data (),
                    bytes_to_copy,
                    &_device_in.buffer[ _device_in.buffer_pos ] );
      data.drop_front ( bytes_to_copy );
      _device_in.buffer_pos += bytes_to_copy;
      _device_in.bytes_missing -= bytes_to_copy;
      if ( _device_in.bytes_missing == 0 ) {
        // Track message from the buffer
        device_in_track_message ( reinterpret_cast< const std::uint8_t * > (
            &_device_in.buffer[ 0 ] ) );
        _device_in.buffer_pos = 0;
      }
    } else {
      // Read the next message from the tile
      const std::uint8_t * data_u8 =
          reinterpret_cast< const std::uint8_t * > ( data.data () );
      std::size_t const message_size = data_u8[ 0 ] + 1;
      if ( message_size <= data.size () ) {
        // The whole message is in the tile
        device_in_track_message ( data_u8 );
        data.drop_front ( message_size );
      } else {
        // Only a part of the message is in the tile
        std::size_t const bytes_to_copy = data.size ();
        std::copy_n ( data.data (), bytes_to_copy, &_device_in.buffer[ 0 ] );
        data.drop_front ( bytes_to_copy );
        _device_in.buffer_pos = bytes_to_copy;
        _device_in.bytes_missing = message_size - bytes_to_copy;
      }
    }
  }
  request_device_state_message ();
}

void
Service::device_in_track_message ( std::uint8_t const * message_n )
{
  std::size_t const data_bytes = message_n[ 0 ];
  if ( data_bytes == 0 ) {
    // Ignore zero length message
  } else {
    std::size_t const message_type = message_n[ 1 ];
    if ( message_type <= _device_in.handlers.size () ) {
      // Call message handler
      ( this->*_device_in.handlers[ message_type ] ) ( message_n );
    } else {
      // Unknown or bad message type: Call default handler.
      device_in_message_default ( message_n );
    }
  }
}

void
Service::device_in_incomplete_message ( std::uint8_t const * message_n,
                                        std::size_t required_carry_bytes_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Message too short: "
         << " carry_bytes: " << std::size_t ( message_n[ 0 ] )
         << " required_carry_bytes: " << required_carry_bytes_n
         << " type: " << std::uint_fast32_t ( message_n[ 1 ] );
  }
}

void
Service::device_in_message_default ( std::uint8_t const * message_n )
{
  log ().cat ( sev::logt::FL_WARNING,
               "EStepper IN: Unhandled message type: ",
               std::uint_fast32_t ( message_n[ 1 ] ) );
}

void
Service::device_in_message_error ( std::uint8_t const * message_n
                                   [[maybe_unused]] )
{
  log ().cat ( sev::logt::FL_ERROR, "EStepper IN error: Error" );
}

void
Service::device_in_message_unknown_message_type (
    std::uint8_t const * message_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Error message: "
         << "Unknown message type:";
    std::size_t const carry_bytes = message_n[ 0 ];
    std::size_t const required_carry_bytes = 3;
    if ( carry_bytes >= required_carry_bytes ) {
      logo << " length: " << std::uint_fast32_t ( message_n[ 2 ] )
           << " type: " << std::uint_fast32_t ( message_n[ 3 ] );
    } else {
      device_in_incomplete_message ( message_n, required_carry_bytes );
    }
  }
}

void
Service::device_in_message_incomplete_message ( std::uint8_t const * message_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Error message: "
         << "Incomplete message:";
    std::size_t const carry_bytes = message_n[ 0 ];
    std::size_t const required_carry_bytes = 3;
    if ( carry_bytes >= required_carry_bytes ) {
      logo << " length: " << std::uint_fast32_t ( message_n[ 2 ] )
           << " type: " << std::uint_fast32_t ( message_n[ 3 ] );
    } else {
      device_in_incomplete_message ( message_n, required_carry_bytes );
    }
  }
}

void
Service::device_in_message_invalid_control_index (
    std::uint8_t const * message_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Error message: "
         << "Invalid control index:";
    std::size_t const carry_bytes = message_n[ 0 ];
    std::size_t const required_carry_bytes = 3;
    if ( carry_bytes >= required_carry_bytes ) {
      logo << " bits: " << std::uint_fast32_t ( message_n[ 2 ] )
           << " index: " << std::uint_fast32_t ( message_n[ 3 ] );
    } else {
      device_in_incomplete_message ( message_n, required_carry_bytes );
    }
  }
}

void
Service::device_in_message_invalid_stepper_index (
    std::uint8_t const * message_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Error message: "
         << "Invalid stepper index:";
    std::size_t const carry_bytes = message_n[ 0 ];
    std::size_t const required_carry_bytes = 3;
    if ( carry_bytes >= required_carry_bytes ) {
      logo << " stepper: " << std::uint_fast32_t ( message_n[ 2 ] );
    } else {
      device_in_incomplete_message ( message_n, required_carry_bytes );
    }
  }
}

void
Service::device_in_message_stepper_buffer_overflow (
    std::uint8_t const * message_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Error message: "
         << "Stepper buffer overflow:";
    std::size_t const carry_bytes = message_n[ 0 ];
    std::size_t const required_carry_bytes = 2;
    if ( carry_bytes >= required_carry_bytes ) {
      logo << " stepper: " << std::uint_fast32_t ( message_n[ 2 ] );
    } else {
      device_in_incomplete_message ( message_n, required_carry_bytes );
    }
  }
}

void
Service::device_in_message_stepper_bad_segment (
    std::uint8_t const * message_n )
{
  if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
    logo << "EStepper IN: Error message: "
         << "Bad stepper segment:";
    std::size_t const carry_bytes = message_n[ 0 ];
    std::size_t const required_carry_bytes = 4;
    if ( carry_bytes >= required_carry_bytes ) {
      logo << " stepper: " << std::uint_fast32_t ( message_n[ 2 ] )
           << " type: " << std::uint_fast32_t ( message_n[ 3 ] ) << " (";
      switch ( message_n[ 3 ] ) {
      case estepper::in::BAD_SEG_UNKNOWN:
        logo << "UNKNOWN";
        break;
      case estepper::in::BAD_SEG_BAD_TYPE:
        logo << "BAD_TYPE";
        break;
      case estepper::in::BAD_SEG_MISSING_DATA_WORD:
        logo << "MISSING_DATA_WORD";
        break;
      default:
        logo << "bad error type";
        break;
      }
      logo << ") byte: " << std::uint_fast32_t ( message_n[ 4 ] );
    } else {
      device_in_incomplete_message ( message_n, required_carry_bytes );
    }
  }
}

void
Service::device_in_message_pong ( std::uint8_t const * message_n
                                  [[maybe_unused]] )
{
  if ( !_ping.running ) {
    return;
  }
  _ping.running = false;
  // Compute latency
  device_state_ref ().io_stats ().set_latency (
      std::chrono::steady_clock::now () - _ping.start_time );
}

void
Service::device_in_message_sensor_1 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 3;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    bool const value = ( message_n[ 3 ] != 0 );
    auto & map = _device_in.map_sensors_1;
    if ( est_sensor_index < map.size () ) {
      const auto & pair = map[ est_sensor_index ];
      if ( pair.first ) {
        // Tracked sensor
        pair.first->set_valid ( true );
        pair.first->set_state ( value != pair.second );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_sensor_8 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 3;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint8_t const value = message_n[ 3 ];
    auto & map = _device_in.map_sensors_8;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked sensor
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_sensor_16 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 4;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint16_t const value = sev::mem::read_uint16_8le ( &message_n[ 3 ] );
    auto & map = _device_in.map_sensors_16;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked sensor
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_sensor_32 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 6;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint32_t const value = sev::mem::read_uint32_8le ( &message_n[ 3 ] );
    auto & map = _device_in.map_sensors_32;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked sensor
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_sensor_64 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 10;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint64_t const value = sev::mem::read_uint64_8le ( &message_n[ 3 ] );
    auto & map = _device_in.map_sensors_64;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked sensor
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_control_1 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 3;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    bool const value = ( message_n[ 3 ] != 0 );
    auto & map = _device_in.map_controls_1;
    if ( est_sensor_index < map.size () ) {
      const auto & pair = map[ est_sensor_index ];
      if ( pair.first ) {
        // Tracked control
        pair.first->set_valid ( true );
        pair.first->set_state ( value != pair.second );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_control_8 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 3;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint8_t const value = message_n[ 3 ];
    auto & map = _device_in.map_controls_8;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked control
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_control_16 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 4;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint16_t const value = sev::mem::read_uint16_8le ( &message_n[ 3 ] );
    auto & map = _device_in.map_controls_16;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked control
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_control_32 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 6;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint32_t const value = sev::mem::read_uint32_8le ( &message_n[ 3 ] );
    auto & map = _device_in.map_controls_32;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked control
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_control_64 ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 10;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_sensor_index = message_n[ 2 ];
    std::uint64_t const value = sev::mem::read_uint64_8le ( &message_n[ 3 ] );
    auto & map = _device_in.map_controls_64;
    if ( est_sensor_index < map.size () ) {
      if ( const auto & sptr = map[ est_sensor_index ] ) {
        // Tracked control
        sptr->set_valid ( true );
        sptr->set_state ( value );
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_stepper_words_processed (
    std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 4;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_stepper_index = message_n[ 2 ];
    std::uint16_t const value = sev::mem::read_uint16_8le ( &message_n[ 3 ] );
    auto & map = _steppers.esteppers;
    if ( est_stepper_index < map.size () ) {
      if ( const auto & sptr = map[ est_stepper_index ] ) {
        // Tracked stepper
        sptr->device_in_message_stepper_words_processed ( value );
      } else {
        DEBUG_ASSERT ( false );
      }
    } else {
      DEBUG_ASSERT ( false );
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

void
Service::device_in_message_stepper_disabled ( std::uint8_t const * message_n )
{
  std::size_t const carry_bytes = message_n[ 0 ];
  std::size_t const required_carry_bytes = 2;
  if ( carry_bytes >= required_carry_bytes ) {
    std::size_t const est_stepper_index = message_n[ 2 ];
    auto & map = _steppers.esteppers;
    if ( est_stepper_index < map.size () ) {
      if ( const auto & sptr = map[ est_stepper_index ] ) {
        // Tracked stepper
        sptr->device_in_message_stepper_disabled ();
      }
    }
  } else {
    device_in_incomplete_message ( message_n, required_carry_bytes );
  }
}

} // namespace snc::svs::fac::tracker::ship::est
