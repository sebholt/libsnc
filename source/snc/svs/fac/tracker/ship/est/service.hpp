/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <estepper/in/messages.hpp>
#include <estepper/out/messages.hpp>
#include <sev/event/pool.hpp>
#include <sev/event/timer_queue/chrono/client.hpp>
#include <sev/mem/flags.hpp>
#include <sev/mem/ring_resizing.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <snc/svs/fac/serial_device/ship/client_track.hpp>
#include <snc/svs/fac/tracker/ship/events.hpp>
#include <vector>

// -- Forward declaration
namespace snc::est
{
class Stepper_Data_Composer;
}

namespace snc::svs::fac::tracker::ship::est
{

// -- Froward declaration
class Sailor;
class Stepper;
class Axis;
class Stepper_Controls;

class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;
  using Provider = Sailor;

  using Message_Handler =
      void ( Service::* ) ( std::uint8_t const * message_n );

  public:
  // -- Construction

  Service ( Provider & provider_n,
            const snc::device::handle::Info_Writeable & device_info_n );

  ~Service ();

  // -- Service provider

  Provider &
  provider ();

  const Provider &
  provider () const;

  // -- Device statics

  const snc::device::handle::Info_Writeable &
  device_info () const
  {
    return _device_info;
  }

  const snc::device::statics::handle::Statics &
  device_statics () const
  {
    return _device_info->statics ();
  }

  // -- Device state

  const snc::device::state::State &
  device_state () const
  {
    return _device_info->state ();
  }

  snc::device::state::State &
  device_state_ref ()
  {
    return _device_info->state_ref ();
  }

  // -- Cell session inteface

  void
  session_begin ();

  void
  session_abort ();

  void
  session_end ();

  // -- State manipulation interface

  void
  set_axis_aligned ( std::size_t axis_index_n, bool flag_n );

  void
  set_io_stats ( const snc::serial_device::Stats_IO & stats_n );

  // -- EStepper OUT device message processing

  void
  device_out_track_stepper_data (
      snc::est::Stepper_Data_Composer const & composer_n );

  void
  device_out_track_message ( std::uint8_t const * message_n );

  void
  device_out_message_ping ( std::uint8_t const * message_n );

  void
  device_out_message_default ( std::uint8_t const * message_n );

  void
  device_out_message_control_1 ( std::uint8_t const * message_n );

  void
  device_out_message_control_8 ( std::uint8_t const * message_n );

  void
  device_out_message_control_16 ( std::uint8_t const * message_n );

  void
  device_out_message_control_32 ( std::uint8_t const * message_n );

  void
  device_out_message_control_64 ( std::uint8_t const * message_n );

  void
  device_out_message_stepper_enable ( std::uint8_t const * message_n );

  void
  device_out_message_stepper_disable ( std::uint8_t const * message_n );

  void
  device_out_message_stepper_data ( std::uint8_t const * message_n );

  // -- EStepper IN device message processing

  void
  device_in_process_data ( const snc::utility::Data_Tile * tile_n );

  void
  device_in_track_message ( std::uint8_t const * message_n );

  void
  device_in_incomplete_message ( std::uint8_t const * message_n,
                                 std::size_t required_carry_bytes_n );

  void
  device_in_message_default ( std::uint8_t const * message_n );

  void
  device_in_message_error ( std::uint8_t const * message_n );

  void
  device_in_message_unknown_message_type ( std::uint8_t const * message_n );

  void
  device_in_message_incomplete_message ( std::uint8_t const * message_n );

  void
  device_in_message_invalid_control_index ( std::uint8_t const * message_n );

  void
  device_in_message_invalid_stepper_index ( std::uint8_t const * message_n );

  void
  device_in_message_stepper_buffer_overflow ( std::uint8_t const * message_n );

  void
  device_in_message_stepper_bad_segment ( std::uint8_t const * message_n );

  void
  device_in_message_pong ( std::uint8_t const * message_n );

  void
  device_in_message_sensor_1 ( std::uint8_t const * message_n );

  void
  device_in_message_sensor_8 ( std::uint8_t const * message_n );

  void
  device_in_message_sensor_16 ( std::uint8_t const * message_n );

  void
  device_in_message_sensor_32 ( std::uint8_t const * message_n );

  void
  device_in_message_sensor_64 ( std::uint8_t const * message_n );

  void
  device_in_message_control_1 ( std::uint8_t const * message_n );

  void
  device_in_message_control_8 ( std::uint8_t const * message_n );

  void
  device_in_message_control_16 ( std::uint8_t const * message_n );

  void
  device_in_message_control_32 ( std::uint8_t const * message_n );

  void
  device_in_message_control_64 ( std::uint8_t const * message_n );

  void
  device_in_message_stepper_words_processed ( std::uint8_t const * message_n );

  void
  device_in_message_stepper_disabled ( std::uint8_t const * message_n );

  protected:
  // -- Utility

  void
  request_device_state_message ();

  private:
  // -- Device information
  snc::device::handle::Info_Writeable _device_info;

  // -- Deserialization
  struct
  {
    std::array< Message_Handler, estepper::out::message_type_count > handlers;
  } _device_out;
  struct
  {
    std::array< Message_Handler, estepper::in::message_type_count > handlers;
    std::vector<
        std::pair< snc::device::state::sensor::handle::I1_Writeable, bool > >
        map_sensors_1;
    std::vector< snc::device::state::sensor::handle::I8_Writeable >
        map_sensors_8;
    std::vector< snc::device::state::sensor::handle::I16_Writeable >
        map_sensors_16;
    std::vector< snc::device::state::sensor::handle::I32_Writeable >
        map_sensors_32;
    std::vector< snc::device::state::sensor::handle::I64_Writeable >
        map_sensors_64;
    std::vector<
        std::pair< snc::device::state::control::handle::I1_Writeable, bool > >
        map_controls_1;
    std::vector< snc::device::state::control::handle::I8_Writeable >
        map_controls_8;
    std::vector< snc::device::state::control::handle::I16_Writeable >
        map_controls_16;
    std::vector< snc::device::state::control::handle::I32_Writeable >
        map_controls_32;
    std::vector< snc::device::state::control::handle::I64_Writeable >
        map_controls_64;
    std::size_t bytes_missing = 0;
    std::size_t buffer_pos = 0;
    std::array< std::byte, 256 > buffer;
  } _device_in;

  struct
  {
    std::vector< std::shared_ptr< Axis > > axes;
    std::vector< std::shared_ptr< Stepper_Controls > > steppers_controls;
    std::vector< std::shared_ptr< Stepper > > esteppers;
  } _steppers;

  struct
  {
    bool running = false;
    std::chrono::steady_clock::time_point start_time;
  } _ping;

  // -- Clients
  snc::svs::fac::serial_device::ship::Client_Track _serial;

  // -- Office event io
  std::chrono::steady_clock::duration _office_egen_interval;
  sev::event::timer_queue::chrono::Client_Steady _office_egen_timer;
};

} // namespace snc::svs::fac::tracker::ship::est
