/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper.hpp"

namespace snc::svs::fac::tracker::ship::est
{

Stepper::Stepper ( const sev::logt::Reference & log_parent_n,
                   sev::unicode::View log_name_n,
                   std::uint_fast32_t index_n,
                   const snc::device::handle::Info_Writeable & device_info_n )
: _log ( log_parent_n, log_name_n )
, _index ( index_n )
, _device_info ( device_info_n )
{
}

Stepper::~Stepper () = default;

} // namespace snc::svs::fac::tracker::ship::est
