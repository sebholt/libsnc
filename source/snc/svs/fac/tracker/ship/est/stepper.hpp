/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/logt/context.hpp>
#include <snc/device/handle.hpp>
#include <snc/device/info.hpp>
#include <cstdint>

// -- Forward declaration
namespace snc::est
{
class Stepper_Data_Composer;
}

namespace snc::svs::fac::tracker::ship::est
{

/// @brief Stepper tracker base class
///
class Stepper
{
  public:
  // -- Construction

  Stepper ( const sev::logt::Reference & log_parent_n,
            sev::unicode::View log_name_n,
            std::uint_fast32_t index_n,
            const snc::device::handle::Info_Writeable & device_info_n );

  virtual ~Stepper ();

  // -- Logging

  const sev::logt::Context &
  log ()
  {
    return _log;
  }

  // -- Device statics

  const snc::device::handle::Info_Writeable &
  device_info () const
  {
    return _device_info;
  }

  const snc::device::statics::handle::Statics &
  device_statics () const
  {
    return _device_info->statics ();
  }

  // -- Device state

  const snc::device::state::State &
  device_state () const
  {
    return _device_info->state ();
  }

  snc::device::state::State &
  device_state_ref ()
  {
    return _device_info->state_ref ();
  }

  // -- Device OUT tracking interface

  virtual void
  device_out_message_stepper_enable () = 0;

  virtual void
  device_out_message_stepper_disable () = 0;

  virtual void
  device_out_stepper_data (
      snc::est::Stepper_Data_Composer const & composer_n ) = 0;

  // -- Device IN tracking interface

  virtual void
  device_in_message_stepper_words_processed ( std::uint16_t num_n ) = 0;

  virtual void
  device_in_message_stepper_disabled () = 0;

  private:
  sev::logt::Context _log;
  std::uint_fast32_t _index = 0;
  snc::device::handle::Info_Writeable _device_info;
};

} // namespace snc::svs::fac::tracker::ship::est
