/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "stepper_controls.hpp"
#include <estepper/segment/types.hpp>
#include <snc/device/statics/statics.hpp>
#include <snc/device/statics/stepper_controls.hpp>
#include <snc/est/config/stepper.hpp>
#include <snc/est/stepper_data_composer.hpp>

namespace snc::svs::fac::tracker::ship::est
{

Stepper_Controls::Stepper_Controls (
    const sev::logt::Reference & log_parent_n,
    std::uint_fast32_t index_n,
    const snc::device::handle::Info_Writeable & device_info_n )
: Super ( log_parent_n,
          sev::string::cat ( "Stepper_Controls[", index_n, "]" ),
          index_n,
          device_info_n )
, _stepper_statics ( device_statics ()->steppers_controls ().get ( index_n ) )
, _stepper_state (
      device_state_ref ().stepper_controls_shared_writeable ( index_n ) )
{
  _steps_queue.set_capacity (
      _stepper_statics->est ().stepper_config ()->steps_queue_capacity () );
}

Stepper_Controls::~Stepper_Controls () = default;

void
Stepper_Controls::device_out_message_stepper_enable ()
{
  stepper_state ()->set_is_enabled ( true );
}

void
Stepper_Controls::device_out_message_stepper_disable ()
{
}

void
Stepper_Controls::device_out_stepper_data (
    snc::est::Stepper_Data_Composer const & composer_n )
{
  if ( composer_n.size () <= _steps_queue.capacity_free () ) {
    _steps_queue.append_copy_not_full ( composer_n.begin (),
                                        composer_n.end () );
  } else {
    throw std::length_error ( "Steps queue overflow" );
  }

  std::uint16_t const * it_end = composer_n.end ();
  std::uint16_t const * it_cur = composer_n.begin ();
  while ( it_cur != it_end ) {
    // New segment
    std::uint8_t const seg_type = *it_cur;
    std::uint8_t const seg_extra = ( *it_cur ) >> 8;
    ++it_cur;
    switch ( seg_type ) {
    case estepper::segment::CTL_SLEEP: {
      std::uint16_t const * it_seg_end = it_cur + seg_extra;
      for ( ; it_cur != it_seg_end; ++it_cur ) {
        _steps_queue_length_ticks += *it_cur;
      }
    } break;
    case estepper::segment::CTL_1:
      ++it_cur;
      break;
    case estepper::segment::CTL_8:
      ++it_cur;
      break;
    case estepper::segment::CTL_16:
      ++it_cur;
      break;
    case estepper::segment::CTL_32:
      it_cur += 2;
      break;
    case estepper::segment::CTL_64:
      it_cur += 4;
      break;
    default:
      throw std::runtime_error ( "Bad new controls stepper segment type" );
    }
  }

  update_state ();
}

void
Stepper_Controls::device_in_message_stepper_words_processed (
    std::uint16_t num_n )
{
  std::size_t num = num_n;
  // Check if the reported count is reasonable
  if ( num > _steps_queue.size () ) {
    num = _steps_queue.size ();
    if ( auto logo = log ().ostr ( sev::logt::FL_ERROR ) ) {
      logo << "EStepper IN: More words reported as processed (" << num_n
           << ") than there are words in the controls stepper queue ("
           << _steps_queue.size () << ")";
    }
  }

  for ( std::size_t ii = 0; ii != num; ++ii ) {
    std::uint16_t const value = _steps_queue.front ();
    _steps_queue.pop_front_not_empty ();
    if ( _segment.count == 0 ) {
      // New segment
      _segment.type = value;
      switch ( _segment.type ) {
      case estepper::segment::CTL_SLEEP:
        _segment.count = ( value >> 8 );
        break;
      case estepper::segment::CTL_1:
        _segment.count = 1;
        break;
      case estepper::segment::CTL_8:
        _segment.count = 1;
        break;
      case estepper::segment::CTL_16:
        _segment.count = 1;
        break;
      case estepper::segment::CTL_32:
        _segment.count = 2;
        break;
      case estepper::segment::CTL_64:
        _segment.count = 4;
        break;
      default:
        throw std::runtime_error (
            "Bad processed controls stepper segment type" );
      }
    } else {
      // Inside segment
      --_segment.count;
      switch ( _segment.type ) {
      case estepper::segment::CTL_SLEEP:
        // Update steps_queue_length_ticks
        DEBUG_ASSERT ( _steps_queue_length_ticks >= value );
        _steps_queue_length_ticks -= value;
        break;
      case estepper::segment::CTL_1:
      case estepper::segment::CTL_8:
      case estepper::segment::CTL_16:
      case estepper::segment::CTL_32:
      case estepper::segment::CTL_64:
        break;
      default:
        throw std::runtime_error (
            "Bad processed controls stepper segment type" );
      }
    }
  }

  update_state ();
}

void
Stepper_Controls::device_in_message_stepper_disabled ()
{
  stepper_state ()->set_is_enabled ( false );
}

void
Stepper_Controls::update_state ()
{
  auto & state = *stepper_state ();
  // Steps queue
  state.set_steps_queue_length ( _steps_queue.size () );
  state.set_steps_queue_ticks ( _steps_queue_length_ticks );
}

} // namespace snc::svs::fac::tracker::ship::est
