/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/mem/ring_fixed.hpp>
#include <snc/device/handle.hpp>
#include <snc/svs/fac/tracker/ship/est/stepper.hpp>

namespace snc::svs::fac::tracker::ship::est
{

/// @brief Controls stepper tracker
///
class Stepper_Controls : public Stepper
{
  // -- Types
  private:
  using Super = Stepper;
  using Steps_Queue = sev::mem::Ring_Fixed< std::uint16_t >;

  public:
  // -- Construction

  Stepper_Controls (
      const sev::logt::Reference & log_parent_n,
      std::uint_fast32_t index_n,
      const snc::device::handle::Info_Writeable & device_info_n );

  ~Stepper_Controls () override;

  // -- Controls stepper info

  const snc::device::statics::handle::Stepper_Controls &
  stepper_statics () const
  {
    return _stepper_statics;
  }

  const snc::device::state::handle::Stepper_Controls_Writeable &
  stepper_state () const
  {
    return _stepper_state;
  }

  // -- Device OUT tracking interface

  void
  device_out_message_stepper_enable () override;

  void
  device_out_message_stepper_disable () override;

  void
  device_out_stepper_data (
      snc::est::Stepper_Data_Composer const & composer_n ) override;

  // -- Device IN tracking interface

  void
  device_in_message_stepper_words_processed ( std::uint16_t num_n ) override;

  void
  device_in_message_stepper_disabled () override;

  private:
  // -- Utility

  void
  update_state ();

  private:
  snc::device::statics::handle::Stepper_Controls _stepper_statics;
  snc::device::state::handle::Stepper_Controls_Writeable _stepper_state;
  /// @brief The step queue mirrors the step queue in the estepper device
  Steps_Queue _steps_queue;
  /// @brief Number of ticks in the queue
  std::uint_fast32_t _steps_queue_length_ticks = 0;
  /// @brief Segment tracking for queue front removal
  struct
  {
    std::uint8_t type = 0;
    std::uint8_t count = 0;
  } _segment;
};

} // namespace snc::svs::fac::tracker::ship::est
