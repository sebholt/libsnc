/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/utility.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/frame/async_proc/types.hpp>
#include <cstdint>

namespace snc::svs::frame::async_proc
{

template < typename SVS >
class Clerk_T : public snc::svp::office::Clerk
{
  // -- Types
  private:
  using Super = snc::svp::office::Clerk;

  public:
  using Service = SVS;

  // -- Construction

  Clerk_T ( const Clerk_Init & init_n, sev::unicode::View name_n );

  Clerk_T ( const Clerk_Init & init_n );

  ~Clerk_T ();

  // -- Cell session interface

  bool
  cell_session_is_ready_to_end () const override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  private:
  // -- Attributes
  Service _service;
};

} // namespace snc::svs::frame::async_proc
