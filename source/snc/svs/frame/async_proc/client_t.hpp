/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/utility.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/bat/ifc/control_unique.hpp>
#include <snc/svs/frame/async_proc/types.hpp>
#include <cstdint>
#include <memory>

namespace snc::svs::frame::async_proc
{

// -- Forward declaration
class Service_Async;

template < typename TPS >
class Client_T : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  using Types = TPS;
  using Request = typename Types::Request;
  using Progress = typename Types::Progress;
  using Result = typename Types::Result;

  // -- Construction

  Client_T ( User * user_n );

  ~Client_T ();

  // -- Accessors

  Service_Async *
  service_async () const
  {
    return _service_async;
  }

  // -- Service start / stop request

  /// @brief Service control interface class
  class Control : public Control_Unique
  {
    public:
    using Super = Control_Unique;

    // -- Construction

    using Super::Super;

    Control ( Control && ctl_n ) = default;

    Control ( const Control & ctl_n ) = delete;

    ~Control ();

    // -- Assignment operators

    Control &
    operator= ( const Control & ctl_n ) = delete;

    Control &
    operator= ( Control && ctl_n ) = default;

    // -- Client interface

    Client_T *
    client () const
    {
      return static_cast< Client_T * > ( client_abstract () );
    }

    // -- Control interface

    template < typename... Args >
    static std::shared_ptr< Request >
    make_request ( Args &&... args_n )
    {
      return std::make_shared< Request > ( std::forward< Args > ( args_n )... );
    }

    bool
    start ( const std::shared_ptr< const Request > & request_n );

    template < typename... Args >
    bool
    issue ( Args &&... args_n )
    {
      return start ( make_request ( std::forward< Args > ( args_n )... ) );
    }

    /// @brief Check for progress updates
    bool
    poll_updates ();

    void
    abort ();

    // -- Type instances

    const Request &
    request () const;

    const Progress &
    progress () const;

    const Result &
    result () const;
  };

  /// @return Create a control instance.
  Control
  control ()
  {
    return Control ( this );
  }

  protected:
  // -- Utility

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  void
  disconnecting () override;

  private:
  Service_Async * _service_async = nullptr;
};

} // namespace snc::svs::frame::async_proc
