/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/event/bit_accu.hpp>
#include <sev/event/bit_accus_async/callback.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/event/tracker_pools.hpp>
#include <sev/thread/thread.hpp>
#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/svs/frame/async_proc/worker_base.hpp>
#include <snc/svs/frame/async_proc/worker_events.hpp>

namespace snc::svs::frame::async_proc
{

// -- Forward declaration
class Client_Base;

class Service_Async : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  struct LEvent
  {
    static constexpr std::uint_fast32_t EVENTS_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t EVENTS_OUT = ( 1 << 1 );
  };

  public:
  using Request_Base_Handle = std::shared_ptr< const Request_Base >;
  using Progress_Base_Handle = std::unique_ptr< Progress_Base >;
  using Result_Base_Handle = std::unique_ptr< Result_Base >;

  struct Variables
  {
    Request_Base_Handle request;
    Progress_Base_Handle progress;
    Result_Base_Handle result;
  };

  // -- Construction

  Service_Async ( Provider_Cell & provider_n,
                  const std::type_info * request_type_info_n,
                  sev::unicode::View log_name_n = "Async-Service" );

  ~Service_Async ();

  // -- Variables

  const Variables &
  vars () const
  {
    return _vars;
  }

  Variables &
  vars_ref ()
  {
    return _vars;
  }

  // -- Interface

  bool
  is_active () const;

  bool
  control_acquirable () const override;

  // -- Central processing

  void
  process_async ();

  void
  process ();

  // -- Client interface

  const std::type_info *
  request_type_info () const
  {
    return _request_type_info;
  }

  bool
  client_start ( const std::shared_ptr< const Request_Base > & request_n );

  bool
  client_poll_updates ();

  void
  client_abort ();

  // -- Protected: control interface
  protected:
  void
  control_released ( Client_Abstract * client_n ) override;

  // -- Utility

  virtual void
  reset_vars_virtual ( Variables & vars_n ) const = 0;

  void
  reset_vars ();

  virtual void
  update_progress ( const Progress_Base & prog_n ) = 0;

  virtual void
  update_result ( const Result_Base & result_n ) = 0;

  virtual std::unique_ptr< Worker_Base >
  make_worker ( const sev::event::queue_io::Link_2 & link_n,
                const std::shared_ptr< const Request_Base > & request_n ) = 0;

  void
  set_updated ();

  private:
  // -- Attributes
  const std::type_info * _request_type_info = nullptr;

  // -- Worker events
  sev::event::Bit_Accu_Fast32 _event_accu;
  sev::event::bit_accus_async::Callback _event_accu_async;

  sev::event::Tracker_Pools< events::out::Abort > _epools;

  // -- Worker event queue connection
  sev::event::queue_io::Connection_2 _connection;

  std::unique_ptr< Worker_Base > _worker;
  sev::thread::Thread _worker_thread;
  bool _worker_join = false;

  // -- Processing state
  bool _updated = false;
  Variables _vars;
};

} // namespace snc::svs::frame::async_proc
