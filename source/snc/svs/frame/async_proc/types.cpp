/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "types.hpp"

namespace snc::svs::frame::async_proc
{

Request_Base::Request_Base () = default;

Request_Base::~Request_Base () = default;

Progress_Base::Progress_Base () = default;

Progress_Base::~Progress_Base () = default;

Result_Base::Result_Base () = default;

Result_Base::~Result_Base () = default;

void
Result_Base::reset ()
{
  end = End::NONE;
  error_message.clear ();
  error_message.shrink_to_fit ();
}

} // namespace snc::svs::frame::async_proc
