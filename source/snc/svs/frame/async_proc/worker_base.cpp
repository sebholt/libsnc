/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#include "worker_base.hpp"
#include <sev/assert.hpp>
#include <snc/svs/frame/async_proc/worker_events.hpp>
#include <utility>

namespace snc::svs::frame::async_proc
{

Worker_Base::Worker_Base ( sev::logt::Context log_n,
                           const sev::event::queue_io::Link_2 & link_n )
: _log ( std::move ( log_n ) )
{
  _connection.set_push_notifier (
      [ &ec = _event_accu ] () { ec.set ( LEvent::EVENTS_OUT ); } );
  _connection.set_incoming_notifier (
      [ &ec = _event_waiter ] () { ec.set ( LEvent::EVENTS_IN ); } );
  _connection.connect ( link_n );
}

Worker_Base::~Worker_Base () = default;

void
Worker_Base::operator() ()
{
  // Feed begin
  this->feed_begin ();

  while ( ( _state != State::DONE )      //
          || !_epool_tracker.all_home () //
          || !_connection.all_empty () ) {

    // Asynchronous event pick up or wait
    _event_accu.set ( _event_waiter.fetch_and_clear_wait (
        _event_accu.is_empty () && ( _state == State::DONE ) ) );

    // Process events
    events ();

    // Send state
    if ( _state == State::FEED ) {
      this->send_progress ();
    } else if ( _state == State::RESULT ) {
      this->send_result ();
      _state = State::DONE;
    }

    // Feed
    if ( _state == State::FEED ) {
      this->feed ();
    }
  }
}

void
Worker_Base::events ()
{
  if ( !_event_accu.test_any ( LEvent::EVENTS_IN | LEvent::EVENTS_OUT ) ) {
    return;
  }

  // Feed from and into event queue
  _connection.feed_queue ( _event_accu, LEvent::EVENTS_IN, LEvent::EVENTS_OUT );

  // Release returned events
  _connection.out ().release_all_with_reset ();

  // Deliver new events
  _connection.in ().process_all (
      [ this ] ( const sev::event::Event & event_n ) {
        switch ( event_n.type () ) {
        case events::out::Type::ABORT:
          if ( _state == State::FEED ) {
            this->feed_abort ();
          }
          break;
        default:
          DEBUG_ASSERT ( false );
        }
      } );
}

void
Worker_Base::feed_end ()
{
  if ( _state == State::FEED ) {
    _state = State::RESULT;
  }
}

} // namespace snc::svs::frame::async_proc
