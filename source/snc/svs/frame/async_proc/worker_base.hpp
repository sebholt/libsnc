/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/event/bit_accus_async/condition.hpp>
#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <sev/event/queue_io/connection.hpp>
#include <sev/logt/context.hpp>
#include <sev/mem/flags.hpp>
#include <snc/svs/frame/async_proc/worker_events.hpp>
#include <string>
#include <string_view>

namespace snc::svs::frame::async_proc
{

class Worker_Base
{
  public:
  // -- Types

  struct LEvent
  {
    static constexpr std::uint_fast32_t EVENTS_IN = ( 1 << 0 );
    static constexpr std::uint_fast32_t EVENTS_OUT = ( 1 << 1 );
  };

  enum State
  {
    FEED,
    RESULT,
    DONE
  };

  // -- Construction

  Worker_Base ( sev::logt::Context log_n,
                const sev::event::queue_io::Link_2 & link_n );

  virtual ~Worker_Base ();

  void
  operator() ();

  protected:
  // -- Interface

  const sev::logt::Context &
  log () const
  {
    return _log;
  }

  sev::event::queue_io::Connection_2 &
  connection ()
  {
    return _connection;
  }

  sev::event::Pool_Tracker &
  epool_tracker ()
  {
    return _epool_tracker;
  }

  virtual bool
  send_progress () = 0;

  virtual void
  send_result () = 0;

  virtual void
  feed_begin () = 0;

  virtual void
  feed_abort () = 0;

  virtual void
  feed () = 0;

  void
  feed_end ();

  private:
  // -- Utility

  void
  events ();

  private:
  // -- Logging
  sev::logt::Context _log;

  // -- Event and state registers
  sev::mem::Flags_Fast32 _event_accu;
  sev::event::bit_accus_async::Condition _event_waiter;
  State _state = State::FEED;

  // -- sev::event
  sev::event::queue_io::Connection_2 _connection;
  sev::event::Pool_Tracker _epool_tracker;
};

} // namespace snc::svs::frame::async_proc
