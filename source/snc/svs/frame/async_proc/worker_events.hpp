/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <sev/event/event.hpp>
#include <snc/svs/frame/async_proc/types.hpp>
#include <cstdint>
#include <memory>
#include <utility>

namespace snc::svs::frame::async_proc::events::in
{

struct Type
{
  static constexpr std::uint_fast32_t PROGRESS = 0;
  static constexpr std::uint_fast32_t RESULT = 1;
};

class Progress : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::PROGRESS;
  using Handle = std::unique_ptr< Progress_Base >;

  Progress ( Handle progress_n )
  : sev::event::Event ( etype )
  , progress ( std::move ( progress_n ) )
  {
  }

  Handle progress;
};

class Result : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::RESULT;
  using Handle = std::unique_ptr< Result_Base >;

  Result ( Handle result_n )
  : sev::event::Event ( etype )
  , result ( std::move ( result_n ) )
  {
  }

  Handle result;
};

} // namespace snc::svs::frame::async_proc::events::in

namespace snc::svs::frame::async_proc::events::out
{

struct Type
{
  static constexpr std::uint_fast32_t ABORT = 0;
};

class Abort : public sev::event::Event
{
  public:
  static constexpr auto etype = Type::ABORT;

  Abort ()
  : sev::event::Event ( etype )
  {
  }
};

} // namespace snc::svs::frame::async_proc::events::out
