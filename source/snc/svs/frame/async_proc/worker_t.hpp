/// pfadi: Numeric machine controller.
///
/// \copyright See LICENSE-pfadi.txt file

#pragma once

#include <snc/svs/frame/async_proc/worker_base.hpp>
#include <snc/svs/frame/async_proc/worker_events.hpp>
#include <string_view>

namespace snc::svs::frame::async_proc
{

template < typename TPS >
class Worker_T : public Worker_Base
{
  // -- Types
  private:
  using Super = Worker_Base;

  public:
  // -- Types
  using Request_Base = snc::svs::frame::async_proc::Request_Base;

  using Types = TPS;
  using Request = typename Types::Request;
  using Progress = typename Types::Progress;
  using Result = typename Types::Result;

  // -- Construction

  Worker_T ( sev::logt::Context log_context_n,
             const sev::event::queue_io::Link_2 & link_n,
             std::shared_ptr< const Request_Base > request_n );

  ~Worker_T ();

  protected:
  // -- Interface

  bool
  send_progress () override;

  void
  send_result () override;

  const Request &
  request ()
  {
    return static_cast< const Request & > ( *_request );
  }

  Progress &
  progress ()
  {
    return _progress;
  }

  Result &
  result ()
  {
    return _result;
  }

  private:
  // -- State
  std::shared_ptr< const Request_Base > _request;
  Progress _progress;
  Result _result;

  // -- Event pools
  sev::event::Pool< events::in::Progress > _event_pool_progress;
  sev::event::Pool< events::in::Result > _event_pool_result;
};

} // namespace snc::svs::frame::async_proc
