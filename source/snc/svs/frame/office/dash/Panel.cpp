/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/frame/office/office/clerk.hpp>
#include <QCoreApplication>
#include <QEvent>

namespace snc::svs::frame::office::dash
{

namespace
{
// QEvent types
constexpr int _qevent_type_async = QEvent::Type::User + 100;
constexpr int _qevent_type_office_done = QEvent::Type::User + 100 + 1;
} // namespace

Panel::Panel ( const Panel_Init & init_n,
               sev::thread::Tracker * thread_tracker_n )
: Super ( init_n, "Office" )
, _queue_io_ref ( sev::event::queue_io::Reference_2::created () )
, _office_battery (
      sev::logt::Context ( dash ().log ().parent_ref (), "Office" ),
      thread_tracker_n,
      "Office" )
{
  _office_connection.set_incoming_notifier ( [ this ] () {
    QCoreApplication::postEvent (
        this,
        new QEvent ( static_cast< QEvent::Type > ( _qevent_type_async ) ) );
  } );
  _office_connection.set_push_notifier (
      [ &ec = event_accu () ] () { ec.set ( LEvent::EVENTS_OUT ); } );
  _office_connection.connect ( _queue_io_ref.link_a () );
}

Panel::~Panel () = default;

void
Panel::session_abort ()
{
  Super::session_abort ();
  abort_office ();
}

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::frame::office::office::Clerk,
                             sev::event::queue_io::Link_2 > (
      _queue_io_ref.link_b () );
}

bool
Panel::office_session_is_ready_to_end () const
{
  return Super::office_session_is_ready_to_end () &&
         _office_connection.all_empty ();
}

void
Panel::process ()
{
  process_office_event_io ();
  process_office_abort ();
}

void
Panel::process_office_event_io ()
{
  // Check of there are pending events
  if ( !event_accu ().test_any ( LEvent::EVENTS_IN | LEvent::EVENTS_OUT ) ) {
    return;
  }

  // Feed event queue
  _office_connection.feed_queue (
      event_accu ().flags (), LEvent::EVENTS_IN, LEvent::EVENTS_OUT );

  // Release returned events
  _office_connection.out ().release_all_casted< Office_Notification > (
      [] ( Office_Notification * event_n ) {
        event_n->pair ().panel ()->office_io ().release ( event_n );
      } );

  // Deliver new events
  _office_connection.in ().process_all_casted< const Office_Event > (
      [] ( const Office_Event & event_n ) {
        event_n.pair ().panel ()->office_event ( event_n );
      } );
}

void
Panel::process_office_abort ()
{
  if ( !office_session_is_aborting () ) {
    return;
  }

  // Send office abort event on demand
  if ( !_office_battery.is_aborting () ) {
    // Check if all panels are ready to end
    for ( auto * fctl : dash ().panels () ) {
      if ( fctl->office_session_is_ready_to_end () ) {
        continue;
      }
      return;
    }
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Office abort: Controllers office sessions ready to end. "
              "Aborting office processor.";
    }
    _office_battery.abort ();
  }

  // Stop office processing battery
  if ( event_accu ().test_any_unset ( LEvent::ABORT_DONE ) ) {
    // End panels office session
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Office abort: Ending office sessions of "
           << dash ().panels ().size () << " panels.";
    }
    for ( auto * fctl : dash ().panels () ) {
      fctl->office_session_end ();
    }

    // Stop processing battery
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Office abort: Stop office processor begin." );
    _office_battery.stop ();
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Office abort: Stop office processor done." );
  }
}

bool
Panel::event ( QEvent * event_n )
{
  if ( event_n->type () == _qevent_type_async ) {
    event_n->accept ();
    event_accu ().set ( LEvent::EVENTS_IN );
    return true;
  }

  if ( event_n->type () == _qevent_type_office_done ) {
    event_n->accept ();
    event_accu ().set ( LEvent::ABORT_DONE );
    return true;
  }

  return Super::event ( event_n );
}

bool
Panel::start_office ()
{
  if ( office_session_is_running () ) {
    return false;
  }

  // Begin panels office session
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Office start: Beginning office sessions of "
         << dash ().panels ().size () << " panels.";
  }
  for ( auto * fctl : dash ().panels () ) {
    fctl->office_session_begin ();
  }

  // Collect cell factories and start battery
  {
    snc::bat::Starter starter;
    // Cell factories
    starter.cell_factories.reserve ( dash ().panels ().size () );
    for ( auto * fctl : dash ().panels () ) {
      if ( auto factory = fctl->office_session_begin_factory () ) {
        starter.cell_factories.emplace_back ( std::move ( factory ) );
      }
    }
    // End callback
    starter.end_callback = [ this ] () {
      QCoreApplication::postEvent ( this,
                                    new QEvent ( static_cast< QEvent::Type > (
                                        _qevent_type_office_done ) ) );
    };

    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Office start: Starting the office processor." );
    _office_battery.start ( std::move ( starter ) );
  }

  return true;
}

void
Panel::abort_office ()
{
  if ( !office_session_is_good () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Office abort!" );

  // Abort panels office session
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Office abort: Aborting office sessions of "
         << dash ().panels ().size () << " panels.";
  }
  for ( auto * fctl : dash ().panels () ) {
    fctl->office_session_abort ();
  }

  request_processing ();
}

} // namespace snc::svs::frame::office::dash
