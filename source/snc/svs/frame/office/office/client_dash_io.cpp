/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_dash_io.hpp"
#include <sev/utility.hpp>
#include <snc/svp/office/clerk.hpp>
#include <snc/svs/frame/office/office/clerk.hpp>
#include <snc/svs/frame/office/office/service.hpp>
#include <stdexcept>

namespace snc::svs::frame::office::office
{

Client_Dash_IO::Client_Dash_IO ( User * user_n, Panel * panel_n )
: Super ( user_n )
, _panel ( panel_n )
{
  session ().set_end_callback (
      [ this ] () { // Check that all event pools have had some capacity.
        // If not, some rarely used events might not have been sendable at all.
        if ( epool_tracker ().any_empty () ) {
          throw std::runtime_error (
              "All dash event pools should have had some capacity." );
        }

        _epool_tracker.clear_all_capacities ();
        _stats_in = 0;
        _pick_up_requested = false;
        _notifications.clear ();
      } );
}

Client_Dash_IO::~Client_Dash_IO () = default;

Client_Dash_IO::Clerk *
Client_Dash_IO::clerk ()
{
  return static_cast< Clerk * > ( user () );
}

const Client_Dash_IO::Clerk *
Client_Dash_IO::clerk () const
{
  return static_cast< const Clerk * > ( user () );
}

void
Client_Dash_IO::notifications_assign ( std::uint_fast32_t messages_n )
{
  if ( !session ().is_good () ) {
    return;
  }

  const std::uint_fast32_t before = notifications ();
  _notifications.assign ( messages_n );
  if ( before == 0 ) {
    request_pick_up ();
  }
}

void
Client_Dash_IO::notification_set ( std::uint_fast32_t messages_n )
{
  if ( !session ().is_good () ) {
    return;
  }

  const std::uint_fast32_t before = notifications ();
  _notifications.set ( messages_n );
  if ( before == 0 ) {
    request_pick_up ();
  }
}

void
Client_Dash_IO::notification_unset ( std::uint_fast32_t messages_n )
{
  _notifications.unset ( messages_n );
}

void
Client_Dash_IO::request_pick_up ()
{
  if ( session ().is_good () && sev::change ( _pick_up_requested, true ) ) {
    service ()->request_pick_up ( static_cast< Clerk * > ( user () ) );
  }
}

void
Client_Dash_IO::pick_up ()
{
  if ( sev::change ( _pick_up_requested, false ) ) {
    static_cast< Clerk * > ( user () )->dash_notify ();
  }
}

void
Client_Dash_IO::submit ( Dash_Notification * event_n )
{
  if ( session ().is_good () ) {
    service ()->submit ( event_n );
  } else {
    release ( event_n );
  }
}

void
Client_Dash_IO::release ( Dash_Notification * event_n )
{
  // Ensure that we own this event
  DEBUG_ASSERT ( epool_tracker ().owned ( event_n ) );
  // Reset release the event
  event_n->pool ()->casted_reset_release_virtual ( event_n );
  // We might be waiting for a free event
  if ( !notifications ().is_empty () ) {
    request_pick_up ();
  }
}

Service *
Client_Dash_IO::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client_Dash_IO::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::frame::office::office
