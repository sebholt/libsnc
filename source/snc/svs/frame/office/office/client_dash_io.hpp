/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/event/pool.hpp>
#include <sev/event/pool_tracker.hpp>
#include <sev/mem/flags.hpp>
#include <snc/bat/ifc/client_abstract.hpp>
#include <snc/svp/dash/events.hpp>
#include <cstdint>
#include <vector>

// -- Forward declaration
namespace snc::svp::dash
{
class Panel;
}
namespace snc::svp::office::office
{
class Clerk;
}

namespace snc::svs::frame::office::office
{

// -- Forward declaration
class Service;

/// @brief Client for event IO
///
class Client_Dash_IO : public snc::bat::ifc::Client_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Client_Abstract;

  public:
  using Panel = snc::svp::dash::Panel;
  using Clerk = snc::svp::office::Clerk;
  using Dash_Event = const snc::svp::dash::event::Out;
  using Dash_Notification = snc::svp::dash::event::In;

  // -- Construction

  Client_Dash_IO ( User * user_n, Panel * panel_n );

  ~Client_Dash_IO ();

  // -- Clerk

  Clerk *
  clerk ();

  const Clerk *
  clerk () const;

  // -- Event pool tracker

  sev::event::Pool_Tracker &
  epool_tracker ()
  {
    return _epool_tracker;
  }

  const sev::event::Pool_Tracker &
  epool_tracker () const
  {
    return _epool_tracker;
  }

  // -- Event allocation

  template < typename T, typename... Args >
  void
  allocate ( sev::event::Pool< T > & pool_n,
             std::size_t num_n,
             Args &&... args_n )
  {
    pool_n.set_size ( num_n,
                      Dash_Notification::Pair ( _panel, clerk () ),
                      std::forward< Args > ( args_n )... );
  }

  template < typename T, typename... Args >
  T *
  acquire ( sev::event::Pool< T > & pool_n, Args &&... args_n )
  {
    if ( !pool_n.is_empty () ) {
      return pool_n.pop_not_empty ();
    }
    return pool_n.create ( Dash_Notification::Pair ( _panel, clerk () ),
                           std::forward< Args > ( args_n )... );
  }

  // -- Incoming dash event statistics

  /// @brief Number of events incoming from the dash
  std::int_fast32_t
  stats_in ()
  {
    return _stats_in;
  }

  void
  stats_in_increment ()
  {
    ++_stats_in;
  }

  std::int_fast32_t
  stats_in_fetch_clear ()
  {
    std::int_fast32_t res = _stats_in;
    _stats_in = 0;
    return res;
  }

  // -- Dash notification bit register

  const sev::mem::Flags_Fast32 &
  notifications () const
  {
    return _notifications;
  }

  void
  notifications_assign ( std::uint_fast32_t messages_n );

  void
  notification_set ( std::uint_fast32_t messages_n );

  void
  notification_unset ( std::uint_fast32_t messages_n );

  /// @brief Returns a valid event pointer if the message was requested
  ///        and the pool is not empty
  template < class T >
  T *
  notify_get ( std::uint_fast32_t message_n, sev::event::Pool< T > & pool_n )
  {
    if ( !_notifications.test_any ( message_n ) || pool_n.is_empty () ) {
      return nullptr;
    }
    _notifications.unset ( message_n );
    return pool_n.pop_not_empty ();
  }

  // -- Interface

  void
  submit ( Dash_Notification * event_n );

  void
  request_pick_up ();

  void
  pick_up ();

  void
  release ( Dash_Notification * event_n );

  // -- Service connection
  protected:
  Service *
  service () const;

  bool
  connect_to ( Service_Abstract * sb_n ) override;

  private:
  Panel * _panel = nullptr;
  sev::mem::Flags_Fast32 _notifications;
  bool _pick_up_requested = false;
  std::int_fast32_t _stats_in = 0;
  sev::event::Pool_Tracker _epool_tracker;
};

} // namespace snc::svs::frame::office::office
