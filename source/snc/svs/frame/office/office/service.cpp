/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/math/exponents.hpp>
#include <sev/utility.hpp>
#include <snc/svp/office/clerk.hpp>

namespace snc::svs::frame::office::office
{

Service::Service ( Provider_Cell & provider_n,
                   const sev::event::queue_io::Link_2 & event_queue_link_n )
: Super ( provider_n )
{
  // -- Event registers
  _event_accu.set_callback ( provider_cell ().processing_request () );
  _event_accu_async.set_callback (
      provider_cell ().processing_request_async () );

  // -- Event queue connection
  _connection.set_incoming_notifier (
      [ &ea = _event_accu_async ] () { ea.set ( LEvent::EVENTS_IN ); } );
  _connection.set_push_notifier (
      [ &ea = _event_accu ] () { ea.set ( LEvent::EVENTS_OUT ); } );
  _connection.connect ( event_queue_link_n );

  // -- Dash event generation
  _pick_up_interval = std::chrono::microseconds ( sev::math::mega_ull / 60 );
  _pick_up_timer.set_callback (
      [ this ] () { this->generate_dash_events (); } );
  _pick_up_timer.connect ( provider_cell ().cell_context ().timer_queue () );
}

Service::~Service () = default;

void
Service::session_begin ()
{
  _notification_clerks.set_capacity (
      provider_cell ().cell_context ().cells ().size () );
}

void
Service::session_end ()
{
  _notification_clerks.clear_capacity ();
}

bool
Service::buffers_empty () const
{
  return _connection.all_empty ();
}

void
Service::process_async ()
{
  _event_accu.set_silent ( _event_accu_async.fetch_and_clear () );
  process ();
}

void
Service::process ()
{
  // Check of there are pending events
  if ( !_event_accu.test_any ( LEvent::EVENTS_IN | LEvent::EVENTS_OUT ) ) {
    return;
  }

  // Feed event queue
  _connection.feed_queue (
      _event_accu.flags (), LEvent::EVENTS_IN, LEvent::EVENTS_OUT );

  // Release returned events
  using Dash_Notification = snc::svp::office::Clerk::Dash_Notification;
  _connection.out ().release_all_casted< Dash_Notification > (
      [] ( Dash_Notification * event_n ) {
        event_n->pair ().clerk ()->dash_io ().release ( event_n );
      } );

  // Deliver new events
  using Dash_Event = snc::svp::office::Clerk::Dash_Event;
  _connection.in ().process_all_casted< const Dash_Event > (
      [] ( const Dash_Event & event_n ) {
        auto * clerk = event_n.pair ().clerk ();
        clerk->dash_io ().stats_in_increment ();
        clerk->dash_event ( event_n );
      } );
}

void
Service::request_pick_up ( snc::svp::office::Clerk * clerk_n )
{
  DEBUG_ASSERT ( !_notification_clerks.is_full () );
  _notification_clerks.push_back ( clerk_n );

  if ( _pick_up_timer.is_running () ) {
    return;
  }

  auto tnow = std::chrono::steady_clock::now ();
  auto elapsed = tnow - _pick_up_timer.point ();
  if ( elapsed < _pick_up_interval ) {
    // Wait for the rest of the interval
    auto remain = _pick_up_interval - elapsed;
    _pick_up_timer.start_single_point ( tnow + remain );
  } else {
    // The interval has elapsed already.
    _pick_up_timer.start_single_point ( tnow );
  }
}

void
Service::generate_dash_events ()
{
  // Poll clerks to generate bridge events
  while ( !_notification_clerks.is_empty () ) {
    auto * clerk = _notification_clerks.front ();
    _notification_clerks.pop_front_not_empty ();
    clerk->dash_io ().pick_up ();
  }
}

} // namespace snc::svs::frame::office::office
