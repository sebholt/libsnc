/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/qt/Settings.hpp>
#include <snc/svp/dash/Panel.hpp>
#include <QString>
#include <QStringList>

namespace snc::svs::frame::settings::dash
{

class Panel : public snc::svp::dash::Panel
{
  Q_OBJECT

  // -- Types
  private:
  using Super = snc::svp::dash::Panel;

  public:
  // -- Properties

  Q_PROPERTY (
      snc::settings::qt::Settings * settings READ settings_ptr CONSTANT )

  // -- Construction

  Panel ( const Panel_Init & init_n );

  ~Panel ();

  // -- Settings

  const snc::settings::qt::Settings &
  settings () const
  {
    return _settings;
  }

  snc::settings::qt::Settings &
  settings ()
  {
    return _settings;
  }

  snc::settings::qt::Settings *
  settings_ptr ()
  {
    return &_settings;
  }

  // -- Session interface

  void
  session_begin () override;

  void
  session_end () override;

  // -- Office session interface

  Clerk_Factory_Handle
  office_session_begin_factory () override;

  // -- Interface

  void
  read_settings ();

  void
  write_settings ();

  private:
  // -- Utility

  QString
  settings_file_abs ( QString dir_n );

  private:
  // -- Attributes
  QString _settings_file_name;
  QStringList _directories;
  QString _settings_dir;
  QString _settings_file;
  snc::settings::qt::Settings _settings;
};

} // namespace snc::svs::frame::settings::dash
