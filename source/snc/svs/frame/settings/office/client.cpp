/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client.hpp"
#include <snc/svs/frame/settings/office/service.hpp>

namespace snc::svs::frame::settings::office
{

Client::Client ( User * user_n )
: Super ( user_n )
{
}

Client::~Client () = default;

snc::settings::Settings *
Client::settings () const
{
  return is_connected () ? &service ()->settings () : nullptr;
}

Service *
Client::service () const
{
  return static_cast< Service * > ( service_abstract () );
}

bool
Client::connect_to ( Service_Abstract * sb_n )
{
  return ( dynamic_cast< Service * > ( sb_n ) != nullptr );
}

} // namespace snc::svs::frame::settings::office
