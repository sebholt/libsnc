/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/string/utility.hpp>
#include <snc/svs/frame/settings/utility.hpp>
#include <QDir>
#include <utility>

namespace snc::svs::frame::settings::office
{

Service::Service ( Provider_Cell & provider_n,
                   const std::vector< std::string > & directories_n )
: Super ( provider_n )
, _settings_file_name ( "office.json" )
, _directories ( directories_n )
{
  if ( !_directories.empty () ) {
    _settings_dir = _directories.front ();
    _settings_file = settings_file_abs ( _settings_dir );
  }
}

void
Service::read ()
{
  // Import from back to front
  auto & dirs = _directories;
  for ( auto itc = dirs.crbegin (), ite = dirs.crend (); itc != ite; ++itc ) {
    auto file = settings_file_abs ( *itc );
    log ().cat ( sev::logt::FL_DEBUG, "Reading office settings file ", file );
    utility::json_import ( _settings.database (), file );
  }
}

void
Service::write ()
{
  if ( _settings_file.empty () ) {
    return;
  }

  // Create settings directory
  {
    QDir dir ( QString::fromStdString ( _settings_dir ) );
    if ( !dir.exists () ) {
      log ().cat (
          sev::logt::FL_DEBUG, "Creating settings directory ", _settings_dir );
      if ( !dir.mkpath ( "." ) ) {
        log ().cat ( sev::logt::FL_DEBUG,
                     "Can't create settings directory ",
                     _settings_dir );
        return;
      }
    }
  }

  log ().cat (
      sev::logt::FL_DEBUG, "Writing office settings file ", _settings_file );
  utility::json_export ( _settings.database (), _settings_file );
}

std::string
Service::settings_file_abs ( std::string_view dir_n )
{
  if ( dir_n.empty () ) {
    return std::string ();
  }
  return sev::string::cat ( dir_n, "/", _settings_file_name );
}

} // namespace snc::svs::frame::settings::office
