/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/bat/ifc/service_abstract.hpp>
#include <snc/settings/settings.hpp>
#include <string>
#include <vector>

namespace snc::svs::frame::settings::office
{

/// @brief Service
///
class Service : public snc::bat::ifc::Service_Abstract
{
  // -- Types
  private:
  using Super = snc::bat::ifc::Service_Abstract;

  public:
  // -- Construction

  Service ( Provider_Cell & provider_n,
            const std::vector< std::string > & directories_n );

  // -- Accessors

  const snc::settings::Settings &
  settings () const
  {
    return _settings;
  }

  snc::settings::Settings &
  settings ()
  {
    return _settings;
  }

  // -- Client Interface

  void
  read ();

  void
  write ();

  private:
  // -- Utility

  std::string
  settings_file_abs ( std::string_view dir_n );

  private:
  // -- Attributes
  std::string _settings_file_name;
  std::vector< std::string > _directories;
  std::string _settings_dir;
  std::string _settings_file;
  snc::settings::Settings _settings;
};

} // namespace snc::svs::frame::settings::office
