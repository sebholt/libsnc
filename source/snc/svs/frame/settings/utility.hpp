/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/settings/types.hpp>
#include <string>
#include <vector>

namespace snc::svs::frame::settings::utility
{

bool
json_import ( snc::settings::Database_Reference db_n,
              const std::string & file_n );

void
json_export ( snc::settings::Database_Reference db_n,
              const std::string & file_n );

} // namespace snc::svs::frame::settings::utility
