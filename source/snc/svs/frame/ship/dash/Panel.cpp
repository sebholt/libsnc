/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "Panel.hpp"
#include <snc/svp/dash/Dash.hpp>
#include <snc/svs/frame/ship/office/clerk.hpp>

namespace snc::svs::frame::ship::dash
{

Panel::Panel ( const Panel_Init & init_n )
: Super ( init_n, "Ship" )
{
}

Panel::~Panel () = default;

Panel::Clerk_Factory_Handle
Panel::office_session_begin_factory ()
{
  return make_clerk_factory< snc::svs::frame::ship::office::Clerk,
                             sev::logt::Reference > (
      dash ().log ().parent_ref () );
}

} // namespace snc::svs::frame::ship::dash
