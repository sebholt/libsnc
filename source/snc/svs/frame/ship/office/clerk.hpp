/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <snc/svp/office/factor.hpp>
#include <snc/svs/frame/ship/office/service.hpp>

namespace snc::svs::frame::ship::office
{

// -- Forward declaration
class Client;
class Client_Session;

class Clerk : public snc::svp::office::Factor
{
  // -- Types
  private:
  using Super = snc::svp::office::Factor;

  public:
  // -- Construction

  Clerk ( const Clerk_Init & init_n,
          sev::logt::Reference const & bridge_log_parent_n );

  ~Clerk () override;

  // -- Cell session interface

  bool
  cell_session_is_ready_to_end () const override;

  // -- Bridge session interface

  void
  bridge_session_begin_factory ( Sailor_Picker & pick_n ) override;

  // -- Central processing

  void
  process_async () override;

  void
  process () override;

  private:
  Service _service;
};

} // namespace snc::svs::frame::ship::office
