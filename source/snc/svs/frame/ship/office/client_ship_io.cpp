/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "client_ship_io.hpp"
#include <sev/assert.hpp>
#include <sev/utility.hpp>
#include <snc/svp/office/factor.hpp>
#include <snc/svs/frame/ship/office/service.hpp>

namespace snc::svs::frame::ship::office
{

Client_Ship_IO::Client_Ship_IO ( Factor * user_n )
: Super ( user_n )
{
  session ().set_begin_callback ( [ this ] () {
    // Check that all event pools have no capacity to begin with.
    // All bridge events should be allocated in bridge_session_begin_sailor().
    if ( epool_tracker ().any_not_empty () ) {
      throw std::runtime_error (
          "There should not be any bridge events to begin with." );
    }
  } );

  session ().set_end_callback ( [ this ] () {
    // Check that all event pools have had some capacity.
    // If not, some rarely used events might not have been sendable at all.
    if ( !sailors ().empty () ) {
      if ( epool_tracker ().any_empty () ) {
        throw std::runtime_error (
            "All bridge event pools should have had some capacity." );
      }
    }

    _epool_tracker.clear_all_capacities ();
    _sailors.clear ();
    _stats_out = 0;
    _pick_up_requested = false;
    _notifications.clear ();
  } );
}

Client_Ship_IO::~Client_Ship_IO () = default;

Client_Ship_IO::Factor *
Client_Ship_IO::factor ()
{
  return static_cast< Factor * > ( user () );
}

const Client_Ship_IO::Factor *
Client_Ship_IO::factor () const
{
  return static_cast< const Factor * > ( user () );
}

void
Client_Ship_IO::notifications_assign ( std::uint_fast32_t messages_n )
{
  if ( !session ().is_good () ) {
    return;
  }

  const std::uint_fast32_t before = notifications ();
  _notifications.assign ( messages_n );
  if ( before == 0 ) {
    request_pick_up ();
  }
}

void
Client_Ship_IO::notification_set ( std::uint_fast32_t messages_n )
{
  if ( !session ().is_good () ) {
    return;
  }

  const std::uint_fast32_t before = notifications ();
  _notifications.set ( messages_n );
  if ( before == 0 ) {
    request_pick_up ();
  }
}

void
Client_Ship_IO::notification_unset ( std::uint_fast32_t messages_n )
{
  _notifications.unset ( messages_n );
}

void
Client_Ship_IO::request_pick_up ()
{
  if ( session ().is_good () && sev::change ( _pick_up_requested, true ) ) {
    service ()->request_pick_up ( static_cast< Factor * > ( user () ) );
  }
}

void
Client_Ship_IO::pick_up ()
{
  if ( sev::change ( _pick_up_requested, false ) ) {
    static_cast< Factor * > ( user () )->bridge_notify ();
  }
}

void
Client_Ship_IO::submit ( Bridge_Notification * event_n )
{
  if ( session ().is_good () ) {
    stats_out_increment ();
    service ()->submit ( event_n );
  } else {
    release ( event_n );
  }
}

void
Client_Ship_IO::release ( Bridge_Notification * event_n )
{
  // Ensure that we own this event
  DEBUG_ASSERT ( epool_tracker ().owned ( event_n ) );
  // Reset release the event
  event_n->pool ()->casted_reset_release_virtual ( event_n );
  // We might be waiting for a free event
  if ( !notifications ().is_empty () ) {
    request_pick_up ();
  }
  user ()->request_processing ();
}

} // namespace snc::svs::frame::ship::office
