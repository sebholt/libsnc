/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/utility.hpp>
#include <snc/device/info.hpp>
#include <snc/svp/ship/sailor_factory_abstract.hpp>
#include <snc/svs/ctl/actor/office/clerk.hpp>
#include <snc/svs/ctl/actor/office/service.hpp>
#include <snc/svs/frame/ship/office/clerk.hpp>

namespace snc::svs::frame::ship::office
{

Service::Service ( Provider & provider_n,
                   sev::logt::Reference const & bridge_log_parent_n )
: Super ( provider_n )
, _ship_battery ( { bridge_log_parent_n, "Ship" },
                  provider_cell ().cell_context ().thread_tracker (),
                  "Ship" )
{
  _event_accu.set_callback ( provider_cell ().processing_request () );
  _event_accu_async.set_callback (
      provider_cell ().processing_request_async () );

  // Bridge connection
  _ship_connection.set_incoming_notifier (
      [ &ea = _event_accu_async ] () { ea.set ( LEvent::BRIDGE_IN ); } );
  _ship_connection.set_push_notifier (
      [ &ea = _event_accu ] () { ea.set ( LEvent::BRIDGE_OUT ); } );
}

Service::Provider &
Service::provider ()
{
  return static_cast< Provider & > ( provider_cell () );
}

const Service::Provider &
Service::provider () const
{
  return static_cast< const Provider & > ( provider_cell () );
}

bool
Service::start ()
{
  if ( is_running () ) {
    DEBUG_ASSERT ( false );
    return false;
  }

  // Sailor clerks
  find_clerks_by_type ( _factors );
  // Allocate buffer for bridge event generation requests
  _notification_factors.set_capacity ( _factors.size () );

  // Event queue
  _event_queue = sev::event::queue_io::Reference_2::created ();
  _ship_connection.connect ( _event_queue.link_a () );

  // Begin ship clerks bridge session
  if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
    logo << "Ship start: Beginning bridge sessions of " << _factors.size ()
         << " factor clerks.";
  }
  for ( auto * client : clients () ) {
    if ( auto * cclient = dynamic_cast< Client_Ship_IO * > ( client ) ) {
      cclient->session ().begin ();
    }
  }
  for ( auto * clerk : _factors ) {
    clerk->bridge_session_begin ();
  }

  // Start ship
  {
    snc::bat::Starter starter;
    // Collect pilot cell factories
    starter.cell_factories.reserve ( _factors.size () * 32 );
    {
      snc::svp::office::Factor::Sailor_Picker pick =
          [ &list = starter.cell_factories ] (
              snc::svp::office::Factor::Sailor_Factory_Handle && handle_n ) {
            list.push_back ( std::move ( handle_n ) );
          };
      for ( auto * clerk : _factors ) {
        clerk->bridge_session_begin_factory ( pick );
      }
    }
    // Fill sailor factories
    {
      auto device_info = snc::device::handle::make_info (
          provider ().tracker ().device_info ()->statics () );
      for ( auto & fac : starter.cell_factories ) {
        auto & cfac =
            dynamic_cast< snc::svp::ship::Sailor_Factory_Abstract & > ( *fac );
        cfac.set_device_info ( device_info );
      }
    }
    // Setup processing end callback
    starter.end_callback = [ &ea = _event_accu_async ] () {
      ea.set ( LEvent::PILOTS_READY_TO_END );
    };

    // Start ship processing battery
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Ship start: Starting ship processor." );
    _ship_battery.start ( std::move ( starter ) );
  }

  // Enable event io
  _ship_event_io = true;

  // Call post sailor construction begin function
  for ( auto * clerk : _factors ) {
    if ( !clerk->ship_io ().sailors ().empty () ) {
      clerk->bridge_session_begin_sailor ();
    }
  }

  signal_state ().send ();
  return is_running ();
}

void
Service::abort ( bool forced_n )
{
  if ( !is_running () ) {
    return;
  }

  log ().cat ( sev::logt::FL_DEBUG_0, "Ship abort! Forced=", forced_n );

  _ship_abort_state.set ( Ship_Abort_State::ABORT );
  if ( forced_n ) {
    _ship_abort_state.set ( Ship_Abort_State::FORCED );
  }

  provider_cell ().request_processing ();
  signal_state ().send ();
}

void
Service::request_pick_up ( snc::svp::office::Factor * factor_n )
{
  DEBUG_ASSERT ( !_notification_factors.is_full () );
  _notification_factors.push_back ( factor_n );
  _event_accu.set ( LEvent::BRIDGE_EVENT_GENERATION );
}

void
Service::process_async ()
{
  _event_accu.set_silent ( _event_accu_async.fetch_and_clear () );
  process ();
}

void
Service::process ()
{
  // Bridge event generation
  if ( _event_accu.test_any_unset ( LEvent::BRIDGE_EVENT_GENERATION ) ) {
    process_bridge_notification ();
  }

  // Process bridge events
  if ( _event_accu.test_any ( LEvent::BRIDGE_IN | LEvent::BRIDGE_OUT ) ) {
    // Feed bridge queue
    _ship_connection.feed_queue (
        _event_accu.flags (), LEvent::BRIDGE_IN, LEvent::BRIDGE_OUT );

    // Release returned events
    _ship_connection.out ().release_all_casted< Bridge_Notification > (
        [] ( Bridge_Notification * event_n ) {
          event_n->pair ().factor ()->ship_io ().release ( event_n );
        } );

    // Deliver incoming events
    if ( _ship_event_io ) {
      _ship_connection.in ().process_all_casted< const Bridge_Event > (
          [] ( const Bridge_Event & event_n ) {
            event_n.pair ().factor ()->bridge_event ( event_n );
          } );
    } else {
      _ship_connection.in ().accept_all ();
    }
  }

  // Process abort
  if ( _ship_abort_state ) {
    if ( _event_accu.test_any_unset ( LEvent::PILOTS_READY_TO_END ) ) {
      _ship_abort_state.set ( Ship_Abort_State::PILOTS_READY_TO_END );
    }
    process_ship_abort ();
  }

  // Process again on demand
  _event_accu.notify_not_empty ();
}

void
Service::process_bridge_notification ()
{
  // Poll clerks to generate bridge events
  while ( !_notification_factors.is_empty () ) {
    auto * factor = _notification_factors.front ();
    _notification_factors.pop_front_not_empty ();
    factor->ship_io ().pick_up ();
  }
}

void
Service::process_ship_abort ()
{
  // -- Soft abort

  if ( !_ship_abort_state.test_any ( Ship_Abort_State::FORCED ) ) {
    // Test if all actors clerks are idling
    for ( auto * cell : provider_cell ().cell_context ().cells () ) {
      if ( auto * clerk =
               dynamic_cast< snc::svs::ctl::actor::office::Clerk * > (
                   cell ) ) {
        if ( !clerk->service ().all_idle () ) {
          return;
        }
        break;
      }
    }
  }

  // -- Hard abort

  // Abort pilot controllers and send pilot abort event
  if ( !_ship_abort_state.test_any_set ( Ship_Abort_State::PILOTS_ABORTED ) ) {
    // Abort pilot controllers bridge session
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Ship abort: Aborting bridge sessions of " << _factors.size ()
           << " factor clerks.";
    }
    for ( auto * client : clients () ) {
      if ( auto * cclient = dynamic_cast< Client_Ship_IO * > ( client ) ) {
        cclient->session ().end ();
      }
    }
    for ( auto * clerk : _factors ) {
      clerk->bridge_session_abort ();
    }

    // Disable event io
    _ship_event_io = false;

    // Send pilot abort event
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Ship abort: Aborting ship processor." );
    _ship_battery.abort ();
  }

  // Test if event buffers are empty
  if ( !_notification_factors.is_empty () ) {
    return;
  }
  if ( !_ship_connection.all_empty () ) {
    return;
  }

  // Test if all pilots are ready to end
  if ( !_ship_abort_state.test_any ( Ship_Abort_State::PILOTS_READY_TO_END ) ) {
    return;
  }

  // Test if all pilot controllers are ready to end
  if ( !_ship_abort_state.test_any (
           Ship_Abort_State::CONTROLLERS_READY_TO_END ) ) {
    for ( auto * clerk : _factors ) {
      if ( clerk->bridge_session_is_ready_to_end () ) {
        continue;
      }
      // Not ready to end
      clerk->log ().cat (
          sev::logt::FL_DEBUG_0,
          "Bridge session abort: Bridge session is not ready to end." );
      return;
    }

    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Ship session abort: Maker clerks are ready to end" );
    _ship_abort_state.set ( Ship_Abort_State::CONTROLLERS_READY_TO_END );
  }

  if ( _ship_battery.is_running () ) {
    // Stop bridge processing battery
    log ().cat ( sev::logt::FL_DEBUG_0,
                 "Ship session abort: Stopping ship processor." );
    _ship_battery.stop ();

    // End pilot controllers bridge session
    if ( auto logo = log ().ostr ( sev::logt::FL_DEBUG_0 ) ) {
      logo << "Ship abort: Ending bridge sessions of " << _factors.size ()
           << " factor clerks.";
    }
    for ( auto * clerk : sev::reverse ( _factors ) ) {
      clerk->bridge_session_end ();
    }

    // Bridge and device both stopped
    _ship_abort_state.clear ();
    _ship_connection.disconnect ();
    _event_queue.clear ();

    // Clean up
    _factors.clear ();

    // Notify
    provider_cell ().request_processing ();
    signal_state ().send ();
  }
}

} // namespace snc::svs::frame::ship::office
