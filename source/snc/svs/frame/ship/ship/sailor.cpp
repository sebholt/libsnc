/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "sailor.hpp"
#include <sev/math/exponents.hpp>

namespace snc::svs::frame::ship::ship
{

Sailor::Sailor ( const Sailor_Init & init_n,
                 sev::event::queue_io::Link_2 eql_link_n )
: Super ( init_n, "Ship" )
, _service ( *this, eql_link_n )
{
}

Sailor::~Sailor () = default;

void
Sailor::cell_session_begin ()
{
  Super::cell_session_begin ();
  _service.session_begin ();
}

bool
Sailor::cell_session_is_ready_to_end () const
{
  return Super::cell_session_is_ready_to_end () &&
         _service.session_is_ready_to_end ();
}

void
Sailor::cell_session_end ()
{
  _service.session_end ();
  Super::cell_session_end ();
}

void
Sailor::process_async ()
{
  _service.process_async ();
}

void
Sailor::process ()
{
  _service.process ();
}

} // namespace snc::svs::frame::ship::ship
