/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "service.hpp"
#include <sev/math/exponents.hpp>
#include <sev/utility.hpp>
#include <snc/bat/cell.hpp>
#include <snc/svp/ship/sailor.hpp>

namespace snc::svs::frame::ship::ship
{

Service::Service ( Provider_Cell & provider_n,
                   const sev::event::queue_io::Link_2 & event_queue_link_n )
: Super ( provider_n )
{
  _event_accu.set_callback ( provider_cell ().processing_request () );
  _event_accu_async.set_callback (
      provider_cell ().processing_request_async () );

  // -- Office queue connection
  _office_connection.set_incoming_notifier (
      [ &ec = _event_accu_async ] () { ec.set ( LEvent::OFFICE_IN ); } );
  _office_connection.set_push_notifier (
      [ &ec = _event_accu ] () { ec.set ( LEvent::OFFICE_OUT ); } );
  _office_connection.connect ( event_queue_link_n );
}

Service::~Service () = default;

void
Service::session_begin ()
{
  // Allocate buffer for office event generation requests
  _office_egen_pilots_buffer.resize (
      provider_cell ().cell_context ().cells ().size (), nullptr );
  _office_egen_pilots.setup ( _office_egen_pilots_buffer.data (),
                              _office_egen_pilots_buffer.size () );
}

bool
Service::session_is_ready_to_end () const
{
  return _office_connection.all_empty ();
}

void
Service::session_end ()
{
}

void
Service::process_async ()
{
  // Fetch asynchronous bits
  _event_accu.set_silent ( _event_accu_async.fetch_and_clear () );
  process ();
}

void
Service::process ()
{
  if ( _event_accu.test_any_unset ( LEvent::OFFICE_EVENT_GENERATION ) ) {
    process_office_event_generation ();
  }

  if ( _event_accu.test_any ( LEvent::OFFICE_IN | LEvent::OFFICE_OUT ) ) {
    process_office_event_io ();
  }

  // Process again on demand
  _event_accu.notify_not_empty ();
}

void
Service::process_office_event_generation ()
{
  // Poll pilots to generate office events
  while ( !_office_egen_pilots.is_empty () ) {
    auto * sailor = static_cast< Sailor * > ( _office_egen_pilots.pop () );
    sailor->office_io ().pick_up ();
  }
}

void
Service::process_office_event_io ()
{
  // Feed event queue
  _office_connection.feed_queue (
      _event_accu.flags (), LEvent::OFFICE_IN, LEvent::OFFICE_OUT );

  // Release returned events
  _office_connection.out ().release_all_casted< Office_Notification > (
      [] ( Office_Notification * event_n ) {
        event_n->pair ().sailor ()->office_io ().release ( event_n );
      } );

  // Deliver incoming events
  _office_connection.in ().process_all_casted< const Office_Event > (
      [] ( const Office_Event & event_n ) {
        auto * sailor = event_n.pair ().sailor ();
        sailor->office_io ().stats_in_increment ();
        sailor->office_event ( event_n );
      } );
}

void
Service::office_notify_register ( Sailor * sailor_n )
{
  _office_egen_pilots.push ( sailor_n );
  _event_accu.set ( LEvent::OFFICE_EVENT_GENERATION );
}

} // namespace snc::svs::frame::ship::ship
