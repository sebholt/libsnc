/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device.hpp"

namespace snc::usb
{

Device::Device ()
: _lusb_device_handle ( 0 )
, _configuration ( -1 )
, _claimed_interface ( -1 )
, _lusb_open_error ( 0 )
, _lusb_configuration_error ( 0 )
, _lusb_claim_interface_error ( 0 )
{
}

Device::~Device ()
{
  close ();
}

void
Device::close ()
{
  if ( is_open () ) {
    // Release interface claim
    release_interface ();
    // Close device handle
    libusb_close ( _lusb_device_handle );
    _lusb_device_handle = 0;
    // Clear open state
    _port_id.clear ();
    _configuration = -1;
    _claimed_interface = -1;
  }
}

void
Device::clear ()
{
  close ();
  // Clear error registers
  _lusb_open_error = 0;
  _lusb_configuration_error = 0;
  _lusb_claim_interface_error = 0;
}

bool
Device::open_by_port_id ( libusb_context * lusb_context_n,
                          const snc::usb::Device_Port_Id & port_id_n )
{
  // Clear before open
  clear ();
  // Open
  if ( ( lusb_context_n != 0 ) && port_id_n.is_valid () ) {
    libusb_device * lusb_dev_match ( 0 );
    // -- Find the device in the available devices list
    {
      libusb_device ** lusb_dev_list ( 0 );
      const int llen =
          libusb_get_device_list ( lusb_context_n, &lusb_dev_list );
      if ( llen >= 0 ) {
        libusb_device ** it_cur ( lusb_dev_list );
        libusb_device ** it_end ( lusb_dev_list + llen );
        for ( ; it_cur != it_end; ++it_cur ) {
          if ( port_id_n == snc::usb::Device_Port_Id ( *it_cur ) ) {
            // Match found!
            // Keep device, ref for later use and leave loop
            lusb_dev_match = *it_cur;
            libusb_ref_device ( lusb_dev_match );
            break;
          }
        }
        // Free list and dereference all devices
        libusb_free_device_list ( lusb_dev_list, 1 );
      }
    }
    // -- Check if the device was found and open it
    if ( lusb_dev_match != 0 ) {
      {
        libusb_device_handle * dev_handle;
        _lusb_open_error = libusb_open ( lusb_dev_match, &dev_handle );
        if ( _lusb_open_error == 0 ) {
          // Device opened!
          // Register device handle
          _lusb_device_handle = dev_handle;
          // Update port id
          _port_id.load ( libusb_get_device ( _lusb_device_handle ) );
        }
      }
      // Unref - Open devices hold an own reference
      libusb_unref_device ( lusb_dev_match );
    }
  }
  return is_open ();
}

bool
Device::set_configuration ( int config_n )
{
  if ( is_open () ) {
    // Release a claimed interface
    release_interface ();
    // Set device configuration
    _lusb_configuration_error =
        libusb_set_configuration ( _lusb_device_handle, config_n );
    if ( _lusb_configuration_error == 0 ) {
      _configuration = config_n;
      return true;
    }
  }
  return false;
}

bool
Device::claim_interface ( int interface_n )
{
  if ( is_open () && configuration_valid () ) {
    if ( _claimed_interface != interface_n ) {
      release_interface ();
      if ( interface_n >= 0 ) {
        // Try to claim the interface
        _lusb_claim_interface_error =
            libusb_claim_interface ( _lusb_device_handle, interface_n );
        if ( _lusb_claim_interface_error == 0 ) {
          // Claim success
          _claimed_interface = interface_n;
        }
      }
    }
  }
  return ( _claimed_interface == interface_n );
}

void
Device::release_interface ()
{
  if ( claimed_interface_valid () ) {
    libusb_release_interface ( _lusb_device_handle, _claimed_interface );
    _claimed_interface = -1;
  }
}

const char *
Device::lusb_open_error_string () const
{
  return error_string_open ( lusb_open_error () );
}

const char *
Device::lusb_configuration_error_string () const
{
  return error_string_set_configuration ( lusb_configuration_error () );
}

const char *
Device::lusb_claim_interface_error_string () const
{
  return error_string_claim_interface ( lusb_claim_interface_error () );
}

} // namespace snc::usb
