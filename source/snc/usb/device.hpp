/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <libusb-1.0/libusb.h>
#include <snc/usb/device_port_id.hpp>
#include <snc/usb/error_string.hpp>
#include <cstdint>

namespace snc::usb
{

/// @brief Utility class to openn a usb device
///
class Device
{
  public:
  // -- Construction

  Device ();

  ~Device ();

  // -- Open / close

  bool
  open_by_port_id ( libusb_context * lusb_context_n,
                    const snc::usb::Device_Port_Id & port_id_n );

  bool
  is_open () const
  {
    return ( _lusb_device_handle != nullptr );
  }

  const snc::usb::Device_Port_Id &
  port_id () const
  {
    return _port_id;
  }

  /// @brief Closes the device but keeps the error return values unchanged.
  void
  close ();

  /// @brief Closes the device if is_open() and clears all errors
  void
  clear ();

  // -- Configuration setting

  int
  configuration () const
  {
    return _configuration;
  }

  bool
  configuration_valid () const
  {
    return ( _configuration >= 0 );
  }

  bool
  set_configuration ( int config_n );

  // -- Interface claim

  bool
  claim_interface ( int interface_n );

  int
  claimed_interface () const
  {
    return _claimed_interface;
  }

  bool
  claimed_interface_valid () const
  {
    return ( _claimed_interface >= 0 );
  }

  void
  release_interface ();

  // -- Libusb return values

  int
  lusb_open_error () const
  {
    return _lusb_open_error;
  }

  const char *
  lusb_open_error_string () const;

  int
  lusb_configuration_error () const
  {
    return _lusb_configuration_error;
  }

  const char *
  lusb_configuration_error_string () const;

  int
  lusb_claim_interface_error () const
  {
    return _lusb_claim_interface_error;
  }

  const char *
  lusb_claim_interface_error_string () const;

  // -- Libusb structs

  libusb_device *
  lusb_device () const
  {
    if ( _lusb_device_handle != nullptr ) {
      return libusb_get_device ( _lusb_device_handle );
    }
    return nullptr;
  }

  libusb_device_handle *
  lusb_device_handle () const
  {
    return _lusb_device_handle;
  }

  // Public attributes
  public:
  libusb_device_handle * _lusb_device_handle;
  int _configuration;
  int _claimed_interface;
  int _lusb_open_error;
  int _lusb_configuration_error;
  int _lusb_claim_interface_error;
  snc::usb::Device_Port_Id _port_id;
};

} // namespace snc::usb
