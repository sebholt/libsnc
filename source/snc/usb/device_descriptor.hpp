/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <libusb-1.0/libusb.h>
#include <array>
#include <cstdint>
#include <string>

namespace snc::usb
{

/// @brief USB device descriptor data
///
class Device_Descriptor
{
  public:
  // -- Types

  typedef std::array< std::uint8_t, 3 > BCD_USB;

  // -- Construction

  Device_Descriptor ();

  // -- Clear / Load

  void
  clear ();

  bool
  is_valid () const
  {
    return _is_valid;
  }

  void
  load ( libusb_device_handle * dev_n );

  // -- Accessors

  const BCD_USB &
  bcd_usb () const
  {
    return _bcd_usb;
  }

  std::uint8_t
  iManufacturer () const
  {
    return _iManufacturer;
  }

  std::uint8_t
  iProduct () const
  {
    return _iProduct;
  }

  std::uint8_t
  iSerial () const
  {
    return _iSerial;
  }

  const std::string &
  string_manufacturer () const
  {
    return _string_manufacturer;
  }

  const std::string &
  string_product () const
  {
    return _string_product;
  }

  const std::string &
  string_serial () const
  {
    return _string_serial;
  }

  private:
  // -- Attributes
  public:
  bool _is_valid = false;
  BCD_USB _bcd_usb;
  std::uint8_t _iManufacturer = 0;
  std::uint8_t _iProduct = 0;
  std::uint8_t _iSerial = 0;
  std::string _string_manufacturer;
  std::string _string_product;
  std::string _string_serial;
};

} // namespace snc::usb
