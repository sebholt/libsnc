/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device_port_id.hpp"

namespace snc::usb
{

void
Device_Port_Id::load ( libusb_device * dev_n )
{
  if ( dev_n != nullptr ) {
    _is_valid = true;
    _bus_number = libusb_get_bus_number ( dev_n );
    _bus_address = libusb_get_device_address ( dev_n );
    _port_number = libusb_get_port_number ( dev_n );
  } else {
    clear ();
  }
}

} // namespace snc::usb
