/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "device_query.hpp"

namespace snc::usb
{

Device_Query::Device_Query () = default;

bool
Device_Query::operator== ( const Device_Query & query_n ) const
{
  return ( manufacturer == query_n.manufacturer ) &&
         ( product == query_n.product ) &&
         ( serial_number == query_n.serial_number );
}

} // namespace snc::usb
