/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "error_string.hpp"
#include <libusb-1.0/libusb.h>

namespace snc::usb
{

const char *
error_string ( libusb_error error_n )
{
  const char * res ( 0 );
  switch ( error_n ) {
  case LIBUSB_SUCCESS:
    res = "LIBUSB_SUCCESS";
    break;
  case LIBUSB_ERROR_IO:
    res = "LIBUSB_ERROR_IO";
    break;
  case LIBUSB_ERROR_INVALID_PARAM:
    res = "LIBUSB_ERROR_INVALID_PARAM";
    break;
  case LIBUSB_ERROR_ACCESS:
    res = "LIBUSB_ERROR_ACCESS";
    break;
  case LIBUSB_ERROR_NO_DEVICE:
    res = "LIBUSB_ERROR_NO_DEVICE";
    break;
  case LIBUSB_ERROR_NOT_FOUND:
    res = "LIBUSB_ERROR_NOT_FOUND";
    break;
  case LIBUSB_ERROR_BUSY:
    res = "LIBUSB_ERROR_BUSY";
    break;
  case LIBUSB_ERROR_TIMEOUT:
    res = "LIBUSB_ERROR_TIMEOUT";
    break;
  case LIBUSB_ERROR_OVERFLOW:
    res = "LIBUSB_ERROR_OVERFLOW";
    break;
  case LIBUSB_ERROR_PIPE:
    res = "LIBUSB_ERROR_PIPE";
    break;
  case LIBUSB_ERROR_INTERRUPTED:
    res = "LIBUSB_ERROR_INTERRUPTED";
    break;
  case LIBUSB_ERROR_NO_MEM:
    res = "LIBUSB_ERROR_NO_MEM";
    break;
  case LIBUSB_ERROR_NOT_SUPPORTED:
    res = "LIBUSB_ERROR_NOT_SUPPORTED";
    break;
  case LIBUSB_ERROR_OTHER:
    res = "LIBUSB_ERROR_OTHER";
    break;
  }
  return res;
}

const char *
error_string_open ( int error_n )
{
  const char * res ( 0 );
  switch ( error_n ) {
  case LIBUSB_ERROR_NO_MEM:
    res = "LIBUSB_ERROR_NO_MEM : Memory allocation error";
    break;
  case LIBUSB_ERROR_ACCESS:
    res = "LIBUSB_ERROR_ACCESS : Insufficient access rights";
    break;
  case LIBUSB_ERROR_NO_DEVICE:
    res = "LIBUSB_ERROR_NO_DEVICE : The device does not exist or has "
          "disappeared";
    break;
  default:
    res = error_string ( (libusb_error)error_n );
    break;
  }
  return res;
}

const char *
error_string_set_configuration ( int error_n )
{
  const char * res ( 0 );
  switch ( error_n ) {
  case LIBUSB_ERROR_NOT_FOUND:
    res = "LIBUSB_ERROR_NOT_FOUND : Requested configuration not available";
    break;
  case LIBUSB_ERROR_BUSY:
    res = "LIBUSB_ERROR_BUSY : Device is busy";
    break;
  case LIBUSB_ERROR_NO_DEVICE:
    res = "LIBUSB_ERROR_NO_DEVICE : Device has disappeared";
    break;
  default:
    res = error_string ( (libusb_error)error_n );
    break;
  }
  return res;
}

const char *
error_string_claim_interface ( int error_n )
{
  const char * res ( 0 );
  switch ( error_n ) {
  case LIBUSB_ERROR_NOT_FOUND:
    res = "LIBUSB_ERROR_NOT_FOUND : Requested interface not available";
    break;
  case LIBUSB_ERROR_BUSY:
    res = "LIBUSB_ERROR_BUSY : Device is busy";
    break;
  case LIBUSB_ERROR_NO_DEVICE:
    res = "LIBUSB_ERROR_NO_DEVICE : Device has disappeared";
    break;
  default:
    res = error_string ( (libusb_error)error_n );
    break;
  }
  return res;
}

const char *
error_string_transfer_submit ( int error_n )
{
  const char * res ( 0 );
  switch ( error_n ) {
  case LIBUSB_ERROR_NO_DEVICE:
    res = "LIBUSB_ERROR_NO_DEVICE : Device has disappeared";
    break;
  case LIBUSB_ERROR_BUSY:
    res = "LIBUSB_ERROR_BUSY : Transfer already submitted";
    break;
  case LIBUSB_ERROR_NOT_SUPPORTED:
    res = "LIBUSB_ERROR_NOT_SUPPORTED : Transfer flags not supported by "
          "operating system";
    break;
  default:
    res = error_string ( (libusb_error)error_n );
    break;
  }
  return res;
}

const char *
error_string_transfer_return ( libusb_transfer_status status_n )
{
  const char * res ( 0 );
  switch ( status_n ) {
  case LIBUSB_TRANSFER_COMPLETED:
    res = "LIBUSB_TRANSFER_COMPLETED : Transfer completed";
    break;
  case LIBUSB_TRANSFER_ERROR:
    res = "LIBUSB_TRANSFER_ERROR : Unknown error";
    break;
  case LIBUSB_TRANSFER_TIMED_OUT:
    res = "LIBUSB_TRANSFER_TIMED_OUT : Transfer timed out";
    break;
  case LIBUSB_TRANSFER_CANCELLED:
    res = "LIBUSB_TRANSFER_CANCELLED : Transfer was cancelled";
    break;
  case LIBUSB_TRANSFER_STALL:
    res = "LIBUSB_TRANSFER_STALL : Device endpoint halt condition detected "
          "(endpoint stalled)";
    break;
  case LIBUSB_TRANSFER_NO_DEVICE:
    res = "LIBUSB_TRANSFER_NO_DEVICE : Device has disappeared";
    break;
  case LIBUSB_TRANSFER_OVERFLOW:
    res = "LIBUSB_TRANSFER_OVERFLOW : Device sent more data than requested";
    break;
  }
  return res;
}

const char *
transfer_status_name ( libusb_transfer_status status_n )
{
  const char * res ( "Unknown transfer status" );
  switch ( status_n ) {
  case LIBUSB_TRANSFER_COMPLETED:
    res = "LIBUSB_TRANSFER_COMPLETED";
    break;
  case LIBUSB_TRANSFER_ERROR:
    res = "LIBUSB_TRANSFER_ERROR";
    break;
  case LIBUSB_TRANSFER_TIMED_OUT:
    res = "LIBUSB_TRANSFER_TIMED_OUT";
    break;
  case LIBUSB_TRANSFER_CANCELLED:
    res = "LIBUSB_TRANSFER_CANCELLED";
    break;
  case LIBUSB_TRANSFER_STALL:
    res = "LIBUSB_TRANSFER_STALL";
    break;
  case LIBUSB_TRANSFER_NO_DEVICE:
    res = "LIBUSB_TRANSFER_NO_DEVICE";
    break;
  case LIBUSB_TRANSFER_OVERFLOW:
    res = "LIBUSB_TRANSFER_OVERFLOW";
    break;
  }
  return res;
}

} // namespace snc::usb
