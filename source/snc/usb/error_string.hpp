/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <libusb-1.0/libusb.h>

namespace snc::usb
{

// -- Error strings accessors

const char *
error_string ( libusb_error error_n );

const char *
error_string_open ( int error_n );

const char *
error_string_set_configuration ( int error_n );

const char *
error_string_claim_interface ( int error_n );

const char *
error_string_transfer_submit ( int error_n );

const char *
error_string_transfer_return ( libusb_transfer_status status_n );

const char *
transfer_status_name ( libusb_transfer_status status_n );

} // namespace snc::usb
