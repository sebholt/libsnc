/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#include "debug_file.hpp"
#include <sev/math/numbers.hpp>
#include <sev/utility.hpp>
#include <cmath>

namespace snc::utility
{

Debug_File::Debug_File ()
: _double_precision ( 6 )
{
}

Debug_File::~Debug_File ()
{
  close ();
}

bool
Debug_File::open ( std::string file_name_n )
{
  close ();

  _file_name = file_name_n;
  if ( !_file_name.empty () ) {
    _ofstream.open ( _file_name );
    if ( is_open () ) {
      _ofstream.precision ( _double_precision );
      _ofstream << std::fixed;
    }
  }

  return is_open ();
}

void
Debug_File::close ()
{
  _ofstream.close ();
}

void
Debug_File::set_double_precision ( std::uint_fast32_t value_n )
{
  if ( sev::change ( _double_precision, value_n ) ) {
    if ( is_open () ) {
      _ofstream.precision ( _double_precision );
      _ofstream << std::fixed;
    }
  }
}

void
Debug_File::write ( const char * text_n )
{
  _ofstream << text_n;
}

void
Debug_File::write ( const std::string & text_n )
{
  _ofstream << text_n;
}

void
Debug_File::write ( double value_n )
{
  _ofstream << value_n;
}

void
Debug_File::write ( std::int8_t value_n )
{
  _ofstream << static_cast< std::int_fast32_t > ( value_n );
}

void
Debug_File::write ( std::uint8_t value_n )
{
  _ofstream << static_cast< std::uint_fast32_t > ( value_n );
}

void
Debug_File::write ( std::int32_t value_n )
{
  _ofstream << value_n;
}

void
Debug_File::write ( std::uint32_t value_n )
{
  _ofstream << value_n;
}

void
Debug_File::write ( std::int64_t value_n )
{
  _ofstream << value_n;
}

void
Debug_File::write ( std::uint64_t value_n )
{
  _ofstream << value_n;
}

void
Debug_File::flush ()
{
  _ofstream.flush ();
}

} // namespace snc::utility
