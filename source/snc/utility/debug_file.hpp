/// libsnc: C++ library for numeric machine control.
///
/// \copyright See LICENSE-libsnc.txt file

#pragma once

#include <sev/lag/vector2.hpp>
#include <sev/lag/vector3.hpp>
#include <cstdint>
#include <fstream>
#include <string>

namespace snc::utility
{

class Debug_File
{
  public:
  // -- Construction

  Debug_File ();

  ~Debug_File ();

  // -- Open / close

  bool
  open ( std::string file_name_n );

  bool
  is_open ()
  {
    return _ofstream.is_open ();
  }

  void
  close ();

  const std::string
  file_name () const
  {
    return _file_name;
  }

  // -- Double precision

  std::uint_fast32_t
  double_precision () const
  {
    return _double_precision;
  }

  void
  set_double_precision ( std::uint_fast32_t value_n );

  // -- Writing

  void
  write ( const char * text_n );

  void
  write ( const std::string & text_n );

  template < std::size_t DIM >
  void
  write ( const sev::lag::Vector< double, DIM > & vector_n )
  {
    _ofstream << vector_n;
  }

  void
  write ( double value_n );

  void
  write ( std::int8_t value_n );

  void
  write ( std::uint8_t value_n );

  void
  write ( std::int32_t value_n );

  void
  write ( std::uint32_t value_n );

  void
  write ( std::int64_t value_n );

  void
  write ( std::uint64_t value_n );

  void
  flush ();

  private:
  std::ofstream _ofstream;
  std::string _file_name;
  std::uint_fast32_t _double_precision;
};

} // namespace snc::utility
