#include <snc/settings/detail/address_parser.hpp>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>

using Address_Parser = snc::settings::detail::Address_Parser;

void
require ( bool flag_n )
{
  if ( flag_n ) {
    return;
  }

  throw std::runtime_error ( "Requirement not met" );
}

void
require_match ( std::string_view is_n, std::string_view should_n )
{
  if ( should_n == is_n ) {
    return;
  }
  std::ostringstream oss;
  oss << "String should be \"" << should_n << "\" but is \"" << is_n << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
require_match ( std::size_t is_n, std::size_t should_n )
{
  if ( should_n == is_n ) {
    return;
  }
  std::ostringstream oss;
  oss << "Integer should be \"" << should_n << "\" but is \"" << is_n << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
frag_name ( Address_Parser & adp_n, std::string_view name_n )
{
  require ( adp_n.iterate () );
  require ( adp_n.good () );
  require ( adp_n.is_name () );
  require_match ( adp_n.name (), name_n );
}

void
frag_array_index ( Address_Parser & adp_n, std::size_t index_n )
{
  require ( adp_n.iterate () );
  require ( adp_n.good () );
  require ( adp_n.is_array_index () );
  require_match ( adp_n.array_index (), index_n );
}

void
finished ( Address_Parser & adp_n )
{
  require ( adp_n.last_fragment () );
  require ( !adp_n.iterate () );
  require ( adp_n.good () );
  require_match ( adp_n.name (), "" );
}

int
main ( int argc, char * argv[] )
{
  (void)argc;
  (void)argv;

  // Empty
  {
    Address_Parser adp ( "" );
    finished ( adp );
  }

  // Single
  {
    Address_Parser adp ( "o" );
    frag_name ( adp, "o" );
    finished ( adp );
  }
  {
    Address_Parser adp ( "onelongname" );
    frag_name ( adp, "onelongname" );
    finished ( adp );
  }
  {
    Address_Parser adp ( "[]" );
    require ( adp.iterate () );
    require ( !adp.good () );
    require ( !adp.is_array_index () );
    require_match ( adp.name (), "" );

    require ( !adp.iterate () );
    require ( !adp.good () );
  }

  // Single with array index
  {
    Address_Parser adp ( "o[123]" );
    frag_name ( adp, "o" );
    frag_array_index ( adp, 123 );
    finished ( adp );
  }
  {
    Address_Parser adp ( "onelongname[9876]" );
    frag_name ( adp, "onelongname" );
    frag_array_index ( adp, 9876 );
    finished ( adp );
  }
  // Single with bad array index
  {
    Address_Parser adp ( "o[]" );
    frag_name ( adp, "o" );

    require ( adp.iterate () );
    require ( !adp.good () );
    require ( adp.is_array_index () );

    require ( !adp.iterate () );
    require ( !adp.good () );
  }
  {
    Address_Parser adp ( "o[z123]" );
    frag_name ( adp, "o" );

    require ( adp.iterate () );
    require ( !adp.good () );
    require ( adp.is_array_index () );

    require ( !adp.iterate () );
    require ( !adp.good () );
  }
  {
    Address_Parser adp ( "o[123]4" );
    frag_name ( adp, "o" );
    frag_array_index ( adp, 123 );

    require ( adp.iterate () );
    require ( !adp.good () );

    require ( !adp.iterate () );
    require ( !adp.good () );
  }

  // Dual
  {
    Address_Parser adp ( "o.t" );
    frag_name ( adp, "o" );
    frag_name ( adp, "t" );
    finished ( adp );
  }
  {
    Address_Parser adp ( "one.two" );
    frag_name ( adp, "one" );
    frag_name ( adp, "two" );
    finished ( adp );
  }

  // Dual with array index
  {
    Address_Parser adp ( "one[653].two" );
    frag_name ( adp, "one" );
    frag_array_index ( adp, 653 );
    frag_name ( adp, "two" );
    finished ( adp );
  }
  {
    Address_Parser adp ( "one.two[2345]" );
    frag_name ( adp, "one" );
    frag_name ( adp, "two" );
    frag_array_index ( adp, 2345 );
    finished ( adp );
  }

  // Nested array indices
  {
    Address_Parser adp ( "one[99][88][77].two[66][55].three" );
    frag_name ( adp, "one" );
    frag_array_index ( adp, 99 );
    frag_array_index ( adp, 88 );
    frag_array_index ( adp, 77 );
    frag_name ( adp, "two" );
    frag_array_index ( adp, 66 );
    frag_array_index ( adp, 55 );
    frag_name ( adp, "three" );
    finished ( adp );
  }

  return 0;
}
