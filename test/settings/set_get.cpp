#include <snc/settings/settings.hpp>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

using Settings = snc::settings::Settings;
using Value = snc::settings::Value;

void
require_value_type ( const Value & val_n, Value::Type type_n )
{
  if ( val_n.type () == type_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Value type was expected to be " << Value::type_name ( type_n )
      << " but is " << val_n.type_name () << "\n";
  throw std::runtime_error ( oss.str () );
}

void
value_none ( const Settings & sett_n, std::string_view key_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::NONE );
}

void
value_bool ( const Settings & sett_n, std::string_view key_n, bool value_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::BOOL );
  auto val_bool = val.as_bool ();
  if ( val_bool == value_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Boolean value for key \"" << key_n << "\" was expected to be \""
      << value_n << "\" but is \"" << val_bool << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
value_double ( const Settings & sett_n, std::string_view key_n, double value_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::DOUBLE );
  auto val_double = val.as_double ();
  if ( val_double == value_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Double value for key \"" << key_n << "\" was expected to be \""
      << value_n << "\" but is \"" << val_double << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
value_string ( const Settings & sett_n,
               std::string_view key_n,
               std::string_view value_n )
{
  auto val = sett_n.get ( key_n );
  require_value_type ( val, Value::Type::STRING );
  auto val_string = val.as_string ();
  if ( val_string == value_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "String value for key \"" << key_n << "\" was expected to be \""
      << value_n << "\" but is \"" << val_string << "\"\n";
  throw std::runtime_error ( oss.str () );
}

void
set_get ()
{
  {
    Settings sett;

    // Set values
    sett.set ( "false", false );
    sett.set ( "true", true );
    sett.set ( "dval", 1.2 );
    sett.set ( "sval", "stream" );
    sett.set ( "obj.tag", "notes" );
    sett.set ( "obj.l2.tag", "hints" );
    sett.set ( "obj.l2.value", 3.9 );

    sett.set ( "arr[0]", 19.56 );
    sett.set ( "arr[1]", 72.0 );
    sett.set ( "arr[1]", 75.0 );
    sett.set ( "arr[2]", false );
    sett.set ( "arr[3]", "bla" );
    sett.set ( "arr[4].sub", "table" );

    // Check values
    value_bool ( sett, "false", false );
    value_bool ( sett, "true", true );
    value_double ( sett, "dval", 1.2 );
    value_string ( sett, "sval", "stream" );

    value_string ( sett, "obj.tag", "notes" );
    value_string ( sett, "obj.l2.tag", "hints" );
    value_double ( sett, "obj.l2.value", 3.9 );

    value_double ( sett, "arr[0]", 19.56 );
    value_double ( sett, "arr[1]", 75.0 );
    value_bool ( sett, "arr[2]", false );
    value_string ( sett, "arr[3]", "bla" );
    value_string ( sett, "arr[4].sub", "table" );

    value_none ( sett, "noexist" );
    value_none ( sett, "obj.l1" );
    value_none ( sett, "arr[100]" );
    value_none ( sett, "arr[0].sub" );
    value_none ( sett, "arr[4].subsub" );
  }

  {
    std::vector< Value > values{ {}, false, 1.2, "songtext" };
    std::vector< Value > values2{ true, 2.3, "speaker", {} };
    std::vector< std::string > addresses{
        "one", "two[0]", "two[2]", "three.val", "three.sub.sub.sub" };

    // Set value and get it
    for ( const auto & val : values ) {
      Settings sett;
      // Set value in addresses
      for ( const auto & addr : addresses ) {
        sett.set ( addr, val );
      }
      // Get value from addresses
      for ( const auto & addr : addresses ) {
        // Check with different default values
        for ( const auto & default_value : values2 ) {
          auto rval = sett.get ( addr, default_value );
          require_value_type ( rval, val.type () );
          if ( rval != val ) {
            throw std::runtime_error ( "Values should be equal" );
          }
        }
      }
    }

    // Test default value for get()
    {
      Settings sett;
      for ( const auto & addr : addresses ) {
        for ( const auto & default_value : values ) {
          auto rval = sett.get ( addr, default_value );
          require_value_type ( rval, default_value.type () );
          if ( rval != default_value ) {
            throw std::runtime_error ( "Values should be default value" );
          }
        }
      }
    }
  }
}

int
main ( int argc, char * argv[] )
{
  (void)argc;
  (void)argv;

  set_get ();

  return 0;
}
