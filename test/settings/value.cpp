#include <snc/settings/value.hpp>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

using Value = snc::settings::Value;

void
require_value_type ( const Value & val_n, Value::Type type_n )
{
  if ( val_n.type () == type_n ) {
    return;
  }

  std::ostringstream oss;
  oss << "Value type was expected to be " << Value::type_name ( type_n )
      << " but is " << val_n.type_name () << "\n";
  throw std::runtime_error ( oss.str () );
}

void
require ( bool flag_n )
{
  if ( !flag_n ) {
    throw std::runtime_error ( "Requirement not met be equal" );
  }
}

void
require_equal ( const Value & va_n, const Value & vb_n )
{
  if ( !( va_n == vb_n ) ) {
    throw std::runtime_error ( "Should be equal" );
  }

  if ( va_n != vb_n ) {
    throw std::runtime_error ( "Should not be unequal" );
  }
}

void
require_unequal ( const Value & va_n, const Value & vb_n )
{
  if ( va_n == vb_n ) {
    throw std::runtime_error ( "Should not be equal" );
  }

  if ( !( va_n != vb_n ) ) {
    throw std::runtime_error ( "Should be unequal" );
  }
}

void
equality ()
{
  {
    Value va;
    Value vb;
    require_equal ( va, vb );
  }

  // Bool
  {
    Value va ( true );
    Value vb ( true );
    require ( va.is_bool () );
    require_equal ( va, vb );
  }
  {
    Value va ( true );
    Value vb ( false );
    require ( vb.is_bool () );
    require_unequal ( va, vb );
  }

  // Double
  {
    Value va ( 0.0 );
    Value vb ( 0.0 );
    require ( va.is_double () );
    require_equal ( va, vb );
  }
  {
    Value va ( 0.0 );
    Value vb ( 1.0 );
    require_unequal ( va, vb );
  }

  // String
  {
    Value va ( "one" );
    Value vb ( "one" );
    require ( va.is_string () );
    require_equal ( va, vb );
  }
  {
    Value va ( "one" );
    Value vb ( "two" );
    require_unequal ( va, vb );
  }
  {
    std::string sa ( "one" );
    std::string sb ( "one" );
    Value va ( sa );
    Value vb ( sb );
    require ( va.is_string () );
    require_equal ( va, vb );
  }
  {
    std::string sa ( "one" );
    std::string sb ( "two" );
    Value va ( sa );
    Value vb ( sb );
    require ( va.is_string () );
    require_unequal ( va, vb );
  }

  // Mixed
  {
    Value va;
    Value vb ( true );
    require_unequal ( va, vb );
  }
  {
    Value va;
    Value vb ( 0.0 );
    require_unequal ( va, vb );
  }
  {
    Value va;
    Value vb ( "two" );
    require_unequal ( va, vb );
  }
  {
    Value va ( true );
    Value vb ( 0.0 );
    require_unequal ( va, vb );
  }
  {
    Value va ( true );
    Value vb ( "two" );
    require_unequal ( va, vb );
  }
}

void
assignment ()
{
  auto set_a = [] () {
    std::vector< Value > vals;
    vals.emplace_back ( false );
    require_value_type ( vals.back (), Value::Type::BOOL );
    vals.emplace_back ( 1.2 );
    require_value_type ( vals.back (), Value::Type::DOUBLE );
    vals.emplace_back ( "one" );
    require_value_type ( vals.back (), Value::Type::STRING );
    return vals;
  };
  auto set_b = [] () {
    std::vector< Value > vals;
    vals.emplace_back ( true );
    vals.emplace_back ( 2.3 );
    vals.emplace_back ( "two" );
    return vals;
  };

  auto check_equality = [ &set_a,
                          &set_b ] ( const std::vector< Value > & sa_n ) {
    auto sa2 = set_a ();
    auto sb2 = set_b ();

    if ( sa_n.size () != sa2.size () ) {
      throw std::runtime_error ( "vector size missmatch" );
    }

    for ( std::size_t ii = 0; ii != sa_n.size (); ++ii ) {
      require_equal ( sa_n.at ( ii ), sa_n.at ( ii ) );
      require_equal ( sa_n.at ( ii ), sa2.at ( ii ) );
      for ( std::size_t jj = 0; jj != sa_n.size (); ++jj ) {
        require_unequal ( sa_n.at ( ii ), sb2.at ( jj ) );
      }
    }
  };

  // Copy construction
  {
    const auto sa = set_a ();
    std::vector< Value > sa_c;
    for ( auto & val : sa ) {
      sa_c.emplace_back ( val );
    }
    check_equality ( sa_c );
  }

  // Move construction
  {
    const auto sa = set_a ();
    std::vector< Value > sa_c;
    for ( auto & val : sa ) {
      sa_c.emplace_back ( std::move ( val ) );
    }
    check_equality ( sa_c );
  }

  const std::size_t repeats_max = 8;

  // Copy assignment of self
  for ( std::size_t reps = 1; reps != repeats_max + 1; ++reps ) {
    auto sa1 = set_a ();
    for ( std::size_t rr = 0; rr != reps; ++rr ) {
      for ( std::size_t ii = 0; ii != sa1.size (); ++ii ) {
        sa1.at ( ii ) = sa1.at ( ii );
      }
    }

    check_equality ( sa1 );
  }

  // Move assignment of self
  for ( std::size_t reps = 1; reps != repeats_max + 1; ++reps ) {
    auto sa1 = set_a ();
    for ( std::size_t rr = 0; rr != reps; ++rr ) {
      for ( std::size_t ii = 0; ii != sa1.size (); ++ii ) {
        sa1.at ( ii ) = std::move ( sa1.at ( ii ) );
      }
    }

    check_equality ( sa1 );
  }
}

int
main ( int argc, char * argv[] )
{
  (void)argc;
  (void)argv;

  equality ();
  assignment ();

  return 0;
}
